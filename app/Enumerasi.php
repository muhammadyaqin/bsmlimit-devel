<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enumerasi extends Model
{
    protected $table = 'enumerasi';
    protected $primaryKey = ['kode','kategori'];
    protected $fillable = ['keterangan'];

    protected $keyType = 'string';
    public $timestamps = false;
    public $incrementing = false;

}
