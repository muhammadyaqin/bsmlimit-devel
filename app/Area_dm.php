<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Cabang;
use App\Region_dm;

class Area_dm extends Model
{
    protected $table = 'area';
    protected $primaryKey = 'kode';
    protected $fillable = ['nama','koderegion'];

    protected $keyType = 'string';
    public $timestamps = false;
    public $incrementing = false;


    public function Region()
    {
        return $this->hasMany(Region_dm::class);
    }

    public function cabang()
    {
        return $this->belongsTo(Cabang::class);
    }
}
