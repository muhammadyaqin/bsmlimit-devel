<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusRefreshment extends Model
{
    protected $table = "staging.refreshment";
     
    protected $fillable = ['nip','statusrefreshment'];
    public $timestamps = false;
}
