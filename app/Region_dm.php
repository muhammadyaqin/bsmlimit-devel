<?php

namespace App;
use App\Area_dm;
use Illuminate\Database\Eloquent\Model;

class Region_dm extends Model
{
    protected $table = 'region';
    protected $primaryKey = 'kode';
    protected $fillable = ['nama'];

    protected $keyType = 'string';
    public $timestamps = false;
    public $incrementing = false;

    public function Area()
    {
        return $this->belongsTo(Area_dm::class);
    }

}
