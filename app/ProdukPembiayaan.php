<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProdukPembiayaan extends Model
{
    protected $table = 'produkpembiayaan';
    protected $primaryKey = 'kode';
    protected $fillable = ['keterangan'];

    protected $keyType = 'string';
    public $timestamps = false;
    public $incrementing = false;
}
