<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HCSPejabat extends Model
{
    protected $table = "staging.hcspejabat";
    protected $fillable = ['nip','namapejabat','tglbergabung','statuskaryawan','kodejabatan','kodecabang','appraisal','sanksi','fraud','email'];
    protected $primaryKey = 'nip';
    public $timestamps = false;

}
