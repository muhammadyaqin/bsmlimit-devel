<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MisPortfolio extends Model
{
    protected $table = "staging.misportfolio";

    protected $primaryKey = 'noloan';

    protected $fillable = [
                            'cif',
                            'namanasabah',
                            'kodecabang',
                            'type',
                            'plafon',
                            'ospokok',
                            'tglcair',
                            'tgljatuhtempo',
                            'kolcif',
                            'styus',
                            'secon',
                            'segmen',
                            'nikpengusul',
                            'nikpemutus',
                            'flagrestruct',
                            'tglrestruct',
                            ];
    
    public $timestamps = false;
    public $incrementing = false;
}
