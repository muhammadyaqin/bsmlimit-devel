<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PelatihanPembiayaan extends Model
{
    protected $table = 'staging.msutrainingpby';
    protected $primaryKey = ['id'];
    protected $fillable = ['nip','namatraining'];

    protected $keyType = 'string';
    public $timestamps = false;
    public $incrementing = false;
}
