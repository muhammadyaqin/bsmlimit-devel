<?php

namespace App;
use App\Area_dm;
use Illuminate\Database\Eloquent\Model;

class Cabang extends Model
{
    protected $table = 'master.cabang';
    protected $primaryKey = 'kode';
    protected $fillable = ['nama','kodearea'];

    protected $keyType = 'string';
    public $timestamps = false;
    public $incrementing = false;
    
    public function Area()
    {
        return $this->hasMany(Area_dm::class);
    }
    
}
