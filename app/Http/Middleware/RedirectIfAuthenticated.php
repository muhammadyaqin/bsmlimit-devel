<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\UserRole;
use Session;
use DB;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::check()) {
            $user = Auth::user();
            $role = UserRole::where('email', $user->email)->first();

            $cek_kodejabatan = DB::select(
                            DB::raw("
                            SELECT h.kodejabatan, u.email, h.nip, h.kodecabang, j.areajabatan 
                            from staging.hcspejabat h
                            inner join master.jabatan j on j.kode = h.kodejabatan
                            inner join master.userapp u on upper(u.email) = upper(h.email)
                            where (u.email = '$user->email')"));
            
            if (count($cek_kodejabatan) != 0) {
                Session::put('nip', $cek_kodejabatan[0]->nip);
                Session::put('kodejabatan', $cek_kodejabatan[0]->kodejabatan);
                Session::put('kodecabang', $cek_kodejabatan[0]->kodecabang);
                Session::put('areajabatan', $cek_kodejabatan[0]->areajabatan);
            }else{
                Session::put('kodejabatan', '');
                Session::put('kodecabang', '');
                Session::put('areajabatan', '');
            }


            $area = Session::get('areajabatan');

            if ($area == 'A') {
                return redirect('/home-area');
            }
            else {
                
            }

            // dd ($role->role);
            $cekrole = DB::table('master.userrole')->where('email', $user->email)->count();
            // dd($cekrole);
            if ($cekrole > 1) {
                return redirect('/home-user');
            } else {
                switch ($role->role){
                    case "ADMIN":
                        return redirect('/Administrator/index');
                        break;
                    case "CABANG":
                        return redirect('/home-cabang');
                        break;
                    case "REGION":
                        return redirect('/home-region');
                        break;
                    case "ERM":
                        return redirect('/home-erm');
                        break;
                    case "IOG":
                        return redirect('/home-iog');
                        break;
                    case "PUSAT":
                        return redirect('/home-pusat');
                        break;
                    case "HCS":
                        return redirect('/home-hcs');
                        break;
                  }
            }


            
            //query untuk get roles
            // if($role->role == 'ADMIN'){
            //     $redirectTo ='/assessment';
            // }elseif ($role->role == 'CABANG') {
            //     $redirectTo = '/pwmp';
            // }
        }

        // if (Auth::guard($guard)->check()) {
        //     return redirect('/home');
        // }

        return $next($request);
    }
}
