<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\UserRole;

class UsersController extends Controller
{
	public function list_user(){
		maintest;
	    $user = DB::table('master.userapp')->orderby('created_at', 'asc')->get();

	    $data['user'] = $user;
	    return view('UserApp.index', $data);
	}

	public function create_user()
	{
	    return view('UserApp.create');
	}

	public function add_user(request $request)
	{
	    $email = $request->input('email');
	    $statusaktif = $request->input('statusaktif');
	    $role = $request->input('role');
	    $created_at = Carbon::now();
	    $updated_at = Carbon::now();
	    DB::table('master.userapp')->insert([
	        'email' => $email,
	        'statusaktif' => $statusaktif,
	        'created_at' => $created_at,
	        'updated_at' => $updated_at,
	    ]);

	    DB::table('master.userrole')->insert([
	        'email' => $email,
	        'role' => $role,
	        'created_at' => $created_at,
	        'updated_at' => $updated_at,
	    ]);

	    return redirect('/user');
	}

	public function edit_user($email)
	{
	    $userapp = DB::table('master.userapp')->where('email', $email)->first();
	    $data['user'] = $userapp;

	    return view('UserApp.edit', $data);
	}

	public function update(Request $request, $email)
	{
	    dd($request);

	    $email = $request->input('email');
	    $statusaktif = $request->input('statusaktif');
	    DB::table('master.userapp')->whereemail('email', $email)->update([
	        'email' => $email,
	        'statusaktif' => $statusaktif,
	    ]);

	    return redirect('/list-user');
	}

	public function selectrole(Request $request, $role)
	{
		// dd($role);
		$userroles = UserRole::where('email', 'irfan@bsm.co.id')->get();
		// $userrole->selected_role = '20';
		foreach ($userroles as $userrole) {
			if ($userrole->role == $role) {
				$userrole->selected_role = '2';
				$userrole->save();
			}else{
				$userrole->selected_role = '0';
				$userrole->save();
			}
		}

		// $data = 0;
		// $email = 'irfan$bsm.co.id';

		// DB::table('master.userrole')->where('email', $email)->where('role', $role)->update([
		//     'selected_role' => $data,
		// ]);
		// dd($userrole);
		// $userrole = UserRole::where('email', );
		dd('test');
	}

	// public function test123(Request $request)
	// {
	// 	// $data = 0;
	// 	// $email = 'irfan$bsm.co.id';

	// 	// DB::table('master.userrole')->where('email', $email)->update([
	// 	//     'selected_role' => $data,
	// 	// ]);

	// 	$userrole = UserRole::where('email', 'irfan@bsm.co.id')->where('role', 'REGION')->first();
	// 	$userrole->selected_role = '0';
	// 	$userrole->save();
		
	// 	dd('test');
	// }
}
