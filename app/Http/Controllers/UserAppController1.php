<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class UserAppController1 extends Controller
{
    public function index()
    {
    	$user = DB::table('master.userapp')->get();

    	$data['user'] = $user;

    	return view('UserApp.index', $data);
    }

    public function create()
    {
        return view('UserApp.create');
    }

    public function createUser(Request $request)
    {
    	$name = $request->input('name');
    	$email = $request->input('email');
    	$statusaktif = $request->input('status_aktif');
    	$role = $request->input('role');
    	$password = bcrypt($request->input('password'));

    	DB::table('master.userapp')->insertGetid([
    	    'nama' => $name,
    	    'email' => $email,
    	    'statusaktif' => $statusaktif,
    	    'role' => $role,
    	    'password' => $password,
    	]);

    	return redirect('/user');
    }

    public function show($email)
    {
    	$pwmp = DB::table('master.userapp')->where('email', $email)->first();
    	$data['pwmp'] = $pwmp;

    	return view('UserApp.detail', $data);
    }
}
