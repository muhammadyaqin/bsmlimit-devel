<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use Auth;
use App\RiwayatPwmp;
use App\Exports\AssessmentERM;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use File;

class AssessmentERMController extends Controller
{
    //
    public function index(Request $request)
    {

       
        // dd($request->input('filterform'));

        $userLogin = Auth::user()->email;
        $userRole = Auth::user()->cekRole()->role;

        $report = DB::table('pwmp.riwayatpwmp as r')
        ->select(
           'r.id',
           // 'r.status_approve_erm',
           'r.nip',
           // 'r.file_sk',
           'r.namapejabat',
           'r.kodejabatan',
           'r.kodecabang',
           'r.fungsi',
           'r.kategorilimit',
           'r.jenispengajuan',
           'r.created_at',
           'r.status',
           'en.keterangan as statuspengajuan',
           'pl.limitnongadai as limitnongadaifix', 
           'pl.limitgadainlm as limitgadainlmfix', 
           'pl.limitgadailm as limitgadailmfix', 
           'pl.limitotomatis as limitotomatisfix', 
           'pl.nosk as noskfix', 
           'pl.tglsk as tglskfix'
       )
       ->leftjoin('pwmp.pengajuanlimit as pl', 'r.id', '=', 'pl.id')
       ->join('master.enumerasi as en', DB::raw('r.status::text'), '=', DB::raw("en.kode and en.kategori = 'STATUS PENGAJUAN'"))
       ->orderBy('r.id', 'desc');

        $name_nip = $request->name_nip;
        if(!is_null($name_nip)) {
            $report = $report->where('r.nip', 'LIKE', '%'. $name_nip. '%')
                ->orWhere('r.namapejabat', 'LIKE', '%'. $name_nip. '%');
        }
        
        $startDate = $request->startDate;
        if (is_null($startDate)) $startDate = date('d-m-Y');
        $report = $report->whereDate('r.created_at' , '>=' , date("Y-m-d",strtotime($startDate)));
        
        $endDate = $request->endDate;
        if (is_null($endDate)) $endDate = date('d-m-Y');
        $report = $report->whereDate('r.created_at' , '<=' , date("Y-m-d",strtotime($endDate)));
        
        $statuspengajuan = $request->status;
        if (!is_null($statuspengajuan)) $report = $report->where('r.status', $statuspengajuan);

        if ($userRole == 'CABANG'){
            $kodejabatanuser = Session::get('kodejabatan');
            $report->whereRaw(DB::raw("
            (
              UPPER(r.created_by) = UPPER('$userLogin')
              OR 
              EXISTS (
                SELECT 1 FROM (
                  WITH RECURSIVE nodes(kode,keterangan,parent) AS (
                    SELECT s1.kode, s1.keterangan, s1.parent
                    FROM master.jabatan s1 WHERE parent = '$kodejabatanuser'
                    UNION
                    SELECT s2.kode, s2.keterangan, s2.parent
                    FROM master.jabatan s2, nodes s1 WHERE s2.parent = s1.kode
                  )
                  SELECT * FROM nodes
                ) rq
                WHERE rq.kode = r.kodejabatan
              )
            )"));
        }

         $filterform = $request->input('filterform');
        if ($filterform == 'Export Excel') {
            $AssessmentERM = new AssessmentERM;
            $AssessmentERM->dataexcel = $report->get();
            return Excel::download($AssessmentERM, 'Rekapitulasi Daftar Riwayat PWMP.xlsx');
        }else{
            $page = 1;
            $next = $page+1;
            $prev = $page-1;
            
            $numbering =0;
            if($request->page){
                $page=$request->page;
                $numbering = $page*10;
            }

            $kodearea = $request->area;
            if(($kodearea != null) && ($kodearea != "")) {
                $area = DB::table('master.cabang')->where('master.cabang.kodearea', $kodearea)->pluck("kode");
                $report = $report->whereIn('r.kodecabang', $area);
            }


            if ($startDate != null && $endDate != null) {
                $hideStatus = 'not_active';
            } else {
                $hideStatus = 'active';
            }

            if ($kodearea != null or $statuspengajuan != null or $name_nip != null) {
                $statusActive = 'notActive';
            } else {
                $statusActive = 'isActive';
            }

            $getarea = DB::table('master.area')->get();

            $enumerasi = DB::table('master.enumerasi')->where('master.enumerasi.kategori', '=', 'STATUS PENGAJUAN')->get();
            $report = $report->paginate(10);
            $data['pwmp'] = $report;
            $date = [
                'unit_kerja' => $kodearea,
                'status' => $statuspengajuan,
                'startDate' => $startDate,
                'endDate' => $endDate,
                'statusAct' => $kodearea,
            ];

            $keteranganStatus = $request->status;

            return view('erm.index', $data, compact('date', 'getarea', 'enumerasi', 'name_nip', 'kodearea', 'statusActive', 'startDate', 'endDate', 'next','prev', 'page', 'numbering', 'keteranganStatus'));    
        }

        
    }

    public function detail($id)
    {
    	$cekstatus = DB::table('pwmp.riwayatpwmp')->select('pwmp.riwayatpwmp.jenispengajuan')
        ->where('pwmp.riwayatpwmp.id', $id)->first();
        // dd($cekstatus->jenispengajuan);
        if ($cekstatus->jenispengajuan == "A" ) {
            $pwmp = DB::select(
                DB::raw(
                "SELECT 
                hp.id,
                hp.nip,
                hp.namapejabat,
                hp.statuskaryawan,
                hp.kodejabatan,
                hp.kodecabang,
                hp.sanksi,
                hp.approve_erm_date,
                hp.appraisal,
                hp.fraud,
                hp.approve_ro_date,
                jb.keterangan AS namajabatan,
                cb.nama AS namacabang,
                ar.kode AS kodearea,
                ar.nama AS namaarea,
                en2.keterangan AS keterangansanksi,
                en1.keterangan AS keteranganappraisal,
                en3.keterangan AS keteranganfraud,
                hp.statussmrisk AS statussertifikasi,
                hp.statusrefreshment,
                hp.file_skpp,
                hp.fungsi,
                hp.kategorilimit,
                hp.created_at,
                hp.status,
                en4.keterangan AS keteranganfungsi,
                kl.keterangan AS keterangankategorilimit,
                COALESCE(hp.pengalamanpby_thn, 0) as age_year,
                COALESCE(hp.pengalamanpby_bln, 0) as age_month,
                COALESCE(hp.pengalamanpwmp_thn, 0) as age_year_pwmp,
                COALESCE(hp.pengalamanpwmp_bln, 0) as age_month_pwmp,
                COALESCE(hp.qltportolancar_nom,0) as nomlancar,
                COALESCE(hp.qltportodpk_nom, 0) as nomdpk,
                COALESCE(hp.qltportonpf_nom, 0) as nomnpf,
                COALESCE(hp.qltportototal_nom, 0) as nomospokok,
                COALESCE(hp.qltportolancar_pct, 0) as pctlancar,
                COALESCE(hp.qltportodpk_pct, 0) as pctdpk,
                COALESCE(hp.qltportonpf_pct, 0) as pctnpf,
                hp.jenispengajuan,
                en5.keterangan AS statuspengajuan,
                pl.limitnongadai,
                pl.limitgadainlm,
                pl.limitgadailm,
                pl.limitotomatis,
                hp.limitnongadai AS nilailimitnongadai,
                hp.limitgadainlm AS nilailimitgadainlm,
                hp.limitgadailm AS nilailimitgadailm,
                hp.limitotomatis AS nilailimitotomatis,
                pl.nosk AS nosknew,
                pl.tglsk AS tglsknew,
                pl.filesk AS file_sk
                FROM pwmp.riwayatpwmp hp
                LEFT JOIN pwmp.pengajuanlimit pl ON pl.id = hp.id
                INNER JOIN master.jabatan jb ON jb.kode = hp.kodejabatan
                INNER JOIN master.cabang cb ON cb.kode = hp.kodecabang
                INNER JOIN master.area ar ON ar.kode = cb.kodearea
                LEFT JOIN master.enumerasi en2 ON en2.kode = hp.sanksi AND en2.kategori = 'SANKSI'
                LEFT JOIN master.enumerasi en1 ON en1.kode = hp.appraisal AND en1.kategori = 'APPRAISAL'
                LEFT JOIN master.enumerasi en3 ON en3.kode = hp.fraud AND en3.kategori = 'FRAUD'
                LEFT JOIN master.enumerasi en4 ON en4.kode = hp.fungsi AND en4.kategori = 'FUNGSI'
                LEFT JOIN master.kategorilimit kl ON kl.kode = hp.kategorilimit
                INNER JOIN master.enumerasi en5 ON en5.kode = hp.status::text AND en5.kategori = 'STATUS PENGAJUAN'
                WHERE (hp.id = '$id')
                "));
        }
        else {
            $pwmp = DB::select(
                DB::raw(
                "SELECT 
                hp.id,
                hp.nip,
                hp.namapejabat,
                hp.statuskaryawan,
                hp.kodejabatan,
                hp.kodecabang,
                hp.sanksi,
                hp.appraisal,
                hp.fraud,
                hp.approve_ro_date,
                jb.keterangan AS namajabatan,
                cb.nama AS namacabang,
                ar.kode AS kodearea,
                ar.nama AS namaarea,
                en2.keterangan AS keterangansanksi,
                en1.keterangan AS keteranganappraisal,
                en3.keterangan AS keteranganfraud,
                hp.statussmrisk AS statussertifikasi,
                hp.statusrefreshment,
                hp.tglsk,
                hp.nosk,
                hp.approve_erm_date,
                hp.file_skpp,
                hp.fungsi,
                hp.kategorilimit,
                hp.created_at,
                hp.status,
                en4.keterangan AS keteranganfungsi,
                kl.keterangan AS keterangankategorilimit,
                COALESCE(hp.pengalamanpby_thn, 0) as age_year,
                COALESCE(hp.pengalamanpby_bln, 0) as age_month,
                COALESCE(hp.pengalamanpwmp_thn, 0) as age_year_pwmp,
                COALESCE(hp.pengalamanpwmp_bln, 0) as age_month_pwmp,
                COALESCE(hp.qltportolancar_nom,0) as nomlancar,
                COALESCE(hp.qltportodpk_nom, 0) as nomdpk,
                COALESCE(hp.qltportonpf_nom, 0) as nomnpf,
                COALESCE(hp.qltportototal_nom, 0) as nomospokok,
                COALESCE(hp.qltportolancar_pct, 0) as pctlancar,
                COALESCE(hp.qltportodpk_pct, 0) as pctdpk,
                COALESCE(hp.qltportonpf_pct, 0) as pctnpf,
                hp.jenispengajuan,
                en5.keterangan AS statuspengajuan,
                pl.limitnongadai,
                pl.limitgadainlm,
                pl.limitgadailm,
                pl.limitotomatis,
                hp.limitnongadai AS nilailimitnongadai,
                hp.limitgadainlm AS nilailimitgadainlm,
                hp.limitgadailm AS nilailimitgadailm,
                hp.limitotomatis AS nilailimitotomatis,
                pl.nosk AS nosknew,
                pl.tglsk AS tglsknew,
                pl.filesk AS file_sk
                FROM pwmp.riwayatpwmp hp
                LEFT JOIN pwmp.pengajuanlimit pl ON pl.id = hp.id
                INNER JOIN master.jabatan jb ON jb.kode = hp.kodejabatan
                INNER JOIN master.cabang cb ON cb.kode = hp.kodecabang
                INNER JOIN master.area ar ON ar.kode = cb.kodearea
                LEFT JOIN pwmp.pwmp pw ON hp.nip = pw.nip
                LEFT JOIN master.enumerasi en2 ON en2.kode = hp.sanksi AND en2.kategori = 'SANKSI'
                LEFT JOIN master.enumerasi en1 ON en1.kode = hp.appraisal AND en1.kategori = 'APPRAISAL'
                LEFT JOIN master.enumerasi en3 ON en3.kode = hp.fraud AND en3.kategori = 'FRAUD'
                LEFT JOIN master.enumerasi en4 ON en4.kode = hp.fungsi AND en4.kategori = 'FUNGSI'
                LEFT JOIN master.kategorilimit kl ON kl.kode = hp.kategorilimit
                INNER JOIN master.enumerasi en5 ON en5.kode = hp.status::text AND en5.kategori = 'STATUS PENGAJUAN'
                WHERE (hp.id = '$id')
                "));
            }
            // dd($pwmp);

            $ambilid = $pwmp[0]->id;
            // dd($ambilnip);
            $msu = DB::table('pwmp.trainingpby')->select('pwmp.trainingpby.nip', 'pwmp.trainingpby.namapelatihan')->where('idriwayatpwmp', $ambilid)->get();

            $file_sk = $pwmp[0]->file_sk;
            $file_skpp = $pwmp[0]->file_skpp;
            // dd($pwmp[0]);
            $cek_file_sk = public_path('/file_skp/sk/'.$file_sk);
            $cek_file_skpp = public_path('/file_skp/skp/'.$file_skpp);

            if(File::exists($cek_file_sk || $cek_file_skpp)){
                $statusActive = 'yes';
            }else{
                $statusActive = 'no';
            }

            $data['msu'] = $msu;
            // dd($msu);
            $data['pwmp'] = $pwmp;
            // dd($pwmp[0]->nip);

            // $enumerasi = DB::table('master.enumerasi')->where('master.enumerasi.kategori', '=', 'STATUS PENGAJUAN')->get();

            

            return view('erm.detail', $data, compact('statusActive'));
    }

    // public function upload(){
    //     $uploadpdf = DB::table('riwayat_pwmp')->get();
    //     $data['data'] = $cabang;
    //     return view('erm.detail',['file' => $uploadpdf]);
    // }

    public function approveERM(Request $request)
    {
        if($request->salinan_sk)
        {
            $file = $request->file('salinan_sk');
            // dd($file);
            $nama_file = time()."_".$file->getClientOriginalName();
     
            // isi dengan nama folder tempat kemana file diupload
            $tujuan_upload = ('file_skp/sk');
            $file->move($tujuan_upload,$nama_file);
    
        }else{
            $nama_file = 'no_file';
        }
        // menyimpan data file yang diupload ke variabel $file
        $limitnongadai = $request->input('limitnongadai');
        $limitgadainlm = $request->input('limitgadainlm');
        $limitgadailm = $request->input('limitgadailm');
        $limitotomatis = $request->input('limitotomatis');
        $updatesk = $request->input('nosk');
        $tglsk = $request->input('tglsk');
    	$id = $request->input('idpermohonan');
        $nip = $request->input('nippermohonan');
    	$approve_date = Carbon::now();
    	$approve_by_erm = Auth::user()->email;

        $formatlimitnongadai = str_replace(',', '', $limitnongadai);
        $formatlimitgadainlm = str_replace(',', '', $limitgadainlm);
        $formatlimitgadailm = str_replace(',', '', $limitgadailm);
        $formatlimitotomatis = str_replace(',', '', $limitotomatis);
        // dd($formatlimitnongadai);

        //untuk jenis pengajuan A
        $namapejabat = $request->input('namapejabat');
        $kodecabang = $request->input('kodecabang');
        $fungsi = $request->input('fungsi');
        $kategorilimit = $request->input('kategorilimit');
        $jenispengajuan = $request->input('jenispengajuan');
        $kodejabatan = $request->input('kodejabatan1');

        DB::table('pwmp.riwayatpwmp')->where('id', $id)->update([
            'approve_by_erm' => $approve_by_erm,
            'approve_erm_date' => $approve_date,
            'status_approve_erm' => 2,
            'status' => 4,
            'file_sk' => $nama_file,
        ]);

        DB::table('pwmp.pengajuanlimit')->where('id', $id)->update([
            'nosk' => $updatesk,
            'tglsk' => $tglsk,
            'filesk' => $nama_file,
            'limitnongadai' => $formatlimitnongadai,
            'limitgadainlm' => $formatlimitgadainlm,
            'limitgadailm' => $formatlimitgadailm,
            'limitotomatis' => $formatlimitotomatis,
        ]);

        if ($jenispengajuan != "A") {
            DB::table('pwmp.pwmp')->where('nip', $nip)->update([
            'nosk' => $updatesk,
            'tglsk' => $tglsk,
            'limitnongadai' => $formatlimitnongadai,
            'limitgadainlm' => $formatlimitgadainlm,
            'limitgadailm' => $formatlimitgadailm,
            'limitotomatis' => $formatlimitotomatis,
            'filesk' => $nama_file,
            ]);
        }

        else {
            DB::table('pwmp.pwmp')->insert([
            'nip' => $nip,
            'namapejabat' => $namapejabat,
            'kodejabatan' => $kodejabatan,
            'kodecabang' => $kodecabang,
            'limitnongadai' => $formatlimitnongadai,
            'limitgadainlm' => $formatlimitgadainlm,
            'limitgadailm' => $formatlimitgadailm,
            'limitotomatis' => $formatlimitotomatis,
            'fungsi' => $fungsi,
            'kategorilimit' => $kategorilimit,
            'nosk' => $updatesk,
            'tglsk' => $tglsk,
            'filesk' => $nama_file
            ]);
        }

    	return redirect()->action('AssessmentERMController@index')->with("success", 'SK Disetujui');
    }

    public function rejectERM(Request $request)
    {
    	$id = $request->input('reject');

    	// dd($approve_by);
    	DB::table('pwmp.riwayatpwmp')->where('id', $id)->update([
    	    'approve_ro' => null,
    	    'approve_ro_date' => null,
    	    'status_approve_ro' => null,
    	    'status' => 6,
    	    'status_approve_erm' => 1,
    	    ]);

    	return redirect()->action('AssessmentERMController@index')->with("reject", '');;
    }
    public function export_excel()
    {
        return Excel::download(new AssessmentERM, 'Rekapitulasi Assessment ERM.xlsx');
    }

    public function updatevaluenominal(Request $request, $id)
    {
        DB::table('pwmp.riwayatpwmp')->where('id', $id)->update([
            'status' => 3,
        ]);

        $limitnongadai = $request->input('limitnongadai');
        $limitgadainlm = $request->input('limitgadainlm');
        $limitgadailm = $request->input('limitgadailm');
        $limitotomatis = $request->input('limitotomatis');

        $formatlimitnongadai = str_replace(',', '', $limitnongadai);
        $formatlimitgadainlm = str_replace(',', '', $limitgadainlm);
        $formatlimitgadailm = str_replace(',', '', $limitgadailm);
        $formatlimitotomatis = str_replace(',', '', $limitotomatis);
        // dd($limitnongadai, $limitgadainlm, $limitgadailm, $limitotomatis);

        DB::table('pwmp.pengajuanlimit')->where('id', $id)->update([
            'limitnongadai' => $formatlimitnongadai,
            'limitgadainlm' => $formatlimitgadainlm,
            'limitgadailm' => $formatlimitgadailm,
            'limitotomatis' => $formatlimitotomatis
        ]);

      return redirect()->back();

    } 

    public function GenerateSK($id)
    {
        // dd($limitnongadai);

        DB::table('pwmp.riwayatpwmp')->where('id', $id)->update([
            'status' => 3,
        ]);
      
        $cekstatus = DB::table('pwmp.riwayatpwmp')->select('pwmp.riwayatpwmp.jenispengajuan')
        ->where('pwmp.riwayatpwmp.id', $id)->first();
        // dd($cekstatus->jenispengajuan);
        if ($cekstatus->jenispengajuan == "A" ) {
            $pwmp = DB::table('pwmp.riwayatpwmp')->select('pwmp.riwayatpwmp.*',
                        'pwmp.riwayatpwmp.limitnongadai as nilailimitnongadai',
                        'pwmp.riwayatpwmp.limitgadainlm as nilailimitgadainlm',
                        'pwmp.riwayatpwmp.limitgadailm as nilailimitgadailm',
                        'pwmp.riwayatpwmp.limitotomatis as nilailimitotomatis',
                        'pwmp.pengajuanlimit.limitnongadai as limitnongadai',
                        'pwmp.pengajuanlimit.limitgadainlm as limitgadainlm',
                        'pwmp.pengajuanlimit.limitgadailm as limitgadailm',
                        'pwmp.pengajuanlimit.limitotomatis as limitotomatis', 
                        'en1.keterangan as statuspengajuan', 
                        'master.area.nama as namaarea',
                        'master.cabang.nama as namacabang',
                        'staging.hcspejabat.statuskaryawan',
                        'master.jabatan.keterangan as namajabatan',
                        'master.area.kode as kodearea', 
                        'master.area.nama as namaarea',
                        'master.area.koderegion as koderegion', 
                        'en2.keterangan as keteranganappraisal', 
                        'staging.smrisk.statussertifikasi', 
                        'en4.keterangan as keterangansanksi', 
                        'en5.keterangan as keteranganfraud', 
                        'staging.refreshment.statusrefreshment', 
                        'calc.age_year',
                        'calc.age_month'
                    )
                    ->join('master.cabang', 'pwmp.riwayatpwmp.kodecabang', '=', 'master.cabang.kode')
                    ->join('master.jabatan', 'pwmp.riwayatpwmp.kodejabatan', '=', 'master.jabatan.kode')
                    ->join('master.enumerasi as en1', DB::raw('pwmp.riwayatpwmp.status::text'), '=', 'en1.kode')
                    ->join('staging.hcspejabat', 'pwmp.riwayatpwmp.nip', '=', 'staging.hcspejabat.nip')
                    ->join('master.enumerasi as en2', DB::raw('staging.hcspejabat.appraisal::text'), '=', 'en2.kode')
                    ->join('master.area', 'master.area.kode', '=', 'master.cabang.kodearea')
                    ->join('staging.smrisk', 'staging.hcspejabat.nip', '=', 'staging.smrisk.nip')
                    ->join('master.enumerasi as en4', 'staging.hcspejabat.sanksi', '=', 'en4.kode')
                    ->join('master.enumerasi as en5', 'staging.hcspejabat.fraud', '=', 'en5.kode')
                    ->leftjoin('staging.refreshment', 'pwmp.riwayatpwmp.nip', '=', 'staging.refreshment.nip')
                    ->leftjoin('staging.vcalcpengalamanpby as calc', 'calc.nip', '=', 'staging.hcspejabat.nip')
                    ->leftjoin('pwmp.pengajuanlimit', 'pwmp.riwayatpwmp.id', '=', 'pwmp.pengajuanlimit.id')
                    ->where('en1.kategori', '=', 'STATUS PENGAJUAN')
                    ->where('en2.kategori', '=', 'APPRAISAL')
                    ->where('en4.kategori', '=', 'SANKSI')
                    ->where('en5.kategori', '=', 'FRAUD')
                    ->where('pwmp.riwayatpwmp.id', $id)->first();
        }
        else {
            $pwmp = DB::table('pwmp.riwayatpwmp')->select('pwmp.riwayatpwmp.*',
                        'pwmp.riwayatpwmp.limitnongadai as nilailimitnongadai',
                        'pwmp.riwayatpwmp.limitgadainlm as nilailimitgadainlm',
                        'pwmp.riwayatpwmp.limitgadailm as nilailimitgadailm',
                        'pwmp.riwayatpwmp.limitotomatis as nilailimitotomatis',
                        'pwmp.pengajuanlimit.limitnongadai as limitnongadai',
                        'pwmp.pengajuanlimit.limitgadainlm as limitgadainlm',
                        'pwmp.pengajuanlimit.limitgadailm as limitgadailm',
                        'pwmp.pengajuanlimit.limitotomatis as limitotomatis', 
                        'en1.keterangan as statuspengajuan', 
                        'master.area.kode as kodearea', 
                        'master.area.nama as namaarea',
                        'master.area.koderegion as koderegion',
                        'master.cabang.nama as namacabang',
                        'master.jabatan.keterangan as namajabatan',
                        'pwmp.pwmp.kategorilimit',
                        'staging.hcspejabat.statuskaryawan', 
                        'master.kategorilimit.keterangan as keterangankategorilimit', 
                        'en2.keterangan as keteranganappraisal', 
                        'en3.keterangan as keteranganfungsi', 
                        'staging.smrisk.statussertifikasi', 
                        'en4.keterangan as keterangansanksi', 
                        'en5.keterangan as keteranganfraud', 
                        'staging.refreshment.statusrefreshment', 
                        'limitnongadai.limitnongadai as nilailimitnongadai',
                        'limitgadainlm.limitgadainlm as nilailimitgadainlm',
                        'limitgadailm.limitgadailm as nilailimitgadailm',
                        'limitotomatis.limitotomatis as nilailimitotomatis',
                        'cpw.age_year',
                        'cpw.age_month',
                        DB::raw("COALESCE(pwmp.pwmp.nip, 'BARU') as statuspermohonan"),
                        'staging.hcspejabat.nip as niphcspejabat',
                        'pq.*'
                    )
                    ->join('master.cabang', 'pwmp.riwayatpwmp.kodecabang', '=', 'master.cabang.kode')
                    ->join('master.jabatan', 'pwmp.riwayatpwmp.kodejabatan', '=', 'master.jabatan.kode')
                    ->join('master.enumerasi as en1', DB::raw('pwmp.riwayatpwmp.status::text'), '=', 'en1.kode')
                    ->join('staging.hcspejabat', 'pwmp.riwayatpwmp.nip', '=', 'staging.hcspejabat.nip')
                    ->join('master.enumerasi as en2', DB::raw('staging.hcspejabat.appraisal::text'), '=', 'en2.kode')
                    ->join('master.area', 'master.area.kode', '=', 'master.cabang.kodearea')
                    ->join('pwmp.pwmp', 'pwmp.riwayatpwmp.nip', '=', 'pwmp.pwmp.nip')
                    ->join('master.kategorilimit', 'pwmp.pwmp.kategorilimit', '=', 'master.kategorilimit.kode')
                    ->join('staging.smrisk', 'staging.hcspejabat.nip', '=', 'staging.smrisk.nip')
                    ->join('master.enumerasi as en3', 'pwmp.riwayatpwmp.fungsi', '=', 'en3.kode')
                    ->join('master.enumerasi as en4', 'staging.hcspejabat.sanksi', '=', 'en4.kode')
                    ->join('master.enumerasi as en5', 'staging.hcspejabat.fraud', '=', 'en5.kode')
                    ->join('pwmp.pwmp as limitnongadai', 'pwmp.riwayatpwmp.nip', '=', 'limitnongadai.nip')
                    ->join('pwmp.pwmp as limitgadainlm', 'pwmp.riwayatpwmp.nip', '=', 'limitgadainlm.nip')
                    ->join('pwmp.pwmp as limitgadailm', 'pwmp.riwayatpwmp.nip', '=', 'limitgadailm.nip')
                    ->join('pwmp.pwmp as limitotomatis', 'pwmp.riwayatpwmp.nip', '=', 'limitotomatis.nip')
                    ->leftjoin('staging.refreshment', 'pwmp.riwayatpwmp.nip', '=', 'staging.refreshment.nip')
                    ->leftjoin('pwmp.portfolio_qualitysum as pq', 'pwmp.riwayatpwmp.nip', '=', 'pq.nip')
                    ->leftjoin('staging.vcalcpengalamanpby as calc', 'calc.nip', '=', 'staging.hcspejabat.nip')
                    ->leftjoin('pwmp.pengajuanlimit', 'pwmp.riwayatpwmp.id', '=', 'pwmp.pengajuanlimit.id')
                    ->leftjoin('pwmp.vcalcpengalamanpwmp as cpw', 'cpw.nip', '=', 'pwmp.pwmp.nip')
                    ->where('en1.kategori', '=', 'STATUS PENGAJUAN')
                    ->where('en2.kategori', '=', 'APPRAISAL')
                    ->where('en3.kategori', '=', 'FUNGSI')
                    ->where('en4.kategori', '=', 'SANKSI')
                    ->where('en5.kategori', '=', 'FRAUD')
                    ->where('pwmp.riwayatpwmp.id', $id)->first();
        }

        $riwayatjabatan = DB::table('staging.hcsriwayatjabatan')->select('staging.hcsriwayatjabatan.kodejabatan',
                        'staging.hcsriwayatjabatan.kodecabang',
                        'staging.hcsriwayatjabatan.tglmulai',
                        'staging.hcsriwayatjabatan.tglberakhir',
                        'master.cabang.nama as namacabang',
                        'master.jabatan.keterangan as namajabatan',
                        'master.area.nama as namaarea')
                        ->join('master.cabang', 'staging.hcsriwayatjabatan.kodecabang', '=', 'master.cabang.kode')
                        ->join('master.jabatan', 'staging.hcsriwayatjabatan.kodejabatan', '=', 'master.jabatan.kode')
                        ->join('master.area', 'master.cabang.kodearea', '=', 'master.area.kode')
                        ->where('staging.hcsriwayatjabatan.nip', $pwmp->nip)->get();
        // dd($pwmp);
        // dd($riwayatjabatan);
       

        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection(
        array('marginLeft' => 1700, 'marginRight' => 1700,
              'marginTop' => 1000, 'marginBottom' => 600));
        $header = array('size' => 16, 'bold' => true);
        $section->addText('SURAT KEPUTUSAN DIREKSI <w:br/> PT BANK SYARIAH MANDIRI <w:br/> NOMOR: 21/         -KEP/DIR',
        array('name' => 'Arial', 'size' => 11, 'bold' => true),
        array('align' => 'center')
        );
        $section->addText('TENTANG <w:br/> PEMBERIAN LIMIT WEWENANG MEMUTUS PEMBIAYAAN',
        array('name' => 'Arial', 'size' => 11, 'bold' => true),
        array('align' => 'center')
        );
        $styleTable = array('borderSize' => 0, 'cellMargin' => 10, 'borderColor' =>'FFFFFF', 'align'=>'both');
        $styleFirstRow = array('borderBottomSize' => 18, 'borderBottomColor' => 'FF0000');
        $styleCell = array('valign' => 'center');
        $fontStyle = array('name' => 'Arial', 'size' => 11, 
                     array('align' => 'both'));
        $phpWord->addTableStyle('Fancy Table', $styleTable, $styleFirstRow);
        $table = $section->addTable('Fancy Table',
        array('align'=>'both')
        );
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars('Menimbang:'), $fontStyle);
        $table->addCell(70)->addText(htmlspecialchars('1.'), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('Bahwa penetapan limit untuk setiap aktivitas fungsional harus memperhatikan risk appetite Bank dalam mengelola risiko serta mencakup akuntabilitas dan jenjang delegasi wewenang yang jelas.'), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars(''), $fontStyle);
        $table->addCell(70)->addText(htmlspecialchars('2.'), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('Bahwa sehubungan dengan hal dimaksud, Direksi memandang perlu menetapkan limit wewenang memutus pembiayaan dan penanganan pembiayaan bermasalah yang antara lain dituangkan dalam Surat Keputusan ini.'), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars('Mengingat:'), $fontStyle);
        $table->addCell(70)->addText(htmlspecialchars('1.'), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('Undang–Undang Nomor 7 tahun 1992 tentang Perbankan sebagaimana telah diubah dengan Undang-Undang Nomor 10 tahun 1998.'), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars(''), $fontStyle);
        $table->addCell(70)->addText(htmlspecialchars('2.'), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('Undang–Undang Nomor 21 tahun 2008 tentang Perbankan Syariah.'), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars(''), $fontStyle);
        $table->addCell(70)->addText(htmlspecialchars('3.'), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('Peraturan Bank Indonesia Nomor 11/3/PBI/2009 tentang Bank Umum Syariah.'), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars(''), $fontStyle);
        $table->addCell(70)->addText(htmlspecialchars('4.'), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('Surat Edaran Bank Indonesia Nomor 11/9/DPbS tentang Bank Umum Syariah.'), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars(''), $fontStyle);
        $table->addCell(70)->addText(htmlspecialchars('5.'), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('Peraturan Otoritas Jasa Keuangan Nomor 65/POJK.03/2016 tentang Penerapan Manajemen Risiko Bagi Bank Umum Syariah dan Unit Usaha Syariah.'), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars(''), $fontStyle);
        $table->addCell(70)->addText(htmlspecialchars('6.'), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('Akta Pernyataan Keputusan Rapat Umum Pemegang Saham Tahunan PT Bank Syariah Mandiri Nomor 44 tanggal 16 Maret 2018. '), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars(''), $fontStyle);
        $table->addCell(70)->addText(htmlspecialchars('7.'), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('Struktur Organisasi PT Bank Syariah Mandiri.'), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars(''), $fontStyle);
        $table->addCell(70)->addText(htmlspecialchars('8.'), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('SPB Pembiayaan dan PTO Limit Wewenang Memutus Pembiayaan.'), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars(''), $fontStyle);
        $table->addCell(70)->addText(htmlspecialchars('9.'), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('Nota ERM No.21/206A-2/ERM tanggal 4 November 2019 perihal Peningkatan Limit Wewenang Memutus Pembiayaan bagi Branch Manager, Area Consumer Banking Manager dan Area Micro & Pawning Manager.'), $fontStyle);
        $section->addText('M E M U T U S K A N',
        array('name' => 'Arial', 'size' => 11, 'bold' => true),
        array('align' => 'center')
        );
        $section->addText('Menetapkan',
        array('name' => 'Arial', 'size' => 11, 'bold' => true)
        );
        $styleTable = array('borderSize' => 0, 'cellMargin' => 80);
        $styleFirstRow = array('borderBottomSize' => 18, 'borderBottomColor' => '0000FF');
        $styleCell = array('valign' => 'center');
        $fontStyle = array('name' => 'Arial', 'size' => 11, 'align' => 'justify');
        $phpWord->addTableStyle('Fancy Table', $styleTable, $styleFirstRow);
        $table = $section->addTable('Fancy Table');
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars('PERTAMA:'), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('Sdr. '
        . ucwords(strtolower($pwmp->namapejabat))
        . '/NIP '
        . $pwmp->nip 
        . ' diberikan limit wewenang untuk memutus pembiayaan dan penanganan pembiayaan bermasalah sebesar '), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars(''), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('A. Limit Non Gadai Rp.'
        . number_format($pwmp->limitnongadai, 0, ',', '.')), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars(''), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('B. Limit Gadai Non Logam Rp.'
        . number_format($pwmp->limitgadainlm, 0, ',', '.')), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars(''), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('C. Limit Gadai Logam Mulia Rp.'
        . number_format($pwmp->limitgadailm, 0, ',', '.')), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars(''), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('D. Limit Otomatis Rp.'
        . number_format($pwmp->limitotomatis, 0, ',', '.')), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars(''), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('Terhitung sejak keputusan ini dibuat'), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars('KEDUA:'), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('Limit wewenang memutus penanganan pembiayaan bermasalah tidak berlaku untuk pembiayaan yang pernah direkomendasikan atau diputuskan oleh Sdr. '
            . ucwords(strtolower($pwmp->namapejabat))
            . '.'), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars('KETIGA:'), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('Pemutusan pembiayaan dimaksud harus tetap berpedoman pada prinsip kehati-hatian dan didasarkan atas kesepakatan mutlak (full consensus) Komite Pembiayaan sesuai dengan limit dan segmentasi yang ditetapkan dalam Kebijakan Pembiayaan dan ketentuan internal PT Bank Syariah Mandiri.'), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars('KEEMPAT'), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('Wewenang Komite Pembiayaan sebagaimana dimaksud pada butir ketiga antara lain mencakup pemutusan pembiayaan baru dan upaya penyelamatan pembiayaan. Khusus untuk perpanjangan pembiayaan berlaku 2 (dua) kali limit wewenang memutus sebagaimana tersebut pada butir pertama. Proses pemutusan pembiayaan harus memperhatikan Kebijakan Pembiayaan, ketentuan internal PT Bank Syariah Mandiri dan peraturan perundang-undangan yang berlaku.'), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars('KELIMA'), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('Limit wewenang memutus pembiayaan dan penanganan pembiayaan bermasalah sebagaimana dimaksud pada butir pertama diberikan kepada Sdr. '
            . ucwords(strtolower($pwmp->namapejabat))
            . ' yang diangkat berdasarkan Surat Keputusan Direksi Perseroan. Apabila Sdr. '
            . ucwords(strtolower($pwmp->namapejabat))
            . ' tidak lagi menjabat sebagai pemegang kewenangan memutus pembiayaan dan penanganan pembiayaan bermasalah baik karena mutasi jabatan, pengunduran diri, pensiun, berhenti, atau diberhentikan dari perseroan, maka Surat Keputusan ini tidak berlaku.'), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars('KEENAM'), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('Apabila di kemudian hari terdapat kekeliruan dalam keputusan ini, akan     diadakan perbaikan sebagaimana mestinya.'), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars('KETUJUH'), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('Dengan berlakunya Surat Keputusan ini, maka Surat Keputusan Direksi Nomor '
            . $pwmp->nosk
            . ' tanggal '
            . \Carbon\Carbon::parse($pwmp->tglsk)->format('d-m-Y')
            . ' dinyatakan tidak berlaku.'), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars(''), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('Salinan  Keputusan ini disampaikan kepada: '), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars(''), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('1. Unit Kerja Terkait'), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars(''), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('2. Human Capital Services Group'), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars(''), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('3. Legal Group'), $fontStyle);
        $section->addTextBreak(1);
        $section->addText('Ditetapkan:       di Jakarta <w:br/>Pada tanggal:  ',
        array('name' => 'Arial', 'size' => 11)
        );
        $section->addText('DIREKSI',
        array('name' => 'Arial', 'size' => 11)
        );
        $section->addTextBreak(2);
        $section->addText('Achmad Syafii<w:br/>Direktur',
        array('name' => 'Arial', 'size' => 11)
        );

        $section->addTextBreak(5);
        $header = array('size' => 16, 'bold' => true);
        $section->addText('SALINAN<w:br/>SURAT KEPUTUSAN DIREKSI <w:br/> PT BANK SYARIAH MANDIRI <w:br/> NOMOR: 21/         -KEP/DIR',
        array('name' => 'Arial', 'size' => 11, 'bold' => true),
        array('align' => 'center')
        );
        $section->addText('TENTANG <w:br/> PEMBERIAN LIMIT WEWENANG MEMUTUS PEMBIAYAAN',
        array('name' => 'Arial', 'size' => 11, 'bold' => true),
        array('align' => 'center')
        );
        $styleTable = array('borderSize' => 0, 'cellMargin' => 10, 'borderColor' =>'FFFFFF');
        $styleFirstRow = array('borderBottomSize' => 18, 'borderBottomColor' => 'FF0000');
        $styleCell = array('valign' => 'center');
        $fontStyle = array('name' => 'Arial', 'size' => 11, 'align' => 'justify');
        $phpWord->addTableStyle('Fancy Table', $styleTable, $styleFirstRow);
        $table = $section->addTable('Fancy Table');
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars('Menimbang:'), $fontStyle);
        $table->addCell(70)->addText(htmlspecialchars('1.'), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('Bahwa penetapan limit untuk setiap aktivitas fungsional harus memperhatikan risk appetite Bank dalam mengelola risiko serta mencakup akuntabilitas dan jenjang delegasi wewenang yang jelas.'), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars(''), $fontStyle);
        $table->addCell(70)->addText(htmlspecialchars('2.'), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('Bahwa sehubungan dengan hal dimaksud, Direksi memandang perlu menetapkan limit wewenang memutus pembiayaan dan penanganan pembiayaan bermasalah yang antara lain dituangkan dalam Surat Keputusan ini.'), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars('Mengingat:'), $fontStyle);
        $table->addCell(70)->addText(htmlspecialchars('1.'), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('Undang–Undang Nomor 7 tahun 1992 tentang Perbankan sebagaimana telah diubah dengan Undang-Undang Nomor 10 tahun 1998.'), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars(''), $fontStyle);
        $table->addCell(70)->addText(htmlspecialchars('2.'), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('Undang–Undang Nomor 21 tahun 2008 tentang Perbankan Syariah.'), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars(''), $fontStyle);
        $table->addCell(70)->addText(htmlspecialchars('3.'), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('Peraturan Bank Indonesia Nomor 11/3/PBI/2009 tentang Bank Umum Syariah.'), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars(''), $fontStyle);
        $table->addCell(70)->addText(htmlspecialchars('4.'), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('Surat Edaran Bank Indonesia Nomor 11/9/DPbS tentang Bank Umum Syariah.'), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars(''), $fontStyle);
        $table->addCell(70)->addText(htmlspecialchars('5.'), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('Peraturan Otoritas Jasa Keuangan Nomor 65/POJK.03/2016 tentang Penerapan Manajemen Risiko Bagi Bank Umum Syariah dan Unit Usaha Syariah.'), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars(''), $fontStyle);
        $table->addCell(70)->addText(htmlspecialchars('6.'), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('Akta Pernyataan Keputusan Rapat Umum Pemegang Saham Tahunan PT Bank Syariah Mandiri Nomor 44 tanggal 16 Maret 2018. '), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars(''), $fontStyle);
        $table->addCell(70)->addText(htmlspecialchars('7.'), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('Struktur Organisasi PT Bank Syariah Mandiri.'), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars(''), $fontStyle);
        $table->addCell(70)->addText(htmlspecialchars('8.'), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('SPB Pembiayaan dan PTO Limit Wewenang Memutus Pembiayaan.'), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars(''), $fontStyle);
        $table->addCell(70)->addText(htmlspecialchars('9.'), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('Nota ERM No.21/206A-2/ERM tanggal 4 November 2019 perihal Peningkatan Limit Wewenang Memutus Pembiayaan bagi Branch Manager, Area Consumer Banking Manager dan Area Micro & Pawning Manager.'), $fontStyle);
        $section->addText('M E M U T U S K A N',
        array('name' => 'Arial', 'size' => 11, 'bold' => true),
        array('align' => 'center')
        );
        $section->addText('Menetapkan',
        array('name' => 'Arial', 'size' => 11, 'bold' => true)
        );
        $styleTable = array('borderSize' => 0, 'cellMargin' => 80);
        $styleFirstRow = array('borderBottomSize' => 18, 'borderBottomColor' => '0000FF');
        $styleCell = array('valign' => 'center');
        $fontStyle = array('name' => 'Arial', 'size' => 11, 'align' => 'justify');
        $phpWord->addTableStyle('Fancy Table', $styleTable, $styleFirstRow);
        $table = $section->addTable('Fancy Table');
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars('PERTAMA:'), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('Sdr. '
        . ucwords(strtolower($pwmp->namapejabat))
        . '/NIP '
        . $pwmp->nip 
        . ' diberikan limit wewenang untuk memutus pembiayaan dan penanganan pembiayaan bermasalah sebesar '), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars(''), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('A. Limit Non Gadai Rp.'
        . number_format($pwmp->limitnongadai, 0, ',', '.')), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars(''), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('B. Limit Gadai Non Logam Rp.'
        . number_format($pwmp->limitgadainlm, 0, ',', '.')), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars(''), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('C. Limit Gadai Logam Mulia Rp.'
        . number_format($pwmp->limitgadailm, 0, ',', '.')), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars(''), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('D. Limit Otomatis Rp.'
        . number_format($pwmp->limitotomatis, 0, ',', '.')), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars(''), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('Terhitung sejak keputusan ini dibuat'), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars('KEDUA:'), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('Limit wewenang memutus penanganan pembiayaan bermasalah tidak berlaku untuk pembiayaan yang pernah direkomendasikan atau diputuskan oleh Sdr. '
            . ucwords(strtolower($pwmp->namapejabat))
            . '.'), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars('KETIGA:'), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('Pemutusan pembiayaan dimaksud harus tetap berpedoman pada prinsip kehati-hatian dan didasarkan atas kesepakatan mutlak (full consensus) Komite Pembiayaan sesuai dengan limit dan segmentasi yang ditetapkan dalam Kebijakan Pembiayaan dan ketentuan internal PT Bank Syariah Mandiri.'), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars('KEEMPAT'), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('Wewenang Komite Pembiayaan sebagaimana dimaksud pada butir ketiga antara lain mencakup pemutusan pembiayaan baru dan upaya penyelamatan pembiayaan. Khusus untuk perpanjangan pembiayaan berlaku 2 (dua) kali limit wewenang memutus sebagaimana tersebut pada butir pertama. Proses pemutusan pembiayaan harus memperhatikan Kebijakan Pembiayaan, ketentuan internal PT Bank Syariah Mandiri dan peraturan perundang-undangan yang berlaku.'), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars('KELIMA'), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('Limit wewenang memutus pembiayaan dan penanganan pembiayaan bermasalah sebagaimana dimaksud pada butir pertama diberikan kepada Sdr. '
            . ucwords(strtolower($pwmp->namapejabat))
            . ' yang diangkat berdasarkan Surat Keputusan Direksi Perseroan. Apabila Sdr. '
            . ucwords(strtolower($pwmp->namapejabat))
            . ' tidak lagi menjabat sebagai pemegang kewenangan memutus pembiayaan dan penanganan pembiayaan bermasalah baik karena mutasi jabatan, pengunduran diri, pensiun, berhenti, atau diberhentikan dari perseroan, maka Surat Keputusan ini tidak berlaku.'), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars('KEENAM'), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('Apabila di kemudian hari terdapat kekeliruan dalam keputusan ini, akan     diadakan perbaikan sebagaimana mestinya.'), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars('KETUJUH'), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('Dengan berlakunya Surat Keputusan ini, maka Surat Keputusan Direksi Nomor '
            . $pwmp->nosk
            . ' tanggal '
            . \Carbon\Carbon::parse($pwmp->tglsk)->format('d-m-Y')
            . ' dinyatakan tidak berlaku.'), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars(''), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('Salinan  Keputusan ini disampaikan kepada: '), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars(''), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('1. Unit Kerja Terkait'), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars(''), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('2. Human Capital Services Group'), $fontStyle);
        $table->addRow();
        $table->addCell(100)->addText(htmlspecialchars(''), $fontStyle);
        $table->addCell(25008)->addText(htmlspecialchars('3. Legal Group'), $fontStyle);
        $section->addText('Ditetapkan:       di Jakarta <w:br/>Pada tanggal:  ',
        array('name' => 'Arial', 'size' => 11)
        );
        $section->addText('DIREKSI',
        array('name' => 'Arial', 'size' => 11)
        );
        $section->addText('ttd,',
        array('name' => 'Arial', 'size' => 11)
        );
        $section->addText('Achmad Syafii<w:br/>Direktur',
        array('name' => 'Arial', 'size' => 11)
        );
        $section->addText('Salinan sesuai dengan aslinya<w:br/>PT  BANK SYARIAH MANDIRI<w:br/>ENTERPRISE RISK MANAGEMENT GROUP',
        array('name' => 'Arial', 'size' => 11)
        );
        $section->addTextBreak(2);
        $section->addText('M. Fanny Fansyuri<w:br/>Group Head',
        array('name' => 'Arial', 'size' => 11)
        );
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('SK Pengajuan Limit.doc');
        return response()->download(public_path('SK Pengajuan Limit.doc'));


    }

    public function GenerateNota (Request $request, $id) {

        DB::table('pwmp.riwayatpwmp')->where('id', $id)->update([
            'status' => 3,
        ]);

        $cekstatus = DB::table('pwmp.riwayatpwmp')->select('pwmp.riwayatpwmp.jenispengajuan')
        ->where('pwmp.riwayatpwmp.id', $id)->first();
        // dd($cekstatus->jenispengajuan);
        if ($cekstatus->jenispengajuan == "A" ) {
            $pwmp = DB::table('pwmp.riwayatpwmp')->select('pwmp.riwayatpwmp.*',
                        'pwmp.riwayatpwmp.limitnongadai as nilailimitnongadai',
                        'pwmp.riwayatpwmp.limitgadainlm as nilailimitgadainlm',
                        'pwmp.riwayatpwmp.limitgadailm as nilailimitgadailm',
                        'pwmp.riwayatpwmp.limitotomatis as nilailimitotomatis',
                        'pwmp.pengajuanlimit.limitnongadai as limitnongadai',
                        'pwmp.pengajuanlimit.limitgadainlm as limitgadainlm',
                        'pwmp.pengajuanlimit.limitgadailm as limitgadailm',
                        'pwmp.pengajuanlimit.limitotomatis as limitotomatis', 
                        'en1.keterangan as statuspengajuan', 
                        'master.area.nama as namaarea',
                        'master.cabang.nama as namacabang',
                        'staging.hcspejabat.statuskaryawan',
                        'master.jabatan.keterangan as namajabatan',
                        'master.area.kode as kodearea', 
                        'master.area.nama as namaarea',
                        'master.area.koderegion as koderegion', 
                        'en2.keterangan as keteranganappraisal', 
                        'staging.smrisk.statussertifikasi', 
                        'en4.keterangan as keterangansanksi', 
                        'en5.keterangan as keteranganfraud', 
                        'staging.refreshment.statusrefreshment', 
                        'calc.age_year',
                        'calc.age_month',
                        'pq.*'
                    )
                    ->join('master.cabang', 'pwmp.riwayatpwmp.kodecabang', '=', 'master.cabang.kode')
                    ->join('master.jabatan', 'pwmp.riwayatpwmp.kodejabatan', '=', 'master.jabatan.kode')
                    ->join('master.enumerasi as en1', DB::raw('pwmp.riwayatpwmp.status::text'), '=', 'en1.kode')
                    ->join('staging.hcspejabat', 'pwmp.riwayatpwmp.nip', '=', 'staging.hcspejabat.nip')
                    ->join('master.enumerasi as en2', DB::raw('staging.hcspejabat.appraisal::text'), '=', 'en2.kode')
                    ->join('master.area', 'master.area.kode', '=', 'master.cabang.kodearea')
                    ->join('staging.smrisk', 'staging.hcspejabat.nip', '=', 'staging.smrisk.nip')
                    ->join('master.enumerasi as en4', 'staging.hcspejabat.sanksi', '=', 'en4.kode')
                    ->join('master.enumerasi as en5', 'staging.hcspejabat.fraud', '=', 'en5.kode')
                    ->leftjoin('staging.refreshment', 'pwmp.riwayatpwmp.nip', '=', 'staging.refreshment.nip')
                    ->leftjoin('pwmp.portfolio_qualitysum as pq', 'pwmp.riwayatpwmp.nip', '=', 'pq.nip')
                    ->leftjoin('staging.vcalcpengalamanpby as calc', 'calc.nip', '=', 'staging.hcspejabat.nip')
                    ->leftjoin('pwmp.pengajuanlimit', 'pwmp.riwayatpwmp.id', '=', 'pwmp.pengajuanlimit.id')
                    ->where('en1.kategori', '=', 'STATUS PENGAJUAN')
                    ->where('en2.kategori', '=', 'APPRAISAL')
                    ->where('en4.kategori', '=', 'SANKSI')
                    ->where('en5.kategori', '=', 'FRAUD')
                    ->where('pwmp.riwayatpwmp.id', $id)->first();
        }
        else {
            $pwmp = DB::table('pwmp.riwayatpwmp')->select('pwmp.riwayatpwmp.*',
                        'pwmp.riwayatpwmp.limitnongadai as nilailimitnongadai',
                        'pwmp.riwayatpwmp.limitgadainlm as nilailimitgadainlm',
                        'pwmp.riwayatpwmp.limitgadailm as nilailimitgadailm',
                        'pwmp.riwayatpwmp.limitotomatis as nilailimitotomatis',
                        'pwmp.pengajuanlimit.limitnongadai as limitnongadai',
                        'pwmp.pengajuanlimit.limitgadainlm as limitgadainlm',
                        'pwmp.pengajuanlimit.limitgadailm as limitgadailm',
                        'pwmp.pengajuanlimit.limitotomatis as limitotomatis', 
                        'en1.keterangan as statuspengajuan', 
                        'master.area.kode as kodearea', 
                        'master.area.nama as namaarea',
                        'master.area.koderegion as koderegion',
                        'master.cabang.nama as namacabang',
                        'master.jabatan.keterangan as namajabatan',
                        'pwmp.pwmp.kategorilimit',
                        'staging.hcspejabat.statuskaryawan', 
                        'master.kategorilimit.keterangan as keterangankategorilimit', 
                        'en2.keterangan as keteranganappraisal', 
                        'en3.keterangan as keteranganfungsi', 
                        'staging.smrisk.statussertifikasi', 
                        'en4.keterangan as keterangansanksi', 
                        'en5.keterangan as keteranganfraud', 
                        'staging.refreshment.statusrefreshment', 
                        'limitnongadai.limitnongadai as nilailimitnongadai',
                        'limitgadainlm.limitgadainlm as nilailimitgadainlm',
                        'limitgadailm.limitgadailm as nilailimitgadailm',
                        'limitotomatis.limitotomatis as nilailimitotomatis',
                        'cpw.age_year',
                        'cpw.age_month',
                        DB::raw("COALESCE(pwmp.pwmp.nip, 'BARU') as statuspermohonan"),
                        'staging.hcspejabat.nip as niphcspejabat',
                        'pq.*'
                    )
                    ->join('master.cabang', 'pwmp.riwayatpwmp.kodecabang', '=', 'master.cabang.kode')
                    ->join('master.jabatan', 'pwmp.riwayatpwmp.kodejabatan', '=', 'master.jabatan.kode')
                    ->join('master.enumerasi as en1', DB::raw('pwmp.riwayatpwmp.status::text'), '=', 'en1.kode')
                    ->join('staging.hcspejabat', 'pwmp.riwayatpwmp.nip', '=', 'staging.hcspejabat.nip')
                    ->join('master.enumerasi as en2', DB::raw('staging.hcspejabat.appraisal::text'), '=', 'en2.kode')
                    ->join('master.area', 'master.area.kode', '=', 'master.cabang.kodearea')
                    ->join('pwmp.pwmp', 'pwmp.riwayatpwmp.nip', '=', 'pwmp.pwmp.nip')
                    ->join('master.kategorilimit', 'pwmp.pwmp.kategorilimit', '=', 'master.kategorilimit.kode')
                    ->join('staging.smrisk', 'staging.hcspejabat.nip', '=', 'staging.smrisk.nip')
                    ->join('master.enumerasi as en3', 'pwmp.riwayatpwmp.fungsi', '=', 'en3.kode')
                    ->join('master.enumerasi as en4', 'staging.hcspejabat.sanksi', '=', 'en4.kode')
                    ->join('master.enumerasi as en5', 'staging.hcspejabat.fraud', '=', 'en5.kode')
                    ->join('pwmp.pwmp as limitnongadai', 'pwmp.riwayatpwmp.nip', '=', 'limitnongadai.nip')
                    ->join('pwmp.pwmp as limitgadainlm', 'pwmp.riwayatpwmp.nip', '=', 'limitgadainlm.nip')
                    ->join('pwmp.pwmp as limitgadailm', 'pwmp.riwayatpwmp.nip', '=', 'limitgadailm.nip')
                    ->join('pwmp.pwmp as limitotomatis', 'pwmp.riwayatpwmp.nip', '=', 'limitotomatis.nip')
                    ->leftjoin('staging.refreshment', 'pwmp.riwayatpwmp.nip', '=', 'staging.refreshment.nip')
                    ->leftjoin('pwmp.portfolio_qualitysum as pq', 'pwmp.riwayatpwmp.nip', '=', 'pq.nip')
                    ->leftjoin('staging.vcalcpengalamanpby as calc', 'calc.nip', '=', 'staging.hcspejabat.nip')
                    ->leftjoin('pwmp.pengajuanlimit', 'pwmp.riwayatpwmp.id', '=', 'pwmp.pengajuanlimit.id')
                    ->leftjoin('pwmp.vcalcpengalamanpwmp as cpw', 'cpw.nip', '=', 'pwmp.pwmp.nip')
                    ->where('en1.kategori', '=', 'STATUS PENGAJUAN')
                    ->where('en2.kategori', '=', 'APPRAISAL')
                    ->where('en3.kategori', '=', 'FUNGSI')
                    ->where('en4.kategori', '=', 'SANKSI')
                    ->where('en5.kategori', '=', 'FRAUD')
                    ->where('pwmp.riwayatpwmp.id', $id)->first();
        }
        // dd ($pwmp);
        $riwayatjabatan = DB::table('staging.hcsriwayatjabatan')->select('staging.hcsriwayatjabatan.kodejabatan',
                        'staging.hcsriwayatjabatan.kodecabang',
                        'staging.hcsriwayatjabatan.tglmulai',
                        'staging.hcsriwayatjabatan.tglberakhir',
                        'master.cabang.nama as namacabang',
                        'master.jabatan.keterangan as namajabatan',
                        'master.area.nama as namaarea')
                        ->join('master.cabang', 'staging.hcsriwayatjabatan.kodecabang', '=', 'master.cabang.kode')
                        ->join('master.jabatan', 'staging.hcsriwayatjabatan.kodejabatan', '=', 'master.jabatan.kode')
                        ->join('master.area', 'master.cabang.kodearea', '=', 'master.area.kode')
                        ->where('staging.hcsriwayatjabatan.nip', $pwmp->nip)->get();
        // dd($pwmp);
        // dd($riwayatjabatan);

        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection(
        array('marginLeft' => 1700, 'marginRight' => 1700,
              'marginTop' => 1000, 'marginBottom' => 600));
        $header = array('size' => 16, 'bold' => true);
        $phpWord->setDefaultFontSize(11);
        $phpWord->setDefaultParagraphStyle(
            array(
                'align'      => 'both',
                'spaceAfter' => \PhpOffice\PhpWord\Shared\Converter::pointToTwip(0),
                'spacing'    => 120,
                )
            );
        $phpWord->addNumberingStyle(
            'multilevel',
            array(
                'type' => 'multilevel',
                'levels' => array(
                    array('format' => 'decimal', 'text' => '%1.', 'left' => 360, 'hanging' => 360, 'tabPos' => 360),
                    array('format' => 'lowerLetter', 'text' => '%2.', 'left' => 720, 'hanging' => 360, 'tabPos' => 720),
                ),
                array('name' => 'Arial', 'size' => 11),
            )
        );
        $section->addImage('img/logo.png',
            array(
                'width'         => 165,
                'height'        => 69,
                'marginTop'     => -40,
                'marginLeft'    => 370,
                'align'         => 'right',
                'wrappingStyle' => 'behind',
                'positioning' => 'relative',
                'posHorizontal' => 'absolute',
                'posHorizontalRel' => 'page',
                'posVertical' => 'absolute',
                'posVerticalRel' => 'line',
            )
        );
        $section->addText('NOTA',
        array('name' => 'Arial', 'size' => 18, 'bold' => true),
        array('align' => 'center')
        );
        $styleTable = array('borderSize' => 0, 'cellMargin' => 10, 'borderColor' =>'000000', 'align'=>'both');
        $styleFirstRow = array('borderBottomSize' => 10, 'borderBottomColor' => 'FF0000');
        $cellColSpan = array('gridSpan' => 2);
        $styleCell = array('valign' => 'center');
        $fontStyle = array('name' => 'Arial', 'size' => 11, 
                     array('align' => 'both'));
        $phpWord->addTableStyle('Fancy Table', $styleTable, $styleFirstRow);
        $table = $section->addTable('Fancy Table',
        array('align'=>'both')
        );
        $table->addRow();
        $table->addCell(7000)->addText(htmlspecialchars('NO         '
        .'21/126-2/ERM       '), $fontStyle);
        $table->addCell(7000)->addText(htmlspecialchars('Tanggal        '
        .'27 Juni 2019     '), $fontStyle);      
        $table->addRow();
        $table->addCell(7000)->addText(htmlspecialchars('Dari         '
        .'ERM       '), $fontStyle);
        $table->addCell(7000)->addText(htmlspecialchars('Kepada        '
        .'Direksi dan SEVP     '), $fontStyle);
        $table->addRow();
        $table->addCell(14000, $cellColSpan)->addText(htmlspecialchars('Perihal         '
        .'PENINGKATAN LIMIT WEWENANG MEMUTUS PEMBIAYAAN BAGI SDR. '
        .$pwmp->namapejabat),$fontStyle); 
        $section->addText('Assalamualaikum Wr. Wb.',
        array('name' => 'Arial', 'size' => 11)
        );
        $section->addText('I.   Latar Belakang',
        array('name' => 'Arial', 'size' => 11, 'bold'=> true)
        );
        $section->addListItem('Evaluasi kualitas pembiayaan yang diputus oleh Sdr. '
        .ucwords(strtolower($pwmp->namapejabat))
        .' '
        .ucwords(strtolower(str_replace("&","&amp;",$pwmp->namajabatan)))
        .' '
        .ucwords(strtolower($pwmp->namaarea)),
        .0, null, 'multilevel');
        $section->addListItem('Rekomendasi Regional Head '
            .$pwmp->koderegion
            .' mengenai peningkatan limit wewenang memutus pembiayaan bagi Sdr. '
            .ucwords(strtolower($pwmp->namapejabat)), 
            .0, null, 'multilevel');
        $section->addListItem('Optimalisasi pemutusan pembiayaan level komite '
            .$pwmp->kategorilimit 
            .' untuk mendukung pertumbuhan bisnis', 0, null, 'multilevel');
        $section->addText('II.   Kajian',
        array('name' => 'Arial', 'size' => 11, 'bold'=> true)
        );
        $phpWord->addNumberingStyle(
            'multilevel1',
            array(
                'type' => 'multilevel',
                'levels' => array(
                    array('format' => 'decimal', 'text' => '%1.', 'left' => 360, 'hanging' => 360, 'tabPos' => 360),
                    array('format' => 'lowerLetter', 'text' => '%2.', 'left' => 720, 'hanging' => 360, 'tabPos' => 720),
                ),
                array('name' => 'Arial', 'size' => 11),
            )
        );
        $section->addListItem('Regional Head '
            .$pwmp->koderegion 
            .' merekomendasikan peningkatan limit wewenang memutus pembiayaan bagi Sdr. '
            .ucwords(strtolower($pwmp->namapejabat))
            .' ' 
            .ucwords(strtolower(str_replace("&","&amp;",$pwmp->namajabatan)))
            .' dari Rp. ' 
            .number_format($pwmp->nilailimitnongadai, 0, ',', '.')
            .' untuk non gadai dan Rp. '
            .number_format($pwmp->nilailimitgadailm, 0, ',', '.')
            .' untuk gadai logam mulia dan Rp. '
            .number_format($pwmp->nilailimitgadainlm, 0, ',', '.')
            .' untuk gadai non logam dan Rp. '
            .number_format($pwmp->nilailimitotomatis, 0, ',', '.')
            .' untuk limit otomatis, menjadi:', 0, null, 'multilevel1');
        $section->addListItem('Rp. '
            .number_format($pwmp->limitnongadai, 0, ',', '.')
            .' untuk non gadai', 1, null, 'multilevel');
        $section->addListItem('Rp. '
            .number_format($pwmp->limitgadailm, 0, ',', '.')
            .' untuk gadai logam mulia', 1, null, 'multilevel');
        $section->addListItem('Rp. '
            .number_format($pwmp->limitgadainlm, 0, ',', '.')
            .' untuk gadai non logam', 1, null, 'multilevel');
        $section->addListItem('Rp. '
            .number_format($pwmp->limitotomatis, 0, ',', '.')
            .' untuk limit otomatis', 1, null, 'multilevel');
        $section->addListItem('Riwayat tugas Sdr. '
            .ucwords(strtolower($pwmp->namapejabat)),
            .0, null, 'multilevel1');
        foreach($riwayatjabatan as $jbt) {
            $section->addListItem(ucwords(strtolower(str_replace("&","&amp;",$jbt->namajabatan)))
                .' ' 
                .ucwords(strtolower($jbt->namaarea))
                .' ' 
                .\Carbon\Carbon::parse($jbt->tglmulai)->format('d-M-Y')
                .' - '
                .\Carbon\Carbon::parse($jbt->tglberakhir)->format('d-M-Y')
                .' ',1, null, 'multilevel1');
        }
        $section->addListItem('Sdr. '
            .ucwords(strtolower($pwmp->namapejabat))
            .' telah mengikuti pelatihan pembiayaan gadai dan pembiayaan mikro. Yang bersangkutan memiliki pengalaman sebagai pemegang kewenangan memutus pembiayaan gadai selama lebih dari '
            .$pwmp->age_year
            .' tahun dengan limit terakhir Rp. '
            .number_format($pwmp->nilailimitnongadai, 0, ',', '.')
            .' untuk non gadai dan Rp. '
            .number_format($pwmp->nilailimitgadailm, 0, ',', '.')
            .' untuk gadai logam mulia dan Rp. '
            .number_format($pwmp->nilailimitgadainlm, 0, ',', '.')
            .' untuk gadai non logam dan Rp. '
            .number_format($pwmp->nilailimitotomatis, 0, ',', '.')
            .' untuk limit otomatis,', 0, null, 'multilevel1');
        $section->addListItem('Portofolio pembiayaan gadai yang diputus oleh Sdr. '
            .ucwords(strtolower($pwmp->namapejabat))
            .' periode Mei 2018 s.d. Mei 2019 sebesar Rp. '
            .number_format($pwmp->nomospokok, 0, ',', '.')
            .' dengan NFP '
            .number_format((float)$pwmp->pctnpf, 2, '.', '')
            .'% dan DPK '
            .number_format((float)$pwmp->pctdpk, 2, '.', '')
            .'% Sdr. '
            .ucwords(strtolower($pwmp->namapejabat))
            .' mulai bertugas/menangani pembiayaan mikro sejak Maret 2019. Sampai dengan saat ini yang bersangkutan belum merekomendasikan persetujuan pembiayaan Mikro.', 0, null, 'multilevel1');
        $section->addListItem('Sesuai SPB Pembiayaan, kriteria pejabat yang dapat diberikan peningkatan limit wewenang memutus pembiayaan adalah:', 0, null, 'multilevel1');
        $section->addListItem('Memiliki pengalaman sebagai pejabat pemegang limit wewenang memutus pembiayaan/penanganan pembiayaan bermasalah minimal 1 tahun;', 1, null, 'multilevel1');
        $section->addListItem('Memiliki sertifikat manajemen risiko;', 1, null, 'multilevel1');
        $section->addListItem('Kualitas pembiayaan yang diputus selama satu tahun terakhir tergolong baik dengan NPF kurang dari 1% dan pembiayaan yang diputus selama 6 bulan terakhir tidak ada yang mengalami downgrade;', 1, null, 'multilevel1');
        $section->addListItem('Tidak sedang dalam proses pembinaan atau pemberian sanksi;', 1, null, 'multilevel1');
        $section->addListItem('Mendapat rekomendasi dari Regional Head.', 1, null, 'multilevel1');
        $section->addListItem('Mengacu pada butir II.5. di atas, Sdr. '
            .ucwords(strtolower($pwmp->namapejabat))
            .' telah memenuhi seluruh kriteria yang dipersyaratkan sebagaimana butir II.5.', 0, null, 'multilevel1');
        $section->addListItem('Berdasarkan hal-hal di atas, Sdr. '
            .ucwords(strtolower($pwmp->namapejabat))
            .' dapat dipertimbangkan untuk diberikan peningkatan limit wewenang memutus pembiayaan dan penanganan pembiayaan bermasalah:', 0, null, 'multilevel1');
        $section->addListItem('Rp. '
            .number_format($pwmp->limitnongadai, 0, ',', '.')
            .' untuk non gadai', 1, null, 'multilevel1');
        $section->addListItem('Rp. '
            .number_format($pwmp->limitgadailm, 0, ',', '.')
            .' untuk gadai logam mulia', 1, null, 'multilevel1');
        $section->addListItem('Rp. '
            .number_format($pwmp->limitgadainlm, 0, ',', '.')
            .' untuk gadai non logam', 1, null, 'multilevel1');
        $section->addListItem('Rp. '
            .number_format($pwmp->limitotomatis, 0, ',', '.')
            .' untuk limit otomatis', 1, null, 'multilevel1');
        $section->addListItem('Limit wewenang yang dimaksud pada butir II.7. berlaku untuk pemutusan pembiayaan baru, perpanjangan dan restrukturisasi/penanganan pembiayaan bermasalah. Penggunaan limit wewenang memutus tersebut mengacu pada ketentuan eksternal dan internal yang berlaku.', 0, null, 'multilevel1');
        $section->addListItem('Untuk menjaga kualitas keputusan pembiayaan pejabat dimaksud, Area Manager dan Regional Head perlu  memonitor dan menyupervisi kualitas pembiayaan yang diputus oleh pejabat-pejabat pada butir II.1. ', 0, null, 'multilevel1');
        
        $phpWord->addNumberingStyle(
            'multilevel2',
            array(
                'type' => 'multilevel',
                'levels' => array(
                    array('format' => 'decimal', 'text' => '%1.', 'left' => 360, 'hanging' => 360, 'tabPos' => 360),
                    array('format' => 'lowerLetter', 'text' => '%2.', 'left' => 720, 'hanging' => 360, 'tabPos' => 720),
                ),
                array('name' => 'Arial', 'size' => 11),
            )
        );
        $section->addText('III.   Rekomendasi',
        array('name' => 'Arial', 'size' => 11, 'bold'=> true)
        );
        $section->addText('Berdasarkan uraian di atas, kami merekomendasikan:',
        array('name' => 'Arial', 'size' => 11)
        );
        $section->addListItem('Peningkatan limit wewenang memutus pembiayaan dan penanganan pembiayaan bermasalah bagi Sdr. '
            .ucwords(strtolower($pwmp->namapejabat))
            .' sebesar:', 0, null, 'multilevel2');
        $section->addListItem('Rp. '
            .number_format($pwmp->limitnongadai, 0, ',', '.')
            .' untuk non gadai', 1, null, 'multilevel1');
        $section->addListItem('Rp. '
            .number_format($pwmp->limitgadailm, 0, ',', '.')
            .' untuk gadai logam mulia', 1, null, 'multilevel1');
        $section->addListItem('Rp. '
            .number_format($pwmp->limitgadainlm, 0, ',', '.')
            .' untuk gadai non logam', 1, null, 'multilevel1');
        $section->addListItem('Rp. '
            .number_format($pwmp->limitotomatis, 0, ',', '.')
            .' untuk limit otomatis', 1, null, 'multilevel1');
        $section->addListItem('Limit wewenang yang dimaksud pada butir III.1. berlaku untuk pemutusan pembiayaan baru, perpanjangan dan restrukturisasi/penanganan pembiayaan bermasalah. Penggunaan limit wewenang memutus tersebut mengacu pada ketentuan eksternal dan internal yang berlaku.', 0, null, 'multilevel2');
        $section->addListItem('Pelaksanaan pelatihan terstruktur di Retail Banking Academy untuk Sdr. '
            .ucwords(strtolower($pwmp->namapejabat)),
            .0, null, 'multilevel2');
        $section->addListItem('Monitoring dan supervisi oleh Area Manager dan Regional Head terhadap kualitas yang diputus oleh pejabat pada butir III.1.', 0, null, 'multilevel2');
        $section->addListItem('Evaluasi limit wewenang memutus pembiayaan selambat-lambatnya dalam jangka waktu 12 bulan bagi pejabat pada butir III.1.', 0, null, 'multilevel2');
        $section->addTextBreak(1);
        $section->addText('Demikian kami sampaikan, apabila Direksi menyetujui usulan tersebut mohon Direksi menandatangani Surat Keputusan Direksi terlampir.',
        array('name' => 'Arial', 'size' => 11)
        );
        $section->addText('Wassalaamu’alaikum Wr. Wb. ',
        array('name' => 'Arial', 'size' => 11)
        );
        $section->addTextBreak(1);
        $section->addText('ENTERPRISE RISK MANAGEMENT GROUP',
        array('name' => 'Arial', 'size' => 11)
        );
        $section->addTextBreak(3);
        $section->addText('M. Fanny Fansyuri<w:br/>Group Head',
        array('name' => 'Arial', 'size' => 11),
        array('align' => 'left')
        );
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('Nota Pengajuan Limit.doc');
        return response()->download(public_path('Nota Pengajuan Limit.doc'));

    }
}
