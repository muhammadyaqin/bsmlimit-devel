<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use App\UserApp;
use App\UserRole;
use App\Role;

use Carbon\Carbon;

class UserAppController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $report = DB::table('master.userapp')->select('master.userapp.*')->orderby('created_at', 'asc');
        // dd($userrole);
        $name_nip = strtoupper($request->nameNIP);
        // dd($name_nip);
        if(!is_null($name_nip)) {
            $report = $report->where(Db::raw("upper(master.userapp.email)"), 'LIKE', '%'. $name_nip. '%');
        }

        $report = $report->paginate(10);
        $data['user'] = $report;
        return view('UserApp.index', $data, compact('report'));
    }

    public function create()
    {
        $role = Role::all();
        return view('UserApp.create',compact('role'));
    }

    public function show($id)
    {
        $user = User::join('role_user', 'users.id', '=', 'role_user.user_id')->where([
                ['role_id', '=', 1], ['users.id', '=', $id]
                ])->
            first();
            
        return view('UserApp.details', compact('user'));
    }

    public function store(Request $request)
    {
        $email = $request->input('email');
        $statusaktif = $request->input('statusaktif');
        $role = $request->input('role');
        $created_at = Carbon::now();
        $updated_at = Carbon::now();

        DB::table('master.userapp')->insert([
            'email' => $email,
            'statusaktif' => $statusaktif,
            'created_at' => $created_at,
            'updated_at' => $updated_at,
        ]);

        DB::table('master.userrole')->insert([
            'email' => $email,
            'role' => $role,
            'created_at' => $created_at,
            'updated_at' => $updated_at,
        ]);
    
        return redirect('/user');
    }

    public function edit($email)
    {
        $userapp = DB::table('master.userapp')->where('email', $email)->first();
        $data['user'] = $userapp;

        return view('UserApp.edit', $data);
    }

    public function update(Request $request, $email)
    {
        // dd($request);

        $email = $request->input('email');
        $statusaktif = $request->input('statusaktif');
        

        $userrole = DB::table('master.userrole')->where('email', $request->pk)->first();
        $role = $userrole->role;

        DB::table('master.userrole')->where('email', $request->pk)->delete();

       // dd( DB::table('master.userapp')->where('email', $email)->get() );
        DB::table('master.userapp')->where('email', $request->pk)->update([
            'email' => $email,
            'statusaktif' => $statusaktif,
        ]);

        DB::table('master.userrole')->insert([
            "email" => $email,
            "role" => $role
        ]);

        // DB::table('master.userrole')->where('email',$request->pk)->update([
        //     "email"=>$email
        // ]);

        // return response()->json( DB::table('master.userapp')
        // // ->select('master.userapp.*')
        // ->join('master.userrole', 'master.userapp.email', '=','master.userrole.email')
        // ->where("master.userapp.email",$request->pk)->get());

        // DB::table('master.userapp')
        // ->join('master.userrole', 'master.userapp.email', 'master.userrole.email')
        // ->where("master.userapp.email",$request->pk)->update([
        //     'email' => $email,
        //     'statusaktif' => $statusaktif,
        
        // ]);
        return redirect('/user');
    }

    public function destroy($email)
    {   
        DB::table('master.userrole')->where('email', $email)->delete();
        $user = UserApp::whereemail($email);
        $user->delete();
        return redirect('/user');
    }
}
