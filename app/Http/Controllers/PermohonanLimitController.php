<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use Auth;
use Session;
use App\Mail\EmailNotifikasi;
use Illuminate\Support\Facades\Mail;
use File;


class PermohonanLimitController extends Controller
{
    //
    public function baru()
    {

    	$query = DB::table('pwmp.pwmp');
	    $pejabat = $query->get();

	    $data['pejabatt'] = $pejabat;

    	return view('limit.baru');
    }

    public function tingkat()
    {
    	return view('limit.tingkat');
    }

    public function autoComplete (Request $request)
    {
      $kodeareajabatan = Session::get('areajabatan');
      $sesskodecabang = '';
      $sesskodearea = '';
      $sesskoderegion = '';
      if ($kodeareajabatan == 'C') $sesskodecabang = "AND cb.kode = '" . Session::get('kodecabang') . "'";
      else if ($kodeareajabatan == 'A') $sesskodearea = "AND ar.kode = '" . Session::get('kodecabang') . "'";
      else if ($kodeareajabatan == 'R') $sesskoderegion = "AND reg.kode = '" . Session::get('kodecabang') . "'";

      $user = Auth::user()->email;
      $cekjabatan = Session::get('kodejabatan');

		  $search = $request->get('searchNIP');

      $userRole = Auth::user()->cekRole()->role;
      $qNonKP = "";
      if ($userRole != 'ERM') {
        $qNonKP = "AND (
          UPPER(hp.email) = UPPER('$user')
          OR 
          EXISTS (
            SELECT 1 FROM (
              WITH RECURSIVE nodes(kode,keterangan,parent) AS (
                SELECT s1.kode, s1.keterangan, s1.parent
                FROM master.jabatan s1 WHERE parent = '$cekjabatan'
                UNION
                SELECT s2.kode, s2.keterangan, s2.parent
                FROM master.jabatan s2, nodes s1 WHERE s2.parent = s1.kode
              )
              SELECT * FROM nodes
            ) rq
            WHERE rq.kode = hp.kodejabatan
          )
        )";        
      }

      $query = DB::select(
                 DB::raw(
                 "SELECT 
                  hp.nip,
                  hp.namapejabat,
                  hp.statuskaryawan,
                  hp.kodejabatan,
                  hp.kodecabang,
                  hp.email,
                  hp.sanksi,
                  hp.appraisal,
                  hp.fraud,
                  jb.keterangan AS namajabatan,
                  cb.nama AS namacabang,
                  ar.kode AS kodearea,
                  ar.nama AS namaarea,
                  en2.keterangan AS keterangansanksi,
                  en1.keterangan AS keteranganappraisal,
                  en3.keterangan AS keteranganfraud,
                  smr.statussertifikasi,
                  re.statusrefreshment,
                  pw.tglsk,
                  pw.nosk,
                  pw.limitnongadai,
                  pw.limitgadailm,
                  pw.limitgadainlm,
                  pw.limitotomatis,
                  pw.fungsi,
                  pw.kategorilimit,
                  en4.keterangan AS keteranganfungsi,
                  kl.keterangan AS keterangankategorilimit,
                  COALESCE(calc.age_year, 0) as age_year,
                  COALESCE(calc.age_month, 0) as age_month,
                  COALESCE(cpw.age_year, 0) as age_year_pwmp,
                  COALESCE(cpw.age_month, 0) as age_month_pwmp,
                  COALESCE(pq.nomlancar,0) as nomlancar,
                  COALESCE(pq.nomdpk, 0) as nomdpk,
                  COALESCE(pq.nomnpf, 0) as nomnpf,
                  COALESCE(pq.nomospokok, 0) as nomospokok,
                  COALESCE(pq.pctlancar, 0) as pctlancar,
                  COALESCE(pq.pctdpk, 0) as pctdpk,
                  COALESCE(pq.pctnpf, 0) as pctnpf,
                  COALESCE(pw.nip, 'BARU') as jenispengajuan
                  FROM staging.hcspejabat hp
                  INNER JOIN master.jabatan jb ON jb.kode = hp.kodejabatan
                  INNER JOIN master.cabang cb ON cb.kode = hp.kodecabang $sesskodecabang
                  INNER JOIN master.area ar ON ar.kode = cb.kodearea $sesskodearea
                  INNER JOIN master.region reg ON reg.kode = ar.koderegion $sesskoderegion
                  INNER JOIN master.enumerasi en2 ON en2.kode = hp.sanksi AND en2.kategori = 'SANKSI'
                  INNER JOIN master.enumerasi en1 ON en1.kode = hp.appraisal AND en1.kategori = 'APPRAISAL'
                  INNER JOIN master.enumerasi en3 ON en3.kode = hp.fraud AND en3.kategori = 'FRAUD'
                  LEFT JOIN staging.smrisk smr ON smr.nip = hp.nip
                  LEFT JOIN staging.refreshment re ON re.nip = hp.nip
                  LEFT JOIN staging.vcalcpengalamanpby calc ON calc.nip = hp.nip
                  LEFT JOIN pwmp.pwmp pw ON pw.nip = hp.nip
                  LEFT JOIN master.enumerasi en4 ON en4.kode = pw.fungsi AND en4.kategori = 'FUNGSI'
                  LEFT JOIN master.kategorilimit kl ON kl.kode = pw.kategorilimit
                  LEFT JOIN pwmp.vcalcpengalamanpwmp cpw ON cpw.nip = pw.nip
                  LEFT JOIN pwmp.portfolio_qualitysum pq ON pq.nip = pw.nip
                  WHERE (hp.nip LIKE '%$search%' OR hp.namapejabat LIKE '%$search%')
                  $qNonKP
                ORDER BY hp.namapejabat
                   ")
             );
        // dd($query);
        // $msu = DB::table('staging.msutrainingpby')->select('msutrainingpby.nip', 'msutrainingpby.namatraining')->where('nip', $search)->get();
        
        // $query['msutraining'] = $msu;

        $data['pejabatt'] = $query;

      return response()->json($data);
    }

    public function PelatihanPembiayaan(Request $request)
    {
        $nipPelatihan = $request->get('nipPelatihan');

        $msu = DB::table('staging.msutrainingpby')->select('msutrainingpby.nip', 'msutrainingpby.namatraining')->where('nip', $nipPelatihan)->get();
        

        return response()->json($msu);
    }

  //   public function loadTable (Request $request)
  //   {
  //   	$kerja = $request->input("kerja");
  //   	$regi = $request->input("region");

  //   	if ($kerja == null) {
  //   		$query = DB::table('riwayat_pwmp')->where('jabatan', $regi)->orderBy('created_at', 'asc')->get();
  //   	}elseif ($regi == null) {
  //   		$query = DB::table('riwayat_pwmp')->where('unit_kerja', $kerja)->orderBy('created_at', 'asc')->get();
  //   	}else{
  //   		$query = DB::table('riwayat_pwmp')->where('unit_kerja', $kerja)->where('jabatan', $regi)->orderBy('created_at', 'asc')->get();
  //   	}

  //       // $pembiayaan = $query->get();
		// // dd($kerja);

  //       // $data['pejabatt'] = $query;

  //       return response()->json($query);
  //   }

  //   public function loadTablePejabat()
  //   {
  //   	$query = DB::table('riwayat_pwmp')->orderBy('created_at', 'asc');
		// $pembiayaan = $query->get();

		// return response()->json($pembiayaan);
  //   }

    public function limitBaru(Request $request)
    {
        $nip = $request->input('nip');
        $name = $request->input('name');
        $statuskaryawan = $request->input('statuskaryawan');
        $kodejabatan = $request->input('kodejabatan');
        $kodecabang = $request->input('kodecabang');
        $kodearea = $request->input('area');
        $limitnongadai = $request->input('limitnongadai');
        $limitgadainlm = $request->input('limitgadainlm');
        $limitgadailm = $request->input('limitgadailm');
        $limitotomatis = $request->input('limit_otomatis');
        $nilailimitnongadai = $request->input('nilailimitnongadai');
        $nilailimitgadainlm = $request->input('nilailimitgadainlm');
        $nilailimitgadailm = $request->input('nilailimitgadailm');
        $nilailimitotomatis = $request->input('nilailimit_otomatis');
        $limitdiajukan = $request->input('limit_diajukan');
        $fungsi = $request->input('fungsi');
        $kategorilimit = $request->input('kategorilimit');
        $smrisk = $request->input('smrisk');
        $appraisal = $request->input('appraisal');
        $sanksi = $request->input('sanksi');
        $lancar = $request->input('lancar');
        $persen_lancar = $request->input('persen_lancar');
        $dpk = $request->input('dpk');
        $persen_dpk = $request->input('persen_dpk');
        $npf = $request->input('npf');
        $persen_npf = $request->input('persen_npf');
        $total = $request->input('total');

        $fraud = $request->input('fraud');
        $refreshment = $request->input('refreshment');
        $calc_year = $request->input('calc_year');
        $calc_month = $request->input('calc_month');
        $nosk = $request->input('nosk');
        $tglsk = $request->input('tglsk');
        $approveby = $request->input('approve_by');
        $jenispengajuan = $request->input('jenispengajuan');
        if ($jenispengajuan == "BARU") {
          $jenispengajuan1 = 'A';
        }
        else {
          $jenispengajuan1 = 'B';
        }
        $approve_ro_date =  Carbon::now();
        $create =  Carbon::now();
        $update =  Carbon::now();

        if (isset($_FILES['file'])){
          if(is_uploaded_file($_FILES['file']['tmp_name'])){

              $nama_file = time()."_".$_FILES["file"]["name"];
              move_uploaded_file($_FILES["file"]["tmp_name"],public_path('/file_skp/skp/').$nama_file);
          }
        }else{
          $nama_file = 'nofile';
        }

        if ($jenispengajuan == "BARU") {
            $pengajuan_limit = DB::table('pwmp.riwayatpwmp')->insertgetid([
              'nip' => $nip,
              'namapejabat' => $name,
              'kodejabatan' => $kodejabatan,
              'kodecabang' => $kodecabang,
              'statuskaryawan' => $statuskaryawan,
              'kategorilimit' => $kategorilimit,
              'statussmrisk' => $smrisk,
              'appraisal' => $appraisal,
              'sanksi' => $sanksi,
              'fraud' => $fraud,
              'statusrefreshment' => $refreshment,
              'limitnongadai' => $nilailimitnongadai,
              'limitgadainlm' => $nilailimitgadainlm,
              'limitgadailm' => $nilailimitgadailm,
              'limitotomatis' => $nilailimitotomatis,
              'fungsi' => $fungsi,
              
              'qltportolancar_nom' => $lancar,
              'qltportolancar_pct' => $persen_lancar,
              'qltportodpk_nom' => $dpk,
              'qltportodpk_pct' => $persen_dpk,
              'qltportonpf_nom' => $npf,
              'qltportonpf_pct' => $persen_npf,
              'qltportototal_nom' => $total,

              'nosk' => $nosk,
              'pengalamanpby_bln' => $calc_month,
              'pengalamanpby_thn' => $calc_year,
              'tglsk' => $tglsk,
              'status' => 1,
              'created_by' => $approveby,
              'file_skpp' => $nama_file,
              'jenispengajuan' => $jenispengajuan1,
              'created_at' => $create,
              'updated_at' => $update,
          ]);
        }else{
            $pengajuan_limit = DB::table('pwmp.riwayatpwmp')->insertgetid([
                'nip' => $nip,
                'namapejabat' => $name,
                'kodejabatan' => $kodejabatan,
                'kodecabang' => $kodecabang,
                'statuskaryawan' => $statuskaryawan,
                'kategorilimit' => $kategorilimit,
                'statussmrisk' => $smrisk,
                'appraisal' => $appraisal,
                'sanksi' => $sanksi,
                'fraud' => $fraud,
                'statusrefreshment' => $refreshment,
                'limitnongadai' => $nilailimitnongadai,
                'limitgadainlm' => $nilailimitgadainlm,
                'limitgadailm' => $nilailimitgadailm,
                'limitotomatis' => $nilailimitotomatis,
                'qltportolancar_nom' => $lancar,
                'qltportolancar_pct' => $persen_lancar,
                'qltportodpk_nom' => $dpk,
                'qltportodpk_pct' => $persen_dpk,
                'qltportonpf_nom' => $npf,
                'qltportonpf_pct' => $persen_npf,
                'qltportototal_nom' => $total,
                'fungsi' => $fungsi,
                'nosk' => $nosk,
                'pengalamanpwmp_bln' => $calc_month,
                'pengalamanpwmp_thn' => $calc_year,
                'tglsk' => $tglsk,
                'created_by' => $approveby,
                'status' => 1,
                'file_skpp' => $nama_file,
                'jenispengajuan' => $jenispengajuan1,
                'jenispengajuan' => $jenispengajuan1,
                'created_at' => $create,
                'updated_at' => $update,
            ]);
        }

        DB::table('pwmp.pengajuanlimit')->insert([
            'id' => $pengajuan_limit,
            'limitnongadai' => $limitnongadai,
            'limitgadainlm' => $limitgadainlm,
            'limitgadailm' => $limitgadailm,
            'limitotomatis' => $limitotomatis,
        ]);

      $namatrainingpby = DB::table('staging.msutrainingpby')->select('staging.msutrainingpby.*')
      ->where('staging.msutrainingpby.nip', $nip)->get();

      foreach ($namatrainingpby as $namatrainingpby) {
        $trainingpengajuan = $namatrainingpby->namatraining;
        $niptrainingpengajuan = $namatrainingpby->nip;

        DB::table('pwmp.trainingpby')->insert([
            'idriwayatpwmp' => $pengajuan_limit,
            'nip' => $niptrainingpengajuan,
            'namapelatihan' => $trainingpengajuan,
        ]);  
      }
        $cekkodecabang = Session::get('kodecabang');
        $cekRegion = DB::table('master.cabang as cab')->select('reg.kode')
        ->leftjoin('master.area as ar', 'cab.kodearea', '=', 'ar.kode')
        ->leftjoin('master.region as reg', 'reg.kode', '=', 'ar.koderegion')
        ->where('cab.kode', $cekkodecabang)->first();
        $cekRH = DB::table('staging.hcspejabat')->select('staging.hcspejabat.email')
        ->where('kodecabang', $cekRegion->kode)
        ->where('kodejabatan', 'RH001')->first();    

        Mail::to($cekRH->email)->send(new EmailNotifikasi());

        return redirect()->action('DashboardController@index');
    }

    public function limitBaruRegion(Request $request)
    {
       $nip = $request->input('nip');
       $name = $request->input('name');
       $statuskaryawan = $request->input('statuskaryawan');
       $kodejabatan = $request->input('kodejabatan');
       $kodecabang = $request->input('kodecabang');
       $kodearea = $request->input('area');
       $limitnongadai = $request->input('limitnongadai');
       $limitgadainlm = $request->input('limitgadainlm');
       $limitgadailm = $request->input('limitgadailm');
       $limitotomatis = $request->input('limit_otomatis');
       $limitdiajukan = $request->input('limit_diajukan');
       $nilailimitnongadai = $request->input('nilailimitnongadai');
       $nilailimitgadainlm = $request->input('nilailimitgadainlm');
       $nilailimitgadailm = $request->input('nilailimitgadailm');
       $nilailimitotomatis = $request->input('nilailimit_otomatis');
       $fungsi = $request->input('fungsi');
       $kategorilimit = $request->input('kategorilimit');
       $smrisk = $request->input('smrisk');
       $appraisal = $request->input('appraisal');
       $sanksi = $request->input('sanksi');

       $lancar = $request->input('lancar');
       $persen_lancar = $request->input('persen_lancar');
       $dpk = $request->input('dpk');
       $persen_dpk = $request->input('persen_dpk');
       $npf = $request->input('npf');
       $persen_npf = $request->input('persen_npf');
       $total = $request->input('total');

       $fraud = $request->input('fraud');
       $refreshment = $request->input('refreshment');
       $calc_year = $request->input('calc_year');
       $calc_month = $request->input('calc_month');
       $nosk = $request->input('nosk');
       $tglsk = $request->input('tglsk');
       $approveby = $request->input('approve_by');
       $jenispengajuan = $request->input('jenispengajuan');
        if ($jenispengajuan == "BARU") {
          $jenispengajuan1 = 'A';
        }
        else {
          $jenispengajuan1 = 'B';
        }
        $approve_ro_date =  Carbon::now();
        $create =  Carbon::now();
        $update =  Carbon::now();

        if (isset($_FILES['file'])){
          if(is_uploaded_file($_FILES['file']['tmp_name'])){

              $nama_file = time()."_".$_FILES["file"]["name"];
              move_uploaded_file($_FILES["file"]["tmp_name"],public_path('/file_skp/skp/').$nama_file);
          }
        }else{
          $nama_file = 'nofile';
        }

        if ($jenispengajuan == "BARU") {
            $pengajuan_limit = DB::table('pwmp.riwayatpwmp')->insertgetid([
              'nip' => $nip,
              'namapejabat' => $name,
              'kodejabatan' => $kodejabatan,
              'kodecabang' => $kodecabang,
              'statuskaryawan' => $statuskaryawan,
              'kategorilimit' => $kategorilimit,
              'statussmrisk' => $smrisk,
              'appraisal' => $appraisal,
              'sanksi' => $sanksi,
              'fraud' => $fraud,
              'statusrefreshment' => $refreshment,
              'limitnongadai' => $nilailimitnongadai,
              'limitgadainlm' => $nilailimitgadainlm,
              'limitgadailm' => $nilailimitgadailm,
              'limitotomatis' => $nilailimitotomatis,
              'fungsi' => $fungsi,
              
              'qltportolancar_nom' => $lancar,
              'qltportolancar_pct' => $persen_lancar,
              'qltportodpk_nom' => $dpk,
              'qltportodpk_pct' => $persen_dpk,
              'qltportonpf_nom' => $npf,
              'qltportonpf_pct' => $persen_npf,
              'qltportototal_nom' => $total,
              'created_by' => $approveby,
              'nosk' => $nosk,
              'pengalamanpby_bln' => $calc_month,
              'pengalamanpby_thn' => $calc_year,
              'tglsk' => $tglsk,
              'status' => 1,
              'file_skpp' => $nama_file,
              'jenispengajuan' => $jenispengajuan1,
              'created_at' => $create,
              'updated_at' => $update,
          ]);

        }elseif($kodejabatan == "RH001") {
          $pengajuan_limit = DB::table('pwmp.riwayatpwmp')->insertgetid([
                'nip' => $nip,
                'namapejabat' => $name,
                'kodejabatan' => $kodejabatan,
                'kodecabang' => $kodecabang,
                'statuskaryawan' => $statuskaryawan,
                'kategorilimit' => $kategorilimit,
                'statussmrisk' => $smrisk,
                'appraisal' => $appraisal,
                'sanksi' => $sanksi,
                'fraud' => $fraud,
                'statusrefreshment' => $refreshment,
                'limitnongadai' => $nilailimitnongadai,
                'limitgadainlm' => $nilailimitgadainlm,
                'limitgadailm' => $nilailimitgadailm,
                'limitotomatis' => $nilailimitotomatis,
                'qltportolancar_nom' => $lancar,
                'qltportolancar_pct' => $persen_lancar,
                'qltportodpk_nom' => $dpk,
                'qltportodpk_pct' => $persen_dpk,
                'qltportonpf_nom' => $npf,
                'qltportonpf_pct' => $persen_npf,
                'qltportototal_nom' => $total,
                'created_by' => $approveby,
                'fungsi' => $fungsi,
                'nosk' => $nosk,
                'pengalamanpwmp_bln' => $calc_month,
                'pengalamanpwmp_thn' => $calc_year,
                'tglsk' => $tglsk,
                'status' => 2,
                'file_skpp' => $nama_file,
                'jenispengajuan' => $jenispengajuan1,
                'created_at' => $create,
                'updated_at' => $update,
            ]);
        }
        else{
            $pengajuan_limit = DB::table('pwmp.riwayatpwmp')->insertgetid([
                'nip' => $nip,
                'namapejabat' => $name,
                'kodejabatan' => $kodejabatan,
                'kodecabang' => $kodecabang,
                'statuskaryawan' => $statuskaryawan,
                'kategorilimit' => $kategorilimit,
                'statussmrisk' => $smrisk,
                'appraisal' => $appraisal,
                'sanksi' => $sanksi,
                'fraud' => $fraud,
                'statusrefreshment' => $refreshment,
                'limitnongadai' => $nilailimitnongadai,
                'limitgadainlm' => $nilailimitgadainlm,
                'limitgadailm' => $nilailimitgadailm,
                'limitotomatis' => $nilailimitotomatis,

                'qltportolancar_nom' => $lancar,
                'qltportolancar_pct' => $persen_lancar,
                'qltportodpk_nom' => $dpk,
                'qltportodpk_pct' => $persen_dpk,
                'qltportonpf_nom' => $npf,
                'qltportonpf_pct' => $persen_npf,
                'qltportototal_nom' => $total,
                'created_by' => $approveby,
                'fungsi' => $fungsi,
                'nosk' => $nosk,
                'pengalamanpwmp_bln' => $calc_month,
                'pengalamanpwmp_thn' => $calc_year,
                'tglsk' => $tglsk,
                'status' => 1,
                'file_skpp' => $nama_file,
                'jenispengajuan' => $jenispengajuan1,
                'created_at' => $create,
                'updated_at' => $update,
            ]);
        }

        DB::table('pwmp.pengajuanlimit')->insert([
            'id' => $pengajuan_limit,
            'limitnongadai' => $limitnongadai,
            'limitgadainlm' => $limitgadainlm,
            'limitgadailm' => $limitgadailm,
            'limitotomatis' => $limitotomatis,
        ]);

        $namatrainingpby = DB::table('staging.msutrainingpby')->select('staging.msutrainingpby.*')
        ->where('staging.msutrainingpby.nip', $nip)->get();

        foreach ($namatrainingpby as $namatrainingpby) {
          $trainingpengajuan = $namatrainingpby->namatraining;
          $niptrainingpengajuan = $namatrainingpby->nip;

          DB::table('pwmp.trainingpby')->insert([
              'idriwayatpwmp' => $pengajuan_limit,
              'nip' => $niptrainingpengajuan,
              'namapelatihan' => $trainingpengajuan,
          ]);  
        }

        $cekkodecabang = Session::get('kodecabang');
        $cekRegion = DB::table('master.cabang as cab')->select('reg.kode')
        ->leftjoin('master.area as ar', 'cab.kodearea', '=', 'ar.kode')
        ->leftjoin('master.region as reg', 'reg.kode', '=', 'ar.koderegion')
        ->where('cab.kode', $cekkodecabang)->first();
        $cekRH = DB::table('staging.hcspejabat')->select('staging.hcspejabat.email')
        ->where('kodecabang', $cekRegion->kode)
        ->where('kodejabatan', 'RH001')->first();  
        
        Mail::to($cekRH->email)->send(new EmailNotifikasi());

        return redirect()->action('DashboardController@index');
    }

     public function limitBaruERM(Request $request)
    {
        $nip = $request->input('nip');
        $name = $request->input('name');
        $statuskaryawan = $request->input('statuskaryawan');
        $kodejabatan = $request->input('kodejabatan');
        $kodecabang = $request->input('kodecabang');
        $kodearea = $request->input('area');
        $limitnongadai = $request->input('limitnongadai');
        $limitgadainlm = $request->input('limitgadainlm');
        $limitgadailm = $request->input('limitgadailm');
        $limitotomatis = $request->input('limit_otomatis');
        $limitdiajukan = $request->input('limit_diajukan');
        $nilailimitnongadai = $request->input('nilailimitnongadai');
        $nilailimitgadainlm = $request->input('nilailimitgadainlm');
        $nilailimitgadailm = $request->input('nilailimitgadailm');
        $nilailimitotomatis = $request->input('nilailimit_otomatis');
        $fungsi = $request->input('fungsi');
        $kategorilimit = $request->input('kategorilimit');
        $smrisk = $request->input('smrisk');
        $appraisal = $request->input('appraisal');
        $sanksi = $request->input('sanksi');

        $lancar = $request->input('lancar');
        $persen_lancar = $request->input('persen_lancar');
        $dpk = $request->input('dpk');
        $persen_dpk = $request->input('persen_dpk');
        $npf = $request->input('npf');
        $persen_npf = $request->input('persen_npf');
        $total = $request->input('total');

        $fraud = $request->input('fraud');
        $refreshment = $request->input('refreshment');
        $calc_year = $request->input('calc_year');
        $calc_month = $request->input('calc_month');
        $nosk = $request->input('nosk');
        $tglsk = $request->input('tglsk');
        $approveby = $request->input('approve_by');
        $jenispengajuan = $request->input('jenispengajuan');
         if ($jenispengajuan == "BARU") {
           $jenispengajuan1 = 'A';
         }
         else {
           $jenispengajuan1 = 'B';
         }
         $approve_ro_date =  Carbon::now();
         $create =  Carbon::now();
         $update =  Carbon::now();

         if (isset($_FILES['file'])){
           if(is_uploaded_file($_FILES['file']['tmp_name'])){

               $nama_file = time()."_".$_FILES["file"]["name"];
               move_uploaded_file($_FILES["file"]["tmp_name"],public_path('/file_skp/skp/').$nama_file);
           }
         }else{
           $nama_file = 'nofile';
         }

         if ($jenispengajuan == "BARU") {
             $pengajuan_limit = DB::table('pwmp.riwayatpwmp')->insertgetid([
               'nip' => $nip,
               'namapejabat' => $name,
               'kodejabatan' => $kodejabatan,
               'kodecabang' => $kodecabang,
               'statuskaryawan' => $statuskaryawan,
               'kategorilimit' => $kategorilimit,
               'statussmrisk' => $smrisk,
               'appraisal' => $appraisal,
               'sanksi' => $sanksi,
               'fraud' => $fraud,
               'statusrefreshment' => $refreshment,
               'limitnongadai' => $nilailimitnongadai,
               'limitgadainlm' => $nilailimitgadainlm,
               'limitgadailm' => $nilailimitgadailm,
               'limitotomatis' => $nilailimitotomatis,
               'fungsi' => $fungsi,
               
               'qltportolancar_nom' => $lancar,
               'qltportolancar_pct' => $persen_lancar,
               'qltportodpk_nom' => $dpk,
               'qltportodpk_pct' => $persen_dpk,
               'qltportonpf_nom' => $npf,
               'qltportonpf_pct' => $persen_npf,
               'qltportototal_nom' => $total,
               'created_by' => $approveby,
               'nosk' => $nosk,
               'pengalamanpby_bln' => $calc_month,
               'pengalamanpby_thn' => $calc_year,
               'tglsk' => $tglsk,
               'status' => 3,
               'file_skpp' => $nama_file,
               'jenispengajuan' => $jenispengajuan1,
               'created_at' => $create,
               'updated_at' => $update,
           ]);
         }else{
             $pengajuan_limit = DB::table('pwmp.riwayatpwmp')->insertgetid([
                 'nip' => $nip,
                 'namapejabat' => $name,
                 'kodejabatan' => $kodejabatan,
                 'kodecabang' => $kodecabang,
                 'statuskaryawan' => $statuskaryawan,
                 'kategorilimit' => $kategorilimit,
                 'statussmrisk' => $smrisk,
                 'appraisal' => $appraisal,
                 'sanksi' => $sanksi,
                 'fraud' => $fraud,
                 'statusrefreshment' => $refreshment,
                  'limitnongadai' => $nilailimitnongadai,
                  'limitgadainlm' => $nilailimitgadainlm,
                  'limitgadailm' => $nilailimitgadailm,
                  'limitotomatis' => $nilailimitotomatis,

                 'qltportolancar_nom' => $lancar,
                 'qltportolancar_pct' => $persen_lancar,
                 'qltportodpk_nom' => $dpk,
                 'qltportodpk_pct' => $persen_dpk,
                 'qltportonpf_nom' => $npf,
                 'qltportonpf_pct' => $persen_npf,
                 'qltportototal_nom' => $total,
                 'created_by' => $approveby,
                 'fungsi' => $fungsi,
                 'nosk' => $nosk,
                 'pengalamanpwmp_bln' => $calc_month,
                 'pengalamanpwmp_thn' => $calc_year,
                 'tglsk' => $tglsk,
                 'status' => 3,
                 'file_skpp' => $nama_file,
                 'jenispengajuan' => $jenispengajuan1,
                 'created_at' => $create,
                 'updated_at' => $update,
             ]);
         }

         DB::table('pwmp.pengajuanlimit')->insert([
             'id' => $pengajuan_limit,
             'limitnongadai' => $limitnongadai,
             'limitgadainlm' => $limitgadainlm,
             'limitgadailm' => $limitgadailm,
             'limitotomatis' => $limitotomatis,
         ]);

         $namatrainingpby = DB::table('staging.msutrainingpby')->select('staging.msutrainingpby.*')
         ->where('staging.msutrainingpby.nip', $nip)->get();

         foreach ($namatrainingpby as $namatrainingpby) {
           $trainingpengajuan = $namatrainingpby->namatraining;
           $niptrainingpengajuan = $namatrainingpby->nip;

           DB::table('pwmp.trainingpby')->insert([
               'idriwayatpwmp' => $pengajuan_limit,
               'nip' => $niptrainingpengajuan,
               'namapelatihan' => $trainingpengajuan,
           ]);  
         }

        return redirect()->action('DashboardController@index');
    }
}
