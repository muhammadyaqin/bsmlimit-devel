<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PWPMP;
use App\Imports\PwmpImport;
use Maatwebsite\Excel\Facades\Excel;
use Session;
use Auth;
use App\Exports\PwmpExport;
use DB;
use File;

class PwmpController extends Controller
{
    public function index(Request $request)
    {
        $userRole = Auth::user()->cekRole()->role;
        // dd(Session()->all());
        $cekareajabatan = Session::get('areajabatan');
        $cekkodecabang = Session::get('kodecabang');
        $kodeareajabatan = Session::get('areajabatan');
        $limitkodecabang = '';
        if ($kodeareajabatan == 'C') $limitkodecabang = "AND pwmp.pwmp.kodecabang ='" . Session::get('kodecabang') . "'";
        else if ($kodeareajabatan == 'A') {
                $limitkodecabang = "
                AND exists (
                  select 1 
                  from master.cabang x 
                  where x.kodearea = '" . Session::get('kodecabang') . "'
                  and x.kode = pwmp.pwmp.kodecabang
                )";
              }
        else if ($kodeareajabatan == 'R') {
                $limitkodecabang = "
                AND exists (
                  select 1 
                  from master.cabang x
                  inner join master.area y on y.kode = x.kodearea
                  where y.koderegion = '" . Session::get('kodecabang') . "'
                  and x.kode = pwmp.pwmp.kodecabang
                )";
        }
        $statusAct = $request->area;
        $statusReg = $request->region;
        $statusCab = $request->cabang;

        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $statusDate = 0;

        $user = Auth::user()->email;
        $cekjabatan = Session::get('kodejabatan');

        if($startDate != null){
            $statusDate = 1;
        }

        if($endDate != null){
            $statusDate = 1;
        }else{
            $startDate = date("d-m-Y");
            $endDate = date("d-m-Y");
        }
        
        $status = $request->status;

        $name_nip = $request->name_nip;

        // $limitkodecabang = '';

        $report = DB::table('pwmp.pwmp')->select(
            'pwmp.pwmp.*',
            'master.cabang.nama',
            'master.jabatan.keterangan',
            'staging.hcspejabat.email'
        )
        ->join('master.cabang','pwmp.pwmp.kodecabang','=','master.cabang.kode')
        ->join('master.area','master.cabang.kodearea','=','master.area.kode')
        ->join('master.jabatan','pwmp.pwmp.kodejabatan','=','master.jabatan.kode')
        ->leftjoin('staging.hcspejabat', 'staging.hcspejabat.nip', '=', 'pwmp.pwmp.nip')
        ->orderBy('pwmp.pwmp.nip', 'desc');

        if($userRole != 'IOG' && $userRole != 'ERM'){
            $report->whereRaw(DB::raw("
            (
              UPPER(staging.hcspejabat.email) = UPPER('$user')
              OR 
              EXISTS (
                SELECT 1 FROM (
                  WITH RECURSIVE nodes(kode,keterangan,parent) AS (
                    SELECT s1.kode, s1.keterangan, s1.parent
                    FROM master.jabatan s1 WHERE parent = '$cekjabatan'
                    UNION
                    SELECT s2.kode, s2.keterangan, s2.parent
                    FROM master.jabatan s2, nodes s1 WHERE s2.parent = s1.kode
                  )
                  SELECT * FROM nodes
                ) rq
               WHERE rq.kode = pwmp.pwmp.kodejabatan
              )
            )$limitkodecabang"));
        }


        // dd($report);
        $page = 1;

        $next = $page+1;
        $prev = $page-1;
        
        $numbering =0;
        if($request->page){
            $page=$request->page;
            $numbering = $page*10;
        }

        if($name_nip != null){
            $report = $report->where('staging.hcspejabat.nip', 'LIKE', '%'. $name_nip. '%')->orWhere('staging.hcspejabat.namapejabat', 'LIKE', '%'. $name_nip. '%');
            // ->whereDate('pwmp.pwmp.created_at' , '<=' , date("Y-m-d",strtotime($endDate)))
            // ->whereDate('pwmp.pwmp.created_at' , '>=' , date("Y-m-d",strtotime($startDate)));
        }

        // // dd($startDate);

        if ($startDate != null && $endDate != null) {
            $hideStatus = 'not_active';
        } else {
            $hideStatus = 'active';
        }

        if(($statusReg != null)&&($statusReg!="")){
            $region = DB::table('master.region')->where('master.region.kode', $statusReg)->pluck("kode");

            $report = $report->whereIn('master.area.koderegion', $region);
        }

        if(($statusAct != null)&&($statusAct!="")){
            $area = DB::table('master.cabang')->where('master.cabang.kodearea', $statusAct)->pluck("kode");

            $report = $report->whereIn('master.area.kode', $area);
        }

        if(($statusCab != null)&&($statusCab!="")){
            $cabang = DB::table('master.cabang')->where('master.cabang.kode', $statusCab)->pluck("kode");

            $report = $report->whereIn('pwmp.pwmp.kodecabang', $cabang);
        }

        if($status != null){
            $report = $report->where('status', $status);
        }

        if ($statusAct != null or $status != null or $name_nip) {
            $statusActive = 'notActive';
        } else {
            $statusActive = 'isActive';
        }

        if ($cekareajabatan == 'C') {
            $getarea = DB::table('master.area as a')
            ->select('a.*')
            ->join('master.cabang as c', 'c.kodearea', '=', 'a.kode')
            ->where('c.kode', $cekkodecabang)
            ->orderBy('a.nama', 'asc')
            ->get();
        }
        else if ($cekareajabatan == 'A') {
            $getarea = DB::table('master.area as a')
            ->select('a.*')
            ->where('a.kode', $cekkodecabang)
            ->orderBy('a.nama', 'asc')
            ->get();
        }
        else if ($cekareajabatan == 'R') {
            // dd($cekkodecabang);
            $getarea = DB::table('master.area as a')
            ->select('a.*')
            ->join('master.region as r', 'r.kode', '=', 'a.koderegion')
            ->where('r.kode', $cekkodecabang)
            ->orderBy('a.nama', 'asc')
            ->get();
        }            
        else if ($cekareajabatan == 'P') {
            $getarea = DB::table('master.area as a')
            ->select('a.*')
            ->orderBy('a.nama', 'asc')
            ->get();
        }
        else {
            $getarea = DB::table('master.area as a')
            ->select('a.*')
            ->orderBy('a.nama', 'asc')
            ->join('master.region as r', 'r.kode', '=', 'a.koderegion')
            ->where('r.kode', $statusReg)
            ->orderBy('a.nama', 'asc')
            ->get();
        }

        $getregion = DB::table('master.region as reg')
        ->select('reg.*')
        ->orderBy('reg.kode','asc')
        ->get();

        $getcabang = DB::table('master.cabang as cab')
        ->select('cab.*')
        ->where('cab.kodearea', $statusAct)
        ->get();

        $enumerasi = DB::table('master.enumerasi')->where('master.enumerasi.kategori', '=', 'STATUS PENGAJUAN')->get();

        $report = $report->paginate(10);

	    $data['pwmp'] = $report;
        $date = [
            'unit_kerja' => $statusAct,
            'status' => $status,
            'startDate' => $startDate,
            'endDate' => $endDate,
            'statusAct' => $statusAct,
        ];

        // dd($report);

    	return view('pwmp.pwmp', $data, compact('date', 'getarea', 'enumerasi', 'name_nip', 'statusAct', 'statusActive', 'next','prev', 'page', 'numbering', 'startDate', 'endDate', 'numbering','getregion','statusReg','getcabang','statusCab'));
    }

    public function import_excel(Request $request) 
	{
		// validasi
		$this->validate($request, [
			'file' => 'required|mimes:csv,xls,xlsx'
		]);
 
		// menangkap file excel
		$file = $request->file('file');
 
		// membuat nama file unik
		$nama_file = rand().$file->getClientOriginalName();
 
		// upload ke folder file_siswa di dalam folder public
		$file->move('file_pwmp',$nama_file);
 
		// import data
		Excel::import(new PwmpImport, public_path('/file_pwmp/'.$nama_file));
 
		// notifikasi dengan session
		Session::flash('sukses','Data PWMP Berhasil Diimport!');
 
		// alihkan halaman kembali
		return redirect('/pwmp');
	}

	public function detail($nip){
		$pwmp = DB::select(
                   DB::raw(
                   "SELECT 
                    pw.nip,
                    pw.namapejabat,
                    hp.statuskaryawan,
                    pw.kodejabatan,
                    pw.filesk,
                    pw.kodecabang,
                    hp.email,
                    hp.sanksi,
                    hp.appraisal,
                    hp.fraud,
                    jb.keterangan AS namajabatan,
                    cb.nama AS namacabang,
                    ar.kode AS kodearea,
                    ar.nama AS namaarea,
                    en2.keterangan AS keterangansanksi,
                    en1.keterangan AS keteranganappraisal,
                    en3.keterangan AS keteranganfraud,
                    smr.statussertifikasi,
                    re.statusrefreshment,
                    pw.tglsk,
                    pw.nosk,
                    pw.limitnongadai,
                    pw.limitgadailm,
                    pw.limitgadainlm,
                    pw.limitotomatis,
                    pw.fungsi,
                    pw.kategorilimit,
                    en4.keterangan AS keteranganfungsi,
                    kl.keterangan AS keterangankategorilimit,
                    COALESCE(calc.age_year, 0) as age_year,
                    COALESCE(calc.age_month, 0) as age_month,
                    COALESCE(cpw.age_year, 0) as age_year_pwmp,
                    COALESCE(cpw.age_month, 0) as age_month_pwmp,
                    COALESCE(pq.nomlancar,0) as nomlancar,
                    COALESCE(pq.nomdpk, 0) as nomdpk,
                    COALESCE(pq.nomnpf, 0) as nomnpf,
                    COALESCE(pq.nomospokok, 0) as nomospokok,
                    COALESCE(pq.pctlancar, 0) as pctlancar,
                    COALESCE(pq.pctdpk, 0) as pctdpk,
                    COALESCE(pq.pctnpf, 0) as pctnpf,
                    COALESCE(pw.nip, 'BARU') as jenispengajuan
                    FROM pwmp.pwmp pw
                    INNER JOIN master.jabatan jb ON jb.kode = pw.kodejabatan
                    INNER JOIN master.cabang cb ON cb.kode = pw.kodecabang
                    INNER JOIN master.area ar ON ar.kode = cb.kodearea
					LEFT JOIN staging.hcspejabat hp ON hp.nip = pw.nip
                    LEFT JOIN master.enumerasi en2 ON en2.kode = hp.sanksi AND en2.kategori = 'SANKSI'
                    LEFT JOIN master.enumerasi en1 ON en1.kode = hp.appraisal AND en1.kategori = 'APPRAISAL'
                    LEFT JOIN master.enumerasi en3 ON en3.kode = hp.fraud AND en3.kategori = 'FRAUD'
                    LEFT JOIN staging.smrisk smr ON smr.nip = pw.nip
                    LEFT JOIN staging.refreshment re ON re.nip = pw.nip
                    LEFT JOIN staging.vcalcpengalamanpby calc ON calc.nip = pw.nip
                    LEFT JOIN master.enumerasi en4 ON en4.kode = pw.fungsi AND en4.kategori = 'FUNGSI'
                    LEFT JOIN master.kategorilimit kl ON kl.kode = pw.kategorilimit
                    LEFT JOIN pwmp.vcalcpengalamanpwmp cpw ON cpw.nip = pw.nip
                    LEFT JOIN pwmp.portfolio_qualitysum pq ON pq.nip = pw.nip
                    WHERE (pw.nip = '$nip')
                     ")
               );

	    $ambilnip = $pwmp[0]->nip;
	    // dd($ambilnip);
	    $msu = DB::table('staging.msutrainingpby')->select('msutrainingpby.nip', 'msutrainingpby.namatraining')->where('nip', $ambilnip)->get();
		$data['msu'] = $msu;

		$data['pwmp'] = $pwmp;

        $cekfile = $pwmp[0]->filesk;

        $img_path=public_path('/file_skp/sk/'.$cekfile);

        if(File::exists($img_path)){
            $statusActive = 'yes';
        }else{
            $statusActive = 'no';
        }

		return view('pwmp.detail_pwmp', $data, compact('statusActive'));
	}

	public function export_excel()
	{
	    return Excel::download(new PwmpExport, 'Rekapitulasi Daftar PWMP.xlsx');
	}
}
