<?php

namespace App\Http\Controllers\Administrator\DataMaster;

use App\Enumerasi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exports\EnumerasiExport;
use App\Exports\EnumerasiReport;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Session;

class EnumerasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $kodkatket = $request->kodkatket;
        if ($kodkatket != null) {
            $statusActive = 'notActive';
        } else {
            $statusActive = 'isActive';
        }

        if($kodkatket != null){
            $enumerasi = DB::table('enumerasi')->where('master.enumerasi.kode', 'LIKE', '%'. $kodkatket. '%')
                                                    ->orWhere('kategori', 'LIKE', '%'. $kodkatket. '%')
                                                    ->orWhere('keterangan', 'LIKE', '%'.$kodkatket.'%')
                                                    ->paginate(10);
        }else{
            $enumerasi = DB::table('enumerasi')->paginate(10);    
        }

        return view ('Administrator.Data-master.enumerasi.index',compact('enumerasi','kodkatket','statusActive'));
    }

    public function export_excel(Request $request)
    {
        return (new EnumerasiReport((object) $request))->download('Enumerasi.xlsx');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $enumerasi = Enumerasi::get();
        return view ('Administrator.Data-master.enumerasi.create',compact('enumerasi'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $enumerasi = new Enumerasi();
        $enumerasi->kode=$request->kode;
        $enumerasi->kategori=$request->kategori;
        $enumerasi->keterangan=$request->keterangan;
        $enumerasi->save();
        
        return redirect ('/Administrator/Data-master/enumerasi');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Enumerasi  $enumerasi
     * @return \Illuminate\Http\Response
     */
    public function show(Enumerasi $enumerasi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Enumerasi  $enumerasi
     * @return \Illuminate\Http\Response
     */
    public function edit($kode)
    {
        //        
        $enumerasi = Enumerasi::where('kode',$kode)->first();
        return view ('Administrator.Data-master.enumerasi.edit',compact('enumerasi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Enumerasi  $enumerasi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $enumerasi = DB::table('enumerasi')->where(['kode'=>$request->kode, 'kategori'=>$request->kategori]);
        $enumerasi->update(['keterangan'=>$request->keterangan]);

        return redirect ('/Administrator/Data-master/enumerasi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Enumerasi  $enumerasi
     * @return \Illuminate\Http\Response
     */
    public function destroy($kode,$kategori)
    {
        //
        DB::table('enumerasi')->where(['kode'=>$kode, 'kategori'=>$kategori])->delete();
        return redirect('/Administrator/Data-master/enumerasi');
    }
}
