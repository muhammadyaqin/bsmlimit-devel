<?php

namespace App\Http\Controllers\Administrator\DataMaster;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Jabatan;
use App\Exports\JabatanExport;
use App\Exports\JabatanReport;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class JabatanController extends Controller
{

    public function index(Request $request)
    {
        $kodejenis = $request->kodejenis;
        $selectparent = $request->selectparent;

        if ($kodejenis != null || $selectparent != null) {
            $statusActive = 'notActive';
        } else {
            $statusActive = 'isActive';
        }
        
        if($kodejenis != null){    
            $jabat = DB::table('jabatan')->where('master.jabatan.kode', 'LIKE', '%'. $kodejenis. '%')
                                                    ->orWhere('level', 'LIKE', '%'. $kodejenis. '%')
                                                    ->orWhere('keterangan', 'LIKE', '%'. $kodejenis. '%')
                                                    ->paginate(10);
        }else if($selectparent != null){
            $jabat = DB::table('jabatan')->where('parent', '=', $selectparent)->paginate(10);
        }else if($kodejenis != null AND $selectparent != null){
            $jabat = DB::table('jabatan')->where('master.jabatan.kode', 'LIKE', '%'. $kodejenis. '%')
                                                    ->orWhere('level', 'LIKE', '%'. $kodejenis. '%')
                                                    ->orWhere('keterangan', 'LIKE', '%'. $kodejenis. '%')
                                                    ->andWhare('parent','=',$selectparent)
                                                    ->paginate(10);
        }else{            
            $jabat = DB::table('jabatan')->paginate(10);    
        }

        $atasan = Jabatan::All();

        return view ('Administrator.Data-master.jabatan.index',compact('jabat','atasan','kodejenis','selectparent','statusActive'));
    }

    public function export_excel(Request $request)
    {
        return Excel::download(new JabatanReport((object) $request),'Jabatan.xlsx');
    }

    public function create()
    {
        $jabat = Jabatan::get();
        return view ('Administrator.Data-master.jabatan.create',compact('jabat'));
    }

    public function store(Request $request)
    {
        $jabat = new Jabatan();
        $jabat->kode=$request->kode;
        $jabat->keterangan=$request->keterangan;
        $jabat->level=$request->level;
        $jabat->parent=$request->kode;
        
        if ($jabat == '0') {
            $jabat->parent=$request->parent;
        } else {
            $jabat->parent=$request->kode;
        }
        $jabat->save();

        return redirect ('Administrator/Data-master/jabatan');
    }

    public function edit($kode)
    {
        $jabat = Jabatan::where('kode',$kode)->first();
        $listjabatan = Jabatan::all();
        return view ('Administrator.Data-master.jabatan.edit',compact('jabat','listjabatan'));
    }

    public function update(Request $request)
    {
        $jabat = Jabatan::where('kode',$request->kode)->first();
        $jabat->keterangan = $request->keterangan;
        $jabat->level = $request->level;
        $jabat->parent = $request->parent;
        $jabat->update();

        return redirect ('/Administrator/Data-master/jabatan');
    }

    public function destroy($kode)
    {
        $jabat = Jabatan::where('kode',$kode)->first();
        $jabat->delete();

        return redirect ('/Administrator/Data-master/jabatan');
    }
}
