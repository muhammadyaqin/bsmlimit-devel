<?php

namespace App\Http\Controllers\Administrator\DataMaster;

use App\KategoriLimit;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exports\KategoriLimitExport;
use App\Exports\KategoriLimitReport;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class KategoriLimitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $kategorilimit = DB::table('kategorilimit')->paginate(10);

        $kodbatket = $request->kodbatket;
        if ($kodbatket != null) {
            $statusActive = 'notActive';
        } else {
            $statusActive = 'isActive';
        }

        if($kodbatket != null){
            $kategorilimit = DB::table('kategorilimit')->where('master.kategorilimit.kode', 'LIKE', '%'. $kodbatket. '%')
                                                    ->orWhere('batasatas', 'LIKE', '%'. $kodbatket. '%')
                                                    ->orWhere('batasbawah', 'LIKE', '%'.$kodbatket.'%')
                                                    ->orWhere('keterangan', 'LIKE', '%'.$kodbatket.'%')
                                                    ->paginate(10);
        }else{
            $kategorilimit = DB::table('kategorilimit')->paginate(10);    
        }
        return view ('Administrator.Data-master.kategorilimit.index',compact('kategorilimit','kodbatket','statusActive'));
    }

    public function export_excel(Request $request)
    {
        return Excel::download(new KategoriLimitReport((object) $request),'KategoriLimit.xlsx');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategorilimit = KategoriLimit::get();
        return view ('Administrator.Data-master.kategorilimit.create',compact('kategorilimit'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $kategorilimit = new KategoriLimit();
        $kategorilimit->kode=$request->kode;
        $kategorilimit->keterangan=$request->keterangan;
        $kategorilimit->batasatas=$request->batasatas;
        $kategorilimit->batasbawah=$request->batasbawah;
        $kategorilimit->save();

        return redirect ('/Administrator/Data-master/kategorilimit');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\KategoriLimit  $kategoriLimit
     * @return \Illuminate\Http\Response
     */
    public function show(KategoriLimit $kategoriLimit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\KategoriLimit  $kategoriLimit
     * @return \Illuminate\Http\Response
     */
    public function edit($kode)
    {
        $kategorilimit = KategoriLimit::where('kode',$kode)->first();
        return view ('Administrator.Data-master.kategorilimit.edit',compact('kategorilimit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\KategoriLimit  $kategoriLimit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $kategorilimit = KategoriLimit::where('kode',$request->kode)->first();
        $kategorilimit->keterangan = $request->keterangan;
        $kategorilimit->batasatas = $request->batasatas;
        $kategorilimit->batasbawah = $request->batasbawah;
        $kategorilimit->update();

        return redirect ('/Administrator/Data-master/kategorilimit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\KategoriLimit  $kategoriLimit
     * @return \Illuminate\Http\Response
     */
    public function destroy($kode)
    {
        $kategorilimit = KategoriLimit::where('kode',$kode)->first();
        $kategorilimit->delete();

        return redirect ('/Administrator/Data-master/kategorilimit');
    }
}
