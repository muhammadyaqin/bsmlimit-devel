<?php

namespace App\Http\Controllers\Administrator\DataMaster;

use App\ProdukPembiayaan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exports\ProdukPembiayaanExport;
use App\Exports\ProdukPembiayaanReport;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class ProdukPembiayaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $kodket = $request->kodket;
        if ($kodket != null) {
            $statusActive = 'notActive';
        } else {
            $statusActive = 'isActive';
        }

        if($kodket != null){
            $produkpembiayaan = DB::table('produkpembiayaan')->where('master.produkpembiayaan.kode', 'LIKE', '%'. $kodket. '%')
                                                    ->orWhere('keterangan', 'LIKE', '%'.$kodket.'%')
                                                    ->paginate(10);
        }else{
            $produkpembiayaan = DB::table('produkpembiayaan')->paginate(10);    
        }

        return view ('Administrator.Data-master.produkpembiayaan.index',compact('produkpembiayaan','kodket','produkpembiayaan','statusActive'));
    }

    public function export_excel(Request $request)
	{
		return Excel::download(new ProdukPembiayaanReport((object) $request), 'ProdukPembiayaanExport.xlsx');
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $produkpembiayaan = ProdukPembiayaan::get();

        return view ('Administrator.Data-master.produkpembiayaan.create',compact('produkpembiayaan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $produkpembiayaan = new ProdukPembiayaan();
        $produkpembiayaan->kode=$request->kode;
        $produkpembiayaan->keterangan=$request->keterangan;
        $produkpembiayaan->save();

        return redirect ('/Administrator/Data-master/produkpembiayaan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProdukPembiayaan  $produkPembiayaan
     * @return \Illuminate\Http\Response
     */
    public function show(ProdukPembiayaan $produkPembiayaan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProdukPembiayaan  $produkPembiayaan
     * @return \Illuminate\Http\Response
     */
    public function edit($kode)
    {
        $produkpembiayaan = ProdukPembiayaan::where('kode',$kode)->first();
        return view ('Administrator.Data-master.produkpembiayaan.edit',compact('produkpembiayaan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProdukPembiayaan  $produkPembiayaan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $produkpembiayaan = ProdukPembiayaan::where('kode',$request->kode)->first();
        $produkpembiayaan->keterangan = $request->keterangan;
        $produkpembiayaan->update();

        return redirect ('/Administrator/Data-master/produkpembiayaan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProdukPembiayaan  $produkPembiayaan
     * @return \Illuminate\Http\Response
     */
    public function destroy($kode)
    {
        $produkpembiayaan = ProdukPembiayaan::where('kode',$kode)->first();
        $produkpembiayaan->delete();
        return redirect ('/Administrator/Data-master/produkpembiayaan');
    }
}
