<?php

namespace App\Http\Controllers\Administrator\DataMaster;

use App\Region_dm;
use App\Area_dm;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exports\RegionExport;
use App\Exports\RegionReport;
use Maatwebsite\Excel\Facades\Excel;
use DB;


class Region_dmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $kodnam = $request->kodnam;
        if ($kodnam != null) {
            $statusActive = 'notActive';
        } else {
            $statusActive = 'isActive';
        }

        if($kodnam != null){
            $region = DB::table('region')->where('master.region.kode', 'LIKE', '%'. $kodnam. '%')
                                                    ->orWhere('nama', 'LIKE', '%'.$kodnam.'%')
                                                    ->paginate(10);
        }else{
            $region = DB::table('region')->paginate(10);    
        }

        return view ('Administrator.Data-master.region.index',compact('region','kodnam','statusActive'));
    }

    public function export_excel(Request $request)
    {
        return Excel::download(new RegionReport((object) $request),'Region.xlsx');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $region = Region_dm::get();
        return view ('Administrator.Data-master.region.create',compact('region'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $region = new Region_dm();
        $region->kode=$request->kode;
        $region->nama=$request->nama;
        $region->save();

        return redirect ('/Administrator/Data-master/region');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Region_dm  $region_dm
     * @return \Illuminate\Http\Response
     */
    public function show(Region_dm $region_dm)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Region_dm  $region_dm
     * @return \Illuminate\Http\Response
     */
    public function edit($kode)
    {
        $region = Region_dm::where('kode',$kode)->first();
        return view ('Administrator.Data-master.region.edit',compact('region'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Region_dm  $region_dm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $kode = $request->input('kode');
        $nama = $request->input('nama');

        DB::table('master.region')->where('kode', $request->kode)->update(['nama' => $nama]);

        return redirect ('/Administrator/Data-master/region');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Region_dm  $region_dm
     * @return \Illuminate\Http\Response
     */
    public function destroy($kode)
    {   
        DB::table('master.area')->where('koderegion', $kode)->update(['koderegion' => null]);
        $region = Region_dm::where('kode',$kode);
        $region->delete();
        
        return redirect ('/Administrator/Data-master/region');
    }
}
