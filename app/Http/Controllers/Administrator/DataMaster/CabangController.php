<?php

namespace App\Http\Controllers\Administrator\DataMaster;

use App\Cabang;
use App\Area_dm;
use App\Exports\CabangExport;
use App\Exports\CabangReport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class CabangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $kodnam = $request->kodnam;
        $selectarea = $request->selectarea;

        if ($kodnam != null || $selectarea != null) {
            $statusActive = 'notActive';
        } else {
            $statusActive = 'isActive';
        }
        
        if($kodnam != null){    
            $cabang = DB::table('cabang')->where('master.cabang.kode', 'LIKE', '%'. $kodnam. '%')
                                                    ->orWhere('nama', 'LIKE', '%'. $kodnam. '%')
                                                    ->paginate(10);
        }else if($selectarea != null){
            $cabang = DB::table('cabang')->where('kodearea', '=', $selectarea)->paginate(10);
        }else if($kodnam != null AND $selectarea != null){
            $cabang = DB::table('cabang')->where('master.cabang.kode', 'LIKE', '%'. $kodnam. '%')
                                                    ->orWhere('nama', 'LIKE', '%'. $kodnam. '%')
                                                    ->andWhare('kodearea','=',$selectarea)
                                                    ->paginate(10);
        }else{            
            $cabang = DB::table('cabang')->paginate(10);    
        }

        $area = Area_dm::all();
        return view ('Administrator.Data-master.unitkerja.index',compact('cabang','area','statusActive','selectarea','kodnam')); 
    }

    public function export_excel(Request $request)
    {
        return Excel::download(new CabangReport((object) $request),'Cabang_Unitkerja.xlsx');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cabang = Cabang::get();
        $area = Area_dm::get();
        return view ('Administrator.Data-master.unitkerja.create',compact('cabang','area'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cabang = new Cabang();
        $cabang->kode=$request->kode;
        $cabang->nama = $request->nama;
        $cabang->kodearea = $request->kodearea;
        $cabang->save();

        return redirect ('/Administrator/Data-master/unitkerja');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cabang  $cabang
     * @return \Illuminate\Http\Response
     */
    public function show(Cabang $cabang)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cabang  $cabang
     * @return \Illuminate\Http\Response
     */
    public function edit($kode)
    {
        $cabang = Cabang::where('kode',$kode)->first();
        $area = Area_dm::get();
        return view ('Administrator.Data-master.unitkerja.edit',compact('cabang','area'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cabang  $cabang
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $cabang = Cabang::where('kode',$request->kode)->first();
        $cabang->nama = $request->nama;
        $cabang->kodearea = $request->kodearea;
        $cabang->update();

        return redirect ('/Administrator/Data-master/unitkerja');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cabang  $cabang
     * @return \Illuminate\Http\Response
     */
    public function destroy($kode)
    {
        $cabang = Cabang::where('kode',$kode)->first();
        $cabang->delete();
        
        return redirect ('/Administrator/Data-master/unitkerja');
    }
}
