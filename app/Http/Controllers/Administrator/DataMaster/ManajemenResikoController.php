<?php

namespace App\Http\Controllers\Administrator\DataMaster;

// use App\MisPortfolio;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Imports\SMRiskImport;
use App\Exports\SMRiskReport;
use Maatwebsite\Excel\Facades\Excel;
use Session;

class ManajemenResikoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $name_nip = $request->name_nip;

        $smrisk = DB::table('staging.smrisk');
                // dd($refreshment);

        if($name_nip != null){
            $smrisk = $smrisk->where('staging.smrisk.nip', 'LIKE', '%'. $name_nip. '%')->orWhere('namapejabat', 'LIKE', '%'. $name_nip. '%');
        }
        // dd($refreshment);

        if ($name_nip != null) {
            $statusActive = 'notActive';
        } else {
            $statusActive = 'isActive';
        }

        $smrisk = $smrisk->select('staging.smrisk.*', 'staging.hcspejabat.namapejabat')
                    ->join('staging.hcspejabat', 'staging.smrisk.nip', '=', 'staging.hcspejabat.nip')
                    ->orderBy('id', 'asc')->paginate(10);

        $data['hcs'] = $smrisk;

        return view ('Administrator.StagingMIS.manajemenresiko.manajemenresiko', $data, compact('statusActive', 'name_nip', 'smrisk'));
    }

    public function export_excel(Request $request)
    {
        return (new SMRiskReport((object) $request))->download('SertifikasiManajemenResiko.xlsx');
    }

    public function detail($nip)
    {
        // dd(DB::table('staging.hcspejabat')->where('nip', $nip)->first());
        $smrisk = DB::table('staging.smrisk')->select('staging.smrisk.*', 'staging.hcspejabat.namapejabat AS namapejabat')
        ->join('staging.hcspejabat', 'staging.smrisk.nip', '=', 'staging.hcspejabat.nip')
        ->where('staging.smrisk.nip', $nip)->first();

        // dd($hcsriwayat);

        $data['smrisk'] = $smrisk;

        return view('Administrator.StagingMIS.manajemenresiko.detail', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MisPortfolio  $misPortfolio
     * @return \Illuminate\Http\Response
     */
    public function show(MisPortfolio $misPortfolio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MisPortfolio  $misPortfolio
     * @return \Illuminate\Http\Response
     */
    public function edit(MisPortfolio $misPortfolio)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MisPortfolio  $misPortfolio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MisPortfolio $misPortfolio)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MisPortfolio  $misPortfolio
     * @return \Illuminate\Http\Response
     */
    public function destroy(MisPortfolio $misPortfolio)
    {
        //
    }

    public function import_excel(Request $request) 
       {
           // validasi
           $this->validate($request, [
               'file' => 'required|mimes:csv,xls,xlsx'
           ]);
    
           // menangkap file excel
           $file = $request->file('file');
    
           // membuat nama file unik
           $nama_file = rand().$file->getClientOriginalName();
    
           // upload ke folder file_siswa di dalam folder public
           $file->move('file_smrisk',$nama_file);
    
           // import data
           Excel::import(new SMRiskImport, public_path('/file_smrisk/'.$nama_file));
    
           // notifikasi dengan session
           Session::flash('sukses','Data Sertifikasi Manajemen Resiko Berhasil Diimport!');
    
           // alihkan halaman kembali
           return redirect('/Administrator/Data-master/manajemenresiko');
        }
}
