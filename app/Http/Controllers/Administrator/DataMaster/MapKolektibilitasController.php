<?php

namespace App\Http\Controllers\Administrator\DataMaster;

use App\MapKolektibilitas;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exports\MapKolektibilitasExport;
use App\Exports\MapKolektibilitasReport;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Session;

class MapKolektibilitasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $kodeasal = $request->kodeasal;
        $kodebaru = $request->kodebaru;
        
        if ($kodeasal != null || $kodebaru != null) {
            $statusActive = 'notActive';
        } else {
            $statusActive = 'isActive';
        }

        if($kodeasal != null && $kodebaru == null){
            $mapkolektibilitas = DB::table('master.mapkolektibilitas')->where('master.mapkolektibilitas.kodeasal', 'LIKE', '%'. $kodeasal. '%')->paginate(10);
        }else if($kodeasal == null && $kodebaru != null){
            $mapkolektibilitas = DB::table('master.mapkolektibilitas')->where('master.mapkolektibilitas.kodebaru', 'LIKE', '%'. $kodebaru. '%')->paginate(10);
        }else if($kodebaru != null && $kodeasal != null){
            $mapkolektibilitas = DB::table('master.mapkolektibilitas')->where('master.mapkolektibilitas.kodeasal', 'LIKE', '%'. $kodeasal. '%')
                                                        ->orWhere('master.mapkolektibilitas.kodebaru', 'LIKE', '%'. $kodebaru. '%')
                                                        ->paginate(10);
        }else{
            $mapkolektibilitas = DB::table('master.mapkolektibilitas')->paginate(10);    
        }

        return view ('Administrator.Data-master.mapkolektibilitas.index',compact('mapkolektibilitas','kodeasal','kodebaru','statusActive'));
    }

    public function export_excel(Request $request)
    {
        return Excel::download(new MapKolektibilitasReport((object) $request), 'MapKolektibilitasExport.xlsx');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mapkolektibilitas = MapKolektibilitas::get();
        return view ('Administrator.Data-master.mapkolektibilitas.create',compact('mapkolektibilitas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $mapkolektibilitas = new MapKolektibilitas();
        $mapkolektibilitas->kodeasal=$request->kodeasal;
        $mapkolektibilitas->kodebaru=$request->kodebaru;
        $mapkolektibilitas->save();
        
        return redirect ('/Administrator/Data-master/mapkolektibilitas');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Enumerasi  $enumerasi
     * @return \Illuminate\Http\Response
     */
    public function show(Enumerasi $enumerasi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Enumerasi  $enumerasi
     * @return \Illuminate\Http\Response
     */
    public function edit($kodeasal)
    {
        //        
        $mapkolektibilitas = MapKolektibilitas::where('kodeasal',$kodeasal)->first();
        return view ('Administrator.Data-master.mapkolektibilitas.edit',compact('mapkolektibilitas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Enumerasi  $enumerasi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $mapkolektibilitas = DB::table('master.mapkolektibilitas')->where(['kodeasal'=>$request->kodeasal]);
        $mapkolektibilitas->update(['kodebaru'=>$request->kodebaru]);

        return redirect ('/Administrator/Data-master/mapkolektibilitas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Enumerasi  $enumerasi
     * @return \Illuminate\Http\Response
     */
    public function destroy($kode)
    {
        //
        DB::table('master.mapkolektibilitas')->where(['kodeasal'=>$kode])->delete();
        return redirect('/Administrator/Data-master/mapkolektibilitas');
    }
}
