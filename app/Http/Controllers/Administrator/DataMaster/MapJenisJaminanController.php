<?php

namespace App\Http\Controllers\Administrator\DataMaster;

use App\MapJenisJaminan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exports\MapJenisJaminanExport;
use App\Exports\MapJenisJaminanReport;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Session;

class MapJenisJaminanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $kodeasal = $request->kodeasal;
        $kodebaru = $request->kodebaru;
        
        if ($kodeasal != null || $kodebaru != null) {
            $statusActive = 'notActive';
        } else {
            $statusActive = 'isActive';
        }

        if($kodeasal != null && $kodebaru == null){
            $mapjenisjaminan = DB::table('master.mapjenisjaminan')->where('master.mapjenisjaminan.kodeasal', 'LIKE', '%'. $kodeasal. '%')->paginate(10);
        }else if($kodeasal == null && $kodebaru != null){
            $mapjenisjaminan = DB::table('master.mapjenisjaminan')->where('master.mapjenisjaminan.kodebaru', 'LIKE', '%'. $kodebaru. '%')->paginate(10);
        }else if($kodebaru != null && $kodeasal != null){
            $mapjenisjaminan = DB::table('master.mapjenisjaminan')->where('master.mapjenisjaminan.kodeasal', 'LIKE', '%'. $kodeasal. '%')
                                                        ->orWhere('master.mapjenisjaminan.kodebaru', 'LIKE', '%'. $kodebaru. '%')
                                                        ->paginate(10);
        }else{
            $mapjenisjaminan = DB::table('master.mapjenisjaminan')->paginate(10);    
        }

        return view ('Administrator.Data-master.mapjenisjaminan.index',compact('mapjenisjaminan','kodeasal','kodebaru','statusActive'));
    }

    public function export_excel(Request $request)
    {
        return Excel::download(new MapJenisJaminanReport((object) $request), 'MapJenisJaminanExport.xlsx');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mapjenisjaminan = MapJenisJaminan::get();
        return view ('Administrator.Data-master.mapjenisjaminan.create',compact('mapjenisjaminan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $mapjenisjaminan = new MapJenisJaminan();
        $mapjenisjaminan->kodeasal=$request->kodeasal;
        $mapjenisjaminan->kodebaru=$request->kodebaru;
        $mapjenisjaminan->save();
        
        return redirect ('/Administrator/Data-master/mapjenisjaminan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Enumerasi  $enumerasi
     * @return \Illuminate\Http\Response
     */
    public function show(Enumerasi $enumerasi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Enumerasi  $enumerasi
     * @return \Illuminate\Http\Response
     */
    public function edit($kodeasal)
    {
        //        
        $mapjenisjaminan = MapJenisJaminan::where('kodeasal',$kodeasal)->first();
        return view ('Administrator.Data-master.mapjenisjaminan.edit',compact('mapjenisjaminan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Enumerasi  $enumerasi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $mapjenisjaminan = DB::table('master.mapjenisjaminan')->where(['kodeasal'=>$request->kodeasal]);
        $mapjenisjaminan->update(['kodebaru'=>$request->kodebaru]);

        return redirect ('/Administrator/Data-master/mapjenisjaminan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Enumerasi  $enumerasi
     * @return \Illuminate\Http\Response
     */
    public function destroy($kode)
    {
        //
        DB::table('master.mapjenisjaminan')->where(['kodeasal'=>$kode])->delete();
        return redirect('/Administrator/Data-master/mapjenisjaminan');
    }
}
