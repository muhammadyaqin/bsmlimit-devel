<?php

namespace App\Http\Controllers\Administrator\DataMaster;

use App\Jabatan_pembiayaan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exports\Jabatan_pembiayaanExport;
use App\Exports\Jabatan_pembiayaanReport;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class JabatanPembiayaanController extends Controller
{
    public function index(Request $request)
    {
    	$jabatanpembiayaan = Jabatan_pembiayaan::paginate(10);
        $nama = $request->nama;
        if ($nama != null) {
            $statusActive = 'notActive';
        } else {
            $statusActive = 'isActive';
        }

        if($nama != null){
            $jabatanpembiayaan = DB::table('jabatanpembiayaan')->where('master.jabatanpembiayaan.nama', 'LIKE', '%'. $nama. '%')
                                                    ->paginate(10);
        }else{
            $jabatanpembiayaan = DB::table('jabatanpembiayaan')->paginate(10);    
        }

        return view ('Administrator.Data-master.jabatanpembiayaan.index',compact('jabatanpembiayaan','nama','statusActive'));
    }

    public function export_excel(Request $request)
    {
        return Excel::download(new Jabatan_pembiayaanReport((object) $request), 'Jabatan_pembiayaanExport.xlsx');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jabatanpembiayaan = Jabatan_pembiayaan::get();
        return view ('Administrator.Data-master.jabatanpembiayaan.create',compact('jabatanpembiayaan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $jabatanpembiayaan = new Jabatan_pembiayaan();
        $jabatanpembiayaan->nama=$request->nama;
        $jabatanpembiayaan->save();
        
        return redirect ('/Administrator/Data-master/jabatanpembiayaan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Enumerasi  $enumerasi
     * @return \Illuminate\Http\Response
     */
    public function show(Enumerasi $enumerasi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Enumerasi  $enumerasi
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //        
        $jabatanpembiayaan = Jabatan_pembiayaan::where('id',$id)->first();
        return view ('Administrator.Data-master.jabatanpembiayaan.edit',compact('jabatanpembiayaan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Enumerasi  $enumerasi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $jabatanpembiayaan = DB::table('jabatanpembiayaan')->where('id',$request->id);
        $jabatanpembiayaan->update(['nama'=>$request->nama]);

        return redirect ('/Administrator/Data-master/jabatanpembiayaan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Enumerasi  $enumerasi
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        DB::table('jabatanpembiayaan')->where('id',$id)->delete();
        return redirect('/Administrator/Data-master/jabatanpembiayaan');
    }
}
