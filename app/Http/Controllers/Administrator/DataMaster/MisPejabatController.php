<?php

namespace App\Http\Controllers\Administrator\DataMaster;

// use App\MisPortfolio;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\HCSPejabat;
use DB;
use App\Imports\HCSPejabatImport;
use App\Exports\HCSPejabatReport;
use Maatwebsite\Excel\Facades\Excel;
use Session;
use Illuminate\Support\Arr;


class MisPejabatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $name_nip = $request->name_nip;

        $hcspejabat = DB::table('staging.hcspejabat')
        ->orderBy('staging.hcspejabat.tglbergabung','desc');
                // dd($refreshment);

        if($name_nip != null){
            $hcspejabat = $hcspejabat->where('staging.hcspejabat.nip', 'LIKE', '%'. $name_nip. '%')->orWhere('namapejabat', 'LIKE', '%'. $name_nip. '%');
        }
        // dd($refreshment);

        if ($name_nip != null) {
            $statusActive = 'notActive';
        } else {
            $statusActive = 'isActive';
        }

        $hcspejabat = $hcspejabat->select('staging.hcspejabat.*')->paginate(10);

        $data['hcs'] = $hcspejabat;

        return view ('Administrator.StagingMIS.pejabat.pejabat', $data, compact('statusActive', 'name_nip', 'hcspejabat'));
    }
    public function export_excel(Request $request) {
        return (new HCSPejabatReport((object) $request))->download('HCSPejabat.xlsx');
    }
    public function detail($nip)
    {
        // dd(DB::table('staging.hcspejabat')->where('nip', $nip)->first());
        // $pejabat = DB::table('staging.hcspejabat')->select('staging.hcspejabat.*', 'master.cabang.nama AS namacabang','master.enumerasi.keterangan AS keteranganappraisal','master.enumerasi.keterangan AS keteranganappraisal')
        // ->join('master.cabang', 'staging.hcspejabat.keterangancabang', '=', 'master.cabang.kode')
        // ->join('master.enumerasi',DB::raw('staging.hcspejabat.appraisal::text'),'=','master.enumerasi.kode')
        // ->where('master.enumerasi.kategori', '=', 'APPRAISAL')
        // ->where('nip', $nip)->first();

        $pejabat = DB::select(
            DB::raw(
                "SELECT hcspjbt.*,
                cb.nama AS namacabang,
                en1.keterangan AS keteranganappraisal,
                en2.keterangan AS keterangansanksi,
                en3.keterangan AS keteranganfraud
                FROM staging.hcspejabat hcspjbt
                LEFT JOIN master.cabang cb ON cb.kode = hcspjbt.keterangancabang 
                LEFT JOIN master.enumerasi en1 ON en1.kode = hcspjbt.appraisal
                LEFT JOIN master.enumerasi en2 ON en2.kode = hcspjbt.sanksi
                LEFT JOIN master.enumerasi en3 ON en3.kode = hcspjbt.fraud
                WHERE
                (hcspjbt.nip LIKE '%$nip%')
                AND en1.kategori = 'APPRAISAL'
                AND en2.kategori = 'SANKSI'
                AND en3.kategori = 'FRAUD'
                "
            )
        );
     // dd($pejabat);
        $data['pejabat'] = $pejabat;

        return view('Administrator.StagingMIS.pejabat.detail', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MisPortfolio  $misPortfolio
     * @return \Illuminate\Http\Response
     */
    public function show(MisPortfolio $misPortfolio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MisPortfolio  $misPortfolio
     * @return \Illuminate\Http\Response
     */
    public function edit(MisPortfolio $misPortfolio)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MisPortfolio  $misPortfolio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MisPortfolio $misPortfolio)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MisPortfolio  $misPortfolio
     * @return \Illuminate\Http\Response
     */
    public function destroy(MisPortfolio $misPortfolio)
    {
        //
    }
     public function import_excel(Request $request) 
        {
            // // validasi
            // $this->validate($request, [
            //     'file' => 'required|mimes:csv,xls,xlsx'
            // ]);
     
            // // menangkap file excel
            // $file = $request->file('file');
     
            // // membuat nama file unik
            // $nama_file = rand().$file->getClientOriginalName();
     
            // // upload ke folder file_siswa di dalam folder public
            // $file->move('file_hcspejabat',$nama_file);
     
            // import data
            $sheet = Excel::toArray(new HCSPejabatImport, $request->file('file'));

            foreach ($sheet[0] as $key => $value) {
                $bariske = $key;
                $barisdata = $value;
                if ($barisdata['nip'] == null || $barisdata['email']  == null || $barisdata['kodejabatan'] == null || $barisdata['kodecabang'] == null) continue;
                $cekcabang = DB::table('master.cabang')->where('kode', $barisdata['kodecabang'])->first();
                if ($cekcabang == null) continue; 
                // dd($barisdata['email']);

                $datahcs = DB::table('staging.hcspejabat')->where('nip', $barisdata['nip'])->get();
                // $datahcs = DB::table('staging.hcspejabat')
                // ->select('staging.hcspejabat.nip','staging.hcspejabat.kodejabatan','staging.hcspejabat.kodecabang', 'master.jabatan.areajabatan', 'master.userapp.email')
                // ->leftjoin('master.jabatan', 'master.jabatan.kode', '=', 'staging.hcspejabat.kodejabatan')
                // ->leftjoin('master.userapp', 'master.userapp.email', '=', DB::raw("UPPER(staging.hcspejabat.email)"))
                // ->where('staging.hcspejabat.nip', $barisdata['nip'])->first();
                // // dd($datahcs->areajabatan);
                // if ($datahcs->areajabatan == 'C') {
                //     $role = 'CABANG';
                // }
                // else if ($datahcs->areajabatan == 'A') {
                //     $role = 'CABANG';
                // }
                // else if ($datahcs->areajabatan == 'R') {
                //     $role = 'REGION';
                // }
                // else if($datahcs->areajabatan == 'P'){
                //     $role = 'PUSAT';
                // }
                // else {
                //     $role = 'ERM';
                // }


                // if ($datahcs->email == null) {
                //     DB::insert('insert into master.userapp (email, statusaktif) values (?, ?)', [
                //         strtoupper($barisdata['email']),
                //         'T'
                //     ]);

                //     DB::insert('insert into master.userrole (email, role) values (?, ?)', [
                //         strtoupper($barisdata['email']),
                //         $role
                //     ]);
                // }
                // $datauserapp = DB::table('master.userapp')->where('email', strtoupper($barisdata['email']))->get();
                // $datauserrole = DB::table('master.userrole')->where('email', strtoupper($barisdata['email']))->get();
                // dd($datahcs);
                //$datahcs = DB::table('staging.hcspejabat')->where('nip', '0000')->get();
                if ($datahcs != null) {
                    DB::update('UPDATE staging.hcspejabat SET nip = ?, namapejabat = ?, tglbergabung = ?, statuskaryawan = ?, kodejabatanhcs = ?, kodecabang = ?, appraisal = ?, sanksi = ?, fraud = ?, email = ? WHERE nip = ?', [
                        $barisdata['nip'],
                        $barisdata['namapejabat'],
                        $barisdata['tglbergabung'],
                        $barisdata['statuskaryawan'],
                        $barisdata['kodejabatan'],
                        $barisdata['kodecabang'],
                        $barisdata['appraisal'],
                        $barisdata['sanksi'],
                        $barisdata['fraud'],
                        $barisdata['email'],
                        $barisdata['nip']
                    ]);
                }
                else {

                    DB::insert('insert into staging.hcspejabat (nip, namapejabat, tglbergabung, statuskaryawan, kodejabatanhcs, kodecabang, appraisal, sanksi, fraud, email) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [
                        $barisdata['nip'],
                        $barisdata['namapejabat'],
                        $barisdata['tglbergabung'],
                        $barisdata['statuskaryawan'],
                        $barisdata['kodejabatan'],
                        $barisdata['kodecabang'],
                        $barisdata['appraisal'],
                        $barisdata['sanksi'],
                        $barisdata['fraud'],
                        $barisdata['email'],
                    ]);
                }
            }

            DB::update('UPDATE staging.hcspejabat hp
                        SET kodejabatan = m.kodejabatanpwmp
                        FROM master.mapjabatanhcs m
                        WHERE m.kodejabatanhcs = hp.kodejabatanhcs');
            DB::insert("INSERT INTO master.userapp (email, created_at, statusaktif)
                        SELECT UPPER(h.email) AS email, CURRENT_TIMESTAMP AS created_at, 'T' AS status_aktif
                        FROM staging.hcspejabat h
                        WHERE NOT EXISTS (
                            SELECT 1 FROM master.userapp u
                            WHERE UPPER(u.email) = UPPER(h.email)
                        )");
            DB::insert("INSERT INTO master.userrole (email, created_at, role)
                        SELECT UPPER(h.email) AS email, CURRENT_TIMESTAMP AS created_at, 
                        CASE j.areajabatan
                            WHEN 'C' THEN 'CABANG'
                            WHEN 'A' THEN 'CABANG'
                            WHEN 'R' THEN 'REGION'
                            WHEN 'P' THEN 'PUSAT'
                            ELSE 'ERM'
                        END AS role
                        FROM staging.hcspejabat h
                        INNER JOIN master.jabatan j ON j.kode = h.kodejabatan
                        WHERE NOT EXISTS (
                            SELECT 1 FROM master.userrole u
                            WHERE UPPER(u.email) = UPPER(h.email)
                        )");

   
            // notifikasi dengan session
            Session::flash('sukses','Data HCS Pejabat Berhasil Diimport!');
     
            // alihkan halaman kembali
            return redirect('/Administrator/Data-master/pejabat');
        }

}
