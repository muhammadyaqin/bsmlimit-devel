<?php

namespace App\Http\Controllers\Administrator\DataMaster;

use App\MapSegmen;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exports\MapSegmenExport;
use App\Exports\MapSegmenReport;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Session;

class MapSegmenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $kodeasal = $request->kodeasal;
        $kodebaru = $request->kodebaru;
        
        if ($kodeasal != null || $kodebaru != null) {
            $statusActive = 'notActive';
        } else {
            $statusActive = 'isActive';
        }

        if($kodeasal != null && $kodebaru == null){
            $mapsegmen = DB::table('master.mapsegmen')->where('master.mapsegmen.kodeasal', 'LIKE', '%'. $kodeasal. '%')->paginate(10);
        }else if($kodeasal == null && $kodebaru != null){
            $mapsegmen = DB::table('master.mapsegmen')->where('master.mapsegmen.kodebaru', 'LIKE', '%'. $kodebaru. '%')->paginate(10);
        }else if($kodebaru != null && $kodeasal != null){
            $mapsegmen = DB::table('master.mapsegmen')->where('master.mapsegmen.kodeasal', 'LIKE', '%'. $kodeasal. '%')
                                                        ->orWhere('master.mapsegmen.kodebaru', 'LIKE', '%'. $kodebaru. '%')
                                                        ->paginate(10);
        }else{
            $mapsegmen = DB::table('master.mapsegmen')->paginate(10);    
        }

        return view ('Administrator.Data-master.mapsegmen.index',compact('mapsegmen','kodeasal','kodebaru','statusActive'));
    }

    public function export_excel(Request $request)
    {
        return Excel::download(new MapSegmenReport((object) $request), 'MapSegmenExport.xlsx');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mapsegmen = MapSegmen::get();
        return view ('Administrator.Data-master.mapsegmen.create',compact('mapsegmen'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $mapsegmen = new MapSegmen();
        $mapsegmen->kodeasal=$request->kodeasal;
        $mapsegmen->kodebaru=$request->kodebaru;
        $mapsegmen->save();
        
        return redirect ('/Administrator/Data-master/mapsegmen');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Enumerasi  $enumerasi
     * @return \Illuminate\Http\Response
     */
    public function show(Enumerasi $enumerasi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Enumerasi  $enumerasi
     * @return \Illuminate\Http\Response
     */
    public function edit($kodeasal)
    {
        //        
        $mapsegmen = MapSegmen::where('kodeasal',$kodeasal)->first();
        return view ('Administrator.Data-master.mapsegmen.edit',compact('mapsegmen'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Enumerasi  $enumerasi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $mapsegmen = DB::table('master.mapsegmen')->where(['kodeasal'=>$request->kodeasal]);
        $mapsegmen->update(['kodebaru'=>$request->kodebaru]);

        return redirect ('/Administrator/Data-master/mapsegmen');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Enumerasi  $enumerasi
     * @return \Illuminate\Http\Response
     */
    public function destroy($kode)
    {
        //
        DB::table('master.mapsegmen')->where(['kodeasal'=>$kode])->delete();
        return redirect('/Administrator/Data-master/mapsegmen');
    }
}
