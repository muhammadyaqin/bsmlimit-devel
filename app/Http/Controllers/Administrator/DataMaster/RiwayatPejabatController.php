<?php

namespace App\Http\Controllers\Administrator\DataMaster;

// use App\MisPortfolio;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Imports\HCSRiwayatPejabatImport;
use App\Exports\HCSRiwayatPejabatReport;
use Maatwebsite\Excel\Facades\Excel;
use Session;

class RiwayatPejabatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $name_nip = $request->name_nip;

        $hcsriwayat = DB::table('staging.hcsriwayatjabatan');
                // dd($refreshment);

        if($name_nip != null){
            $hcsriwayat = $hcsriwayat->where('staging.hcsriwayatjabatan.nip', 'LIKE', '%'. $name_nip. '%')->orWhere('namapejabat', 'LIKE', '%'. $name_nip. '%');;
        }
        // dd($refreshment);

        if ($name_nip != null) {
            $statusActive = 'notActive';
        } else {
            $statusActive = 'isActive';
        }

        $hcsriwayat = $hcsriwayat->select('staging.hcsriwayatjabatan.*', 'staging.hcspejabat.namapejabat')
                    ->join('staging.hcspejabat', 'staging.hcsriwayatjabatan.nip', '=', 'staging.hcspejabat.nip')
                    ->orderBy('id', 'asc')->paginate(10);

        $data['hcsriwayat'] = $hcsriwayat;

        return view ('Administrator.StagingMIS.riwayatpejabat.riwayatpejabat',compact('statusActive', 'name_nip', 'hcsriwayat'));
    }

    public function export_excel(Request $request)
    {
        return (new HCSRiwayatPejabatReport((object) $request))->download('HCSRiwayatPejabat.xlsx');
    }
    
    public function detail($nip)
    {
        // dd(DB::table('staging.hcspejabat')->where('nip', $nip)->first());
        $hcsriwayat = DB::table('staging.hcsriwayatjabatan')->select('staging.hcsriwayatjabatan.*', 'staging.hcspejabat.namapejabat AS namapejabat')
        ->join('staging.hcspejabat', 'staging.hcsriwayatjabatan.nip', '=', 'staging.hcspejabat.nip')
        ->where('staging.hcsriwayatjabatan.nip', $nip)->first();

        // dd($hcsriwayat);

        $data['hcsriwayat'] = $hcsriwayat;

        return view('Administrator.StagingMIS.riwayatpejabat.detail', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MisPortfolio  $misPortfolio
     * @return \Illuminate\Http\Response
     */
    public function show(MisPortfolio $misPortfolio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MisPortfolio  $misPortfolio
     * @return \Illuminate\Http\Response
     */
    public function edit(MisPortfolio $misPortfolio)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MisPortfolio  $misPortfolio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MisPortfolio $misPortfolio)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MisPortfolio  $misPortfolio
     * @return \Illuminate\Http\Response
     */
    public function destroy(MisPortfolio $misPortfolio)
    {
        //
    }

    public function import_excel(Request $request) 
        {
            // // validasi
            // $this->validate($request, [
            //     'file' => 'required|mimes:csv,xls,xlsx'
            // ]);
     
            // // menangkap file excel
            // $file = $request->file('file');
     
            // // membuat nama file unik
            // $nama_file = rand().$file->getClientOriginalName();
     
            // // upload ke folder file_siswa di dalam folder public
            // $file->move('file_hcsriwayatpejabat',$nama_file);
     
            // // import data
            // Excel::import(new HCSRiwayatPejabatImport, public_path('/file_hcsriwayatpejabat/'.$nama_file));

            $sqlvalues = "";
            $sheet = Excel::toArray(new HCSRiwayatPejabatImport, $request->file('file'));
            foreach ($sheet[0] as $key => $value) {
                $bariske = $key;
                $barisdata = $value;
            if ($barisdata['nip'] == null || $barisdata['kodejabatan'] == null || $barisdata['kodecabang'] == null || $barisdata['tglmulai'] == null || $barisdata['tglberakhir'] == null) continue;
            // DB::insert('insert into temp.uploadhcsriwayatjabatan (nip, kodejabatan, kodecabang, tglmulai, tglberakhir) values (?, ?, ?, ?, ?)',[
            //         $barisdata['nip'],
            //         $barisdata['kodejabatan'],
            //         $barisdata['kodecabang'],
            //         $barisdata['tglmulai'],
            //         $barisdata['tglberakhir'],
            //     ]);
            $sqlvalues .= sprintf("('%s', '%s', '%s', '%s', '%s'), ", $barisdata['nip'], $barisdata['kodejabatan'], $barisdata['kodecabang'], $barisdata['tglmulai'], $barisdata['tglberakhir']);
            }

            $sqlinsertmulti = "INSERT INTO temp.uploadhcsriwayatjabatan (nip, kodejabatan, kodecabang, tglmulai, tglberakhir) VALUES " . $sqlvalues;
            // $sqlinsertmulti //trim
            $sqltrim = rtrim($sqlinsertmulti, ", ");
            // dd($sqltrim);
            DB::insert($sqltrim);

            DB::insert('insert into staging.hcsriwayatjabatan (nip, kodejabatan, kodecabang, tglmulai, tglberakhir)
                        select nip, kodejabatan, kodecabang, tglmulai, tglberakhir 
                        from temp.uploadhcsriwayatjabatan h
                        where exists (
                            select 1 from staging.hcspejabat x
                            where x.nip = h.nip
                        )
                        and exists (
                            select 1 from master.jabatan x
                            where x.kode = h.kodejabatan
                        )
                        and exists (
                            select 1 from master.cabang x
                            where x.kode = h.kodecabang
                        )
                        ');
     
            // notifikasi dengan session
            Session::flash('sukses','Data HCS Riwayat Pejabat Berhasil Diimport!');
     
            // alihkan halaman kembali
            return redirect('/Administrator/Data-master/riwayatjabatan');
        }
}
