<?php

namespace App\Http\Controllers\Administrator\DataMaster;

use App\MisJaminan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\MisJaminanExport;
use App\Exports\MisJaminanReport;
use DB;

class MisJaminanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $nomorloan = $request->nomorloan;
        if ($nomorloan != null) {
            $statusActive = 'notActive';
        } else {
            $statusActive = 'isActive';
        }

        if($nomorloan != null){
            $misjaminan = DB::table('staging.misjaminan')->where('staging.misjaminan.noloan', 'LIKE', '%'. $nomorloan. '%')
                                                    ->paginate(10);
        }else{
            $misjaminan = DB::table('staging.misjaminan')->paginate(10);    
        }
        return view ('Administrator.StagingMIS.jaminan.mis_jaminan',compact('misjaminan','misjaminan','statusActive','nomorloan'));
    }

    public function export_excel(Request $request)
    {
        return Excel::download(new MisJaminanReport((object) $request),'MisJaminanExport.xlsx');
    }

    public function import_excel(Request $request) 
    {
       // validasi
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);
 
        // menangkap file excel
        $file = $request->file('file');
 
        // membuat nama file unik
        $nama_file = rand().$file->getClientOriginalName();
 
        // upload ke folder file_siswa di dalam folder public
        $file->move('file_misjaminan',$nama_file);
 
        // import data
        Excel::import(new PengalamanPembiayaan, public_path('/file_misjaminan/'.$nama_file));
 
        // notifikasi dengan session
        Session::flash('sukses','Data Jaminan Berhasil Diimport!');
 
        // alihkan halaman kembali
        return redirect('/Administrator/Data-master/jaminan');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MisPortfolio  $misPortfolio
     * @return \Illuminate\Http\Response
     */
    public function show(MisPortfolio $misPortfolio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MisPortfolio  $misPortfolio
     * @return \Illuminate\Http\Response
     */
    public function edit(MisPortfolio $misPortfolio)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MisPortfolio  $misPortfolio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MisPortfolio $misPortfolio)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MisPortfolio  $misPortfolio
     * @return \Illuminate\Http\Response
     */
    public function destroy(MisPortfolio $misPortfolio)
    {
        //
    }
}
