<?php

namespace App\Http\Controllers\Administrator\DataMaster;

use App\ProdukLimitOtomatis;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exports\ProdukLimitOtomatisExport;
use App\Exports\ProdukLimitOtomatisReport;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class ProdukLimitOtomatisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $kodket = $request->kodket;
        if ($kodket != null) {
            $statusActive = 'notActive';
        } else {
            $statusActive = 'isActive';
        }

        if($kodket != null){
            $produklimitotomatis = DB::table('produklimitotomatis')->where('master.produklimitotomatis.kode', 'LIKE', '%'. $kodket. '%')
                                                    ->orWhere('keterangan', 'LIKE', '%'.$kodket.'%')
                                                    ->paginate(10);
        }else{
            $produklimitotomatis = DB::table('produklimitotomatis')->paginate(10);    
        }

        return view ('Administrator.Data-master.produklimitotomatis.index',compact('produklimitotomatis','kodket','statusActive'));
    }

    public function export_excel(Request $request)
    {
        return Excel::download(new ProdukLimitOtomatisReport((object) $request) ,'ProdukLimitOtomatis.xlsx');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $produklimitotomatis = ProdukLimitOtomatis::get();
        return view ('Administrator.Data-master.produklimitotomatis.create',compact('produklimitotomatis'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $produklimitotomatis = new ProdukLimitOtomatis();
        $produklimitotomatis->kode=$request->kode;
        $produklimitotomatis->keterangan=$request->keterangan;
        $produklimitotomatis->save();

        return redirect ('/Administrator/Data-master/produklimitotomatis');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProdukLimitOtomatis  $produkLimitOtomatis
     * @return \Illuminate\Http\Response
     */
    public function show(ProdukLimitOtomatis $produkLimitOtomatis)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProdukLimitOtomatis  $produkLimitOtomatis
     * @return \Illuminate\Http\Response
     */
    public function edit($kode)
    {
        $produklimitotomatis = ProdukLimitOtomatis::where('kode',$kode)->first();
        return view ('Administrator.Data-master.produklimitotomatis.edit',compact('produklimitotomatis'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProdukLimitOtomatis  $produkLimitOtomatis
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $produklimitotomatis = ProdukLimitOtomatis::where('kode',$request->kode)->first();
        $produklimitotomatis->keterangan = $request->keterangan;
        $produklimitotomatis->update();

        return redirect ('/Administrator/Data-master/produklimitotomatis');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProdukLimitOtomatis  $produkLimitOtomatis
     * @return \Illuminate\Http\Response
     */
    public function destroy($kode)
    {
        $produklimitotomatis = ProdukLimitOtomatis::where('kode',$kode)->first();
        $produklimitotomatis->delete();
        
        return redirect ('/Administrator/Data-master/produklimitotomatis');
    }
}
