<?php

namespace App\Http\Controllers\Administrator\DataMaster;

use App\Area_dm;
use App\Region_dm;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exports\AreaExport;
use App\Exports\AreaReport;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class Area_dmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $kodnam = $request->kodnam;
        $selectregion = $request->selectregion;

        if ($kodnam != null || $selectregion != null) {
            $statusActive = 'notActive';
        } else {
            $statusActive = 'isActive';
        }
        
        if($kodnam != null){    
            $area = DB::table('area')->where('master.area.kode', 'LIKE', '%'. $kodnam. '%')
                                                    ->orWhere('nama', 'LIKE', '%'. $kodnam. '%')
                                                    ->paginate(10);
        }else if($selectregion != null){
            $area = DB::table('area')->where('koderegion', '=', $selectregion)->paginate(10);
        }else if($kodnam != null AND $selectregion != null){
            $area = DB::table('area')->where('master.area.kode', 'LIKE', '%'. $kodnam. '%')
                                                    ->orWhere('nama', 'LIKE', '%'. $kodnam. '%')
                                                    ->andWhare('koderegion','=',$selectregion)
                                                    ->paginate(10);
        }else{            
            $area = DB::table('area')->paginate(10);    
        }

        $region = Region_dm::all();
        // dd($opsi);
        return view ('Administrator.Data-master.area.index',compact('area','region','kodnam','selectregion','statusActive'));
    }

    public function export_excel(Request $request)
    {
        return Excel::download(new AreaReport((object) $request),'Area.xlsx');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $area = Area_dm::get();
        $region = Region_dm::get();
        return view ('Administrator.Data-master.area.create',compact('area','region'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $area = new Area_dm();
        $area->kode=$request->kode;
        $area->nama=$request->nama;
        $area->koderegion=$request->koderegion;
        $area->save();

        return redirect ('/Administrator/Data-master/area');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Area_dm  $area_dm
     * @return \Illuminate\Http\Response
     */
    public function show(Area_dm $area_dm)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Area_dm  $area_dm
     * @return \Illuminate\Http\Response
     */
    public function edit($kode)
    {
        $area = Area_dm::where('kode',$kode)->first();
        $region = Region_dm::get();
        return view ('Administrator.Data-master.area.edit',compact('area','region'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Area_dm  $area_dm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $area = Area_dm::where('kode',$request->kode)->first();
        $area->nama = $request->nama;
        $area->kode = $request->kode;
        $area->koderegion = $request->koderegion;
        $area->update();

        return redirect ('/Administrator/Data-master/area');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Area_dm  $area_dm
     * @return \Illuminate\Http\Response
     */
    public function destroy($kode)
    {
        DB::table('master.cabang')->where('kodearea', $kode)->update(['kodearea' => null]);
        $area = Area_dm::where('kode',$kode)->delete();
        
        return redirect ('/Administrator/Data-master/area');
    }
}
