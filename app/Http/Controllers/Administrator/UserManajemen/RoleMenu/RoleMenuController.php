<?php

namespace App\Http\Controllers\Administrator\UserManajemen\RoleMenu;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

use App\UserApp;
use App\Role;
use App\UserRole;

class RoleMenuController extends Controller
{
    public function index()
    {
        $userole = DB::table('master.userrole')->paginate(10);
        // $role = DB::table('master.userapp')->get();
        return view ('Administrator.User_Manajemen.Role_Menu.index',compact('userole'));
    }
}
