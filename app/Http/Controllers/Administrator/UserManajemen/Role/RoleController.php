<?php

namespace App\Http\Controllers\Administrator\UserManajemen\Role;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Role;
use DB;
use Schema;

class RoleController extends Controller
{
    public function index()
    {
        $role = DB::table('master.role')->paginate(10);
        return view ('Administrator.User_Manajemen.Role.index',compact('role'));
    }

    public function create()
    {
        return view ('Administrator.User_Manajemen.Role.create');
    }
	
	public function store(Request $request)
    {
        $role = DB::table('master.role')->insert(
        		[
        			'kode' => $request->kode,
        			'keterangan' => $request->keterangan
        		]
        	);
        
        return redirect ('/user_manajemen/role/index');
    }

    public function edit($kode)
    {
        $role = DB::table('master.role')->where('kode', $kode)->first();
        $data['role'] = $role;

        return view('Administrator.User_Manajemen.Role.edit', $data);
    }

    public function update(Request $request)
    {
        DB::table('master.role')->where('kode', $request->kode)->update([
            'keterangan' => $request->keterangan,
        ]);

        return redirect('/user_manajemen/role/index');
    }

    public function destroy($kode)
    {   
        $userrole = DB::table('master.userrole')->where('role',$kode)->get();
        if($userrole != null){

            $userapp = null;
            foreach ($userrole as $key => $ur) {//cari user yg punya email sama dengan di master.userrole
               $userapp[] = DB::table('master.userapp')->where('email', $ur->email)->get();
            }

            if($userapp != null){

                foreach ($userapp as $key => $ua) {//hapus data user
                    $dataUserapp[] = DB::table('master.userapp as child')
                        ->join('master.userrole as parent', 'child.email', '=', 'parent.email')
                        ->where('parent.email',$ua[0]->email)
                        ->get();
                }
                // echo "<pre>";
                // print_r($dataUserapp);
                // echo "</pre>";
            }

            //hapus data userrole
            DB::table('master.userrole')->where('role',$kode)->delete();   
        }

        //hapus data role
        DB::table('master.role')->where('kode', $kode)->delete();
        return redirect('/user_manajemen/role/index');
    }
}
