<?php

namespace App\Http\Controllers\Administrator\UserManajemen\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
   public function index()
   {
       $user = User::all();
       return view ('Administrator.User_Manajemen.User.index',compact('user'));
   }
}
