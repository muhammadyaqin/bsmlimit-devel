<?php

namespace App\Http\Controllers\Administrator\Beranda;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BerandaController extends Controller
{
    public function index()
    {
        return view ("Administrator.Beranda.home.index");
    }
}
