<?php

namespace App\Http\Controllers\Administrator\Staging;

use App\PelatihanPembiayaan;
use App\HCSPejabat;
use App\Exports\PelatihanPembiayaanExport;
use App\Exports\PelatihanPembiayaanReport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class PelatihanPembiayaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $nip = $request->nip;
        $namatraining = $request->namatraining;
        
        if ($nip != null || $namatraining != null) {
            $statusActive = 'notActive';
        } else {
            $statusActive = 'isActive';
        }

        if($nip != null && $namatraining == null){
            $pelatihanpembiayaan = DB::table('staging.msutrainingpby')->where('staging.msutrainingpby.nip', 'LIKE', '%'. $nip. '%')->paginate(10);
        }else if($nip == null && $namatraining != null){
            $pelatihanpembiayaan = DB::table('staging.msutrainingpby')->where('staging.msutrainingpby.namatraining', 'LIKE', '%'. $namatraining. '%')->paginate(10);
        }else if($namatraining != null && $nip != null){
            $pelatihanpembiayaan = DB::table('staging.msutrainingpby')->where('staging.msutrainingpby.nip', 'LIKE', '%'. $nip. '%')
                                                        ->orWhere('staging.msutrainingpby.namatraining', 'LIKE', '%'. $namatraining. '%')
                                                        ->paginate(10);
        }else{
            $pelatihanpembiayaan = DB::table('staging.msutrainingpby')->paginate(10);    
        }
        return view ('Administrator.StagingMIS.pelatihanpembiayaan.pelatihanpembiayaan',compact('pelatihanpembiayaan','nip','namatraining','statusActive'));
    }

    public function export_excel(Request $request)
    {
        return Excel::download(new PelatihanPembiayaanReport((object) $request),'PelatihanPembiayaanExport.xlsx');
    }

    public function import_excel(Request $request) 
    {
       // validasi
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);
 
        // menangkap file excel
        $file = $request->file('file');
 
        // membuat nama file unik
        $nama_file = rand().$file->getClientOriginalName();
 
        // upload ke folder file_siswa di dalam folder public
        $file->move('file_pelatihanpembiayaan',$nama_file);
 
        // import data
        Excel::import(new PengalamanPembiayaan, public_path('/file_pelatihanpembiayaan/'.$nama_file));
 
        // notifikasi dengan session
        Session::flash('sukses','Data Pelatihan Pembiayaan Berhasil Diimport!');
 
        // alihkan halaman kembali
        return redirect('/Administrator/Data-master/pelatihanpembiayaan');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pejabat = HCSPejabat::all();
        return view ('Administrator.StagingMIS.pelatihanpembiayaan.create',compact('pejabat'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('staging.msutrainingpby')->insert([
                'nip' => $request->nip,
                'namatraining' => $request->namatraining
            ]);        
        return redirect ('/Administrator/Data-master/pelatihanpembiayaan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PelatihanPembiayaan  $pelatihanpembiayaan
     * @return \Illuminate\Http\Response
     */
    public function show(PelatihanPembiayaan $pelatihanpembiayaan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PelatihanPembiayaan  $pelatihanpembiayaan
     * @return \Illuminate\Http\Response
     */
    public function edit($kode)
    {        
        $pelatihanpembiayaan = PelatihanPembiayaan::where('id',$kode)->first();
        $pejabat = HCSPejabat::all();
        return view ('Administrator.StagingMIS.pelatihanpembiayaan.edit',compact('pelatihanpembiayaan','pejabat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PelatihanPembiayaan  $pelatihanpembiayaan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $pelatihanpembiayaan = PelatihanPembiayaan::where('id',$request->kode)->first();
        $pelatihanpembiayaan->nip = $request->nip;
        $pelatihanpembiayaan->namatraining = $request->namatraining;
        $pelatihanpembiayaan->update();

        return redirect ('/Administrator/Data-master/pelatihanpembiayaan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PelatihanPembiayaan  $pelatihanpembiayaan
     * @return \Illuminate\Http\Response
     */
    public function destroy($kode)
    {
        $pelatihanpembiayaan = PelatihanPembiayaan::where('id',$kode)->first();
        $pelatihanpembiayaan->delete();

        return redirect ('/Administrator/Data-master/pelatihanpembiayaan');
    }
}
