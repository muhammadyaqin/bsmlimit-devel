<?php

namespace App\Http\Controllers\Administrator\Staging;

use App\MisPortfolio;
use App\Exports\MisPortfolioExport;
use App\Exports\MisPortfolioReport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class MisPortfolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $mispo = DB::table('staging.misportfolio')
        //         ->select('staging.misportfolio.*','master.cabang.nama as namacabang','master.region.nama as namaregion')
        //         ->join('master.cabang','master.cabang.kode','staging.misportfolio.kodecabang')
        //         ->join('master.area','master.area.kode','master.cabang.kodearea')
        //         ->join('master.region','master.region.kode','master.area.koderegion')
        //         ->paginate(10);

                // dd($refreshment);

        $name_nip = $request->name_nip;

        $mispo = DB::table('staging.misportfolio');

        if($name_nip != null){
            $mispo = $mispo->where('staging.misportfolio.cif', 'LIKE', '%'. $name_nip. '%')->orWhere('namanasabah', 'LIKE', '%'. $name_nip. '%');
        }
        // dd($refreshment);

        if ($name_nip != null) {
            $statusActive = 'notActive';
        } else {
            $statusActive = 'isActive';
        }

        $mispo = $mispo->select('staging.misportfolio.*','master.cabang.nama as namacabang','master.region.nama as namaregion')
                    ->join('master.cabang','master.cabang.kode','staging.misportfolio.kodecabang')
                    ->join('master.area','master.area.kode','master.cabang.kodearea')
                    ->join('master.region','master.region.kode','master.area.koderegion')
                    ->orderBy('noloan', 'asc')->paginate(10);

        // $region = DB::table('staging.misportfolio')->join('master.');
        return view ('Administrator.StagingMIS.portfolio.mis_portfolio',compact('mispo','name_nip','statusActive'));
    }

    public function export_excel(Request $request)
    {
        return Excel::download(new MisPortfolioReport((object) $request),'MisPortfolioExport.xlsx');
    }

    public function import_excel(Request $request) 
    {
       // validasi
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);
 
        // menangkap file excel
        $file = $request->file('file');
 
        // membuat nama file unik
        $nama_file = rand().$file->getClientOriginalName();
 
        // upload ke folder file_siswa di dalam folder public
        $file->move('file_portfolio',$nama_file);
 
        // import data
        Excel::import(new MisPortfolio, public_path('/file_portfolio/'.$nama_file));
 
        // notifikasi dengan session
        Session::flash('sukses','Data Portfolio Berhasil Diimport!');
 
        // alihkan halaman kembali
        return redirect('/Administrator/Data-master/portfolio');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MisPortfolio  $misPortfolio
     * @return \Illuminate\Http\Response
     */
    public function show(MisPortfolio $misPortfolio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MisPortfolio  $misPortfolio
     * @return \Illuminate\Http\Response
     */
    public function edit(MisPortfolio $misPortfolio)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MisPortfolio  $misPortfolio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MisPortfolio $misPortfolio)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MisPortfolio  $misPortfolio
     * @return \Illuminate\Http\Response
     */
    public function destroy(MisPortfolio $misPortfolio)
    {
        //
    }
}
