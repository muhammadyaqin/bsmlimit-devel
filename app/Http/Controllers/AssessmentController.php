<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use Auth;
use App\RiwayatPwmp;
use App\Exports\RiwayatPwmpExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use Session;
use App\UserRole;
use App\User;
use App\Mail\EmailNotifikasi;
use Illuminate\Support\Facades\Mail;
use File;


class AssessmentController extends Controller
{
    //
    public function index(Request $request)
    {

       $kodeareajabatan = Session::get('areajabatan');
             $limitkodecabang = '';
             if ($kodeareajabatan == 'C') $limitkodecabang = "AND r.kodecabang ='" . Session::get('kodecabang') . "'";
             else if ($kodeareajabatan == 'A') {
               $limitkodecabang = "
               AND exists (
                 select 1 
                 from master.cabang x 
                 where x.kodearea = '" . Session::get('kodecabang') . "'
                 and x.kode = r.kodecabang
               )";
             }
             else if ($kodeareajabatan == 'R') {
               $limitkodecabang = "
               AND exists (
                 select 1 
                 from master.cabang x
                 inner join master.area y on y.kode = x.kodearea
                 where y.koderegion = '" . Session::get('kodecabang') . "'
                 and x.kode = r.kodecabang
               )";
             }
             else {

             }
        // dd($request->input('filterform'));

        $userLogin = Auth::user()->email;
        $userRole = Auth::user()->cekRole()->role;
        $cekkodecabang = Session::get('kodecabang');
        // dd(Session()->all());
        $cekareajabatan = Session::get('areajabatan');

        $report = DB::table('pwmp.riwayatpwmp as r')
        ->select(
           'r.id',
           // 'r.status_approve_erm',
           'r.nip',
           // 'r.file_sk',
           'r.namapejabat',
           'r.created_at',
           'r.status',
           'en.keterangan as statuspengajuan',
           'pl.limitnongadai as limitnongadaifix', 
           'pl.limitgadainlm as limitgadainlmfix', 
           'pl.limitgadailm as limitgadailmfix', 
           'pl.limitotomatis as limitotomatisfix', 
           'pl.nosk as noskfix', 
           'pl.tglsk as tglskfix'
       )
       ->leftjoin('pwmp.pengajuanlimit as pl', 'r.id', '=', 'pl.id')
       ->join('master.enumerasi as en', DB::raw('r.status::text'), '=', DB::raw("en.kode and en.kategori = 'STATUS PENGAJUAN'"))
       ->orderBy('r.id', 'desc');

        $name_nip = $request->name_nip;
        if(!is_null($name_nip)) {
            $report = $report->where('r.nip', 'LIKE', '%'. $name_nip. '%')
                ->orWhere('r.namapejabat', 'LIKE', '%'. $name_nip. '%');
        }
        
        $startDate = $request->startDate;
        if (is_null($startDate)) $startDate = date('d-m-Y');
        $report = $report->whereDate('r.created_at' , '>=' , date("Y-m-d",strtotime($startDate)));
        
        $endDate = $request->endDate;
        if (is_null($endDate)) $endDate = date('d-m-Y');
        $report = $report->whereDate('r.created_at' , '<=' , date("Y-m-d",strtotime($endDate)));
        
        $statuspengajuan = $request->status;
        if (!is_null($statuspengajuan)) $report = $report->where('r.status', $statuspengajuan);

        $nip = Session::get('nip');

        // $limitkodecabang = '';

        if ($userRole == 'CABANG'){
            $kodejabatanuser = Session::get('kodejabatan');
            $report->whereRaw(DB::raw("
            (
              UPPER(r.nip) = UPPER('$nip')
              OR 
              EXISTS (
                SELECT 1 FROM (
                  WITH RECURSIVE nodes(kode,keterangan,parent) AS (
                    SELECT s1.kode, s1.keterangan, s1.parent
                    FROM master.jabatan s1 WHERE parent = '$kodejabatanuser'
                    UNION
                    SELECT s2.kode, s2.keterangan, s2.parent
                    FROM master.jabatan s2, nodes s1 WHERE s2.parent = s1.kode
                  )
                  SELECT * FROM nodes
                ) rq
                WHERE rq.kode = r.kodejabatan
              )
            )
            $limitkodecabang
            "));
        }else{
            $kodejabatanuser = Session::get('kodejabatan');
            $report->whereRaw(DB::raw("
            (
              UPPER(r.nip) = UPPER('$nip')
              OR 
              EXISTS (
                SELECT 1 FROM (
                  WITH RECURSIVE nodes(kode,keterangan,parent) AS (
                    SELECT s1.kode, s1.keterangan, s1.parent
                    FROM master.jabatan s1 WHERE parent = '$kodejabatanuser'
                    UNION
                    SELECT s2.kode, s2.keterangan, s2.parent
                    FROM master.jabatan s2, nodes s1 WHERE s2.parent = s1.kode
                  )
                  SELECT * FROM nodes
                ) rq
                WHERE rq.kode = r.kodejabatan
              )
            )
            $limitkodecabang
            "));
        }

         $filterform = $request->input('filterform');
        if ($filterform == 'Export Excel') {
            $RiwayatPwmpExport = new RiwayatPwmpExport;
            $RiwayatPwmpExport->dataexcel = $report->get();
            return Excel::download($RiwayatPwmpExport, 'Rekapitulasi Daftar Riwayat PWMP.xlsx');
        }else{
            $page = 1;
            $next = $page+1;
            $prev = $page-1;
            
            $numbering =0;
            if($request->page){
                $page=$request->page;
                $numbering = $page*10;
            }
            $kodearea = $request->area;
            if(($kodearea != null) && ($kodearea != "")) {
                $area = DB::table('master.cabang')->where('master.cabang.kodearea', $kodearea)->pluck("kode");
                $report = $report->whereIn('r.kodecabang', $area);
            }


            if ($startDate != null && $endDate != null) {
                $hideStatus = 'not_active';
            } else {
                $hideStatus = 'active';
            }

            if ($kodearea != null or $statuspengajuan != null or $name_nip != null) {
                $statusActive = 'notActive';
            } else {
                $statusActive = 'isActive';
            }

            if ($cekareajabatan == 'C') {
                $getarea = DB::table('master.area as a')
                ->select('a.*')
                ->join('master.cabang as c', 'c.kodearea', '=', 'a.kode')
                ->where('c.kode', $cekkodecabang)
                ->orderBy('a.nama', 'asc')
                ->get();
            }
            else if ($cekareajabatan == 'A') {
                $getarea = DB::table('master.area as a')
                ->select('a.*')
                ->where('a.kode', $cekkodecabang)
                ->orderBy('a.nama', 'asc')
                ->get();
            }
            else if ($cekareajabatan == 'R') {
                // dd($cekkodecabang);
                $getarea = DB::table('master.area as a')
                ->select('a.*')
                ->join('master.region as r', 'r.kode', '=', 'a.koderegion')
                ->where('r.kode', $cekkodecabang)
                ->orderBy('a.nama', 'asc')
                ->get();
            }            
            else if ($cekareajabatan == 'P' || $cekareajabatan == '') {
                $getarea = DB::table('master.area as a')
                ->select('a.*')
                ->orderBy('a.nama', 'asc')
                ->get();
            }
            

            $enumerasi = DB::table('master.enumerasi')->where('master.enumerasi.kategori', '=', 'STATUS PENGAJUAN')->get();

            $report = $report->paginate(10);
            $data['pwmp'] = $report;
            $date = [
                'unit_kerja' => $kodearea,
                'status' => $statuspengajuan,
                'startDate' => $startDate,
                'endDate' => $endDate,
                'statusAct' => $kodearea,
            ];

            $keteranganStatus = $request->status;

            return view('assessment.index_kualitas', $data, compact('date', 'getarea', 'enumerasi', 'name_nip', 'kodearea', 'statusActive', 'startDate', 'endDate', 'next','prev', 'page', 'numbering', 'keteranganStatus'));    
        }

        
    }

    public function detail($id)
    {
        $userLogin = Auth::user()->email;
        
        $cekstatus = DB::table('pwmp.riwayatpwmp')->select('pwmp.riwayatpwmp.jenispengajuan')
        ->where('pwmp.riwayatpwmp.id', $id)->first();
        // dd($cekstatus->jenispengajuan);
        if ($cekstatus->jenispengajuan == "A" ) {
            $pwmp = DB::select(
                DB::raw(
                "SELECT 
                hp.id,
                hp.nip,
                hp.namapejabat,
                hp.created_by,
                hp.statuskaryawan,
                hp.kodejabatan,
                hp.kodecabang,
                hp.sanksi,
                hp.appraisal,
                hp.fraud,
                hp.approve_ro_date,
                jb.keterangan AS namajabatan,
                cb.nama AS namacabang,
                ar.kode AS kodearea,
                ar.nama AS namaarea,
                en2.keterangan AS keterangansanksi,
                en1.keterangan AS keteranganappraisal,
                en3.keterangan AS keteranganfraud,
                hp.statussmrisk AS statussertifikasi,
                hp.statusrefreshment,
                hp.tglsk,
                hp.nosk,
                hp.file_skpp,
                hp.fungsi,
                hp.kategorilimit,
                hp.created_at,
                hp.approve_erm_date,
                hp.status,
                en4.keterangan AS keteranganfungsi,
                kl.keterangan AS keterangankategorilimit,
                COALESCE(hp.pengalamanpby_thn, 0) as age_year,
                COALESCE(hp.pengalamanpby_bln, 0) as age_month,
                COALESCE(hp.pengalamanpwmp_thn, 0) as age_year_pwmp,
                COALESCE(hp.pengalamanpwmp_bln, 0) as age_month_pwmp,
                COALESCE(hp.qltportolancar_nom,0) as nomlancar,
                COALESCE(hp.qltportodpk_nom, 0) as nomdpk,
                COALESCE(hp.qltportonpf_nom, 0) as nomnpf,
                COALESCE(hp.qltportototal_nom, 0) as nomospokok,
                COALESCE(hp.qltportolancar_pct, 0) as pctlancar,
                COALESCE(hp.qltportodpk_pct, 0) as pctdpk,
                COALESCE(hp.qltportonpf_pct, 0) as pctnpf,
                hp.jenispengajuan,
                en5.keterangan AS statuspengajuan,
                pl.limitnongadai,
                pl.limitgadainlm,
                pl.limitgadailm,
                pl.limitotomatis,
                hp.limitnongadai AS nilailimitnongadai,
                hp.limitgadainlm AS nilailimitgadainlm,
                hp.limitgadailm AS nilailimitgadailm,
                hp.limitotomatis AS nilailimitotomatis,
                pl.nosk AS nosknew,
                pl.tglsk AS tglsknew,
                pl.filesk AS file_sk
                FROM pwmp.riwayatpwmp hp
                LEFT JOIN pwmp.pengajuanlimit pl ON pl.id = hp.id
                INNER JOIN master.jabatan jb ON jb.kode = hp.kodejabatan
                INNER JOIN master.cabang cb ON cb.kode = hp.kodecabang
                INNER JOIN master.area ar ON ar.kode = cb.kodearea
                LEFT JOIN master.enumerasi en2 ON en2.kode = hp.sanksi AND en2.kategori = 'SANKSI'
                LEFT JOIN master.enumerasi en1 ON en1.kode = hp.appraisal AND en1.kategori = 'APPRAISAL'
                LEFT JOIN master.enumerasi en3 ON en3.kode = hp.fraud AND en3.kategori = 'FRAUD'
                LEFT JOIN master.enumerasi en4 ON en4.kode = hp.fungsi AND en4.kategori = 'FUNGSI'
                LEFT JOIN master.kategorilimit kl ON kl.kode = hp.kategorilimit
                INNER JOIN master.enumerasi en5 ON en5.kode = hp.status::text AND en5.kategori = 'STATUS PENGAJUAN'
                WHERE (hp.id = '$id')
                "));
        }
        else {
            $pwmp = DB::select(
                DB::raw(
                "SELECT 
                hp.id,
                hp.nip,
                hp.namapejabat,
                hp.statuskaryawan,
                hp.kodejabatan,
                hp.kodecabang,
                hp.sanksi,
                hp.created_by,
                hp.appraisal,
                hp.fraud,
                hp.approve_ro_date,
                jb.keterangan AS namajabatan,
                cb.nama AS namacabang,
                ar.kode AS kodearea,
                ar.nama AS namaarea,
                en2.keterangan AS keterangansanksi,
                en1.keterangan AS keteranganappraisal,
                en3.keterangan AS keteranganfraud,
                hp.statussmrisk AS statussertifikasi,
                hp.statusrefreshment,
                hp.tglsk,
                hp.nosk,
                hp.approve_erm_date,
                hp.file_skpp,
                hp.fungsi,
                hp.kategorilimit,
                hp.created_at,
                hp.status,
                en4.keterangan AS keteranganfungsi,
                kl.keterangan AS keterangankategorilimit,
                COALESCE(hp.pengalamanpby_thn, 0) as age_year,
                COALESCE(hp.pengalamanpby_bln, 0) as age_month,
                COALESCE(hp.pengalamanpwmp_thn, 0) as age_year_pwmp,
                COALESCE(hp.pengalamanpwmp_bln, 0) as age_month_pwmp,
                COALESCE(hp.qltportolancar_nom,0) as nomlancar,
                COALESCE(hp.qltportodpk_nom, 0) as nomdpk,
                COALESCE(hp.qltportonpf_nom, 0) as nomnpf,
                COALESCE(hp.qltportototal_nom, 0) as nomospokok,
                COALESCE(hp.qltportolancar_pct, 0) as pctlancar,
                COALESCE(hp.qltportodpk_pct, 0) as pctdpk,
                COALESCE(hp.qltportonpf_pct, 0) as pctnpf,
                hp.jenispengajuan,
                en5.keterangan AS statuspengajuan,
                pl.limitnongadai,
                hp.approve_erm_date,
                pl.limitgadainlm,
                pl.limitgadailm,
                pl.limitotomatis,
                hp.limitnongadai AS nilailimitnongadai,
                hp.limitgadainlm AS nilailimitgadainlm,
                hp.limitgadailm AS nilailimitgadailm,
                hp.limitotomatis AS nilailimitotomatis,
                pl.nosk AS nosknew,
                pl.tglsk AS tglsknew,
                pl.filesk AS file_sk
                FROM pwmp.riwayatpwmp hp
                LEFT JOIN pwmp.pengajuanlimit pl ON pl.id = hp.id
                INNER JOIN master.jabatan jb ON jb.kode = hp.kodejabatan
                INNER JOIN master.cabang cb ON cb.kode = hp.kodecabang
                INNER JOIN master.area ar ON ar.kode = cb.kodearea
                LEFT JOIN pwmp.pwmp pw ON hp.nip = pw.nip
                LEFT JOIN master.enumerasi en2 ON en2.kode = hp.sanksi AND en2.kategori = 'SANKSI'
                LEFT JOIN master.enumerasi en1 ON en1.kode = hp.appraisal AND en1.kategori = 'APPRAISAL'
                LEFT JOIN master.enumerasi en3 ON en3.kode = hp.fraud AND en3.kategori = 'FRAUD'
                LEFT JOIN master.enumerasi en4 ON en4.kode = hp.fungsi AND en4.kategori = 'FUNGSI'
                LEFT JOIN master.kategorilimit kl ON kl.kode = hp.kategorilimit
                INNER JOIN master.enumerasi en5 ON en5.kode = hp.status::text AND en5.kategori = 'STATUS PENGAJUAN'
                WHERE (hp.id = '$id')
                "));
            }
        

        // dd($pwmp[0]->created_by);
        $ambilid = $pwmp[0]->id;
        $msu = DB::table('pwmp.trainingpby')->select('pwmp.trainingpby.nip', 'pwmp.trainingpby.namapelatihan')->where('idriwayatpwmp', $ambilid)->get();

        $sessrole = Auth::user()->cekRole()->role;
        $cekjabatan = Session::get('kodejabatan');
        $allowapprove = False;
        if($sessrole == 'REGION' && $cekjabatan == "RH001" && $pwmp[0]->status == 1){
            $allowapprove = True;   
        } else if($sessrole == 'PUSAT' && $pwmp[0]->status == 1) {
            $querymasterjabatan = DB::table('staging.hcspejabat')->select('master.jabatan.parent')
            ->join('master.jabatan', 'master.jabatan.kode','=','staging.hcspejabat.kodejabatan')
            ->where(DB::raw('UPPER(staging.hcspejabat.email)'), strtoupper($pwmp[0]->created_by))->first();
            if ($cekjabatan == $querymasterjabatan->parent) {
                $allowapprove = True;
            }
        }

        $file_sk = $pwmp[0]->file_sk;
        $file_skpp = $pwmp[0]->file_skpp;
        // dd($pwmp[0]);
        $cek_file_sk = public_path('/file_skp/sk/'.$file_sk);
        $cek_file_skpp = public_path('/file_skp/skp/'.$file_skpp);

        if(File::exists($cek_file_sk || $cek_file_skpp)){
            $statusActive = 'yes';
        }else{
            $statusActive = 'no';
        }

    	$data['msu'] = $msu;

        $data['pwmp'] = $pwmp;

        // $enumerasi = DB::table('master.enumerasi')->where('master.enumerasi.kategori', '=', 'STATUS PENGAJUAN')->get();


    	return view('assessment.detail', $data, compact('allowapprove', 'statusActive'));
    }

    public function testemail () {
        Mail::to("muhammadirfan5426@gmail.com")->send(new EmailNotifikasi());
        return "Email Telah Dikirim";
    }

    public function approveKC(Request $request)
    {

        $id = $request->input('id');
        $limitnongadai = $request->input('limitnongadai');
        $limitgadainlm = $request->input('limitgadainlm');
        $limitgadailm = $request->input('limitgadailm');
        $limitotomatis = $request->input('limitotomatis');
        $approve_by = Auth::user()->email;
        $approve_date = Carbon::now();

        $formatlimitnongadai = str_replace(',', '', $limitnongadai);
        $formatlimitgadainlm = str_replace(',', '', $limitgadainlm);
        $formatlimitgadailm = str_replace(',', '', $limitgadailm);
        $formatlimitotomatis = str_replace(',', '', $limitotomatis);
        // dd($id, $limitnongadai, $limitgadainlm, $limitgadainlm, $limitotomatis);

        DB::table('pwmp.riwayatpwmp')->where('id', $id)->update([
            'approve_ro' => $approve_by,
            'approve_ro_date' => $approve_date,
            'status' => 2,
            'status_approve_ro' => 1,
            'status_approve_erm' => 1,
        ]);

        DB::table('pwmp.pengajuanlimit')->where('id', $id)->update([
            'limitnongadai' => $formatlimitnongadai,
            'limitgadainlm' => $formatlimitgadainlm,
            'limitgadailm' => $formatlimitgadailm,
            'limitotomatis' => $formatlimitotomatis,
        ]);

        Mail::to("isgqa01@bsm.co.id")->send(new EmailNotifikasi());
        // return "Email Telah Dikirim";

        return redirect()->action('AssessmentController@index')->with("success", '');
    }

    public function rejectKC(Request $request)
    {
        $id = $request->input('reject');
        // dd($id);
        DB::table('pwmp.riwayatpwmp')->where('id', $id)->update([
            'status' => 5,
        ]);

        return redirect()->action('AssessmentController@index')->with("reject", '');;
    }

    public function export_excel()
    {
        return Excel::download(new RiwayatPwmpExport, 'Rekapitulasi Daftar Riwayat PWMP.xlsx');
    }
}
