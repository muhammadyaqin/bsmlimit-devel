<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

class HomeUserController extends Controller
{
    public function homeADMIN()
    {
    	return view('home-user.home_admin');
    }

    public function homeCABANG()
    {
        // dd(Session()->all());
    	return view('home-user.home_cabang');
    }

    public function homerEGION()
    {
    	return view('home-user.home_region');
    }

    public function homeERM()
    {
    	return view('home-user.home_erm');
    }

    public function homeIOG()
    {
        return view('home-user.home_iog');
    }

    public function homePUSAT()
    {
        return view('home-user.home_pusat');
    }
    public function homearea()
    {
        return view('home-user.home_area');
    }

    public function homeuser()
    {
        return view('home-user.home_user');
    }

    public function homehcs()
    {
        return view('home-user.home_hcs');
    }
}
