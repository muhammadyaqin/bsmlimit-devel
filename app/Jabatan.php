<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jabatan extends Model
{
    protected $table = 'jabatan';
    protected $primaryKey = 'kode';
    protected $fillable = ['keterangan','level','parent'];

//parent di ambil dari kode

//di balde nya output dropdown nya keterangn input data base nya kode

    protected $keyType = 'string';
    public $timestamps = false;
    public $incrementing = false;
}
