<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProdukLimitOtomatis extends Model
{
    protected $table = 'produklimitotomatis';
    protected $primaryKey = 'kode';
    protected $fillable = ['keterangan'];

    protected $keyType = 'string';
    public $timestamps = false;
    public $incrementing = false;
}
