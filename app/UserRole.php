<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use App\UserRole;
use App\User;
use App\UserApp;
use App\Role;

class UserRole extends Authenticatable
{
    protected $table = 'master.userrole';
    protected $primaryKey = 'email';
    protected $keyType = 'string';

    public function role(){
        return $this->belongsTo(Role::class);
    }

    public function check_role(){
        return $this->role->kode;
    }
}
