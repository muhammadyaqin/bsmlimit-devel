<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SMRisk extends Model
{
    protected $table = "staging.smrisk";
     
    protected $fillable = ['nip','statussertifikasi'];
    public $timestamps = false;
}
