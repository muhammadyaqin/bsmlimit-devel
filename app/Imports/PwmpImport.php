<?php

namespace App\Imports;

use App\PWMP;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class PwmpImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new PWMP([
            'nip' => $row[1],
            'namapejabat' => $row[2],
            'kodejabatan' => $row[3],
            'kodecabang' => $row[4],
            'limitnongadai' => $row[5],
            'limitgadainlm' => $row[6],
            'limitgadailm' => $row[7],
            'limitotomatis' => $row[8],
            'fungsi' => $row[9],
            'kategorilimit' => $row[10],
            'nosk' => $row[11],
            'tglsk' => $row[12],
        ]);
    }

    public function startRow(): int
    {
        return 2;
    }
}
