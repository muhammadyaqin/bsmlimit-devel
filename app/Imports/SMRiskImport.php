<?php

namespace App\Imports;

use App\SMRisk;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class SMRiskImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new SMRisk([
            'nip' => $row[0],
            'statussertifikasi' => $row[1],
        ]);
    }
    public function startRow(): int
    {
        return 2;
    }
}
