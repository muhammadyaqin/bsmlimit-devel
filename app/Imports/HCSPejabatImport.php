<?php

namespace App\Imports;

use App\HCSPejabat;
use Maatwebsite\Excel\Concerns\ToModel;
// use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class HCSPejabatImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if (!isset($row['nip'])) {
            return null;
        }
        return new HCSPejabat([
            'nip' => $row['nip'],
            'namapejabat' => $row['namapejabat'],
            'tglbergabung' => $row['tglbergabung'],
            'statuskaryawan' => $row['statuskaryawan'],
            'kodejabatan' => $row['kodejabatan'],
            'kodecabang' => $row['kodecabang'],
            'appraisal' => $row['appraisal'],
            'sanksi' => $row['sanksi'],
            'fraud' => $row['fraud'],
            'email' => $row['email'],
        ]);
    }
}
