<?php

namespace App\Imports;

use App\HCSRiwayatPejabat;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class HCSRiwayatPejabatImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new HCSRiwayatPejabat([
            'nip' => $row['nip'],
            'kodejabatan' => $row['kodejabatan'],
            'kodecabang' => $row['kodecabang'],
            'tglmulai' => $row['tglmulai'],
            'tglberakhir' => $row['tglberakhir'],
        ]);
    }

    // public function startRow(): int
    // {
    //     return 2;
    // }
}
