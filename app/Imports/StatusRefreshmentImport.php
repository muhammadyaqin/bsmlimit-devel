<?php

namespace App\Imports;

use App\StatusRefreshment;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class StatusRefreshmentImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new StatusRefreshment([
            'nip' => $row[0],
            'statusrefreshment' => $row[1],
        ]);
    }

    public function startRow(): int
    {
        return 2;
    }
}
