<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PengalamanPembiayaan extends Model
{
    protected $table = 'staging.hcspengalamanpby';
    protected $primaryKey = ['id'];
    protected $fillable = ['nip','kodejabatan','keteranganjabatan','kodecabang','keterangancabang','tglmulai','tglberakhir'];

    protected $keyType = 'string';
    public $timestamps = false;
    public $incrementing = false;
}
