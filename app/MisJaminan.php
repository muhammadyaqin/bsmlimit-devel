<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MisJaminan extends Model
{
    protected $table = "staging.misjaminan";

    protected $primaryKey = 'id';

    protected $fillable = [
                            'noloan',
                            'kodejaminan',
                            'keterangan'
                            ];
    
    public $timestamps = false;
    public $incrementing = false;
}
