<?php

namespace App\Exports;

use App\ProdukPembiayaan;
use Maatwebsite\Excel\Concerns\FromCollection;

class ProdukPembiayaanExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return ProdukPembiayaan::all();
    }
}
