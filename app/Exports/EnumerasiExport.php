<?php

namespace App\Exports;

use App\Enumerasi;
use Maatwebsite\Excel\Concerns\FromCollection;

class EnumerasiExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Enumerasi::all();
    }
}
