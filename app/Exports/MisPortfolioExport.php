<?php

namespace App\Exports;

use App\MisPortfolio;
use Maatwebsite\Excel\Concerns\FromCollection;

class MisPortfolioExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return MisPortfolio::all();
    }
}
