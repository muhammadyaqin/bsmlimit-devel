<?php

namespace App\Exports;

use App\KategoriLimit;
use Maatwebsite\Excel\Concerns\FromCollection;

class KategoriLimitExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return KategoriLimit::all();
    }
}
