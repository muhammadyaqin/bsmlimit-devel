<?php

namespace App\Exports;

use App\RiwayatPwmp;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Auth;
use DB;

class AssessmentERM implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
   public $dataexcel;

   public function collection()
   {
       return $this->dataexcel;
   }

    public function headings(): array
    {
        return [
            'ID PENGAJUAN',
            'NIP',
            'NAMA',
            'KODE JABATAN',
            'KODE CABANG',
            'FUNGSI',
            'KATEGORI LIMIT',
            'JENIS PENGAJUAN',
            'TANGGAL PENGAJUAN',
            'STATUS',
            'KETERANGAN STATUS',
            'LIMIT NON GADAI',
            'LIMIT GADAI NON LOGAM MULIA',
            'LIMIT GADAI LOGAM MULIA',
            'LIMIT OTOMATIS',
            'NOMOR SK',
            'TANGGAL SK'];
    }
}
