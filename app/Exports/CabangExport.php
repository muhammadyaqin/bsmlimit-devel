<?php

namespace App\Exports;

use App\Cabang;
use Maatwebsite\Excel\Concerns\FromCollection;

class CabangExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Cabang::all();
    }
}
