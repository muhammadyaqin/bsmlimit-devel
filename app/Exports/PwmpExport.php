<?php

namespace App\Exports;

use App\PWMP;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Carbon\Carbon;

class PwmpExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $report = new PWMP;

        $report = $report->select('pwmp.pwmp.nip', 'pwmp.pwmp.namapejabat',  'master.jabatan.keterangan', 'master.area.nama', 'pwmp.pwmp.limitnongadai', 'pwmp.pwmp.limitgadainlm','pwmp.pwmp.limitgadailm','pwmp.pwmp.limitotomatis','pwmp.pwmp.nosk', 'pwmp.pwmp.tglsk')
                ->leftjoin('master.area','pwmp.pwmp.kodecabang','=','master.area.kode')
                ->leftjoin('master.jabatan','pwmp.pwmp.kodejabatan','=','master.jabatan.kode')
                ->orderBy('pwmp.pwmp.nip', 'desc')->get();

        return $report;
    }

    public function headings(): array
    {
        return [
            'NIP',
            'NAMA',
            'JABATAN',
            'UNIT KERJA',
            'LIMIT NON GADAI',
            'LIMIT GADAI NON LOGAM',
            'LIMIT GADAI LOGAM MULIA',
            'LIMIT OTOMATIS',
            'NOMOR SK',
            'TANGGAL SK'];
    }

}
