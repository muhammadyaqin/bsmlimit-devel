<?php

namespace App\Exports;

use App\PengalamanPembiayaan;
use Maatwebsite\Excel\Concerns\FromCollection;

class PengalamanPembiayaanExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return PengalamanPembiayaan::all();
    }
}
