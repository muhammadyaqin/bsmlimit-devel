<?php

namespace App\Exports;

use App\Region_dm;
use Maatwebsite\Excel\Concerns\FromCollection;

class RegionExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Region_dm::all();
    }
}
