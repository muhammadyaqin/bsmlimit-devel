<?php

namespace App\Exports;

use App\Area_dm;
use Maatwebsite\Excel\Concerns\FromCollection;

class AreaExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Area_dm::all();
    }
}
