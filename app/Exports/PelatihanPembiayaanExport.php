<?php

namespace App\Exports;

use App\PelatihanPembiayaan;
use Maatwebsite\Excel\Concerns\FromCollection;

class PelatihanPembiayaanExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return PelatihanPembiayaan::all();
    }
}
