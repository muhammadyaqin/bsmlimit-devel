<?php

namespace App\Exports;

use App\MisJaminan;
use Maatwebsite\Excel\Concerns\FromCollection;

class MisJaminanExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return MisJaminan::all();
    }
}
