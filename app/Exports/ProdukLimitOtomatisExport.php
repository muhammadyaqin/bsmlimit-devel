<?php

namespace App\Exports;

use App\ProdukLimitOtomatis;
use Maatwebsite\Excel\Concerns\FromCollection;

class ProdukLimitOtomatisExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return ProdukLimitOtomatis::all();
    }
}
