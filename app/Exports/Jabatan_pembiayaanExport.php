<?php

namespace App\Exports;

use App\Jabatan_pembiayaan;
use Maatwebsite\Excel\Concerns\FromCollection;

class Jabatan_pembiayaanExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Jabatan_pembiayaan::all();
    }
}
