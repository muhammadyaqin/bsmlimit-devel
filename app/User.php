<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Notifiable;
use App\UserRole;

class User extends Authenticatable
{
	use Notifiable;

    protected $table = 'master.userapp';
    protected $primaryKey = 'email';
    protected $keyType = 'string';

	function cekRole(){
		return UserRole::where('email', $this->email)->first();
	}

}