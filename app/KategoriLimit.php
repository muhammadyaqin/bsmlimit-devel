<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriLimit extends Model
{
    protected $table = 'kategorilimit';
    protected $primaryKey = 'kode';
    protected $fillable = ['keterangan','batasbawah','batasatas'];

    protected $keyType = 'string';
    public $timestamps = false;
    public $incrementing = false;
}
