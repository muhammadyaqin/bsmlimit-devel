<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jabatan_pembiayaan extends Model
{
    protected $table = 'jabatanpembiayaan';
    protected $primaryKey = 'id';
    protected $fillable = ['nama'];

    protected $keyType = 'string';
    public $timestamps = false;
    public $incrementing = false;
}
