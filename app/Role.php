<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\UserRole;

class Role extends Model
{
    protected $table = 'master.role';
    protected $primaryKey = 'kode';
    protected $keyType = 'string';

    function cekRole(){
		return Role::where('kode', $this->kode)->first();
    }

}
