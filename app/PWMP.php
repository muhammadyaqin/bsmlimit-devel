<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PWMP extends Model
{
    protected $table = "pwmp.pwmp";

    public function getDates()
	{
	    return array('created_at','updated_at');
	}

    protected $primaryKey = 'nip';

    protected $fillable = ['nip','namapejabat','kodejabatan','kodecabang','limitnongadai','limitgadainlm','limitgadailm','limitotomatis','fungsi','kategorilimit','nosk','tgslsk','status'];
}
