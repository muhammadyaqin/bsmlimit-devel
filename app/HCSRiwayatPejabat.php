<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HCSRiwayatPejabat extends Model
{
    protected $table = "temp.hcsriwayatjabatan";
    protected $fillable = ['nip','kodejabatan','kodecabang','tglmulai','tglberakhir'];
    public $timestamps = false;

}
