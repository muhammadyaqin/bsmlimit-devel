<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RiwayatPwmp extends Model
{
   protected $table = 'pwmp.riwayatpwmp';

   public function getDates()
    {
        return array('created_at','updated_at');
    }
}
