<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::auth();

Route::group([ 'middleware' => 'auth' ], function() {
		Route::get('/', function () {
		    return view('index');
		});

		#Route::get('/home', 'HomeController@index')->name('home');
		Route::get('/home','DashboardController@index');
		Route::get('/portofolio','portofolio@index');
		Route::get('/portofolio/hitung','portofolio@hitung');
		Route::get('/limit/baru','PermohonanLimitController@baru');
		Route::get('/limit/tambah','PermohonanLimitController@tingkat');
		Route::get('/assessment','AssessmentController@index');
		Route::get('/assessment/{id}/detail','AssessmentController@detail');
		Route::get('/Administrator/index','Administrator\Beranda\BerandaController@index');

		//Data Staging MIS
		//Portofolio
		Route::get('/Administrator/Data-master/portfolio','Administrator\Staging\MisPortfolioController@index');
		Route::get('/misportofolio/import_excel','Administrator\Staging\MisPortfolioController@import_excel');

		//Jaminan
		Route::get('/Administrator/Data-master/jaminan','Administrator\DataMaster\MisJaminanController@index');
		Route::get('/Administrator/Data-master/jaminan/create','Administrator\DataMaster\MisJaminanController@create');
		Route::get('/jaminan/import_excel','Administrator\DataMaster\MisJaminanController@import_excel');

		//Pejabat
		Route::get('/Administrator/Data-master/pejabat','Administrator\DataMaster\MisPejabatController@index');
		Route::get('/Administrator/Data-master/pejabat/{nip}/detail','Administrator\DataMaster\MisPejabatController@detail');
		Route::post('/hcspejabat/import_excel','Administrator\DataMaster\MisPejabatController@import_excel');

		//RiwayatPejabat
		Route::get('/Administrator/Data-master/riwayatjabatan','Administrator\DataMaster\RiwayatPejabatController@index');
		Route::get('/Administrator/Data-master/riwayatjabatan/{nip}/detail','Administrator\DataMaster\RiwayatPejabatController@detail');
		Route::post('/riwayatjabatan/import_excel','Administrator\DataMaster\RiwayatPejabatController@import_excel');

		//ManajemenResiko
		Route::get('/Administrator/Data-master/manajemenresiko','Administrator\DataMaster\ManajemenResikoController@index');
		Route::get('/Administrator/Data-master/manajemenresiko/{nip}/detail','Administrator\DataMaster\ManajemenResikoController@detail');
		Route::post('/smrisk/import_excel','Administrator\DataMaster\ManajemenResikoController@import_excel');

		//Refreshment
		Route::get('/Administrator/Data-master/refreshment','Administrator\DataMaster\RefreshmentController@index');
		Route::get('/Administrator/Data-master/refreshment/{nip}/detail','Administrator\DataMaster\RefreshmentController@detail');
		Route::post('/refreshment/import_excel','Administrator\DataMaster\RefreshmentController@import_excel');
		// Route::get('/Administrator/Data-master/pelatihan-pembiayaan','Administrator\DataMaster\RefreshmentController@index');

		//Pelatihan Pembiayaan
		Route::get('/Administrator/Data-master/pelatihanpembiayaan','Administrator\Staging\PelatihanPembiayaanController@index');
		Route::get('/Administrator/Data-master/pelatihanpembiayaan/create','Administrator\Staging\PelatihanPembiayaanController@create');
		Route::post('/Administrator/Data-master/pelatihanpembiayaan/store','Administrator\Staging\PelatihanPembiayaanController@store');
		Route::get('/Administrator/Data-master/pelatihanpembiayaan/edit/{kode}','Administrator\Staging\PelatihanPembiayaanController@edit');
		Route::post('/Administrator/Data-master/pelatihanpembiayaan/update/{kode}','Administrator\Staging\PelatihanPembiayaanController@update');
		Route::post('/Administrator/Data-master/pelatihanpembiayaan/delete/{kode}','Administrator\Staging\PelatihanPembiayaanController@destroy');
		Route::get('/pelatihanpembiayaan/export_excel','Administrator\Staging\PelatihanPembiayaanController@export_excel');

		//Pengalaman Pembiayaan
		Route::get('/Administrator/Data-master/pengalamanpembiayaan','Administrator\Staging\PengalamanPembiayaanController@index');
		Route::get('/Administrator/Data-master/pengalamanpembiayaan/create','Administrator\Staging\PengalamanPembiayaanController@create');
		Route::post('/Administrator/Data-master/pengalamanpembiayaan/store','Administrator\Staging\PengalamanPembiayaanController@store');
		Route::get('/Administrator/Data-master/pengalamanpembiayaan/edit/{kode}','Administrator\Staging\PengalamanPembiayaanController@edit');
		Route::post('/Administrator/Data-master/pengalamanpembiayaan/update/{kode}','Administrator\Staging\PengalamanPembiayaanController@update');
		Route::post('/Administrator/Data-master/pengalamanpembiayaan/delete/{kode}','Administrator\Staging\PengalamanPembiayaanController@destroy');
		Route::get('/pengalamanpembiayaan/export_excel','Administrator\Staging\PengalamanPembiayaanController@export_excel');
		Route::get('/pengalamanpembiayaan/import_excel','Administrator\Staging\PengalamanPembiayaanController@import_excel');

		//Crud Daftar Produk Pembiayaan
		Route::get('/Administrator/Data-master/produkpembiayaan', 'Administrator\DataMaster\ProdukPembiayaanController@index');
		Route::get('/Administrator/Data-master/produkpembiayaan/create','Administrator\DataMaster\ProdukPembiayaanController@create');
		Route::post('/Administrator/Data-master/produkpembiayaan/store','Administrator\DataMaster\ProdukPembiayaanController@store');
		Route::get('/Administrator/Data-master/produkpembiayaan/edit/{kode}','Administrator\DataMaster\ProdukPembiayaanController@edit');
		Route::post('/Administrator/Data-master/produkpembiayaan/update/{kode}','Administrator\DataMaster\ProdukPembiayaanController@update');
		Route::post('/Administrator/Data-master/produkpembiayaan/delete/{kode}','Administrator\DataMaster\ProdukPembiayaanController@destroy');
		//Export excel route
		Route::get('/Administrator/Data-master/produkpembiayaan/export_excel', 'Administrator\DataMaster\ProdukPembiayaanController@export_excel');

		//Crud Daftar Produk Limit Otomatis
		Route::get('/Administrator/Data-master/produklimitotomatis', 'Administrator\DataMaster\ProdukLimitOtomatisController@index');
		Route::get('/Administrator/Data-master/produklimitotomatis/create','Administrator\DataMaster\ProdukLimitOtomatisController@create');
		Route::post('/Administrator/Data-master/produklimitotomatis/store','Administrator\DataMaster\ProdukLimitOtomatisController@store');
		Route::get('/Administrator/Data-master/produklimitotomatis/edit/{kode}','Administrator\DataMaster\ProdukLimitOtomatisController@edit');
		Route::post('/Administrator/Data-master/produklimitotomatis/update/{kode}','Administrator\DataMaster\ProdukLimitOtomatisController@update');
		Route::get('/Administrator/Data-master/produklimitotomatis/delete/{kode}','Administrator\DataMaster\ProdukLimitOtomatisController@destroy');
		//Export excel route Eumerasi
		Route::post('/Administrator/Data-master/produklimitotomatis/delete/{kode}','Administrator\DataMaster\ProdukLimitOtomatisController@destroy');
		Route::get('/Administrator/Data-master/produklimitotomatis/export_excel','Administrator\DataMaster\ProdukLimitOtomatisController@export_excel');

		//Crud Daftar Category LImit
		Route::get('/Administrator/Data-master/kategorilimit', 'Administrator\DataMaster\KategoriLimitController@index');
		Route::get('/Administrator/Data-master/kategorilimit/create','Administrator\DataMaster\KategoriLimitController@create');
		Route::post('/Administrator/Data-master/kategorilimit/store','Administrator\DataMaster\KategoriLimitController@store');
		Route::get('/Administrator/Data-master/kategorilimit/edit/{kode}','Administrator\DataMaster\KategoriLimitController@edit');
		Route::post('/Administrator/Data-master/kategorilimit/update/{kode}','Administrator\DataMaster\KategoriLimitController@update');
		Route::get('/Administrator/Data-master/kategorilimit/delete/{kode}','Administrator\DataMaster\KategoriLimitController@destroy');
		//Export excel route Eumerasi
		Route::get('/Administrator/Data-master/kategorilimit/export_excel','Administrator\DataMaster\KategoriLimitController@export_excel');

		Route::post('/Administrator/Data-master/kategorilimit/delete/{kode}','Administrator\DataMaster\KategoriLimitController@destroy');
		Route::get('/Administrator/Data-master/kategorilimit/export_excel','Administrator\DataMaster\KategoriLimitController@export_excel');

		//Crud Daftar Mapping Segment
		// Route::get('/Administrator/Data-master/mappingsegmen', 'Administrator\DataMaster\MappingSegmenController@index');
		// Route::get('/Administrator/Data-master/mappingsegmen/create','Administrator\DataMaster\MappingSegmenController@create');
		// Route::post('/Administrator/Data-master/mappingsegmen/store','Administrator\DataMaster\MappingSegmenController@store');
		// Route::get('/Administrator/Data-master/mappingsegmen/edit/{id}','Administrator\DataMaster\MappingSegmenController@edit');
		// Route::post('/Administrator/Data-master/mappingsegmen/update/','Administrator\DataMaster\MappingSegmenController@update');
		// Route::get('/Administrator/Data-master/mappingsegmen/delete/{id}','Administrator\DataMaster\MappingSegmenController@destroy');

		//Crud Daftar Jabatan
		Route::get('/Administrator/Data-master/jabatan', 'Administrator\DataMaster\JabatanController@index');
		Route::get('/Administrator/Data-master/jabatan/create','Administrator\DataMaster\JabatanController@create');
		Route::post('/Administrator/Data-master/jabatan/store','Administrator\DataMaster\JabatanController@store');
		Route::get('/Administrator/Data-master/jabatan/edit/{kode}','Administrator\DataMaster\JabatanController@edit');
		Route::post('/Administrator/Data-master/jabatan/update/{kode}','Administrator\DataMaster\JabatanController@update');
		Route::get('/Administrator/Data-master/jabatan/delete/{kode}','Administrator\DataMaster\JabatanController@destroy');
		//Export excel route Eumerasi
		Route::post('/Administrator/Data-master/jabatan/delete/{kode}','Administrator\DataMaster\JabatanController@destroy');
		Route::get('/Administrator/Data-master/jabatan/export_excel','Administrator\DataMaster\JabatanController@export_excel');

		//Crud Daftar Region
		Route::get('/Administrator/Data-master/region', 'Administrator\DataMaster\Region_dmController@index');
		Route::get('/Administrator/Data-master/region/create','Administrator\DataMaster\Region_dmController@create');
		Route::post('/Administrator/Data-master/region/store','Administrator\DataMaster\Region_dmController@store');
		Route::get('/Administrator/Data-master/region/edit/{kode}','Administrator\DataMaster\Region_dmController@edit');
		Route::post('/Administrator/Data-master/region/update/{kode}','Administrator\DataMaster\Region_dmController@update');
		Route::get('/Administrator/Data-master/region/delete/{kode}','Administrator\DataMaster\Region_dmController@destroy');
		//Export excel route Eumerasi
		Route::get('/Administrator/Data-master/region/export_excel','Administrator\DataMaster\Region_dmController@export_excel');
		Route::post('/Administrator/Data-master/region/delete/{kode}','Administrator\DataMaster\Region_dmController@destroy');

		//Crud Daftar Area
		Route::get('/Administrator/Data-master/area', 'Administrator\DataMaster\Area_dmController@index');
		Route::get('/Administrator/Data-master/area/create','Administrator\DataMaster\Area_dmController@create');
		Route::post('/Administrator/Data-master/area/store','Administrator\DataMaster\Area_dmController@store');
		Route::get('/Administrator/Data-master/area/edit/{kode}','Administrator\DataMaster\Area_dmController@edit');
		Route::post('/Administrator/Data-master/area/update/{kode}','Administrator\DataMaster\Area_dmController@update');
		Route::get('/Administrator/Data-master/area/delete/{kode}','Administrator\DataMaster\Area_dmController@destroy');
		//Export excel route Eumerasi
		Route::get('/Administrator/Data-master/area/export_excel','Administrator\DataMaster\Area_dmController@export_excel');

		Route::post('/Administrator/Data-master/area/delete/{kode}','Administrator\DataMaster\Area_dmController@destroy');

		//Crud Daftar UnitKerja/Cabang
		Route::get('/Administrator/Data-master/unitkerja', 'Administrator\DataMaster\CabangController@index');
		Route::get('/Administrator/Data-master/unitkerja/create','Administrator\DataMaster\CabangController@create');
		Route::post('/Administrator/Data-master/unitkerja/store','Administrator\DataMaster\CabangController@store');
		Route::get('/Administrator/Data-master/unitkerja/edit/{kode}','Administrator\DataMaster\CabangController@edit');
		Route::post('/Administrator/Data-master/unitkerja/update/{kode}','Administrator\DataMaster\CabangController@update');
		Route::get('/Administrator/Data-master/unitkerja/delete/{kode}','Administrator\DataMaster\CabangController@destroy');
		//Export excel route Eumerasi
		Route::get('/Administrator/Data-master/unitkerja/export_excel','Administrator\DataMaster\CabangController@export_excel');
		Route::post('/Administrator/Data-master/unitkerja/delete/{kode}','Administrator\DataMaster\CabangController@destroy');

		//Enumerasi
		Route::get('/Administrator/Data-master/enumerasi','Administrator\DataMaster\EnumerasiController@index');
		Route::get('/Administrator/Data-master/enumerasi/create','Administrator\DataMaster\EnumerasiController@create');
		Route::post('/Administrator/Data-master/enumerasi/store','Administrator\DataMaster\EnumerasiController@store');
		Route::get('/Administrator/Data-master/enumerasi/edit/{kode}','Administrator\DataMaster\EnumerasiController@edit');
		Route::post('/Administrator/Data-master/enumerasi/update/{kode}','Administrator\DataMaster\EnumerasiController@update');
		Route::post('/Administrator/Data-master/enumerasi/delete/{kode}/{kategori}','Administrator\DataMaster\EnumerasiController@destroy');
		Route::get('/Administrator/Data-master/enumerasi/export_excel','Administrator\DataMaster\EnumerasiController@export_excel');

		//jabatan pembiayaan
		Route::get('/Administrator/Data-master/jabatanpembiayaan','Administrator\DataMaster\JabatanPembiayaanController@index');
		Route::get('/Administrator/Data-master/jabatanpembiayaan/create','Administrator\DataMaster\JabatanPembiayaanController@create');
		Route::post('/Administrator/Data-master/jabatanpembiayaan/store','Administrator\DataMaster\JabatanPembiayaanController@store');
		Route::get('/Administrator/Data-master/jabatanpembiayaan/edit/{kode}','Administrator\DataMaster\JabatanPembiayaanController@edit');
		Route::post('/Administrator/Data-master/jabatanpembiayaan/update/{kode}','Administrator\DataMaster\JabatanPembiayaanController@update');
		Route::post('/Administrator/Data-master/jabatanpembiayaan/delete/{kode}','Administrator\DataMaster\JabatanPembiayaanController@destroy');
		Route::get('/Administrator/Data-master/jabatanpembiayaan/export_excel','Administrator\DataMaster\JabatanPembiayaanController@export_excel');



		//Route UserApp
		Route::resource('user', 'UserAppController');

		Route::post("user/update/{user}", "UserAppController@update");

		//Route User Role
		Route::get('/user_manajemen/user_role/index','Administrator\UserManajemen\RoleMenu\RoleMenuController@index');
		Route::get('/user-role/{email}/edit','Administrator\UserManajemen\RoleMenu\RoleMenuController@detail');
		Route::get('/user_manajemen/user_role/create','Administrator\UserManajemen\RoleMenu\RoleMenuController@create');
		Route::post('/user_manajemen/user_role/store','Administrator\UserManajemen\RoleMenu\RoleMenuController@store');

		//Route Role
		Route::get('/user_manajemen/role/index','Administrator\UserManajemen\Role\RoleController@index');
		Route::get('/user_manajemen/role/create','Administrator\UserManajemen\Role\RoleController@create');
		Route::post('/user_manajemen/role/store','Administrator\UserManajemen\Role\RoleController@store');
		Route::get('/user_manajemen/role/edit/{kode}','Administrator\UserManajemen\Role\RoleController@edit');
		Route::post('/user_manajemen/role/update','Administrator\UserManajemen\Role\RoleController@update');
		Route::post('/user_manajemen/role/delete/{kode}','Administrator\UserManajemen\Role\RoleController@destroy');


		Route::get('/pwmp','PwmpController@index');
		Route::get('/pwmp/{nip}/detail','PwmpController@detail');
		Route::get('/pwmp/export_excel', 'PwmpController@export_excel');


		Route::get('/pwmp-masa-percobaan','PwmpMasaPercobaanController@index');
		Route::get('/pwmp-belum-memenuhi-syarat','PwmpBelumMemenuhiSyaratController@index');
		Route::get('/pwmp-pindah-rumpun-jabatan','PwmpPindahJabatanController@index');
		Route::get('/pwmp-tidak-aktif','PwmpTidakAktifController@index');
		Route::post('/pwmp/import_excel', 'PwmpController@import_excel');
		Route::get('/kualitas-pembiayaan','KualitasPembiayaanController@index');


		Route::post('/limit/baru/autocomplete','PermohonanLimitController@autoComplete');
		Route::get('/kualitas/pembiayaan/loadtable','PermohonanLimitController@loadTable');
		Route::get('/loadtable/pejabat','PermohonanLimitController@loadTablePejabat');
		Route::post('/ajukan/limit/baru','PermohonanLimitController@limitBaru');
		Route::post('/ajukan/limit/baru/region','PermohonanLimitController@limitBaruRegion');
		Route::post('/ajukan/limit/baru/erm','PermohonanLimitController@limitBaruERM');
		Route::post('/limit/baru/pelatihan','PermohonanLimitController@PelatihanPembiayaan');

		Route::post('/approve/assessment/kc','AssessmentController@approveKC');
		Route::post('/reject/assessment/kc','AssessmentController@rejectKC');

		Route::get('/erm/assessment','AssessmentERMController@index');
		Route::get('/erm/assessment/export_excel','AssessmentERMController@export_excel');
		Route::get('/erm/assessment/{id}/detail','AssessmentERMController@detail');
		Route::post('/approve/erm','AssessmentERMController@approveERM');
		Route::post('/reject/assessment/erm','AssessmentERMController@rejectERM');
		Route::post('/generate/{id}/assessment/erm','AssessmentERMController@GenerateSK');
		Route::post('/update/{id}/assessment/erm','AssessmentERMController@updatevaluenominal');
		Route::post('/generatenota/{id}/assessment/erm','AssessmentERMController@GenerateNota');

		Route::get('/Administrator/Data-master/mapsegmen','Administrator\DataMaster\MapSegmenController@index');
		Route::get('/Administrator/Data-master/mapsegmen/create','Administrator\DataMaster\MapSegmenController@create');
		Route::post('/Administrator/Data-master/mapsegmen/store','Administrator\DataMaster\MapSegmenController@store');
		Route::get('/Administrator/Data-master/mapsegmen/edit/{kode}','Administrator\DataMaster\MapSegmenController@edit');
		Route::post('/Administrator/Data-master/mapsegmen/update/{kode}','Administrator\DataMaster\MapSegmenController@update');
		Route::post('/Administrator/Data-master/mapsegmen/delete/{kode}','Administrator\DataMaster\MapSegmenController@destroy');
		Route::post('/Administrator/Data-master/mapsegmen/export_excel','Administrator\DataMaster\MapSegmenController@export_excel');

		//Map Jenis Jaminan
		Route::get('/Administrator/Data-master/mapjenisjaminan','Administrator\DataMaster\MapJenisJaminanController@index');
		Route::get('/Administrator/Data-master/mapjenisjaminan/create','Administrator\DataMaster\MapJenisJaminanController@create');
		Route::post('/Administrator/Data-master/mapjenisjaminan/store','Administrator\DataMaster\MapJenisJaminanController@store');
		Route::get('/Administrator/Data-master/mapjenisjaminan/edit/{kode}','Administrator\DataMaster\MapJenisJaminanController@edit');
		Route::post('/Administrator/Data-master/mapjenisjaminan/update/{kode}','Administrator\DataMaster\MapJenisJaminanController@update');
		Route::post('/Administrator/Data-master/mapjenisjaminan/delete/{kode}','Administrator\DataMaster\MapJenisJaminanController@destroy');
		Route::post('/Administrator/Data-master/mapjenisjaminan/export_excel','Administrator\DataMaster\MapJenisJaminanController@export_excel');

		//Map Kolektibilitas
		Route::get('/Administrator/Data-master/mapkolektibilitas','Administrator\DataMaster\MapKolektibilitasController@index');
		Route::get('/Administrator/Data-master/mapkolektibilitas/create','Administrator\DataMaster\MapKolektibilitasController@create');
		Route::post('/Administrator/Data-master/mapkolektibilitas/store','Administrator\DataMaster\MapKolektibilitasController@store');
		Route::get('/Administrator/Data-master/mapkolektibilitas/edit/{kode}','Administrator\DataMaster\MapKolektibilitasController@edit');
		Route::post('/Administrator/Data-master/mapkolektibilitas/update/{kode}','Administrator\DataMaster\MapKolektibilitasController@update');
		Route::post('/Administrator/Data-master/mapkolektibilitas/delete/{kode}','Administrator\DataMaster\MapKolektibilitasController@destroy');
		Route::post('/Administrator/Data-master/mapkolektibilitas/export_excel','Administrator\DataMaster\MapKolektibilitasController@export_excel');


		Route::get('/upload','AssessmentERMController@upload');

		Route::get('/home-admin','HomeUserController@homeADMIN');
		Route::get('/home-cabang','HomeUserController@homeCABANG');
		Route::get('/home-erm','HomeUserController@homeERM');
		Route::get('/home-region','HomeUserController@homeREGION');
		Route::get('/assessment/export_excel', 'AssessmentController@export_excel');

		Route::get('/home-iog','HomeUserController@homeIOG');
		Route::get('/home-pusat','HomeUserController@homePUSAT');
		Route::get('/home-area','HomeUserController@homearea');

		Route::get('/home-user','HomeUserController@homeuser');

		Route::get('/home-hcs','HomeUserController@homehcs');

		Route::get('/select-role/{role}','UsersController@selectrole');
		Route::get('/updateSelected','UsersController@test123');
 
 });