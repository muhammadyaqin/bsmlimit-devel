<?php

require_once "../vendor/autoload.php";
require_once "../vendor/laravel/framework/src/Illuminate/Foundation/helpers.php";

function dbconnect() {
  $dbuser = env("DBUSER_PORTOFOLIO");
  $dbpassword = env("PASS_PORTOFOLIO");
  $dbhost = env("HOST_PORTOFOLIO");
  $dbport = env("PORT_PORTOFOLIO");
  $dbname = env("DBNAME_PORTOFOLIO");
  $connectionstring = sprintf("host=%s port=%s dbname=%s user=%s password=%s", $dbhost, $dbport, $dbname, $dbuser, $dbpassword);
  $dbconnect = pg_connect($connectionstring);

  return $dbconnect;
}

function formatString($str, $data) {
  return preg_replace_callback('#{(\w+?)(\.(\w+?))?}#', function($m) use ($data){
    return count($m) === 2 ? $data[$m[1]] : $data[$m[1]][$m[3]];
  }, $str);
}

function setResolvedLoan($dbconnect) {
  $sSQL = "
  insert into pwmp.portfolio_resolved
  select * from pwmp.portfolio_valid
  ";
  pg_query($dbconnect, $sSQL);
}

function setQuality($dbconnect) {
  $sSQL = "truncate table pwmp.portfolio_quality";
  pg_query($dbconnect, $sSQL);
  
  $sSQL = "truncate table pwmp.portfolio_qualitysum";
  pg_query($dbconnect, $sSQL);

  $sSQL = "
  insert into pwmp.portfolio_quality
  select pw.nip,'1',0,0,0,0,0,0,0,0,0 from pwmp.pwmp pw
  ";
  pg_query($dbconnect, $sSQL);

  $sSQL = "
  insert into pwmp.portfolio_quality
  select pw.nip,'2',0,0,0,0,0,0,0,0,0 from pwmp.pwmp pw
  ";
  pg_query($dbconnect, $sSQL);

  $sSQL = "
  insert into pwmp.portfolio_quality
  select pw.nip,'3',0,0,0,0,0,0,0,0,0 from pwmp.pwmp pw
  ";
  pg_query($dbconnect, $sSQL);

  $sSQL = "
  insert into pwmp.portfolio_quality
  select pw.nip,'4',0,0,0,0,0,0,0,0,0 from pwmp.pwmp pw
  ";
  pg_query($dbconnect, $sSQL);

  $sSQL = "
  insert into pwmp.portfolio_quality
  select pw.nip,'5',0,0,0,0,0,0,0,0,0 from pwmp.pwmp pw
  ";
  pg_query($dbconnect, $sSQL);

  foreach (['nikpemutusbisnis','nikpemutusrisk'] as $fieldnik) {
    $sqlParams = [];
    $sqlParams['nikpemutus'] = $fieldnik;

    $sSQL = "
    update pwmp.portfolio_quality as pq
    set 
    nomplafon = sq1.nomplafon + pq.nomplafon, 
    nomlancar = sq1.nomospokok
    from (
      select 
      pv.{nikpemutus}, 
      ms.kodebaru as kodesegmenbaru,
      sum(pv.plafon) as nomplafon,
      sum(pv.ospokok) as nomospokok
      from pwmp.portfolio_resolved pv
      inner join master.mapsegmen ms on ms.kodeasal = pv.segmen
      inner join master.mapkolektibilitas mk on mk.kodeasal = pv.kolcif
      where mk.kodebaru = 'LANCAR'
      group by pv.{nikpemutus}, ms.kodebaru
      order by pv.{nikpemutus}, ms.kodebaru
    ) as sq1
    where 
    pq.nip = sq1.{nikpemutus}
    and pq.segmen = sq1.kodesegmenbaru
    ";
    $sSQL = formatString($sSQL, $sqlParams);
    pg_query($dbconnect, $sSQL);

    $sSQL = "
    update pwmp.portfolio_quality as pq
    set 
    nomplafon = sq1.nomplafon + pq.nomplafon, 
    nomdpk = sq1.nomospokok
    from (
      select 
      pv.{nikpemutus}, 
      ms.kodebaru as kodesegmenbaru,
      sum(pv.plafon) as nomplafon,
      sum(pv.ospokok) as nomospokok
      from pwmp.portfolio_resolved pv
      inner join master.mapsegmen ms on ms.kodeasal = pv.segmen
      inner join master.mapkolektibilitas mk on mk.kodeasal = pv.kolcif
      where mk.kodebaru = 'DPK'
      group by pv.{nikpemutus}, ms.kodebaru
      order by pv.{nikpemutus}, ms.kodebaru
    ) as sq1
    where 
    pq.nip = sq1.{nikpemutus}
    and pq.segmen = sq1.kodesegmenbaru
    ";
    $sSQL = formatString($sSQL, $sqlParams);
    pg_query($dbconnect, $sSQL);

    $sSQL = "
    update pwmp.portfolio_quality as pq
    set 
    nomplafon = sq1.nomplafon + pq.nomplafon, 
    nomnpf= sq1.nomospokok
    from (
      select 
      pv.{nikpemutus}, 
      ms.kodebaru as kodesegmenbaru,
      sum(pv.plafon) as nomplafon,
      sum(pv.ospokok) as nomospokok
      from pwmp.portfolio_resolved pv
      inner join master.mapsegmen ms on ms.kodeasal = pv.segmen
      inner join master.mapkolektibilitas mk on mk.kodeasal = pv.kolcif
      where mk.kodebaru = 'NPF'
      group by pv.{nikpemutus}, ms.kodebaru
      order by pv.{nikpemutus}, ms.kodebaru
    ) as sq1
    where 
    pq.nip = sq1.{nikpemutus}
    and pq.segmen = sq1.kodesegmenbaru
    ";
    $sSQL = formatString($sSQL, $sqlParams);
    pg_query($dbconnect, $sSQL);
  }

  $sSQL = "
  update pwmp.portfolio_quality
  set
  nomospokok = nomlancar + nomdpk + nomnpf
  ";
  pg_query($dbconnect, $sSQL);

  $sSQL = "
  update pwmp.portfolio_quality
  set 
  pctlancar = (nomlancar/nomospokok)*100,
  pctdpk = (nomdpk/nomospokok)*100,
  pctnpf = (nomnpf/nomospokok)*100
  where coalesce(nomospokok,0,nomospokok) > 0.0
  ";
  pg_query($dbconnect, $sSQL);

  $sSQL = "
  insert into pwmp.portfolio_qualitysum (nip, nomplafon, nomlancar, nomdpk, nomnpf, nomospokok, pctlancar, pctdpk, pctnpf, pctospokok)
  select nip, sum(nomplafon), sum(nomlancar), sum(nomdpk), sum(nomnpf), sum(nomospokok), 0,0,0,100
  from pwmp.portfolio_quality
  group by nip
  ";
  pg_query($dbconnect, $sSQL);
    
  $sSQL = "
  update pwmp.portfolio_qualitysum
  set 
  pctlancar = (nomlancar/nomospokok)*100,
  pctdpk = (nomdpk/nomospokok)*100,
  pctnpf = (nomnpf/nomospokok)*100
  where coalesce(nomospokok,0,nomospokok) > 0.0
  ";
  pg_query($dbconnect, $sSQL);

  $sSQL = "
  update pwmp.portfolio_quality as pq
  set pctospokok = (pq.nomospokok/pqs.nomospokok)*100
  from 
  pwmp.portfolio_qualitysum pqs
  where 
  pqs.nip = pq.nip
  and pqs.nomospokok > 0.0
  ";
  pg_query($dbconnect, $sSQL);
}

function main() {
  try {
    $dbconnect = dbconnect();
    pg_query("BEGIN");
    
    echo "INITIATE\n";
    $sSQL = 'TRUNCATE TABLE pwmp.portfolio_invalid';
    pg_query($dbconnect, $sSQL);
    $sSQL = 'TRUNCATE TABLE pwmp.portfolio_valid';
    pg_query($dbconnect, $sSQL);
  
    echo "FORMULA 1\n";
    $f1processorder = [1,2,3,4];
    $f1processdct = [
      1 => ['descr' => 'Limit Non-Gadai'],
      2 => ['descr' => 'Limit Gadai-LM'],
      3 => ['descr' => 'Limit Gadai-NLM'],
      4 => ['descr' => 'Limit Otomatis'],
    ];
    foreach($f1processorder as $iP) {
      $processdescr = $f1processdct[$iP]['descr'];
      $processcode = sprintf('01.%02d', $iP);
  
      #LoanSelect
      if ($iP == 1){
        $loanselect = "
        SELECT 
        mp.*
        FROM staging.misportfolio mp
        WHERE 
        EXISTS (
          SELECT 1 FROM pwmp.pwmp p
          WHERE p.nip = mp.nikpemutus
          AND p.fungsi = 'B'
        )
        AND NOT EXISTS (
          SELECT 1 FROM pwmp.portfolio_resolved x
          WHERE x.noloan = mp.noloan
        )
        AND NOT EXISTS (
          SELECT 1 FROM pwmp.portfolio_valid x
          WHERE x.noloan = mp.noloan
        )
        AND NOT EXISTS (
          SELECT 1 FROM pwmp.portfolio_invalid x
          WHERE x.noloan = mp.noloan
        )
        AND mp.type NOT IN ('RHN0001','RHN0002') --NON GADAI
        --AND mp.ospokok > 0.0
        ";
      } else if (in_array($iP, [2,3])) {
        if ($iP == 2) { $jenisjaminan = '1'; }
        else if ($iP == 3) { $jenisjaminan = '2'; }
  
        $loanselect = "
        SELECT 
        mp.*
        FROM staging.misportfolio mp
        INNER JOIN (SELECT DISTINCT noloan, kodejaminan FROM staging.misjaminan) mja 
          ON mja.noloan = mp.noloan
        INNER JOIN master.mapjenisjaminan mjj ON mjj.kodeasal = mja.kodejaminan
        WHERE 
        EXISTS (
          SELECT 1 FROM pwmp.pwmp p
          WHERE p.nip = mp.nikpemutus
          AND p.fungsi = 'B'
        )
        AND NOT EXISTS (
          SELECT 1 FROM pwmp.portfolio_resolved x
          WHERE x.noloan = mp.noloan
        )
        AND NOT EXISTS (
          SELECT 1 FROM pwmp.portfolio_valid x
          WHERE x.noloan = mp.noloan
        )
        AND NOT EXISTS (
          SELECT 1 FROM pwmp.portfolio_invalid x
          WHERE x.noloan = mp.noloan
        )
        AND mp.type IN ('RHN0001','RHN0002') --GADAI
        AND mjj.kodebaru = '%s'
        AND mp.ospokok > 0.0
        ";
        $sSQL = sprintf($sSQL, $jenisjaminan);
      } else if ($iP == 4) {
        $loanselect = "
        SELECT 
        mp.*
        FROM staging.misportfolio mp
        WHERE 
        EXISTS (
          SELECT 1 FROM pwmp.pwmp p
          WHERE p.nip = mp.nikpemutus
          AND p.fungsi = 'B'
        )
        AND NOT EXISTS (
          SELECT 1 FROM pwmp.portfolio_resolved x
          WHERE x.noloan = mp.noloan
        )
        AND NOT EXISTS (
          SELECT 1 FROM pwmp.portfolio_valid x
          WHERE x.noloan = mp.noloan
        )
        AND NOT EXISTS (
          SELECT 1 FROM pwmp.portfolio_invalid x
          WHERE x.noloan = mp.noloan
        )
        AND EXISTS (
          SELECT 1 FROM master.produklimitotomatis x
          WHERE x.kode = mp.type
        ) --OTOMATIS
        --AND mp.ospokok > 0.0
        ";
      } else {
        echo 'FORMULA TIDAK DIDEFINISIKAN #LoanSelect\n'; 
        break;
      }
  
      #LimitSelect
      if ($iP == 1) {
        $limitselect = 'pw.limitnongadai';
      } else if ($iP == 2) { 
        $limitselect = 'pw.limitgadailm';
      } else if ($iP == 3) {
        $limitselect = 'pw.limitgadainlm';
      } else if ($iP == 4) {
        $limitselect = 'pw.limitotomatis';
      } else {
        echo 'FORMULA TIDAK DIDEFINISIKAN #LimitSelect\n'; 
        break;
      }
      
      $sqlParams = [];
      $sqlParams['loanselect'] = $loanselect;
      $sqlParams['limitselect'] = $limitselect;
      $sqlParams['processcode'] = $processcode;
  
      echo sprintf("--%s: %s\n", $processcode, $processdescr);
      echo "----01/06\n";
      $sSQL = 'TRUNCATE TABLE temp.portfolio_tmp1';
      pg_query($dbconnect, $sSQL);
  
      echo "----02/06\n";
      $sSQL = 'TRUNCATE TABLE temp.portfolio_sum1';
      pg_query($dbconnect, $sSQL);
  
      echo "----03/06\n";
      $sSQL = "
      INSERT INTO temp.portfolio_tmp1
      {loanselect}
      ";
      $sSQL = formatString($sSQL, $sqlParams);
      pg_query($dbconnect, $sSQL);
  
      echo "----04/06\n";
      $sSQL ="
      INSERT INTO temp.portfolio_sum1
      SELECT nikpemutus, cif, SUM(plafon) AS sumplafon, 'F'
      FROM temp.portfolio_tmp1 mt
      GROUP BY nikpemutus, cif
      ";
      pg_query($dbconnect, $sSQL);
  
      echo "----05/06\n";
      $sSQL ="
      UPDATE temp.portfolio_sum1 AS ms
      SET isvalid = 'T'
      FROM pwmp.pwmp as pw
      WHERE 
      ms.nikpemutus = pw.nip
      AND ms.sumplafon <= {limitselect}
      ";
      $sSQL = formatString($sSQL, $sqlParams);
      pg_query($dbconnect, $sSQL);
  
      echo "----06/06\n";
      $sSQL ="
      INSERT INTO pwmp.portfolio_valid(
        noloan,cif,namanasabah,kodecabang,type,
        plafon,ospokok,tglcair,tgljatuhtempo,kolcif,
        styus,secon,segmen,nikpengusul,nikpemutusbisnis,
        flagrestruct,tglrestruct,processinsert)
      select mt.*, '{processcode}' AS process
      FROM temp.portfolio_tmp1 mt
      INNER JOIN temp.portfolio_sum1 ms ON ms.nikpemutus = mt.nikpemutus
        AND ms.cif = mt.cif
        AND ms.isvalid = 'T'
      ";
      $sSQL = formatString($sSQL, $sqlParams);
      pg_query($dbconnect, $sSQL);
    }
  
    $formulanumber = [2,3,4,5,6,7,8,9,10,11,12,13,14,15,16];
    foreach ($formulanumber as $iF) {
      echo sprintf("FORMULA 2 : %02d\n", $iF);
      
      #ProcessOrder
      if (in_array($iF, [2,3,15,16])) {
        $processorder = [
          ['branchtype' => 'cabang', 'position' => "'PO003','POM003'"],
          ['branchtype' => 'cabang', 'position' => "'BM003'"],
          ['branchtype' => 'area', 'position' => "'AMPM002'"],
          ['branchtype' => 'cabang', 'position' => "'AMPM002'"],
          ['branchtype' => 'area', 'position' => "'AM002'"],
          ['branchtype' => 'cabang', 'position' => "'AM002'"],
          ['branchtype' => 'region', 'position' => "'RRBM002'"],
          ['branchtype' => 'cabang', 'position' => "'RRBM002'"],
          ['branchtype' => 'region', 'position' => "'RH001'"],
          ['branchtype' => 'cabang', 'position' => "'RH001'"]
        ];
      } else if ($iF == 4) {
        $processorder = [
          ['branchtype' => 'cabang', 'position' => "'MBM003'"],
          ['branchtype' => 'area', 'position' => "'AMPM002'"],
          ['branchtype' => 'cabang', 'position' => "'AMPM002'"],
          ['branchtype' => 'cabang', 'position' => "'BM003'"],
          ['branchtype' => 'area', 'position' => "'AM002'"],
          ['branchtype' => 'cabang', 'position' => "'AM002'"],
          ['branchtype' => 'region', 'position' => "'RH001'"],
          ['branchtype' => 'cabang', 'position' => "'RH001'"]
        ];
      } else if ($iF == 5) {
        $processorder = [
          ['branchtype' => 'cabang', 'position' => "'BM003'"],
          ['branchtype' => 'area', 'position' => "'ABBM002'"],
          ['branchtype' => 'cabang', 'position' => "'ABBM002'"],
          ['branchtype' => 'area', 'position' => "'AM002'"],
          ['branchtype' => 'cabang', 'position' => "'AM002'"],
          ['branchtype' => 'region', 'position' => "'RH001'"],
          ['branchtype' => 'cabang', 'position' => "'RH001'"],
          ['branchtype' => 'pusat', 'position' => "'EBOBS3'"],
          ['branchtype' => 'cabang', 'position' => "'EBOBS3'"],
          ['branchtype' => 'pusat', 'position' => "'GHBS34'"],
          ['branchtype' => 'cabang', 'position' => "'GHBS34'"]
        ];
      } else if ($iF == 6) {
        $processorder = [
          ['branchtype' => 'cabang', 'position' => "'BM003'"],
          ['branchtype' => 'area', 'position' => "'ACFM002'"],
          ['branchtype' => 'cabang', 'position' => "'ACFM002'"]
        ];
      } else if ($iF == 7) {
        $processorder = [
          ['branchtype' => 'cabang', 'position' => "'BM003'"],
          ['branchtype' => 'area', 'position' => "'ACFM002'"],
          ['branchtype' => 'cabang', 'position' => "'ACFM002'"],
          ['branchtype' => 'area', 'position' => "'AM002'"],
          ['branchtype' => 'cabang', 'position' => "'AM002'"],
          ['branchtype' => 'region', 'position' => "'RH001'"],
          ['branchtype' => 'cabang', 'position' => "'RH001'"],
          //['branchtype' => 'pusat', 'position' => "'EBOBS5'"],
          //['branchtype' => 'cabang', 'position' => "'EBOBS5'"],
          ['branchtype' => 'pusat', 'position' => "'GHBS5'"],
          ['branchtype' => 'cabang', 'position' => "'GHBS5'"]
        ];
      } else if ($iF == 8) {
        $processorder = [
          ['branchtype' => 'region', 'position' => "'RWBM002'"],
          ['branchtype' => 'cabang', 'position' => "'RWBM002'"],
          ['branchtype' => 'pusat', 'position' => "'GHBS2'"],
          ['branchtype' => 'cabang', 'position' => "'GHBS2'"]
        ];
      } else if ($iF == 9) {
        $processorder = [
          ['branchtype' => 'pusat', 'position' => "'GHBS1'"],
          ['branchtype' => 'cabang', 'position' => "'GHBS1'"]
        ];
      } else if ($iF == 10) {
        $processorder = [
          ['branchtype' => 'area', 'position' => "'AFRM002'"],
          ['branchtype' => 'cabang', 'position' => "'AFRM002'"],
          ['branchtype' => 'region', 'position' => "'RFRRM002'"],
          ['branchtype' => 'cabang', 'position' => "'RFRRM002'"],
          ['branchtype' => 'pusat', 'position' => "'ERRORS3'"],
          ['branchtype' => 'cabang', 'position' => "'ERRORS3'"],
          ['branchtype' => 'pusat', 'position' => "'GHRS345'"],
          ['branchtype' => 'cabang', 'position' => "'GHRS345'"]
        ];
      } else if ($iF == 11) {
        $processorder = [
          ['branchtype' => 'pusat', 'position' => "'DHRS2'"],
          ['branchtype' => 'cabang', 'position' => "'DHRS2'"],
          ['branchtype' => 'pusat', 'position' => "'ERRORS2'"],
          ['branchtype' => 'cabang', 'position' => "'ERRORS2'"],
          ['branchtype' => 'pusat', 'position' => "'GHRS2'"],
          ['branchtype' => 'cabang', 'position' => "'GHRS2'"]
        ];
      } else if ($iF == 12) {
        $processorder = [
          ['branchtype' => 'pusat', 'position' => "'DHRS1'"],
          ['branchtype' => 'cabang', 'position' => "'DHRS1'"],
          ['branchtype' => 'pusat', 'position' => "'ERRORS1'"],
          ['branchtype' => 'cabang', 'position' => "'ERRORS1'"],
          ['branchtype' => 'pusat', 'position' => "'GHRS1'"],
          ['branchtype' => 'cabang', 'position' => "'GHRS1'"]
        ];
      } else if ($iF == 13) {
        $processorder = [
          ['branchtype' => 'area', 'position' => "'ACRM002'"],
          ['branchtype' => 'cabang', 'position' => "'ACRM002'"],
          ['branchtype' => 'region', 'position' => "'RFRRM002'"],
          ['branchtype' => 'cabang', 'position' => "'RFRRM002'"],
          ['branchtype' => 'pusat', 'position' => "'DHRC345'"],
          ['branchtype' => 'cabang', 'position' => "'DHRC345'"],
          ['branchtype' => 'pusat', 'position' => "'GHRC345'"],
          ['branchtype' => 'cabang', 'position' => "'GHRC345'"]
        ];
      } else if ($iF == 14) {
        $processorder = [
          ['branchtype' => 'pusat', 'position' => "'DHRC12'"],
          ['branchtype' => 'cabang', 'position' => "'DHRC12'"],
          ['branchtype' => 'pusat', 'position' => "'GHRC12'"],
          ['branchtype' => 'cabang', 'position' => "'GHRC12'"]
        ];
      } else {
        echo 'FORMULA TIDAK DIDEFINISIKAN #ProcessOrder\n'; 
        break;
      }
      #--
  
      #LoanSelect
      if (in_array($iF, [2,3])) {
        if ($iF == 2) { $jenisjaminan = '1'; }
        else if ($iF == 3) { $jenisjaminan = '2'; }
  
        $loanselect = "
        SELECT 
        mp.*
        FROM staging.misportfolio mp
        INNER JOIN (SELECT DISTINCT noloan, kodejaminan FROM staging.misjaminan) mja 
          ON mja.noloan = mp.noloan
        INNER JOIN master.mapjenisjaminan mjj ON mjj.kodeasal = mja.kodejaminan
        WHERE 
        NOT EXISTS (
          SELECT 1 FROM pwmp.portfolio_resolved x
          WHERE x.noloan = mp.noloan
        )
        AND NOT EXISTS (
          SELECT 1 FROM pwmp.portfolio_valid x
          WHERE x.noloan = mp.noloan
        )
        AND NOT EXISTS (
          SELECT 1 FROM pwmp.portfolio_invalid x
          WHERE x.noloan = mp.noloan
        )
        AND mp.type IN ('RHN0001','RHN0002') --GADAI
        AND mjj.kodebaru = '%s'
        AND mp.ospokok > 0.0
        ";
        $loanselect = sprintf($loanselect, $jenisjaminan);
      } else if (in_array($iF, [15,16])) {
        $loanselect = "
        SELECT 
        mp.*
        FROM staging.misportfolio mp
        --INNER JOIN (SELECT DISTINCT noloan, kodejaminan FROM staging.misjaminan) mja 
        --  ON mja.noloan = mp.noloan
        --INNER JOIN master.mapjenisjaminan mjj ON mjj.kodeasal = mja.kodejaminan
        WHERE 
        NOT EXISTS (
          SELECT 1 FROM pwmp.portfolio_resolved x
          WHERE x.noloan = mp.noloan
        )
        AND NOT EXISTS (
          SELECT 1 FROM pwmp.portfolio_valid x
          WHERE x.noloan = mp.noloan
        )
        AND NOT EXISTS (
          SELECT 1 FROM pwmp.portfolio_invalid x
          WHERE x.noloan = mp.noloan
        )
        AND mp.type IN ('RHN0001','RHN0002') --GADAI
        --AND mjj.kodebaru = '%s'
        AND mp.ospokok > 0.0
        ";
        $loanselect = sprintf($loanselect, $jenisjaminan);
      } else if (in_array($iF, [4,5,7,8,9])) {
        if ($iF == 4) { $kodesegmen = '4'; }
        else if ($iF == 5) { $kodesegmen = '3'; }
        else if ($iF == 7) { $kodesegmen = '5'; }
        else if ($iF == 8) { $kodesegmen = '2'; }
        else if ($iF == 9) { $kodesegmen = '1'; }
  
        $loanselect = "
        SELECT 
        mp.*
        FROM staging.misportfolio mp
        INNER JOIN master.mapsegmen ms ON ms.kodeasal = mp.segmen
        WHERE 
        NOT EXISTS (
          SELECT 1 FROM pwmp.portfolio_resolved x
          WHERE x.noloan = mp.noloan
        )
        AND NOT EXISTS (
          SELECT 1 FROM pwmp.portfolio_valid x
          WHERE x.noloan = mp.noloan
        )
        AND NOT EXISTS (
          SELECT 1 FROM pwmp.portfolio_invalid x
          WHERE x.noloan = mp.noloan
        )
        AND mp.type NOT IN ('RHN0001','RHN0002') --NON GADAI
        AND ms.kodebaru = '%s'
        --AND mp.ospokok > 0.0
        ";
        $loanselect = sprintf($loanselect, $kodesegmen);
      } else if (in_array($iF, [6])) {
        if ($iF == 6) { $kodesegmen = '5'; }
        
        $loanselect = "
        SELECT 
        mp.*
        FROM staging.misportfolio mp
        INNER JOIN master.mapsegmen ms ON ms.kodeasal = mp.segmen
        WHERE 
        NOT EXISTS (
          SELECT 1 FROM pwmp.portfolio_resolved x
          WHERE x.noloan = mp.noloan
        )
        AND NOT EXISTS (
          SELECT 1 FROM pwmp.portfolio_valid x
          WHERE x.noloan = mp.noloan
        )
        AND NOT EXISTS (
          SELECT 1 FROM pwmp.portfolio_invalid x
          WHERE x.noloan = mp.noloan
        )
        AND EXISTS (
          SELECT 1 FROM master.produklimitotomatis x
          WHERE x.kode = mp.type
        ) --OTOMATIS
        AND ms.kodebaru = '%s'
        --AND mp.ospokok > 0.0
        ";
        $loanselect = sprintf($loanselect, $kodesegmen);
      } else if (in_array($iF, [10])) {
        $loanselect = "
        SELECT 
        mp.*
        FROM staging.misportfolio mp
        INNER JOIN master.mapsegmen ms ON ms.kodeasal = mp.segmen
        WHERE 
        /*NOT EXISTS (
          SELECT 1 FROM pwmp.portfolio_resolved x
          WHERE x.noloan = mp.noloan
        )
        AND NOT EXISTS (
          SELECT 1 FROM pwmp.portfolio_valid x
          WHERE x.noloan = mp.noloan
        )
        AND NOT EXISTS (
          SELECT 1 FROM pwmp.portfolio_invalid x
          WHERE x.noloan = mp.noloan
        )
        AND */mp.type NOT IN ('RHN0001','RHN0002') --NON GADAI
        AND ms.kodebaru = '3'
        --AND mp.ospokok > 0.0
        ";
      } else if (in_array($iF, [11,12])) {
        if ($iF == 11) { $kodesegmen = '2'; }
        else if ($iF == 12) { $kodesegmen = '1'; }
  
        $loanselect = "
        SELECT 
        mp.*
        FROM staging.misportfolio mp
        INNER JOIN master.mapsegmen ms ON ms.kodeasal = mp.segmen
        WHERE 
        /*NOT EXISTS (
          SELECT 1 FROM pwmp.portfolio_resolved x
          WHERE x.noloan = mp.noloan
        )
        AND NOT EXISTS (
          SELECT 1 FROM pwmp.portfolio_valid x
          WHERE x.noloan = mp.noloan
        )
        AND NOT EXISTS (
          SELECT 1 FROM pwmp.portfolio_invalid x
          WHERE x.noloan = mp.noloan
        )
        AND */mp.type NOT IN ('RHN0001','RHN0002') --NON GADAI
        AND ms.kodebaru = '%s'
        --AND mp.ospokok > 0.0
        ";
        $loanselect = sprintf($loanselect, $kodesegmen);
      } else if (in_array($iF, [13])) {
        $loanselect = "
        SELECT 
        mp.*
        FROM staging.misportfolio mp
        INNER JOIN master.mapsegmen ms ON ms.kodeasal = mp.segmen
        WHERE 
        /*NOT EXISTS (
          SELECT 1 FROM pwmp.portfolio_resolved x
          WHERE x.noloan = mp.noloan
        )
        AND NOT EXISTS (
          SELECT 1 FROM pwmp.portfolio_valid x
          WHERE x.noloan = mp.noloan
        )
        AND NOT EXISTS (
          SELECT 1 FROM pwmp.portfolio_invalid x
          WHERE x.noloan = mp.noloan
        )
        AND */mp.type NOT IN ('RHN0001','RHN0002') --NON GADAI
        AND ms.kodebaru in ('3','4','5')
        AND mp.flagrestruct IS NOT NULL
        --AND mp.ospokok > 0.0
        ";
      } else if (in_array($iF, [14])) {
        $loanselect = "
        SELECT 
        mp.*
        FROM staging.misportfolio mp
        INNER JOIN master.mapsegmen ms ON ms.kodeasal = mp.segmen
        WHERE 
        /*NOT EXISTS (
          SELECT 1 FROM pwmp.portfolio_resolved x
          WHERE x.noloan = mp.noloan
        )
        AND NOT EXISTS (
          SELECT 1 FROM pwmp.portfolio_valid x
          WHERE x.noloan = mp.noloan
        )
        AND NOT EXISTS (
          SELECT 1 FROM pwmp.portfolio_invalid x
          WHERE x.noloan = mp.noloan
        )
        AND */mp.type NOT IN ('RHN0001','RHN0002') --NON GADAI
        AND ms.kodebaru in ('1','2')
        AND mp.flagrestruct IS NOT NULL
        --AND mp.ospokok > 0.0
        ";
      } else {
        echo 'FORMULA TIDAK DIDEFINISIKAN #LoanSelect\n'; 
        break;
      }
  
      #LimitSelect
      if (in_array($iF, [2,15])) {
        $limitselect = 'rp.limitgadailm';
      } else if (in_array($iF, [3,16])) {
        $limitselect = 'rp.limitgadainlm';
      } else if (in_array($iF, [4,5,7,8,9,10,11,12,13,14])) {
        $limitselect = 'rp.limitnongadai';
      } else if (in_array($iF, [6])) {
        $limitselect = 'rp.limitotomatis';
      } else {
        echo 'FORMULA TIDAK DIDEFINISIKAN #LimitSelect\n'; 
        break;
      }
  
      #SumPLafonMinimal
      if (in_array($iF, [2,3,4,5,6,7,8,9,11,12,13,14,15,16])) {
        $sumplafonminimal = 0.0;
      } else if ($iF == 10) {
        $sumplafonminimal = 1500000000.0;
      } else {
        echo 'FORMULA TIDAK DIDEFINISIKAN #SumPLafonMinimal\n'; 
        break;
      }
  
      #FieldNikPemutus
      if (in_array($iF, [2,3,4,5,6,7,8,9,13,14,15,16])) {
        $fieldnikpemutus = 'nikpemutusbisnis';
      } else if (in_array($iF, [10,11,12])) {
        $fieldnikpemutus = 'nikpemutusrisk';
      } else {
        echo 'FORMULA TIDAK DIDEFINISIKAN #FieldNikPemutus\n'; 
        break;
      }
  
      $iP = 0;
      foreach ($processorder as $processdct) {
        $iP++;
        $processcode = sprintf('%02d.%02d', $iF, $iP);
  
        $branchtype = $processdct['branchtype'];
        $position = $processdct['position'];
  
        if ($branchtype == 'pusat') {
          $joinriwayatpwmpcabang = "
          --INNER JOIN master.cabang c ON c.kode = mp.kodecabang
          --INNER JOIN master.area a ON a.kode = c.kodearea
          --INNER JOIN master.region r ON r.kode = a.koderegion
          --INNER JOIN pwmp.riwayatpwmp rp ON rp.kodecabang = r.kodepusat
          INNER JOIN pwmp.riwayatpwmp rp ON rp.kodecabang in ('KP')
          ";
        } else if ($branchtype == 'area') {
          $joinriwayatpwmpcabang = "
          INNER JOIN master.cabang c ON c.kode = mp.kodecabang
          INNER JOIN pwmp.riwayatpwmp rp ON rp.kodecabang = c.kodearea
          ";
        } else if ($branchtype == 'region') {
          $joinriwayatpwmpcabang = "
          INNER JOIN master.cabang c ON c.kode = mp.kodecabang
          INNER JOIN master.area a ON a.kode = c.kodearea
          INNER JOIN pwmp.riwayatpwmp rp ON rp.kodecabang = a.koderegion
          ";
        } else {
          $joinriwayatpwmpcabang = 'INNER JOIN pwmp.riwayatpwmp rp ON rp.kodecabang = mp.kodecabang';
        }
  
        $sqlParams = [];
        $sqlParams['loanselect'] = $loanselect;
        $sqlParams['processcode'] = $processcode;
        $sqlParams['joinriwayatpwmpcabang'] = $joinriwayatpwmpcabang;
        $sqlParams['position'] = $position;
        $sqlParams['limitselect'] = $limitselect;
        $sqlParams['sumplafonminimal'] = $sumplafonminimal;
        $sqlParams['fieldnikpemutus'] = $fieldnikpemutus;
  
        echo sprintf("--POSITION: %s (%s)\n", $position, $branchtype);
        
        echo "----01/09\n";
        $sSQL = 'TRUNCATE TABLE temp.portfolio_tmp2';
        pg_query($dbconnect, $sSQL);

        $sSQL = 'TRUNCATE TABLE temp.portfolio_tmp3';
        pg_query($dbconnect, $sSQL);
        
        echo "----02/09\n";
        $sSQL = 'TRUNCATE TABLE temp.portfolio_sum2';
        pg_query($dbconnect, $sSQL);
        
        echo "----03/09\n";
        $sSQL = 'TRUNCATE TABLE temp.tmp_loandoublenik';
        pg_query($dbconnect, $sSQL);
        
        echo "----04/09\n";
        $sSQL = "
        INSERT INTO temp.portfolio_tmp2
        {loanselect}
        ";
        $sSQL = formatString($sSQL, $sqlParams);
        pg_query($dbconnect, $sSQL);
        
        echo "----05/09\n";
        if (in_array($iF, [10,11,12,13,14])) {
          $sSQL = "
          INSERT INTO temp.portfolio_tmp3
          SELECT * FROM temp.portfolio_tmp2
          ";
          pg_query($dbconnect, $sSQL);

          $sSQL = "
          DELETE FROM temp.portfolio_tmp2 mt
          WHERE EXISTS (
            SELECT 1 FROM pwmp.portfolio_resolved x
            WHERE x.noloan = mt.noloan
          )
          ";
          pg_query($dbconnect, $sSQL);

          $sSQL = "
          INSERT INTO temp.portfolio_sum2
          SELECT cif, SUM(plafon) AS sumplafon
          FROM temp.portfolio_tmp3 mt
          GROUP BY cif
          ";
          pg_query($dbconnect, $sSQL);  
        } else {
          $sSQL = "
          INSERT INTO temp.portfolio_sum2
          SELECT cif, SUM(plafon) AS sumplafon
          FROM temp.portfolio_tmp2 mt
          GROUP BY cif
          ";
          pg_query($dbconnect, $sSQL);  
        }
        
        echo "----06/09\n";
        $sSQL = "
        INSERT INTO temp.tmp_loandoublenik
        SELECT noloan, COUNT(1) AS countnikpemutus, '{processcode}' AS process FROM (
          SELECT DISTINCT
          mp.noloan,
          rp.nip AS nikpemutus
          FROM temp.portfolio_tmp2 mp
          INNER JOIN temp.portfolio_sum2 ps ON ps.cif = mp.cif 
            AND ps.sumplafon > {sumplafonminimal}
          {joinriwayatpwmpcabang}
            AND rp.tglsk <= mp.tglcair
            AND rp.kodejabatan IN ({position})
            AND {limitselect} >= ps.sumplafon
          INNER JOIN staging.hcsriwayatjabatan rj ON rj.nip = rp.nip 
            AND rj.kodecabang = rp.kodecabang
            AND rj.kodejabatan = rp.kodejabatan
            AND rj.kodejabatan IN ({position})
            --AND rj.tglmulai <= rp.tglsk
            AND rj.tglberakhir >= rp.tglsk
          WHERE 
          --mp.tglcair >= rj.tglmulai
          --AND mp.tglcair <= rj.tglberakhir
          mp.tglcair > rj.tglmulai
          AND mp.tglcair < rj.tglberakhir
        ) g
        GROUP BY noloan
        HAVING COUNT(1) > 1
        ";
        $sSQL = formatString($sSQL, $sqlParams);
        pg_query($dbconnect, $sSQL);
        
        echo "----07/09\n";
        $sSQL = "
        INSERT INTO pwmp.portfolio_invalid (
          noloan,cif,namanasabah,kodecabang,type,
          plafon,ospokok,tglcair,tgljatuhtempo,kolcif,
          styus,secon,segmen,nikpengusul,{fieldnikpemutus},
          flagrestruct,tglrestruct,processinsert)
        SELECT DISTINCT
        mp.noloan,
        mp.cif,
        mp.namanasabah,
        mp.kodecabang,
        mp.type,
        mp.plafon,
        mp.ospokok,
        mp.tglcair,
        mp.tgljatuhtempo,
        mp.kolcif,
        mp.styus,
        mp.secon,
        mp.segmen,
        mp.nikpengusul,
        rp.nip AS nikpemutus,
        mp.flagrestruct,
        mp.tglrestruct,
        '{processcode}' AS process
        FROM temp.portfolio_tmp2 mp
        INNER JOIN temp.portfolio_sum2 ps ON ps.cif = mp.cif 
          AND ps.sumplafon > {sumplafonminimal}
        {joinriwayatpwmpcabang}
          AND rp.tglsk <= mp.tglcair
          AND rp.kodejabatan IN ({position})
          AND {limitselect} >= ps.sumplafon
        INNER JOIN staging.hcsriwayatjabatan rj ON rj.nip = rp.nip 
          AND rj.kodecabang = rp.kodecabang
          AND rj.kodejabatan = rp.kodejabatan
          AND rj.kodejabatan IN ({position})
          --AND rj.tglmulai <= rp.tglsk
          AND rj.tglberakhir >= rp.tglsk
        WHERE 
        --mp.tglcair >= rj.tglmulai
        --AND mp.tglcair <= rj.tglberakhir
        mp.tglcair > rj.tglmulai
        AND mp.tglcair < rj.tglberakhir
        AND EXISTS (
          SELECT 1 FROM temp.tmp_loandoublenik x
          WHERE x.noloan = mp.noloan
        )
        ";
        $sSQL = formatString($sSQL, $sqlParams);
        pg_query($dbconnect, $sSQL);
        
        echo "----08/09\n";
        $sSQL = "
        INSERT INTO pwmp.portfolio_valid (
          noloan,cif,namanasabah,kodecabang,type,
          plafon,ospokok,tglcair,tgljatuhtempo,kolcif,
          styus,secon,segmen,nikpengusul,{fieldnikpemutus},
          flagrestruct,tglrestruct,processinsert)
        SELECT DISTINCT
        mp.noloan,
        mp.cif,
        mp.namanasabah,
        mp.kodecabang,
        mp.type,
        mp.plafon,
        mp.ospokok,
        mp.tglcair,
        mp.tgljatuhtempo,
        mp.kolcif,
        mp.styus,
        mp.secon,
        mp.segmen,
        mp.nikpengusul,
        rp.nip AS nikpemutus,
        mp.flagrestruct,
        mp.tglrestruct,
        '{processcode}' AS process
        FROM temp.portfolio_tmp2 mp
        INNER JOIN temp.portfolio_sum2 ps ON ps.cif = mp.cif
          AND ps.sumplafon > {sumplafonminimal}
        {joinriwayatpwmpcabang}
          AND rp.tglsk <= mp.tglcair
          AND rp.kodejabatan IN ({position})
          AND {limitselect} >= ps.sumplafon
        INNER JOIN staging.hcsriwayatjabatan rj ON rj.nip = rp.nip 
          AND rj.kodecabang = rp.kodecabang
          AND rj.kodejabatan = rp.kodejabatan
          AND rj.kodejabatan IN ({position})
          --AND rj.tglmulai <= rp.tglsk
          AND rj.tglberakhir >= rp.tglsk
        WHERE 
        --mp.tglcair >= rj.tglmulai
        --AND mp.tglcair <= rj.tglberakhir
        mp.tglcair > rj.tglmulai
        AND mp.tglcair < rj.tglberakhir
        AND NOT EXISTS (
          SELECT 1 FROM temp.tmp_loandoublenik x
          WHERE x.noloan = mp.noloan
        )
        AND NOT EXISTS (
          SELECT 1 FROM pwmp.portfolio_valid x
          WHERE x.noloan = mp.noloan
        )
        ";
        $sSQL = formatString($sSQL, $sqlParams);
        pg_query($dbconnect, $sSQL);
  
        if (in_array($iF, [10,11,12])) {
          echo "----09/09\n";
          $sSQL = "
          UPDATE pwmp.portfolio_valid pv
          SET 
          nikpemutusrisk = sq1.nikpemutus,
          processupdate = '{processcode}'
          FROM (
            SELECT DISTINCT
            mp.noloan,
            rp.nip AS nikpemutus
            FROM temp.portfolio_tmp2 mp
            INNER JOIN temp.portfolio_sum2 ps ON ps.cif = mp.cif
              AND ps.sumplafon > {sumplafonminimal}
            {joinriwayatpwmpcabang}
              AND rp.tglsk <= mp.tglcair
              AND rp.kodejabatan IN ({position})
              AND {limitselect} >= ps.sumplafon
            INNER JOIN staging.hcsriwayatjabatan rj ON rj.nip = rp.nip 
              AND rj.kodecabang = rp.kodecabang
              AND rj.kodejabatan = rp.kodejabatan
              AND rj.kodejabatan IN ({position})
              --AND rj.tglmulai <= rp.tglsk
              AND rj.tglberakhir >= rp.tglsk
            WHERE 
            --mp.tglcair >= rj.tglmulai
            --AND mp.tglcair <= rj.tglberakhir
            mp.tglcair > rj.tglmulai
            AND mp.tglcair < rj.tglberakhir
            AND NOT EXISTS (
              SELECT 1 FROM temp.tmp_loandoublenik x
              WHERE x.noloan = mp.noloan
            )
            AND EXISTS (
              SELECT 1 FROM pwmp.portfolio_valid x
              WHERE x.noloan = mp.noloan
            )
          ) sq1 
          WHERE sq1.noloan = pv.noloan
          AND sq1.nikpemutus <> pv.nikpemutusbisnis
          ";
          $sSQL = formatString($sSQL, $sqlParams);
          pg_query($dbconnect, $sSQL);
        } else {
          echo "----09/09: PASS...\n";
        }
      }
    }
    
    setResolvedLoan($dbconnect);
    setQuality($dbconnect);
  
    pg_query("COMMIT");
    echo "Done!";
  
  } catch (Exception $e) {
    pg_query("ROLLBACK");
    echo sprintf("Caught exception: %s \n", $e->getMessage());
  } finally {
    #closing database connection.
    if ($dbconnect) {
      pg_close($dbconnect);
      echo "PostgreSQL connection is closed";
    }
  }  
}

main();
?>