@extends('layouts.backend')
@section('content')
    <div class="container-fluid">

      <!-- Page Heading -->
      <h1 class="h3 mb-2 text-gray-800">PWMP Belum Memenuhi Syarat</h1>
      <p class="mb-4">Daftar Pejabat Pemegang Kewenangan Belum memenuhi Syarat</p>

      <!-- DataTales Example -->
      <div class="card shadow mb-4">
        <div class="card-header">
          <div class="col-md-12">
            <div class="row" style="margin-top: 10px;">
              <div class="col-md-3">
                  <div class="form-group">
                    <label><b>Tanggal Pengajuan</b></label>
                      <form>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <div class="input-group-text"><img src="img/Assessment/kalender icon.png" width="25px" height="20px"></div>
                          </div>
                        <input type="text" class="form-control pull-right" name="daterange"/>
                        </div>
                      </form>
                  </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label><b>Status</b></label>
                    <button type="button" class="form-control" data-toggle="dropdown"></button>
                      <div class="dropdown-menu">
                          <a class="dropdown-item">
                            <!-- Default unchecked -->
                            <div class="custom-control custom-checkbox">
                              <input type="checkbox" class="custom-control-input" id="checkbox1">
                              <label class="custom-control-label" for="checkbox1">Check me</label>
                            </div>
                          <div class="dropdown-divider"></div>
                          </a>
                          <a class="dropdown-item" href="#">
                            <div class="custom-control custom-checkbox">
                              <input type="checkbox" class="custom-control-input" id="checkbox2">
                              <label class="custom-control-label" for="checkbox2">Check me</label>
                            </div>
                          </a>
                          <a class="dropdown-item" href="#">
                            <div class="custom-control custom-checkbox">
                              <input type="checkbox" class="custom-control-input" id="checkbox3">
                              <label class="custom-control-label" for="checkbox3">Check me</label>
                            </div>
                          </a>
                          <a class="dropdown-item" href="#">
                            <div class="custom-control custom-checkbox">
                              <input type="checkbox" class="custom-control-input" id="checkbox4">
                              <label class="custom-control-label" for="checkbox4">Check me</label>
                            </div>
                          </a>
                          <a class="dropdown-item" href="#">
                            <div class="custom-control custom-checkbox">
                              <input type="checkbox" class="custom-control-input" id="checkbox5">
                              <label class="custom-control-label" for="checkbox5">Check me</label>
                            </div>
                          </a>
                          <a class="dropdown-item" href="#">
                            <div class="custom-control custom-checkbox">
                              <input type="checkbox" class="custom-control-input" id="checkbox6">
                              <label class="custom-control-label" for="checkbox6">Check me</label>
                            </div>
                          </a>
                        </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label><b>Unit Kerja</b></label>
                    <select id="unit_kerja" name="cars" class="form-control">
                      <option value="">--</option>
                      <option value="1">Area 1</option>
                      <option value="2">Area 2</option>
                      <option value="3">Area 3</option>
                    </select>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label><b>Region</b></label>
                    <select id="region" name="cars" class="form-control">
                      <option value="">--</option>
                      <option value="afrm">AFRM</option>
                      <option value="hcs">HCS</option>
                    </select>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-9">
                <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-append">
                          <div class="btn btn-success" style="margin-top: 10px;">
                            <i class="fas fa-search fa-sm"></i>
                          </div>
                        </div>
                        <input id="search_nip" type="text" class="form-control" placeholder="Search Keywords" aria-label="Search" aria-describedby="basic-addon2" style="margin-top: 10px;">
                      </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                    <a href="#" class="btn btn-success btn-icon-split" style="margin-top: 10px; margin-top: 10px;border-top-width: 0px;border-left-width: 20px; border-right-width: 20px; margin-right: 20px;">
                      <span class="text">Cari</span>
                    </a>
                    <a href="#" class="btn btn-info btn-icon-split" style="margin-top: 10px; margin-top: 10px;border-top-width: 0px;border-left-width: 20px; border-right-width: 20px;">
                      <span class="text">Reset</span>
                    </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="card-header">
          <div class="col-md-3">
            <div class="form-group">
                <a href="#" class="btn btn-success btn-icon-split" style="margin-top: 10px; margin-top: 10px;border-top-width: 0px;border-left-width: 20px; border-right-width: 20px; margin-right: 20px;">
                  <span class="text">Excel.Xlsx</span>
                </a>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable_kualitas" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>NIP</th>
                  <th>Nama</th>
                  <th>Jabatan</th>
                  <th>Cabang</th>
                  <th>Limit Non Gadai</th>
                  <th>Limit Gadai Non Logam Mulia</th>
                  <th>Limit Gadai Logam Mulia</th>
                  <th>No SK</th>
                  <th>Tanggal SK</th>
                  <th>Manage</th>
                </tr>
              </thead>
              <tbody>
                
              </tbody>
            </table>
          </div>
        </div>
      </div>

    </div>
@endsection

@section('jscustom')

  <script type="text/javascript">
    $(document).ready(function() {

      $("#search_nip").keyup(function() {
        $('#dataTable_kualitas').DataTable().search($(this).val()).draw() ;
      });

        $('#dataTable_kualitas').DataTable({
                "dom": 'lrtip',
                responsive: true,
                deferRender: true,
                language: {
                  emptyTable: "Nip has No History",
                  search: "_INPUT_",
              },
            });
      renderTable();
    });

      $( "#unit_kerja" ).change(function() {

        var kerja = $('#unit_kerja').val();
        var region = $('#region').val();
          if (kerja == "" && region == "") 
          {
            renderTable();
          }else{
            tablePejabat();
          }
      });
      $( "#region" ).change(function() {
        var kerja = $('#unit_kerja').val();
        var region = $('#region').val();
          if (kerja == "" && region == "") 
          {
            renderTable();
          }else{
            tablePejabat();
          }
      });

        function renderTable(){
          var kerja = $('#unit_kerja').val();
          var region = $('#region').val();
              $('#dataTable_kualitas').DataTable().destroy();
              $('#dataTable_kualitas').DataTable({
                "order": [],
                "dom": 'lrtip',
                  responsive: true,
                  deferRender:    true,
                  language: {
                    emptyTable: "No Data in Date Range",
                    search: "_INPUT_",
                      // searchPlaceholder: "Search Patient",
                },
            
                'ajax'       : {
                  "type"   : "GET",
                  "url"    :"{{url('/loadtable/pejabat')}}",
                  
                  "dataSrc": function (jsons) {
                    // console.log(jsons)
                    var return_data = new Array();
                    var index = 1;
                    // check_draw.length = 0;
                    jsons.forEach(function(json){

                      return_data.push({
                      // 'no' : ' ' + index,
                      'dibawah_nip' : json.nip,
                      'dibawah_name' : json.name, 
                      'dibawah_kd_jabatan' : json.kd_jabatan,
                      'dibawah_kd_cabang' : json.kd_cabang,                  
                      'dibawah_limit_non_gadai' : json.limit_non_gadai,
                      'dibawah_limit_non_logam' : json.limit_non_logam,
                      'dibawah_limit_logam' : json.limit_logam,
                      'dibawah_no_sk' : json.no_sk,
                      'dibawah_tanggal_sk' : json.tanggal_sk,
                      'action' : renderActtion(json.id),
                      })

                      index = index + 1;
                    })

                    return return_data;
                  }
                },
                "columns"    : [
                  {'data': 'dibawah_nip'},
                  {'data': 'dibawah_name'},
                  {'data': 'dibawah_kd_jabatan'},
                  {'data': 'dibawah_kd_cabang'},
                  {'data': 'dibawah_limit_non_gadai'},
                  {'data': 'dibawah_limit_non_logam'},
                  {'data': 'dibawah_limit_logam'},
                  {'data': 'dibawah_no_sk'},
                  {'data': 'dibawah_tanggal_sk'},
                  {'data': 'action'},
                ],
              });
            }


      function renderActtion(id){
          var action = '';

          action += '<center>';
            
          action += '<button class="btn btn-sm btn-info btn-fill" style="font-size: 10px" ><span class="ti-trash"></span> View</button>';
          action += '</center>';

          return action;
      }

        function tablePejabat(){
          var kerja = $('#unit_kerja').val();
          var region = $('#region').val();

          console.log(kerja)
          console.log(region)
              $('#dataTable_kualitas').DataTable().destroy();
              $('#dataTable_kualitas').DataTable({
                "order": [],
                "dom": 'lrtip',
                  responsive: true,
                  deferRender:    true,
                  language: {
                    emptyTable: "No Data in Date Range",
                    search: "_INPUT_",
                      // searchPlaceholder: "Search Patient",
                },
            
                'ajax'       : {
                  "type"   : "GET",
                  "url"    :"{{url('/kualitas/pembiayaan/loadtable')}}",
                  "data"   : {
                    "kerja" : kerja,
                    "region" : region,   
                  },
                  "dataSrc": function (jsons) {
                    // console.log(jsons)
                    var return_data = new Array();
                    var index = 1;
                    // check_draw.length = 0;
                    jsons.forEach(function(json){

                      return_data.push({
                      // 'no' : ' ' + index,
                      'dibawah_nip' : json.nip,
                      'dibawah_name' : json.name, 
                      'dibawah_kd_jabatan' : json.kd_jabatan,
                      'dibawah_kd_cabang' : json.kd_cabang,                  
                      'dibawah_limit_non_gadai' : json.limit_non_gadai,
                      'dibawah_limit_non_logam' : json.limit_non_logam,
                      'dibawah_limit_logam' : json.limit_logam,
                      'dibawah_no_sk' : json.no_sk,
                      'dibawah_tanggal_sk' : json.tanggal_sk,
                      'action' : renderActtion(json.id),
                      })

                      index = index + 1;
                    })

                    return return_data;
                  }
                },
                "columns"    : [
                  {'data': 'dibawah_nip'},
                  {'data': 'dibawah_name'},
                  {'data': 'dibawah_kd_jabatan'},
                  {'data': 'dibawah_kd_cabang'},
                  {'data': 'dibawah_limit_non_gadai'},
                  {'data': 'dibawah_limit_non_logam'},
                  {'data': 'dibawah_limit_logam'},
                  {'data': 'dibawah_no_sk'},
                  {'data': 'dibawah_tanggal_sk'},
                  {'data': 'action'},
                ],
              });
            }

  </script>

@stop