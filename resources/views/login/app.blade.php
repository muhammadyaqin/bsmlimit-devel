<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{url('/public/css/login/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('/public/css/login/bootstrap-theme.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{ asset('/public/css/login.css')}}">

    <!-- Scripts -->
    <script src="{{url('/public/css/login/jquery-3.2.1.slim.min.js')}}"></script>
    <script src="{{url('/public/css/login/popper.min.js')}}"></script>
    <script src="{{url('/public/css/login/bootstrap.min.js')}}"></script>

    <title>Login - Limit Wewenang Memutus Pembiayaan</title>

</head>
<body>
    <div id="app">
        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
<script src="{{url('/public/css/login/jquery-1.5.2.js')}}"></script>
</html>
