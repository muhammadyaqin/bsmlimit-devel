@extends('layouts.backend')
@section('content')
  <div class="card">
  	<form method="POST" action="/limit/tambah">
  		<ul class="list-group list-group-flush">
  			<div class="row">
  				<li class="list-group-item header col-lg-12">Rekap Pengajuan Permohonan Limit</li>
  				<li class="list-group-item col-lg-12">
  					<div class="header" id="flip">Filter : </div>
            <div id="panel">
            <div class="row"> 
              <div class="col-lg-3"> Tanggal Pengajuan : <br>
                <form>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <div class="input-group-text"><img src="img/Assessment/kalender icon.png" width="25px" height="20px"></div>
                    </div>
                  <input type="text" name="daterange" />
                  </div>
                </form>
              </div>
              <div class="col-lg-3">
               Status : <br>
               <div class="dropdown">
                <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Pilih Status
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck" name="example1">
                    <label class="custom-control-label" for="customCheck">Semua</label>
                  </div>
                  <div class="dropdown-divider"></div>
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck" name="example1">
                    <label class="custom-control-label" for="customCheck">Baru</label>
                  </div>
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck" name="example1">
                    <label class="custom-control-label" for="customCheck">Direkomendasikan</label>
                  </div>
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck" name="example1">
                    <label class="custom-control-label" for="customCheck">Diproses</label>
                  </div>
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck" name="example1">
                    <label class="custom-control-label" for="customCheck">Disetujui</label>
                  </div>
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck" name="example1">
                    <label class="custom-control-label" for="customCheck">Ditolak</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3">
              Unit Kerja
              <div class="dropdown">
                <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Pilih Unit Kerja
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <a class="dropdown-item" href="#">Action</a>
                  <a class="dropdown-item" href="#">Another action</a>
                  <a class="dropdown-item" href="#">Something else here</a>
                </div>
              </div>
            </div>
          </div>
            <div class="row" style="margin-top: 10px;">
                <form class="d-none d-sm-inline-block form-inline mr-auto navbar-search col-lg-8">
                  <div class="input-group">
                    <button class="btn btn-success" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Cari NIP, Nama..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                    </div>
                  </div>
                </form>
                <div class="col-lg-4">
                  <button type="button" class="btn btn-success">Cari</button>
                  <button type="button" class="btn btn-light">Reset</button>
                </div>
            </div>
          </div>
          </li>
          <li class="list-group-item col-lg-12">
  					<div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>NIP</th>
                      <th>Nama</th>
                      <th>Jabatan</th>
                      <th>Unit Kerja</th>
                      <th>Tgl Pengajuan</th>
                      <th>Nominal Limit</th>
                      <th>Status</th>
                      <th> </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>10514181</td>
                      <td>Mayuga Wicaksana</td>
                      <td>AFRM</td>
                      <td>Area Jakarta</td>
                      <td>2011/04/25</td>
                      <td>Rp. 500.000.000</td>
                      <td><img src="img/status/baru.png" height="8px" width="8px" style="margin-right: 10px;">Baru</td>
                      <td><a href="assessment/detail"><img src="img/icon/cta_opendetail.png" width="30px" height="24px" style="margin-right: 10px;"></a><a href="#"><img src="img/icon/cta_downloadnota.png" width="30px" height="24px"></a></td>
                    </tr>
                    <tr>
                      <td>10514181</td>
                      <td>Mayuga Wicaksana</td>
                      <td>AFRM</td>
                      <td>Area Jakarta</td>
                      <td>2011/04/25</td>
                      <td>Rp. 500.000.000</td>
                      <td><img src="img/status/direkomendasikan.png" height="8px" width="8px" style="margin-right: 10px;">Direkomendasikan</td>
                      <td><a href="assessment/detail"><img src="img/icon/cta_opendetail.png" width="30px" height="24px" style="margin-right: 10px;"></a><a href="#"><img src="img/icon/cta_downloadnota.png" width="30px" height="24px"></a></td>
                    </tr>
                    <tr>
                      <td>10514181</td>
                      <td>Mayuga Wicaksana</td>
                      <td>AFRM</td>
                      <td>Area Jakarta</td>
                      <td>2011/04/25</td>
                      <td>Rp. 500.000.000</td>
                      <td><img src="img/status/erm.png" height="8px" width="8px" style="margin-right: 10px;">Proses ERM</td>
                      <td><a href="assessment/detail"><img src="img/icon/cta_opendetail.png" width="30px" height="24px" style="margin-right: 10px;"></a><a href="#"><img src="img/icon/cta_downloadnota.png" width="30px" height="24px"></a></td>
                    </tr>
                    <tr>
                      <td>10514181</td>
                      <td>Mayuga Wicaksana</td>
                      <td>AFRM</td>
                      <td>Area Jakarta</td>
                      <td>2011/04/25</td>
                      <td>Rp. 500.000.000</td>
                      <td><img src="img/status/setuju.png" height="8px" width="8px" style="margin-right: 10px;">Disetujui</td>
                      <td><a href="assessment/detail"><img src="img/icon/cta_opendetail.png" width="30px" height="24px" style="margin-right: 10px;"></a><a href="#"><img src="img/icon/cta_downloadnota.png" width="30px" height="24px"></a></td>
                    </tr>
                    <tr>
                      <td>10514181</td>
                      <td>Mayuga Wicaksana</td>
                      <td>AFRM</td>
                      <td>Area Jakarta</td>
                      <td>2011/04/25</td>
                      <td>Rp. 500.000.000</td>
                      <td><img src="img/status/ditolak.png" height="8px" width="8px" style="margin-right: 10px;">Ditolak</td>
                      <td><a href="assessment/detail"><img src="img/icon/cta_opendetail.png" width="30px" height="24px" style="margin-right: 10px;"></a><a href="#"><img src="img/icon/cta_downloadnota.png" width="30px" height="24px"></a></td>
                    </tr>
                    <tr>
                      <td>10514181</td>
                      <td>Mayuga Wicaksana</td>
                      <td>AFRM</td>
                      <td>Area Jakarta</td>
                      <td>2011/04/25</td>
                      <td>Rp. 500.000.000</td>
                      <td><img src="img/status/baru.png" height="8px" width="8px" style="margin-right: 10px;">Baru</td>
                      <td><a href="assessment/detail"><img src="img/icon/cta_opendetail.png" width="30px" height="24px" style="margin-right: 10px;"></a><a href="#"><img src="img/icon/cta_downloadnota.png" width="30px" height="24px"></a></td>
                    </tr>
                    <tr>
                      <td>10514181</td>
                      <td>Mayuga Wicaksana</td>
                      <td>AFRM</td>
                      <td>Area Jakarta</td>
                      <td>2011/04/25</td>
                      <td>Rp. 500.000.000</td>
                      <td><img src="img/status/direkomendasikan.png" height="8px" width="8px" style="margin-right: 10px;">Direkomendasikan</td>
                      <td><a href="assessment/detail"><img src="img/icon/cta_opendetail.png" width="30px" height="24px" style="margin-right: 10px;"></a><a href="#"><img src="img/icon/cta_downloadnota.png" width="30px" height="24px"></a></td>
                    </tr>
                    <tr>
                      <td>10514181</td>
                      <td>Mayuga Wicaksana</td>
                      <td>AFRM</td>
                      <td>Area Jakarta</td>
                      <td>2011/04/25</td>
                      <td>Rp. 500.000.000</td>
                      <td><img src="img/status/erm.png" height="8px" width="8px" style="margin-right: 10px;">Proses ERM</td>
                      <td><a href="assessment/detail"><img src="img/icon/cta_opendetail.png" width="30px" height="24px" style="margin-right: 10px;"></a><a href="#"><img src="img/icon/cta_downloadnota.png" width="30px" height="24px"></a></td>
                    </tr>
                    <tr>
                      <td>10514181</td>
                      <td>Mayuga Wicaksana</td>
                      <td>AFRM</td>
                      <td>Area Jakarta</td>
                      <td>2011/04/25</td>
                      <td>Rp. 500.000.000</td>
                      <td><img src="img/status/setuju.png" height="8px" width="8px" style="margin-right: 10px;">Disetujui</td>
                      <td><a href="assessment/detail"><img src="img/icon/cta_opendetail.png" width="30px" height="24px" style="margin-right: 10px;"></a><a href="#"><img src="img/icon/cta_downloadnota.png" width="30px" height="24px"></a></td>
                    </tr>
                    <tr>
                      <td>10514181</td>
                      <td>Mayuga Wicaksana</td>
                      <td>AFRM</td>
                      <td>Area Jakarta</td>
                      <td>2011/04/25</td>
                      <td>Rp. 500.000.000</td>
                      <td><img src="img/status/ditolak.png" height="8px" width="8px" style="margin-right: 10px;">Ditolak</td>
                      <td><a href="assessment/detail"><img src="img/icon/cta_opendetail.png" width="30px" height="24px" style="margin-right: 10px;"></a><a href="#"><img src="img/icon/cta_downloadnota.png" width="30px" height="24px"></a></td>
                    </tr>
                    <tr>
                      <td>10514181</td>
                      <td>Mayuga Wicaksana</td>
                      <td>AFRM</td>
                      <td>Area Jakarta</td>
                      <td>2011/04/25</td>
                      <td>Rp. 500.000.000</td>
                      <td><img src="img/status/setuju.png" height="8px" width="8px" style="margin-right: 10px;">Disetujui</td>
                      <td><a href="assessment/detail"><img src="img/icon/cta_opendetail.png" width="30px" height="24px" style="margin-right: 10px;"></a><a href="#"><img src="img/icon/cta_downloadnota.png" width="30px" height="24px"></a></td>
                    </tr>
                  </tbody>
                </table>
              </div>
  				</li>
  			</div>
  		</div>
  	</ul>
  </form>
</div>
@endsection