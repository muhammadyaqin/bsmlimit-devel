@extends('layouts.backend')
@section('content')

<style type="text/css">
  .select2-container--default .select2-selection--single{
    border: 0
  }
</style>
    <div class="container-fluid">

      <!-- Page Heading -->
      <div class="card border-left-success">
        <div class="card-body">
          <h1 class="h3 mb-2 text-gray-800" style="margin-top: 10px;"><b>Riwayat PWMP</b></h1>
        </div>
      </div>
      <div class="card shadow mb-4">
        <div class="card-header">
          <div class="col-md-12">
          <form action="{{URL('/assessment')}}" method="get">
            <div class="row" style="margin-top: 10px;">
              <div class="col-md-6">
                  <div class="form-group">
                    <label><b>Tgl. Pengajuan</b></label>
                      <form>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <div class="input-group-text"><img src="{{URL::asset('public/img/Assessment/kalender icon.png')}}" width="25px" height="20px"></div>
                          </div>
                          <input type="hidden" value="{{$startDate}}" name="startDate" id="startDate">
                          <input type="hidden" value="{{$endDate}}" name="endDate" id="endDate">
                        <input type="text" class="form-control pull-right" id="daterange_pengajuan" name="test" autocomplete="off" />
                        </div>
                      </form>
                  </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label><b>Status Pengajuan</b></label>
                    <select id="status" name="status" class="form-control">
                        <option value="">ALL</option>
                      @foreach($enumerasi as $enum)
                        <option {{ $keteranganStatus == $enum->kode ? "selected" : ""}} @if($kodearea == $enum->kode) selected @endif value="{{$enum->kode}}">{{$enum->keterangan}}</option>
                      @endforeach
                    </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label><b>Area</b></label>
                    <select id="area" name="area" class="form-control">
                        <option value="">ALL</option>
                      @foreach($getarea as $area)
                        <option @if($kodearea == $area->kode) selected @endif value="{{$area->kode}}">{{$area->nama}}</option>
                      @endforeach
                    </select>
                </div>
              </div>
               <div class="col-md-6">
                <div class="form-group">
                  <label><b>Keywords</b></label>
                    <input type="text" name="name_nip" value="{{$name_nip}}" placeholder="Nama atau NIP" class="form-control" id="name_nip"  onkeyup="this.value = this.value.toUpperCase();">
                </div>
              </div>
                <div class="form-group" style="margin-left: 10px;">
                  <input type="submit" name="filterform" value="Search" class="btn btn-success" style="margin-top: 8px;">
                  <a href="{{url('/assessment')}}" class="btn btn-info" style="text-align:left; margin-top: 8px;">Reset</a>
                  <input type="hidden" name="excel" value="1">
                  <input type="submit" name="filterform" value="Export Excel" class="btn btn-primary" style="padding-left: 20px; padding-right: 20px; margin-top: 8px;">
                </div>
            </div>
          </form>
          </div>
        </div>
        <div class="card-body">
            <style type="text/css">
              table#grid_riwayatpwmp {
                width: 100%;
              }
              table#grid_riwayatpwmp th {
                font-size: 12px;
                text-align: center;
              }
              table#grid_riwayatpwmp th.tanggal {
                width: 90px;
              }
              table#grid_riwayatpwmp th.manage {
                width: 80px;
              }
              table#grid_riwayatpwmp td {
                font-size: 12px;
                vertical-align: top;
              }
              table#grid_riwayatpwmp td.nominal {
                text-align: right
              }
              table#grid_riwayatpwmp td.statuspengajuan {
                text-align: center
              }
              table#grid_riwayatpwmp td.statuspengajuan div.statusdefault {
                padding: 0px 3px;
                color: white;
              }
              table#grid_riwayatpwmp td.statuspengajuan div.statuswarning {
                color: black;
              }
              table#grid_riwayatpwmp td.manage {
                text-align: center
              }
              table#grid_riwayatpwmp td.manage a {
                margin-right: 1px;
              }
              table#grid_riwayatpwmp td.no_index {
                text-align: center;
              }
            </style>
            <table id="grid_riwayatpwmp">
              <tr>
                <th>No</th>
                <th>NIP</th>
                <th>Nama</th>
                <th>Limit Non Gadai</th>
                <th>Limit Gadai<br/>Non Logam Mulia</th>
                <th>Limit Gadai<br/>Logam Mulia</th>
                <th>Limit Otomatis</th>
                <th class="tanggal">Tgl. SK</th>
                <th>Status Pengajuan</th>
                <th class="tanggal">Tgl. Pengajuan</th>
                <th class="manage">Manage</th>
              </tr>
              @foreach($pwmp as $index => $riwayat_pwmp)
                <tr>
                  <td class="no_index">{{ $numbering+$index+1 }}</td>
                  <td>{{ $riwayat_pwmp->nip }}</td>
                  <td>{{ $riwayat_pwmp->namapejabat }}</td>
                  @if ($riwayat_pwmp->status != 4)
                  <td class="nominal">{{ number_format($riwayat_pwmp->limitnongadaifix, 0, ',', '.') }}</td>
                  <td class="nominal">{{ number_format($riwayat_pwmp->limitgadainlmfix, 0, ',', '.') }}</td>
                  <td class="nominal">{{ number_format($riwayat_pwmp->limitgadailmfix, 0, ',', '.') }}</td>
                  <td class="nominal">{{ number_format($riwayat_pwmp->limitotomatisfix, 0, ',', '.') }}</td>
                  <td class="nominal">{{ \Carbon\Carbon::parse($riwayat_pwmp->tglskfix)->format('d-m-Y') }}</td>
                  @else
                  <td class="nominal">{{ number_format($riwayat_pwmp->limitnongadaifix, 0, ',', '.') }}</td>
                  <td class="nominal">{{ number_format($riwayat_pwmp->limitgadainlmfix, 0, ',', '.') }}</td>
                  <td class="nominal">{{ number_format($riwayat_pwmp->limitgadailmfix, 0, ',', '.') }}</td>
                  <td class="nominal">{{ number_format($riwayat_pwmp->limitotomatisfix, 0, ',', '.') }}</td>
                  <td class="nominal">{{ \Carbon\Carbon::parse($riwayat_pwmp->tglskfix)->format('d-m-Y') }}</td>
                  @endif 
                  <td class="statuspengajuan">
                    @if ($riwayat_pwmp->status == 1)
                      <div class="card bg-info text-white shadow">
                        <div class="card-body statusdefault">
                          <div class="text-white-10 xs">{{$riwayat_pwmp->statuspengajuan}}</div>
                        </div>
                      </div>
                    @elseif ($riwayat_pwmp->status == 2)
                      <div class="card bg-warning text-white shadow">
                        <div class="card-body statusdefault statuswarning">
                          <div class="text-white-10 xs">{{$riwayat_pwmp->statuspengajuan}}</div>
                        </div>
                      </div>
                    @elseif ($riwayat_pwmp->status == 3)
                      <div class="card bg-primary text-white text-white shadow">
                        <div class="card-body statusdefault">
                          <div class="text-white-10 xs">{{$riwayat_pwmp->statuspengajuan}}</div>
                        </div>
                      </div>
                    @elseif ($riwayat_pwmp->status == 4)
                      <div class="card bg-success text-white shadow">
                        <div class="card-body statusdefault">
                          <div class="text-white-10 xs">{{$riwayat_pwmp->statuspengajuan}}</div>
                        </div>
                      </div>
                    @elseif ($riwayat_pwmp->status == 5)
                      <div class="card bg-danger text-white shadow">
                        <div class="card-body statusdefault">
                          <div class="text-white-10 xs">{{$riwayat_pwmp->statuspengajuan}}</div>
                        </div>
                      </div>
                    @elseif ($riwayat_pwmp->status == 6)
                      <div class="card bg-danger text-white shadow">
                        <div class="card-body statusdefault">
                          <div class="text-white-10 xs">{{$riwayat_pwmp->statuspengajuan}}</div>
                        </div>
                      </div>
                    @endif
                  </td>
                  <td>{{ \Carbon\Carbon::parse($riwayat_pwmp->created_at)->format('d-m-Y H:i:s') }}</td>
                  <td class="manage">
                    <a href="{{URL::to('/assessment/'.$riwayat_pwmp->id.'/detail')}}"><img src="{{url::asset('public/img/icon/cta_opendetail.png')}}" width="26px"></a>
                  </td>
                </tr>
              @endforeach
            </table>
           <div class="row pull-right mr-3 mt-3 justify-content-end" >
                    {{ $pwmp->appends(request()->except('page'))->links() }}
            </div>
        </div>
      </div>

    </div>

@endsection

@section('jscustom')

  <script type="text/javascript">
    $(document).ready(function() {
      $('#daterange_pengajuan').val($('#startDate').val() + ' - ' + $('#endDate').val())
      $('#daterange_pengajuan').daterangepicker({
        locale:{
          format:'DD-MM-YYYY'
        }
      })
      $("#search_nip").keyup(function() {
        $('#dataTable_ass').DataTable().search($(this).val()).draw() ;
      });

        $('#dataTable_ass').DataTable({
            "dom": 'lrtip',
            responsive: true,
            deferRender: true,
            language: {
              emptyTable: "Nip has No History",
              search: "_INPUT_",
          },
        });
    });

    $('#daterange_pengajuan').on('apply.daterangepicker', function(ev, picker) {
      $('#startDate').val(picker.startDate.format('DD-MM-YYYY'))
      $('#endDate').val(picker.endDate.format('DD-MM-YYYY'))
    });

    $('#area').select2();
    $('body').find('span.select2-selection__rendered').addClass('form-control')
    $('body').find('span.select2-selection--single').css({
      border:0
    })

    $('#status').select2();
    $('body').find('span.select2-selection__rendered').addClass('form-control')
    $('body').find('span.select2-selection--single').css({
      border:0
    })
  </script>

  <style>
    table {
      border-collapse: collapse;
      width: 30px;
    }

    td, th {
      border: 1px solid #dddddd;
      text-align: left;
      padding: 8px;
    }

    tr:nth-child(even) {

    }
  </style>

@stop