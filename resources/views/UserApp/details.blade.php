@extends('Administrator.Data-master.layouts.master')

@section('content')
<div class="container-fluid">
    <div class="card border-left-success">
        <div class="card-header">
            <h1 class="h3 mb-2 text-gray-800" style="margin-top: 10px;"><b>Detail User</b></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form class="form-horizontal" id="product" action="{{url('/Administrator/Data-master/jabatan/store')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 text-right control-label col-form-label" required>Kode Jabatan</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="kode" value="{{$user->name}}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 text-right control-label col-form-label" required>Jenis Jabatan</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="keterangan" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 text-right control-label col-form-label" required>level</label>
                                    <div class="col-sm-9">
                                        <input type="number" name="level" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 text-right control-label col-form-label" required>Parent</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="parent" class="form-control">
                                    </div>
                                </div>
                                {{-- <div class="form-group row">
                                    <label class="col-sm-3 text-right control-label col-form-label">Status</label>
                                    <div class="col-sm-9">
                                        <input type="number" name="status" class="form-control">
                                    </div>
                                </div>             --}}
                            </div>
                        </div>
                        <div class="card-footer">
                            <a href="{{URL('/user')}}" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Back</a> 
                            <button class="btn btn-primary pull-right" type="submit" name="submit_image" value="Upload Image"><i class="fa fa-save"></i> Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection