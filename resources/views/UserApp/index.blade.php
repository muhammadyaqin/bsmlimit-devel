@extends('Administrator.Data-master.layouts.master')

@section('content')
    <div class="container-fluid mt-5">
      <div class="card border-left-success">
            <div class="card-header">
                <h1 class="h3 mb-2 text-gray-800" style="margin-top: 10px;"><b>Daftar User</b></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-2">
                    <div class="card-body">
                        <div class="row">
                            <div></div>
                              <div class="col-md-9 pull-left">
                                  <a href="{{url('/user/create')}}" class="btn btn-primary ml-3"><i class="fa fa-plus"></i>Tambah Data</a>
                                  <form action="{{url('/user')}}" method="get">
                                      <div class="row" style="margin-top: 10px;">
                                          <div class="col-md-4">
                                              <div class="form-group">
                                                  <label><b style="margin-left: 15px;">Keywords</b></label>
                                                <input type="text" name="nameNIP" style="margin-left: 15px;" value="" placeholder="Email" class="form-control" id="nameNIP">
                                              </div>
                                          </div>
                                          <!-- <div class="col-md-3">
                                              <div class="form-group">
                                                  <label><b>-</b></label>
                                                  <input type="text" name="kodebaru" value="" placeholder="Kode Baru" class="form-control" id="kodebaru"  onkeyup="this.value = this.value.toUpperCase();">
                                              </div>
                                          </div> -->
                                          <div class="col-md-6 mt-4">
                                              <div class="form-group">
                                                  <button class="btn btn-success" value="Search" type="submit" style="width:90px; text-align:left; margin-top: 8px;">
                                                      <i class="fa fa-search"></i>Search</button>
                                                  {{-- <input type="submit" value="Search" class="btn btn-success" style="margin-top: 8px;" class="fa fa-search"> --}}
                                                  
                                                      <a href="{{url('/user')}}" class="btn btn-info" style="text-align:left; margin-top: 8px;">Reset</a>
                                              </div>
                                          </div>
                                      </div>
                                  </form>
                              </div>
                            <div class="col-md-3 pull-right">
                                Halaman : {{$user->currentPage() }} <br/>
                                Jumlah Data : 1 -
                                {{$user->perPage() }}
                                dari :
                                {{$user->total() }} <br/>
                            </div>
                        </div>
                        <div style="min-height:300px">
                            <table class="table table-responsive-sm">
                                <thead>
                                    <tr>
                                        <th class="text-center">No</th>
                                        <th>Email</th>
                                        <th>Status Aktif</th>
                                        <th><center>Action</center></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $no = 1; ?>
                                   @foreach ($user as $item)
                                   <tr>
                                   <td class="text-center">{{ $no++ }}</td>
                                       <td>{{$item->email}}</td>
                                       <td>@if($item->statusaktif == 'T')
                                          <div class="card bg-success text-white shadow">
                                            <center>Aktif</center>
                                          </div>
                                          @else
                                          <div class="card bg-danger text-white shadow">
                                            <center>Tidak Aktif</center>
                                          </div>
                                          @endif
                                       </td>
                                       <td>
                                          <div class="row justify-content-md-center">
                                            <div class="col col-sm-3">
                                              <a href="{{ url('/user/'.$item->email. '/edit') }}">
                                                  <i class="fa fa-pencil" style="color:cornflowerblue"></i>
                                              </a>
                                            </div>

                                            <div class="col col-sm-3">
                                              <form action="{{ url('user/'.$item->email) }}" method="POST" style="display: inline;">
                                                {{ csrf_field() }}
                                                {{ method_field('delete') }}
                                                  <!-- <button type="submit" onclick="return confirm('Are you sure want to delete this user?');" class="btn btn-danger" value="Delete"></button> -->
                                                  <button type="submit" style="background:none; border:none;" onclick="return confirm('Are you sure want to delete this user?');" >
                                                    <img src="{{url('public/img/icon/cta_delete.jpeg')}}" width="26px">
                                                  </button>
                                              </form>
                                            </div>
                                          </div>
                                       </td>
                                    </tr>
                                    @endforeach
                                </tbody>

                            </table>
                        </div>
                        <div class="row pull-right mr-3 mt-3" >
                            {{ $user->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
