@extends('Administrator.Data-master.layouts.master')

@section('content')
<div class="container-fluid">
    <div class="card border-left-success">
        <div class="card-header">
            <h1 class="h3 mb-2 text-gray-800" style="margin-top: 10px;"><b>Edit User</b></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{url('/user/update/'.$user->email)}}" method="POST"  enctype="multipart/form-data">
                    {{ csrf_field() }}
                        <div class="row">
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 text-right control-label col-form-label" required>Email</label>
                                    <div class="col-sm-9">
                                        <input type="email" value="{{$user->email}}" name="email" class="form-control"> 
                                        <input hidden type="email" value="{{$user->email}}" name="pk" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 text-right control-label col-form-label" required>Role</label>
                                    <div class="col-sm-9">
                                        <select id="role" name="role" class="form-control">
                                        <option value="{{$user->role}}">{{$user->role}}</option>
                                            @foreach ($roles as $role)
                                                @if($user->role != $role->keterangan)
                                                    <option value="{{$role->keterangan}}">{{$role->keterangan}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <a href="{{url('/user_manajemen/user_role/index')}}" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Back</a> 
                            <button class="btn btn-primary pull-right" type="submit" name="submit"><i class="fa fa-save"></i> Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection