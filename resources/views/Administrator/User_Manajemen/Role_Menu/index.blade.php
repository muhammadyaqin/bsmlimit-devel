@extends('Administrator.Data-master.layouts.master')
@section('content')
<div class="container-fluid mt-5">
    <div class="card border-left-success">
        <div class="card-header">
            <h1 class="h3 mb-2 text-gray-800" style="margin-top: 10px;"><b>User Role</b></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card shadow-2">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-9 pull-left" style="width:100%">
                            <a href="{{url('/user_manajemen/user_role/create')}}" class="btn btn-primary ml-3"><i class="fa fa-plus"></i>Tambah Data</a>
                        </div>
                        <div class="col-md-3 pull-right">
                            Halaman : {{$userole->currentPage() }} <br/>
                            Jumlah Data : 1 -
                            {{$userole->perPage() }}
                            dari :
                            {{$userole->total() }} <br/>
                        </div>
                    </div>
                    <div style="min-height:300px">
                        <table class="table table-responsive-sm">
                            <thead>
                                <tr class="text-center">
                                    <th style="width:250px">Email</th>
                                    <th style="width:250px">Role</th>
                                    <!-- <th style="width:250px">Action</th> -->
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($userole as $item)
                                <tr class="text-center">
                                    <td> {{$item->email}} </td>
                                    <td> {{$item->role}} </td>
                                    <!-- <td>
                                       <div class="row justify-content-md-center">
                                         <div class="col col-sm-3">
                                           <a href="{{ url('/user-role/'.$item->email. '/edit') }}">
                                               <i class="fa fa-pencil" style="color:cornflowerblue"></i>
                                           </a>
                                         </div>

                                         <div class="col col-sm-3">
                                           <form action="{{ url('user/'.$item->email) }}" method="POST" style="display: inline;">
                                             {{ csrf_field() }}
                                             {{ method_field('delete') }}
                                               <button type="submit" onclick="return confirm('Are you sure want to delete this user?');" class="btn btn-danger" value="Delete"></button>
                                               <button type="submit" style="background:none; border:none;" onclick="return confirm('Are you sure want to delete this user?');" >
                                                 <img src="{{url('img/icon/cta_delete.jpeg')}}" width="26px">
                                               </button>
                                           </form>
                                         </div>
                                       </div>
                                    </td> -->
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row pull-right mr-3 mt-3" >
                        {{ $userole->appends(request()->except('page'))->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
