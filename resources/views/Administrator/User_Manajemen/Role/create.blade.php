@extends('Administrator.Data-master.layouts.master')

@section('content')
<div class="container-fluid">
    <div class="card border-left-success">
        <div class="card-header">
            <h1 class="h3 mb-2 text-gray-800" style="margin-top: 10px;"><b>Form Tambah Role</b></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <form class="form-horizontal" id="product" action="{{url('/user_manajemen/role/store')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                    <div class="card-body">                
                        <div class="row">
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 text-right control-label col-form-label" required>Kode</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="kode" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 text-right control-label col-form-label" required>Keterangan</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="keterangan" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <a href="{{url('/user_manajemen/role/index')}}" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Back</a>
                        <button class="btn btn-primary pull-right" type="submit" name="submit_image" value="Upload Image"><i class="fa fa-save"></i> Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
