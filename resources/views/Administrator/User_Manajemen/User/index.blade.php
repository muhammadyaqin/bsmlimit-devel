@extends('Administrator.Data-master.layouts.master')
@section('content')
<div class="container-fluid mt-5">
    <div class="card border-left-success">
        <div class="card-header">
            <h1 class="h3 mb-2 text-gray-800" style="margin-top: 10px;"><b>Daftar User</b></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card shadow-2">
                <div class="card-body">
                    <div class="row">
                        <div class="toolbar mb-3 mr-5" style="width:100%">
                            <a href="{{url('/user_manajemen/user/create')}}" class="btn btn-primary ml-3"><i class="fa fa-plus"></i>Tambah Data</a>
                        </div>
                    </div>
                    <div style="min-height:300px">
                        <table class="table table-responsive-sm">
                            <thead>
                                <tr class="text-center">
                                    <th style="width:50px">#</th>
                                    <th style="width:250px">Nama</th>
                                    <th style="width:250px">NIP</th>
                                    <th style="width:250px">Email</th>
                                    <th style="width:250px">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($user as $item)
                                <tr class="text-center">
                                    <td> # </td>
                                    <td> {{$item->name}} </td>
                                    <td> {{$item->NIP}} </td>
                                    <td> no field </td>
                                    <td>
                                        <div class="btn-group">
                                            <a href=" {{url('/user_manajemen/user/edit/'.$item->id)}} " class="dropdown-item"> <i class="fa fa-edit  text-warning"> Edit</i> </a>
                                            <a id="delete" data-id={{$item->id}} href="{{url('/user_manajemen/user/delete/'.$item->id)}}" class="dropdown-item"> <i class="fa fa-trash text-danger"> Delete</i> </a>
                                            <form id="delete-{{$item->id}}" method="POST" action="{{url('/user_manajemen/user/delete/'.$item->id)}}">
                                                {{ csrf_field() }}
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
