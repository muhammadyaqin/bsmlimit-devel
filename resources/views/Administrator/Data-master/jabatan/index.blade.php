@extends('Administrator.Data-master.layouts.master')

@section('content')
<style>
    .featured-box .collapsible-detail {
      cursor: pointer;
      padding: 5px;
      width: 100%;
      font-size: 15px;
      outline: none;
      border: none;
      background-color: transparent;
      z-index: 1;
    }
    .activedetail, .collapsible-detail:hover {
      font-weight: bold;
    }
    .collapsible-detail:after {
      content: "\25BC";
      margin-left: 5px;
    }
    .activedetail:after {
      content: "\25B2";
    }
    
    hr {
      border: 0;
      border-top: 1px solid #808080;
      width: 115%;
      position: relative;
      left: -20px;
    }
    .content {
      padding: 0 18px;
      max-height: 0;
      overflow: hidden;
      transition: max-height 0.2s ease-out;
      background-color: transparent;
    }
    .spn {
      padding-left: 5px;
      display: inline-flex;
      width: 95%;
    }
    .fsize-90 {
      font-size: 90%;
    }
    .activedetail, .collapsible-detail:hover {
        color: #18B696;
    }
</style>
    <div class="container-fluid mt-5">
        <div class="card border-left-success">
            <div class="card-header">
                <h1 class="h3 mb-2 text-gray-800" style="margin-top: 10px;"><b>Daftar Jabatan</b></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-2">
                    <div class="card-header">Daftar Jabatan</div>

                    <div class="featured-box featured-box-primary text-left" style="width:100%;">
                        <div class="box-content">
                            <button class="collapsible-detail text-left pl-3 pr-3 mb-2"><strong>Filter :</strong></button>
                            <div class="content">
                                <div class="container mb-3">
                                    <div class="row">    
                                        <form action="{{url('/Administrator/Data-master/jabatan')}}" method="get">
                                            <div class="row" style="margin-top: 10px;">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label><b>Keywords</b></label>
                                                        <input type="text" name="kodejenis" value="{{$kodejenis}}" placeholder="Kode Jabatan, Level atau Jenis Jabatan" class="form-control" id="kodejenis"  onkeyup="this.value = this.value.toUpperCase();">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label><b>Dropdown</b></label>                                            
                                                        <select name="selectparent" class="form-control">
                                                            <option value="">Pilih Atasan</option>
                                                            @foreach($atasan as $key => $ats)
                                                                @if($statusActive == 'notActive' AND $ats->kode == $selectparent)
                                                                    <option value="{{$ats->kode}}" selected>{{$ats->keterangan}}</option>
                                                                @else
                                                                    <option value="{{$ats->kode}}">{{$ats->keterangan}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>                                        
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <label></label>
                                                    <div class="form-group">
                                                        <button class="btn btn-success" value="Search" type="submit" style="width:90px; text-align:left; margin-top: 8px;">
                                                            <i class="fa fa-search"></i>Search</button>
                                                        {{-- <input type="submit" value="Search" class="btn btn-success" style="margin-top: 8px;" class="fa fa-search"> --}}
                                                        @if($statusActive == 'notActive')
                                                            <a href="{{url('/Administrator/Data-master/jabatan')}}" class="btn btn-info" style="text-align:left; margin-top: 8px;">Reset</a>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                  
                    <div class="card-body">
                        <div class="row mb-3">
                            <div class="col-md-9 pull-left">
                                <a href="{{url('/Administrator/Data-master/jabatan/export_excel')}}" class="btn btn-success btn-sm ml-3"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export.Xlsx </a>
                                <a href="{{url('/Administrator/Data-master/jabatan/create')}}" class="btn btn-primary btn-sm ml-3"><i class="fa fa-plus"></i> Tambah Data</a>
                                {{-- <a href="{{url('/Administrator/Data-master/jabatan/export_excel')}}" class="btn btn-secondary btn-sm ml-3"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export.Xlsx </a>
                                <a href="{{url('/Administrator/Data-master/jabatan/create')}}" class="btn btn-primary btn-sm ml-3"><i class="fa fa-plus"></i> Tambah Data</a> --}}
                            </div>
                            {{-- <div class="col-md-3">
                                Halaman : {{ $produkpembiayaan->currentPage() }} <br/>
                                Ditampilkan : 1 - {{ $jabat->perPage() }} 
                                dari : {{ $jabat->total() }} <br/>
                                <a href="{{url('/Administrator/Data-master/jabatan/export_excel')}}" class="btn btn-success btn-sm ml-3"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export.Xlsx </a>
                                <a href="{{url('/Administrator/Data-master/jabatan/create')}}" class="btn btn-primary btn-sm ml-3"><i class="fa fa-plus"></i> Tambah Data</a>
                            </div> --}}
                            <div class="col-md-3 pull-right">
                                {{-- Halaman : {{$jabat->currentPage() }} <br/> --}}
                                Ditampilkan : 1 -
                                {{$jabat->perPage() }}
                                dari :
                                {{$jabat->total() }} <br/>
                            </div>
                        </div>

                        <div style="min-height:300px">
                            <table class="table table-responsive-sm">
                                <thead>
                                    <tr>
                                        <th style="width:50px">#</th>
                                        <th style="width:250px">Kode Jabatan</th>
                                        <th style="width:250px">Jenis Jabatan</th>
                                        <th style="width:250px">Level</th>
                                        <th style="width:250px">Atasan</th>
                                        <th style="width:250px">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($jabat as $i=>$jabats)
                                    <tr>
                                        <td> {{$i+1}} </td>
                                        <td> {{ $jabats->kode }} </td>
                                        <td> {{ $jabats->keterangan }} </td>
                                        <td> {{ $jabats->level }} </td>
                                        <td>
                                            @foreach($atasan as $key => $ats)
                                                @if($ats->kode == $jabats->parent)
                                                    {{ $ats->keterangan }}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            <div class="row">
                                                <div class="col col-sm-3">
                                                    <a href=" {{url('/Administrator/Data-master/jabatan/edit/'.$jabats->kode)}}">
                                                        <i class="fa fa-pencil" style="color:cornflowerblue"></i>
                                                    </a>
                                                </div>

                                                <div class="col col-sm-3">
                                                    <form action="{{url('/Administrator/Data-master/jabatan/delete/'.$jabats->kode)}}" method="POST" style="display: inline;">
                                                        {{ csrf_field() }}
                                                        <button type="submit" style="background:none; border:none;" onclick="return confirm('Anda yakin akan menghapus data jabatan dengan Kode Jabatan = {{$jabats->kode}} ?');" >
                                                            <img src="{{url('public/img/icon/cta_delete.jpeg')}}" width="26px">
                                                        </button>
                                                    </form>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="row pull-right mr-3 mt-3" >
                            {{ $jabat->appends(request()->except('page'))->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <script>
        var coll = document.getElementsByClassName("collapsible-detail");
        var i;
        
        for (i = 0; i < coll.length; i++) {
            coll[i].addEventListener("click", function() {
            this.classList.toggle("activedetail");
            var content = this.nextElementSibling;
            if (content.style.maxHeight){
                content.style.maxHeight = null;
            } else {
                content.style.maxHeight = content.scrollHeight + "px";
            } 
            });
        }
    </script>

@endsection
