@extends('Administrator.Data-master.layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="card border-left-success">
            <div class="card-header">
                <h1 class="h3 mb-2 text-gray-800" style="margin-top: 10px;"><b>Form Edit Enumerasi</b></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form class="form-horizontal" id="product" action="{{url('/Administrator/Data-master/enumerasi/update/'.$enumerasi->kode)}}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-sm-12 col-lg-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 text-right control-label col-form-label" required>Kode</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="kode" class="form-control" value="{{$enumerasi->kode}}" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 text-right control-label col-form-label" required>Kategori</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="kategori" class="form-control" value="{{$enumerasi->kategori}}" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 text-right control-label col-form-label" required>Keterangan</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="keterangan" class="form-control" value="{{$enumerasi->keterangan}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <div class="card-footer">
                            <a href="{{url('/Administrator/Data-master/enumerasi')}}" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Back</a> 
                            <button class="btn btn-primary pull-right" type="submit" name="submit" value="Upload"><i class="fa fa-save"></i> Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

