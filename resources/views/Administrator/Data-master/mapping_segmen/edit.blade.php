@extends('Administrator.Data-master.layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <strong>Form Product</strong> 
                    </div>
                    <div class="card-body">
                        <form class="form-horizontal" id="product" action="{{url('/Administrator/Data-master/mappingsegmen/update/')}}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-sm-12 col-lg-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 text-right control-label col-form-label">Kode Segmen</label>
                                        <div class="col-sm-9">
                                            <input type="text" value={{$map_segmen->id}} name="id" class="form-control" hidden>
                                            <input type="text" value={{$map_segmen->kodesegmen}} name="kodesegmen" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 text-right control-label col-form-label">Jenis Segmen</label>
                                        <div class="col-sm-9">
                                            <input type="text" value={{$map_segmen->jenis_segmen}} name="jenis_segmen"class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <div class="card-footer">
                            <div class="float-right">
                                <a href="{{url('/Administrator/Data-master/mappingsegmen')}}" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Back</a> 
                                <button class="btn btn-primary" type="submit" name="submit" value="Upload Image"><i class="fa fa-save"></i> Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

