@extends('Administrator.Data-master.layouts.master')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <strong>Form Product</strong> 
                </div>
                <div class="card-body">
                    <form class="form-horizontal" id="product" action="{{url('/Administrator/Data-master/mappingsegmen/store')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 text-right control-label col-form-label" required>Kode Segmen</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="kodesegmen" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 text-right control-label col-form-label" required>Jenis Segmen</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="jenis_segmen" class="form-control">
                                    </div>
                                </div>            
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="float-right">
                                <a href="{{url('/Administrator/Data-master/mappingsegmen')}}" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Back</a> 
                                <button class="btn btn-primary" type="submit" name="submit_image" value="Upload Image"><i class="fa fa-save"></i> Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection