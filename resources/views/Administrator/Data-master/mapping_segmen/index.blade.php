@extends('Administrator.Data-master.layouts.master')

@section('content')
<style>
    .featured-box .collapsible-detail {
      cursor: pointer;
      padding: 5px;
      width: 100%;
      font-size: 15px;
      outline: none;
      border: none;
      background-color: transparent;
      z-index: 1;
    }
    .activedetail, .collapsible-detail:hover {
      font-weight: bold;
    }
    .collapsible-detail:after {
      content: "\25BC";
      margin-left: 5px;
    }
    .activedetail:after {
      content: "\25B2";
    }
    
    hr {
      border: 0;
      border-top: 1px solid #808080;
      width: 115%;
      position: relative;
      left: -20px;
    }
    .content {
      padding: 0 18px;
      max-height: 0;
      overflow: hidden;
      transition: max-height 0.2s ease-out;
      background-color: transparent;
    }
    .spn {
      padding-left: 5px;
      display: inline-flex;
      width: 95%;
    }
    .fsize-90 {
      font-size: 90%;
    }
    .activedetail, .collapsible-detail:hover {
        color: #18B696;
    }
</style>
    <div class="container-fluid mt-5">
        <div class="card border-left-success">
            <div class="card-header">
                <h1 class="h3 mb-2 text-gray-800" style="margin-top: 10px;"><b>Daftar Mapping Segmen</b></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-2">
                    <div class="featured-box featured-box-primary text-left" style="width:100%;">
                        <div class="box-content">
                            <button class="collapsible-detail text-left pl-3 pr-3 mb-2"><strong>Filter :</strong></button>
                            <div class="content">
                                <div class="container mb-3">
                                    <div class="row">
                                        <div class="col lg-12">                    
                                            <form action="  " class="form-inline" method="get">
                                                <div class="col lg-3 text-center pl-md-5">
                                                    <label for="kota" >Level : </label>
                                                    <select id="kota" class="form-control" name="kota">
                                                        <option value="">Level</option>
                                                    <option value="#" > kota </option>
                                                    </select>
                                                </div>
                                                <div class="col lg-3 text-center pl-md-5">
                                                    <label for="toko">Atasan : </label>
                                                    <select id="toko" class="form-control" name="toko">
                                                        <option value="">Atasan</option>
                                                            <option value="#">toko</option>
                                                    </select>
                                                </div>
                                                <div class="col lg-3 text-center pl-md-5">
                                                    <label>Search : </label>
                                                    <input class="form-control form-control ml-3 w-75" type="text" placeholder="Search" aria-label="Search">
                                                </div>
                                                <div class="col lg-3 input-group rounded pl-md-5">
                                                    <span class="input-group-prepend">
                                                        <button class="btn btn-primary" type="submit" style="border-top-right-radius: 4px !important;border-bottom-right-radius:4px !important;">
                                                        <i class="fa fa-search"></i> Cari</button>
                                                        &nbsp;
                                                        <button class="btn btn-primary" type="submit" style="border-top-right-radius: 4px !important;border-bottom-right-radius:4px !important;">
                                                            Reset</button>
                                                    </span>
                                                </div>
                                            </form>    
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>

                    <div class="card-body">
                        <div class="row">
                            <div class="toolbar mb-3 mr-5" style="width:100%">
                                <a href="{{url('/Administrator/Data-master/mappingsegmen/create')}}" class="btn btn-primary pull-right ml-3"><i class="fa fa-plus"></i>Tambah Data</a>
                            </div>
                        </div>
                        <div style="min-height:300px">
                            <table class="table table-responsive-sm">
                                <thead>
                                    <tr>
                                        <th style="width:50px">#</th>
                                        <th style="width:250px">Kode Segmen</th>
                                        <th style="width:250px">Jenis Segmen</th>
                                        <th style="width:250px">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($map_segmen as $item)
                                    <tr>
                                        <td> # </td>
                                        <td> {{$item->kodesegmen}} </td>
                                        <td> {{$item->jenis_segmen}} </td>
                                        <td>
                                            <div class="row">
                                                <div class="col col-sm-3">
                                                    <a href=" {{url('/Administrator/Data-master/mappingsegmen/edit/'.$item->id)}} ">
                                                        <i class="fa fa-pencil" style="color:cornflowerblue"></i>
                                                    </a>
                                                </div>

                                                <div class="col col-sm-3">
                                                    <a id="delete" data-id={{$item->id}} href="{{url('/Administrator/Data-master/mappingsegmen/delete/'.$item->id)}}">
                                                        <img src="{{url('public/img/icon/cta_delete.jpeg')}}" width="26px">
                                                    </a>
                                                </div>

                                                <form id="delete-{{$item->id}}" method="POST" action="{{url('/Administrator/Data-master/mappingsegmen/delete/'.$item->id)}}">
                                                    {{ csrf_field() }}
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        var coll = document.getElementsByClassName("collapsible-detail");
        var i;
        
        for (i = 0; i < coll.length; i++) {
            coll[i].addEventListener("click", function() {
            this.classList.toggle("activedetail");
            var content = this.nextElementSibling;
            if (content.style.maxHeight){
                content.style.maxHeight = null;
            } else {
                content.style.maxHeight = content.scrollHeight + "px";
            } 
            });
        }
    </script>

@endsection
