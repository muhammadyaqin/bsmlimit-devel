@extends('Administrator.Data-master.layouts.master')

@section('content')
<style>
    .featured-box .collapsible-detail {
      cursor: pointer;
      padding: 5px;
      width: 100%;
      font-size: 15px;
      outline: none;
      border: none;
      background-color: transparent;
      z-index: 1;
    }
    .activedetail, .collapsible-detail:hover {
      font-weight: bold;
    }
    .collapsible-detail:after {
      content: "\25BC";
      margin-left: 5px;
    }
    .activedetail:after {
      content: "\25B2";
    }
    
    hr {
      border: 0;
      border-top: 1px solid #808080;
      width: 115%;
      position: relative;
      left: -20px;
    }
    .content {
      padding: 0 18px;
      max-height: 0;
      overflow: hidden;
      transition: max-height 0.2s ease-out;
      background-color: transparent;
    }
    .spn {
      padding-left: 5px;
      display: inline-flex;
      width: 95%;
    }
    .fsize-90 {
      font-size: 90%;
    }
    .activedetail, .collapsible-detail:hover {
        color: #18B696;
    }
</style>
    <div class="container-fluid mt-5">
        <div class="card border-left-success">
            <div class="card-header">
                <h1 class="h3 mb-2 text-gray-800" style="margin-top: 10px;"><b>Daftar Map Jenis Jaminan</b></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-2">
                    <div class="featured-box featured-box-primary text-left" style="width:100%;">
                        <div class="box-content">
                            <button class="collapsible-detail text-left pl-3 pr-3 mb-2"><strong>Filter :</strong></button>
                            <div class="content">
                                <div class="container mb-3">
                                    <div class="row">                   
                                        <form action="{{url('/Administrator/Data-master/mapjenisjaminan')}}" method="get">
                                            <div class="row" style="margin-top: 10px;">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label><b>Keywords</b></label>
                                                        <input type="text" name="kodeasal" value="{{$kodeasal}}" placeholder="Kode Asal" class="form-control" id="kodeasal"  onkeyup="this.value = this.value.toUpperCase();">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label><b>-</b></label>
                                                        <input type="text" name="kodebaru" value="{{$kodebaru}}" placeholder="Kode Baru" class="form-control" id="kodebaru"  onkeyup="this.value = this.value.toUpperCase();">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 mt-4">
                                                    <div class="form-group">
                                                        <button class="btn btn-success" value="Search" type="submit" style="width:90px; text-align:left; margin-top: 8px;">
                                                            <i class="fa fa-search"></i>Search</button>
                                                        {{-- <input type="submit" value="Search" class="btn btn-success" style="margin-top: 8px;" class="fa fa-search"> --}}
                                                        @if($statusActive == 'notActive')
                                                            <a href="{{url('/Administrator/Data-master/mapjenisjaminan')}}" class="btn btn-info" style="text-align:left; margin-top: 8px;">Reset</a>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div class="card-body">
                        <div class="row ">
                            <div class="col-md-9 pull-left">
                                <form action="{{url('/Administrator/Data-master/mapjenisjaminan/export_excel')}}" method="POST">
                                    {{ csrf_field() }}
                                    @foreach($mapjenisjaminan as $key => $data)
                                        <input type="hidden" name="mapjenisjaminan[kodeasal][]" value="{{$data->kodeasal}}">
                                        <input type="hidden" name="mapjenisjaminan[kodebaru][]" value="{{$data->kodebaru}}">
                                    @endforeach
                                    <button type="submit" class="btn btn-success btn-sm ml-3"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export.Xlsx </button>
                                   
                                    <a href="{{url('/Administrator/Data-master/mapjenisjaminan/create')}}" class="btn btn-primary btn-sm ml-3"><i class="fa fa-plus"></i> Tambah Data</a>
                                </form>
                            </div>
                            <div class="col-md-3 pull-right">
                                Halaman : {{$mapjenisjaminan->currentPage() }} <br/>
                                Jumlah Data : 1 -
                                {{$mapjenisjaminan->perPage() }}
                                dari :
                                {{$mapjenisjaminan->total() }} <br/>
                            </div>
                        </div>
                        <div style="min-height:300px">
                            <table class="table table-responsive-sm">
                                <thead>
                                    <tr>
                                        <th style="width:50px">No</th>
                                        <th style="width:50px">Kode Asal</th>
                                        <th style="width:50px">Kode Baru</th>
                                        <th style="width:450px"></th>
                                        <th style="width:30px">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    $nomor = ($mapjenisjaminan->currentPage() - 1) * ($mapjenisjaminan->perPage() + 1);
                                ?>
                                @foreach ($mapjenisjaminan as  $key => $item)
                                    <tr>
                                        <td>
                                            @if($mapjenisjaminan->currentPage() == 1)
                                                {{$nomor+$key+1}}
                                            @elseif($mapjenisjaminan->currentPage() > 2)
                                                {{$nomor+$key-($mapjenisjaminan->currentPage()-2)}}
                                            @else
                                                {{$nomor+$key}}
                                            @endif
                                        </td>
                                        <td>
                                            {{$item->kodeasal}}
                                        </td>
                                        <td>
                                            {{$item->kodebaru}} 
                                        </td>
                                        <td></td>
                                        <td>
                                            <div class="row">
                                                <div class="col col-sm-2">
                                                    <a href=" {{url('/Administrator/Data-master/mapjenisjaminan/edit/'.$item->kodeasal)}} ">
                                                        <i class="fa fa-pencil" style="color:cornflowerblue"></i>
                                                    </a>
                                                </div>
                                                
                                                <div class="col col-sm-2">
                                                    <form action="{{url('/Administrator/Data-master/mapjenisjaminan/delete/'.$item->kodeasal)}}" method="POST" style="display: inline;">
                                                        {{ csrf_field() }}
                                                        <button type="submit" style="background:none; border:none;" onclick="return confirm('Anda yakin akan menghapus data mapjenisjaminan dengan Kode Asal = {{$item->kodeasal}}?');" >
                                                            <img src="{{url('public/img/icon/cta_delete.jpeg')}}" width="26px">
                                                        </button>
                                                    </form>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="row pull-right mr-3 mt-3" >
                            {{ $mapjenisjaminan->appends(request()->except('page'))->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        var coll = document.getElementsByClassName("collapsible-detail");
        var i;
        
        for (i = 0; i < coll.length; i++) {
            coll[i].addEventListener("click", function() {
            this.classList.toggle("activedetail");
            var content = this.nextElementSibling;
            if (content.style.maxHeight){
                content.style.maxHeight = null;
            } else {
                content.style.maxHeight = content.scrollHeight + "px";
            } 
            });
        }
    </script>
@endsection
