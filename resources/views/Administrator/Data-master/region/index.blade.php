@extends('Administrator.Data-master.layouts.master')

@section('content')
<style>
    .featured-box .collapsible-detail {
      cursor: pointer;
      padding: 5px;
      width: 100%;
      font-size: 15px;
      outline: none;
      border: none;
      background-color: transparent;
      z-index: 1;
    }
    .activedetail, .collapsible-detail:hover {
      font-weight: bold;
    }
    .collapsible-detail:after {
      content: "\25BC";
      margin-left: 5px;
    }
    .activedetail:after {
      content: "\25B2";
    }
    
    hr {
      border: 0;
      border-top: 1px solid #808080;
      width: 115%;
      position: relative;
      left: -20px;
    }
    .content {
      padding: 0 18px;
      max-height: 0;
      overflow: hidden;
      transition: max-height 0.2s ease-out;
      background-color: transparent;
    }
    .spn {
      padding-left: 5px;
      display: inline-flex;
      width: 95%;
    }
    .fsize-90 {
      font-size: 90%;
    }
    .activedetail, .collapsible-detail:hover {
        color: #18B696;
    }

</style>
    <div class="container-fluid mt-5">    
        <div class="card border-left-success">
            <div class="card-header">
                <h1 class="h3 mb-2 text-gray-800" style="margin-top: 10px;"><b>Daftar Region</b></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-2">
                    <div class="featured-box featured-box-primary text-left" style="width:100%;">
                        <div class="box-content">
                            <button class="collapsible-detail text-left pl-3 pr-3 mb-2"><strong>Filter :</strong></button>
                            <div class="content">
                                <div class="container mb-3">
                                    <div class="row">                   
                                        <form action="{{url('/Administrator/Data-master/region')}}" method="get">
                                            <div class="row" style="margin-top: 10px;">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label><b>Keywords</b></label>
                                                        <input type="text" name="kodnam" value="{{$kodnam}}" placeholder="Kode dan nama" class="form-control" id="kodkatket"  onkeyup="this.value = this.value.toUpperCase();">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 mt-4">
                                                    <div class="form-group">
                                                        <button class="btn btn-success" value="Search" type="submit" style="width:90px; text-align:left; margin-top: 8px;">
                                                            <i class="fa fa-search"></i>Search</button>
                                                        {{-- <input type="submit" value="Search" class="btn btn-success" style="margin-top: 8px;" class="fa fa-search"> --}}
                                                        @if($statusActive == 'notActive')
                                                            <a href="{{url('/Administrator/Data-master/region')}}" class="btn btn-info" style="text-align:left; margin-top: 8px;">Reset</a>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-9 pull-left">
                                <a href="{{url('/Administrator/Data-master/region/export_excel')}}" class="btn btn-success btn-sm ml-3"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export.Xlsx </a>
                                <a href="{{url('/Administrator/Data-master/region/create')}}" class="btn btn-primary btn-sm ml-3"><i class="fa fa-plus"></i> Tambah Data</a>
                            </div>
                            <div class="col-md-3 pull-right">
                                Halaman : {{$region->currentPage() }} <br/>
                                Jumlah Data : 1 -
                                {{$region->perPage() }}
                                dari :
                                {{$region->total() }} <br/>
                            </div>
                        </div>
                        {{-- <div class="row">
                    <div class="card-body">
                        <form action="{{url('/Administrator/Data-master/region')}}" method="get">
                            <div class="row" style="margin-top: 10px;">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label><b>Keywords</b></label>
                                        <input type="text" name="kodnam" value="{{$kodnam}}" placeholder="Kode Region atau Nama Region" class="form-control" id="kodnam"  onkeyup="this.value = this.value.toUpperCase();">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label></label>
                                    <div class="form-group">
                                        <input type="submit" value="Search" class="btn btn-success" style="margin-top: 8px;" class="fa fa-search">
                                        @if($statusActive == 'notActive')
                                            <a href="{{url('/Administrator/Data-master/region')}}" class="btn btn-info" style="text-align:left; margin-top: 8px;">Reset</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="row">
                            <div class="toolbar mb-3 mr-5" style="width:100%">
                                <a href="{{url('/Administrator/Data-master/region/create')}}" class="btn btn-primary ml-3"><i class="fa fa-plus"></i> Tambah Data</a>
                                <div class="col-md-3 pull-right">
                                    Halaman : {{$region->currentPage() }} <br/>
                                    Jumlah Data : 1 -
                                    {{$region->perPage() }}
                                    dari :
                                    {{$region->total() }} <br/>
                                </div>
                            </div>
                        </div> --}}
                        {{-- <div class="table-responsive mt-3 mb-4" style="min-height:300px">
                            <table class="table table-responsive-sm table-striped">
                        </div> --}}
                        <div style="min-height:300px">
                            <table class="table table-responsive-sm">
                                <thead>
                                    <tr>
                                        <th style="width:50px">#</th>
                                        <th style="width:250px">Kode Region</th>
                                        <th style="width:250px">Nama Region</th>
                                        <th style="width:250px">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($region as $key => $item)
                                    <tr>
                                        <td> {{$key+1}} </td>
                                        <td> {{$item->kode}} </td>
                                        <td> {{$item->nama}} </td>
                                        <td>
                                            <div class="row">
                                                <div class="col col-sm-3">
                                                    <a href=" {{url('/Administrator/Data-master/region/edit/'.$item->kode)}} ">
                                                        <i class="fa fa-pencil"  style="color:cornflowerblue"></i>
                                                    </a>
                                                </div>
                                                
                                                <div class="col col-sm-3">
                                                    <form action="{{url('/Administrator/Data-master/region/delete/'.$item->kode)}}" method="POST" style="display: inline;">
                                                        {{ csrf_field() }}
                                                        <button type="submit" style="background:none; border:none;" onclick="return confirm('Setiap data area dengan Kode Region = {{$item->kode}} akan ikut terhapus. Anda yakin akan menghapus data region dengan Kode Region = {{$item->kode}} ?');" >
                                                            <img src="{{url('public/img/icon/cta_delete.jpeg')}}" width="26px">
                                                        </button>
                                                    </form>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="row pull-right mr-3 mt-3" >
                            {{ $region->appends(request()->except('page'))->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        var coll = document.getElementsByClassName("collapsible-detail");
        var i;
        
        for (i = 0; i < coll.length; i++) {
            coll[i].addEventListener("click", function() {
            this.classList.toggle("activedetail");
            var content = this.nextElementSibling;
            if (content.style.maxHeight){
                content.style.maxHeight = null;
            } else {
                content.style.maxHeight = content.scrollHeight + "px";
            } 
            });
        }
    </script>

@endsection
