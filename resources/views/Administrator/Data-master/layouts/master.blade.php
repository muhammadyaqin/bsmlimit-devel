<!DOCTYPE html>
<html lang="en">

<head>
  @include('Administrator.Data-master.layouts.head')
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar" style="background: #00583D;">
            @include('Administrator.Data-master.layouts.sidebar')
        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

          <!-- Main Content -->
          <div id="content">

            <!-- Topbar -->
            <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
              @include('Administrator.Data-master.layouts.navbar')
            </nav>
            <!-- End of Topbar -->

            <!-- Begin Page Content -->
            <div class="container-fluid">
              @yield('content')
            </div>
            <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->

      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
          </form>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="{{ URL::asset('/public/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{ URL::asset('/public/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

  <!-- Core plugin JavaScript-->
  <script src="{{ URL::asset('/public/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

  <!-- Custom scripts for all pages-->
  <script src="{{ URL::asset('/public/js/sb-admin-2.min.js')}}"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

  <!-- Page level plugins -->
  <script src="{{ URL::asset('/public/vendor/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{ URL::asset('/public/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

  <!-- Page level custom scripts -->
  <script src="{{ URL::asset('/public/js/demo/datatables-demo.js')}}"></script>
  <script src="{{ URL::asset('/public/js/select_2.js')}}"></script>
  <script src="{{ URL::asset('/public/js/swal.js')}}"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
   <!-- Slide Up and Down Script -->
  <script> 
    $(document).ready(function(){
      $("#flip").click(function(){
        $("#panel").slideToggle("slow");
      });
    });
  </script>

  <script>
    @if(Session::has('success'))
      Swal.fire({
        icon: 'success',
        title: 'Berhasil',
        text: "{{ Session::get('success') }}",
      })
    @endif

    @if(Session::has('portofolio'))
      Swal.fire({
        icon: 'success',
        title: 'Kalkulasi Berhasil',
        text: "{{ Session::get('portofolio') }}",
      })
    @endif

    @if(Session::has('info'))
      Swal.fire({
        icon: 'info',
        title: 'Info',
        text: "{{ Session::get('info') }}",
      })
    @endif


    @if(Session::has('warning'))
      Swal.fire({
        icon: 'warnign',
        title: 'Warning...',
        text: "{{ Session::get('warning') }}",
      })
    @endif


    @if(Session::has('error'))
      Swal.fire({
        icon: 'error',
        title: 'Gagal...',
        text: "{{ Session::get('error') }}",
      })
    @endif
  </script>

  <!-- Slide Up and Down Script -->

  <!-- Date Range Picker -->
  <script type="text/javascript">
    $(function() {
      $('input[name="daterange"]').daterangepicker({
        opens: 'left'
      }, function(start, end, label) {
        console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
      });
    });
  </script>
  <!-- Date Range Picker -->


@yield('jscustom')
</body>

</html>