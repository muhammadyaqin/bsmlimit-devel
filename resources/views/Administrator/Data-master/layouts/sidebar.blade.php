<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar" style="background: #00583D;">
        <!-- Sidebar - Brand -->
        @if(Auth::user()->cekRole()->role == "REGION")
          <div class="sidebar-brand-icon" style="margin-bottom: 35%;">
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{URL::asset('/home-region')}}">
              <img src="{{url('/public/img/Login/Logo.png')}}" width="76.76px" height="40px">
            </a>
          </div>
        @elseif(Auth::user()->cekRole()->role == "PUSAT")
          <div class="sidebar-brand-icon" style="margin-bottom: 35%;">
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{URL::asset('/home-pusat')}}">
              <img src="{{url('/public/img/Login/Logo.png')}}" width="76.76px" height="40px">
            </a>
          </div>
        @elseif(Auth::user()->cekRole()->role == "CABANG")
          <div class="sidebar-brand-icon" style="margin-bottom: 35%;">
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{URL::asset('/home-cabang')}}">
              <img src="{{url('/public/img/Login/Logo.png')}}" width="76.76px" height="40px">
            </a>
          </div>
        @elseif(Auth::user()->cekRole()->role == "ERM")
          <div class="sidebar-brand-icon" style="margin-bottom: 35%;">
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{URL::asset('/home-erm')}}">
              <img src="{{url('/public/img/Login/Logo.png')}}" width="76.76px" height="40px">
            </a>
          </div>
        @elseif(Auth::user()->cekRole()->role == "HCS")
          <div class="sidebar-brand-icon" style="margin-bottom: 35%;">
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{URL::asset('/home-hcs')}}">
              <img src="{{url('/public/img/Login/Logo.png')}}" width="76.76px" height="40px">
            </a>
          </div>
        @elseif(Auth::user()->cekRole()->role == "IOG")
          <div class="sidebar-brand-icon" style="margin-bottom: 35%;">
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{URL::asset('/home-iog')}}">
              <img src="{{url('/public/img/Login/Logo.png')}}" width="76.76px" height="40px">
            </a>
          </div>
        @endif
          <div class="sidebar-brand-icon" style="margin-bottom: 35%;">
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{URL::asset('/Administrator/index')}}">
              <img src="{{url('/public/img/Login/Logo.png')}}" width="76.76px" height="40px">
            </a>
          </div>
        <!-- Divider -->
        <hr class="sidebar-divider my-0">
        <!-- Nav Item - Dashboard -->
        <li class="nav-item active" style="background: rgba(255, 255, 255, 0.1);">
          <a class="nav-link" href="{{URL::asset('/Administrator/index')}}">
            <img src="{{url('/public/img/Sidebar/beranda.png')}}" width="20px" height="20px">
            <span>Beranda</span></a>
        </li>
        <!-- Nav Item - Data Master -->
        <li class="nav-item">
          <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
            <img src="{{url('/public/img/Sidebar/data_master.png')}}" width="20px" height="20px">
            <span>Data Master</span>
          </a>
          <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="{{url('/Administrator/Data-master/enumerasi')}}">Enumerasi</a>
                <a class="collapse-item" href="{{url('/Administrator/Data-master/region')}}">Region</a>
                <a class="collapse-item" href="{{url('/Administrator/Data-master/area')}}">Area</a>
                <a class="collapse-item" href="{{url('/Administrator/Data-master/unitkerja')}}">Cabang</a>
                <a class="collapse-item" href="{{url('/Administrator/Data-master/jabatan')}}">Jabatan PWMP</a>
                <a class="collapse-item" href="{{url('/Administrator/Data-master/jabatanpembiayaan')}}">Jabatan Pembiayaan</a>
                <a class="collapse-item" href="{{url('/Administrator/Data-master/produkpembiayaan')}}">Produk Pembiayaan</a>
              <a class="collapse-item" href="{{url('/Administrator/Data-master/produklimitotomatis')}}">Produk Limit Otomatis</a>
              <a class="collapse-item" href="{{url('/Administrator/Data-master/kategorilimit')}}">Kategori Limit</a>
            </div>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages3" aria-expanded="true" aria-controls="collapsePages3">
            <img src="{{url('/public/img/Sidebar/data_master.png')}}" width="20px" height="20px">
            <span>Data Mapping</span>
          </a>
          <div id="collapsePages3" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="{{url('/Administrator/Data-master/mapsegmen')}}">Map Segmen</a>
                <a class="collapse-item" href="{{url('/Administrator/Data-master/mapjenisjaminan')}}">Map Jenis Jaminan</a>
                <a class="collapse-item" href="{{url('/Administrator/Data-master/mapkolektibilitas')}}">Map Kolektibilitas</a>
            </div>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages1" aria-expanded="true" aria-controls="collapsePages1">
            <img src="{{url('/public/img/Sidebar/stagging.png')}}" width="20px" height="20px">
            <span>Staging</span>
          </a>
          <div id="collapsePages1" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="{{url('/Administrator/Data-master/portfolio')}}">MIS Portfolio</a>
              <a class="collapse-item" href="{{url('/Administrator/Data-master/jaminan')}}">MIS Jaminan</a>
              <!-- <a class="collapse-item" href="{{url('/Administrator/Data-master/pejabat')}}">HCS Pejabat</a> -->
              <!-- <a class="collapse-item" href="{{url('/Administrator/Data-master/riwayatjabatan')}}">HCS Riwayat Jabatan</a> -->
              <a class="collapse-item" href="{{url('/Administrator/Data-master/manajemenresiko')}}">Sertifikasi Manage Resiko</a>
              <a class="collapse-item" href="{{url('/Administrator/Data-master/refreshment')}}">Refreshment</a>
              <a class="collapse-item" href="{{url('/Administrator/Data-master/pelatihanpembiayaan')}}">Pelatihan Pembiayaan</a>
              <!-- <a class="collapse-item" href="{{url('/Administrator/Data-master/pengalamanpembiayaan')}}">Pengalaman Pembiayaan</a> -->
            </div>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages2" aria-expanded="true" aria-controls="collapsePages1">
            <img src="{{url('/public/img/Sidebar/user icon.png')}}" width="20px" height="20px">
            <span>User Manajemen</span>
          </a>
          <div id="collapsePages2" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="{{url('/user')}}">User</a>
              <a class="collapse-item" href="{{url('/user_manajemen/user_role/index')}}">User Role</a>
              <a class="collapse-item" href="{{url('/user_manajemen/role/index')}}">Role</a>
            </div>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{URL('/portofolio')}}">
            <img src="{{url('/public/img/Sidebar/data_master.png')}}" width="20px" height="20px">
            <span>Kalkulasi Portofolio</span></a>
        </li>

        
        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
          <button class="rounded-circle border-0" id="sidebarToggle">
          </button>
        </div>
        <div class="footer-sidebar" style="position: relative; top: 90%;">
          <p style="position: relative; left: 10%; font-style: normal; font-weight: normal; font-size: 14px; color: #FFFFFF;">Copyright © 2020 <br>
              PT Bank Syariah Mandiri</p>
        </div>

      </ul>
