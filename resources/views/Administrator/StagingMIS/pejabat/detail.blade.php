@extends('layouts.backend')
@section('content')
<div class="card mb-4">
  <div class="card-header border-left-success h-100 py-2">
    <div class="card-body">
      <div class="row no-gutters align-items-center">
        <div class="col mr-2">
          <div class="row">
            <a class="nav-link" href="{{URL::asset('/Administrator/Data-master/pejabat')}}">
              <img src="{{url('public/img/icon/back.png')}}" width="30px" height="30px">
            </a>
            <div class="h5 mb-0 font-weight-bold text-gray-800" style="margin-top: 12px;">Detail Data Pejabat</div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="card-body">
            <div class="row">
              @foreach($pejabat as $pjbt)
              <div class="col-md-6">
                    <input type="hidden" name="nip" value="{{$pjbt->nip}}" readonly="" class="form-control" id="nip">
                <div class="form-group">
                  <label><b>Nomor Induk Pegawai</b></label>
                  <p>{{$pjbt->nip}}</p>
                </div>
                <div class="form-group">
                  <label><b>Nama</b></label>
                  <p>{{$pjbt->namapejabat}}</p>
                </div>
                <div class="form-group">
                  <label><b>Status Karyawan</b></label>
                  <p>{{$pjbt->statuskaryawan}}</p>
                </div>
                <div class="form-group">
                  <label><b>Jabatan</b></label>
                  <p>{{$pjbt->keteranganjabatan}}</p>
                </div>
                <div class="form-group">
                  <label><b>Cabang</b></label>
                  <p>{{$pjbt->namacabang}}</p>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label><b>Tanggal Bergabung</b></label>
                  <p>{{ \Carbon\Carbon::parse($pjbt->tglbergabung)->format('d-m-Y') }}</p>
                </div>
                <div class="form-group">
                  <label><b>Email</b></label>
                  <p>{{$pjbt->email}}</p>
                </div>
                <div class="form-group">
                  <label><b>Sanksi</b></label>
                  <p>{{$pjbt->keterangansanksi}}</p>
                </div>
                <div class="form-group">
                  <label><b>Fraud</b></label>
                  <p>{{$pjbt->keteranganfraud}}</p>
                </div>
                <div class="form-group">
                  <label><b>Appraisal</b></label>
                  <p>{{$pjbt->keteranganappraisal}}</p>
                </div>
              </div>
              @endforeach
          </div>



@endsection