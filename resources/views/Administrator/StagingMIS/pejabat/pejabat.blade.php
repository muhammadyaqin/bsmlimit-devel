@extends('layouts.backend')
@section('content')

<style>
    .featured-box .collapsible-detail {
      cursor: pointer;
      padding: 5px;
      width: 100%;
      font-size: 15px;
      outline: none;
      border: none;
      background-color: transparent;
      z-index: 1;
    }
    .activedetail, .collapsible-detail:hover {
      font-weight: bold;
    }
    .collapsible-detail:after {
      content: "\25BC";
      margin-left: 5px;
    }
    .activedetail:after {
      content: "\25B2";
    }
    
    hr {
      border: 0;
      border-top: 1px solid #808080;
      width: 115%;
      position: relative;
      left: -20px;
    }
    .content {
      padding: 0 18px;
      max-height: 0;
      overflow: hidden;
      transition: max-height 0.2s ease-out;
      background-color: transparent;
    }
    .spn {
      padding-left: 5px;
      display: inline-flex;
      width: 95%;
    }
    .fsize-90 {
      font-size: 90%;
    }
    .activedetail, .collapsible-detail:hover {
        color: #18B696;
    }
</style>
    {{-- notifikasi form validasi --}}
    @if ($errors->has('file'))
    <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('file') }}</strong>
    </span>
    @endif

    {{-- notifikasi sukses --}}
    @if ($sukses = Session::get('sukses'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $sukses }}</strong>
    </div>
    @endif

    <div class="container-fluid mt-5">
        <div class="card border-left-success">
            <div class="card-header">
                <h1 class="h3 mb-2 text-gray-800" style="margin-top: 10px;"><b>Daftar HCS Pejabat</b></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-2">
                    <div class="featured-box featured-box-primary text-left" style="width:100%;">
                        <div class="box-content">
                            <button class="collapsible-detail text-left pl-3 pr-3 mb-2"><strong>Filter :</strong></button>
                            <div class="content">
                                <div class="container mb-3">
                                    <div class="row">                   
                                        <form action="{{url('/Administrator/Data-master/pejabat')}}" method="get">
                                          <div class="row" style="margin-top: 10px;">
                                             <div class="col-md-6">
                                              <div class="form-group">
                                                <label><b>Keywords</b></label>
                                                  <input type="text" name="name_nip" value="{{$name_nip}}" placeholder="Nama dan NIP" class="form-control" id="name_nip"  onkeyup="this.value = this.value.toUpperCase();">
                                              </div>
                                            </div>
                                            <div class="col-md-6 mt-4">
                                              <div class="form-group" >
                                                <button class="btn btn-success" value="Search" type="submit" style="width:90px; text-align:left; margin-top: 8px;">
                                                    <i class="fa fa-search"></i>Search</button>
                                                {{-- <input type="submit" value="Search" class="btn btn-success" style="margin-top: 8px;" class="fa fa-search"> --}}
                                                @if($statusActive == 'notActive')
                                                    <a href="{{url('/Administrator/Data-master/pejabat')}}" class="btn btn-info" style="text-align:left; margin-top: 8px;">Reset</a>
                                                @else
                    
                                                @endif
                                              </div>
                                            </div>
                                          </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div class="card-body">
                        <div class="row ">
                            <div class="col-md-9 pull-left">
                                <a href="{{url('/hcspejabat/import_excel')}}" class="btn btn-success btn-sm ml-3"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export.Xlsx </a>
                                <a href="#" class="btn btn-primary btn-sm ml-3" data-toggle="modal" data-target="#importExcel"><i class="fa fa-plus"></i> Upload Data Excel</a>
                            </div>
                            <div class="col-md-3">
                                Halaman : {{$hcs->currentPage() }} <br/>
                                Jumlah Data : 1 -
                                {{$hcs->perPage() }}
                                dari :
                                {{$hcs->total() }} <br/>
                            </div>
                        </div>
                        <!-- Import Excel -->
                        <div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <form method="post" action="{{url('/hcspejabat/import_excel')}}" enctype="multipart/form-data">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Import Excel</h5>
                                        </div>
                                        <div class="modal-body">
                        
                                            {{ csrf_field() }}
                        
                                            <label>Pilih File Excel</label>
                                            <div class="form-group">
                                                <input type="file" name="file" required="required">
                                            </div>
                        
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Import</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- Import Excel -->                        
                        <div style="min-height:300px">
                            <table class="table table-responsive-sm">
                                <thead>
                                    <tr>
                                        <th>NIP</th>
                                        <th>Nama Pejabat</th>
                                        <th>Tanggal Bergabung</th>
                                        <th>Status Karyawan</th>
                                        <th>Manage</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($hcs as $item)
                                    <tr>
                                        <td> {{$item->nip}} </td>
                                        <td> {{$item->namapejabat}} </td>
                                        <td> {{ \Carbon\Carbon::parse($item->tglbergabung)->format('d-m-Y') }}   </td>
                                        <td> {{$item->statuskaryawan}} </td>
                                        <td>
                                            <a href="{{URL::to('/Administrator/Data-master/pejabat/'.$item->nip.'/detail')}}">
                                                <img src="{{url::asset('public/img/icon/cta_opendetail.png')}}" width="26px">
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="row pull-right mr-3 mt-3" >
                            {{ $hcs->appends(request()->except('page'))->links() }}
                        </div>
                        
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script>
    var coll = document.getElementsByClassName("collapsible-detail");
    var i;
    
    for (i = 0; i < coll.length; i++) {
        coll[i].addEventListener("click", function() {
        this.classList.toggle("activedetail");
        var content = this.nextElementSibling;
        if (content.style.maxHeight){
            content.style.maxHeight = null;
        } else {
            content.style.maxHeight = content.scrollHeight + "px";
        } 
        });
    }
</script>    
@endsection
