@extends('Administrator.Data-master.layouts.master')

@section('content')
<div class="container-fluid">
    <div class="card border-left-success">
        <div class="card-header">
            <h1 class="h3 mb-2 text-gray-800" style="margin-top: 10px;"><b>Form Edit Pelatihan Pembiayaan</b></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form class="form-horizontal" id="product" action="{{url('/Administrator/Data-master/pelatihanpembiayaan/update')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <input type="text" value="{{$pelatihanpembiayaan->id}}" hidden />
                                    <label class="col-sm-3 text-right control-label col-form-label" required>NIP</label>
                                    <div class="col-sm-9">
                                        <select name="nip" class="form-control">
                                            <option value="">-- Pilih NIP --</option>
                                            @foreach($pejabat as $pjb)
                                                @if($pjb->nip == $pelatihanpembiayaan->nip)
                                                    <option value="{{$pjb->nip}}" selected>{{$pjb->nip}} - {{$pjb->namapejabat}}</option>
                                                @else
                                                    <option value="{{$pjb->nip}}">{{$pjb->nip}} - {{$pjb->namapejabat}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 text-right control-label col-form-label" required>Nama Training</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="namatraining" value="{{$pelatihanpembiayaan->namatraining}}" class="form-control">
                                    </div>
                                </div>            
                            </div>
                        </div>
                        <div class="card-footer">
                            <a href="{{url('/Administrator/Data-master/pelatihanpembiayaan')}}" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Back</a> 
                            <button class="btn btn-primary pull-right" type="submit" name="submit_image" value="save"><i class="fa fa-save"></i> Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection