@extends('Administrator.Data-master.layouts.master')

@section('content')
<style>
    .featured-box .collapsible-detail {
      cursor: pointer;
      padding: 5px;
      width: 100%;
      font-size: 15px;
      outline: none;
      border: none;
      background-color: transparent;
      z-index: 1;
    }
    .activedetail, .collapsible-detail:hover {
      font-weight: bold;
    }
    .collapsible-detail:after {
      content: "\25BC";
      margin-left: 5px;
    }
    .activedetail:after {
      content: "\25B2";
    }
    
    hr {
      border: 0;
      border-top: 1px solid #808080;
      width: 115%;
      position: relative;
      left: -20px;
    }
    .content {
      padding: 0 18px;
      max-height: 0;
      overflow: hidden;
      transition: max-height 0.2s ease-out;
      background-color: transparent;
    }
    .spn {
      padding-left: 5px;
      display: inline-flex;
      width: 95%;
    }
    .fsize-90 {
      font-size: 90%;
    }
    .activedetail, .collapsible-detail:hover {
        color: #18B696;
    }
</style>
    {{-- notifikasi form validasi --}}
    @if ($errors->has('file'))
    <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('file') }}</strong>
    </span>
    @endif

    {{-- notifikasi sukses --}}
    @if ($sukses = Session::get('sukses'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $sukses }}</strong>
    </div>
    @endif

    <div class="container-fluid mt-5">
        <div class="card border-left-success">
            <div class="card-header">
                <h1 class="h3 mb-2 text-gray-800" style="margin-top: 10px;"><b>Daftar Pelatihan Pembiayaan</b></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-2">
                    <div class="card-body">
                        <div class="row ">
                            <div class="col-md-9 pull-left">
                                <a href="{{url('/pelatihanpembiayaan/export_excel')}}" class="btn btn-success btn-sm ml-3"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export.Xlsx </a>                                
                                <a href="{{url('/Administrator/Data-master/pelatihanpembiayaan/create')}}" class="btn btn-primary btn-sm ml-3"><i class="fa fa-plus"></i> Tambah Data</a>
                            </div>
                            <div class="col-md-3">
                                Halaman : {{$pelatihanpembiayaan->currentPage() }} <br/>
                                Jumlah Data : 1 -
                                {{$pelatihanpembiayaan->perPage() }}
                                dari :
                                {{$pelatihanpembiayaan->total() }} <br/>
                            </div>
                        </div>    

                        <div style="min-height:300px">
                            <table class="table table-responsive-sm">
                                <thead>
                                    <tr>
                                        <th style="width:120px">NIP</th>
                                        <th style="width:250px">Nama Training</th>
                                        <th style="width:250px">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($pelatihanpembiayaan as $index => $pp)
                                    <tr>
                                        <td> {{$pp->nip}} </td>
                                        <td> {{$pp->namatraining}} </td>
                                        <td>
                                            <div class="row">
                                                <div class="col col-sm-3">
                                                    <a href=" {{url('/Administrator/Data-master/pelatihanpembiayaan/edit/'.$pp->id)}} ">
                                                        <i class="fa fa-pencil" style="color:cornflowerblue"></i>
                                                    </a>
                                                </div>
                                                
                                                <div class="col col-sm-3">
                                                    <form action="{{url('/Administrator/Data-master/pelatihanpembiayaan/delete/'.$pp->id)}}" method="POST" style="display: inline;">
                                                        {{ csrf_field() }}
                                                        <button type="submit" style="background:none; border:none;" onclick="return confirm('Anda yakin akan menghapus data pelatihan pembiayaan ini?');" >
                                                            <img src="{{url('public/img/icon/cta_delete.jpeg')}}" width="26px">
                                                        </button>
                                                    </form>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="row pull-right mr-3 mt-3" >
                            {{ $pelatihanpembiayaan->appends(request()->except('page'))->links() }}
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script>
    var coll = document.getElementsByClassName("collapsible-detail");
    var i;
    
    for (i = 0; i < coll.length; i++) {
        coll[i].addEventListener("click", function() {
        this.classList.toggle("activedetail");
        var content = this.nextElementSibling;
        if (content.style.maxHeight){
            content.style.maxHeight = null;
        } else {
            content.style.maxHeight = content.scrollHeight + "px";
        } 
        });
    }
</script>
@endsection
