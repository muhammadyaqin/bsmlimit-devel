@extends('layouts.backend')
@section('content')

    {{-- notifikasi form validasi --}}
    @if ($errors->has('file'))
    <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('file') }}</strong>
    </span>
    @endif

    {{-- notifikasi sukses --}}
    @if ($sukses = Session::get('sukses'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $sukses }}</strong>
    </div>
    @endif

    <div class="container-fluid mt-5">
        <div class="card border-left-success">
            <div class="card-header">
                <h1 class="h3 mb-2 text-gray-800" style="margin-top: 10px;"><b>Daftar Pengalaman Pembiayaan</b></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-2">
                    <div class="card-body">
                                <form action="{{url('/Administrator/Data-master/pengalamanpembiayaan')}}" method="get">
                                  <div class="row" style="margin-top: 10px;">
                                     <div class="col-md-3">
                                      <div class="form-group">
                                        <label><b>Keywords</b></label>
                                          <input type="text" name="name_nip" value="" placeholder="NIP" class="form-control" id="name_nip"  onkeyup="this.value = this.value.toUpperCase();">
                                      </div>
                                    </div>
                                    <div class="col-md-3">
                                      <label></label>
                                      <div class="form-group">
                                        <input type="submit" value="Search" class="btn btn-success" style="margin-top: 8px;" class="fa fa-search">
                                        @if($statusActive == 'notActive')
                                            <a href="{{url('/Administrator/Data-master/pengalamanpembiayaan')}}" class="btn btn-info" style="text-align:left; margin-top: 8px;">Reset</a>
                                        @else

                                        @endif
                                      </div>
                                    </div>
                                  </div>
                                </form> 
                        <div class="row ">
                            <div class="col-md-9 pull-left">
                                <a href="{{url('/pengalamanpembiayaan/export_excel')}}" class="btn btn-success btn-sm ml-3"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export.Xlsx </a>
                                <a href="#" class="btn btn-primary btn-sm ml-3" data-toggle="modal" data-target="#importExcel"><i class="fa fa-plus"></i> Upload Data Excel</a>
                            </div>
                            <div class="col-md-3">
                                Halaman : {{$pengalamanpembiayaan->currentPage() }} <br/>
                                Jumlah Data : 1 -
                                {{$pengalamanpembiayaan->perPage() }}
                                dari :
                                {{$pengalamanpembiayaan->total() }} <br/>
                            </div>
                        </div>
                        <!-- Import Excel -->
                        <div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <form method="post" action="{{url('/pengalamanpembiayaan/import_excel')}}" enctype="multipart/form-data">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Import Excel</h5>
                                        </div>
                                        <div class="modal-body">
                        
                                            {{ csrf_field() }}
                        
                                            <label>Pilih File Excel</label>
                                            <div class="form-group">
                                                <input type="file" name="file" required="required">
                                            </div>
                        
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Import</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- Import Excel -->  
                        <div style="min-height:300px">
                            <table class="table table-responsive-sm">
                                <thead>
                                    <tr>
                                        <th style="width:150px">NIP</th>
                                        <th style="width:250px">Kode Jabatan</th>
                                        <th style="width:250px">Kode Cabang</th>
                                        <th style="width:250px">Tanggal Mulai</th>
                                        <th style="width:250px">Tanggal Berkahir</th>
                                        <th style="width:250px">Keterangan Jabatan</th>
                                        <th style="width:250px">Keterangan Cabang</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($pengalamanpembiayaan as $item)
                                    <tr>
                                        <td> {{$item->nip}} </td>
                                        <td> {{$item->kodejabatan}} </td>
                                        <td> {{$item->kodecabang}} </td>
                                        <td> {{ \Carbon\Carbon::parse($item->tglmulai)->format('d-m-Y') }}   </td>
                                        <td> {{ \Carbon\Carbon::parse($item->tglberakhir)->format('d-m-Y') }}   </td>
                                        <td> {{$item->keteranganjabatan}} </td>
                                        <td> {{$item->keterangancabang}} </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="row pull-right mr-3 mt-3" >
                            {{ $pengalamanpembiayaan->appends(request()->except('page'))->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
