@extends('layouts.backend')
@section('content')
<div class="card mb-4">
    <div class="card-header border-left-success h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="row">
              <a class="nav-link" href="{{URL::asset('/Administrator/Data-master/riwayatjabatan')}}">
                <img src="{{url('public/img/icon/back.png')}}" width="30px" height="30px">
              </a>
              <div class="h5 mb-0 font-weight-bold text-gray-800" style="margin-top: 12px;">Detail Data Riwayat Pejabat</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-6">
              <input type="hidden" name="nip" value="{{$hcsriwayat->nip}}" readonly="" class="form-control" id="nip">
          <div class="form-group">
            <label><b>Nomor Induk Pegawai</b></label>
            <p>{{$hcsriwayat->nip}}</p>
          </div>
          <div class="form-group">
            <label><b>Nama</b></label>
            <p>{{$hcsriwayat->namapejabat}}</p>
          </div>
          <div class="form-group">
            <label><b>Jabatan</b></label>
            <p>{{$hcsriwayat->keteranganjabatan}}</p>
          </div>
        </div>
         <div class="col-md-6">
          <div class="form-group">
            <label><b>Cabang</b></label>
            <p>{{$hcsriwayat->keterangancabang}}</p>
          </div>
          <div class="form-group">
            <label><b>Tanggal Mulai</b></label>
            <p>{{ \Carbon\Carbon::parse($hcsriwayat->tglmulai)->format('d-m-Y') }}</p>
          </div>
          <div class="form-group">
            <label><b>Tanggal Berakhir</b></label>
            <p>{{ \Carbon\Carbon::parse($hcsriwayat->tglberakhir)->format('d-m-Y') }}</p>
          </div>
        </div>
      </div>
    </div>
</div>



@endsection