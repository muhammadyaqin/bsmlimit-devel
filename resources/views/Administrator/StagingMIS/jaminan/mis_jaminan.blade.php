@extends('Administrator.Data-master.layouts.master')

@section('content')
    <div class="container-fluid mt-5">
        <div class="card border-left-success">
            <div class="card-header">
                <h1 class="h3 mb-2 text-gray-800" style="margin-top: 10px;"><b>Daftar MIS Jaminan</b></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-2">
                    <div class="card-body">
                        <div class="row ">
                            <div class="col-md-9 pull-left">
                                <a href="{{url('/jaminan/import_excel')}}" class="btn btn-success btn-sm ml-3"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export.Xlsx </a>
                                <a href="{{url('/Administrator/Data-master/jaminan/create')}}" class="btn btn-primary btn-sm ml-3"><i class="fa fa-plus"></i> Tambah Data</a>
                            </div>

                            <div class="col-md-3">
                                Halaman : {{$misjaminan->currentPage() }} <br/>
                                Jumlah Data : 1 -
                                {{$misjaminan->perPage() }}
                                dari :
                                {{$misjaminan->total() }} <br/>
                            </div>
                        </div>
                        <div style="min-height:300px">
                            <table class="table table-responsive-sm">
                                <thead>
                                    <tr>
                                        <th style="width:250px">Nomor Loan</th>
                                        <th style="width:250px">Kode Jaminan</th>
                                        <th style="width:250px">Keterangan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($misjaminan as $item)
                                    <tr>
                                        <td> {{$item->noloan}} </td>
                                        <td> {{$item->kodejaminan}} </td>
                                        <td> {{$item->keterangan}} </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="row pull-right mr-3 mt-3" >
                            {{ $misjaminan->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
