@extends('Administrator.Data-master.layouts.master')

@section('content')
    <div class="container-fluid mt-5">
        <div class="card border-left-success">
            <div class="card-header">
                <h1 class="h3 mb-2 text-gray-800" style="margin-top: 10px;"><b>Daftar Portfolio</b></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-2">
                    <div class="card-body">
                        <div class="row ">
                            <div class="col-md-9 pull-left">
                                <a href="{{url('/misportofolio/import_excel')}}" class="btn btn-success btn-sm ml-3"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export.Xlsx </a>
                                <a href="{{url('/Administrator/Data-master/produkpembiayaan/create')}}" class="btn btn-primary btn-sm ml-3"><i class="fa fa-plus"></i> Tambah Data</a>
                            </div>
                            <div class="col-md-3">
                                Halaman : {{$mispo->currentPage() }} <br/>
                                Jumlah Data : 1 -
                                {{$mispo->perPage() }}
                                dari :
                                {{$mispo->total() }} <br/>
                            </div>
                        </div>
                        <div style="min-height:300px">
                            <table class="table table-responsive-sm">
                                <thead>
                                    <tr>
                                        <th style="width:50px">NIP</th>
                                        <th style="width:250px">Nama</th>
                                        <th style="width:250px">Unit Kerja</th>
                                        <th style="width:250px">Region</th>
                                        <th style="width:250px">Plafon</th>
                                        <th style="width:250px">OS Pokok</th>
                                        {{-- <th style="width:250px">Total</th> --}}
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($mispo as $item)
                                    <tr>
                                        <td> {{$item->cif}} </td>
                                        <td> {{$item->namanasabah}} </td>
                                        <td> {{$item->namacabang}} </td>
                                        <td> {{$item->namaregion}} </td>
                                        <td> {{$item->plafon}} </td>
                                        <td> {{$item->ospokok}} </td>
                                        {{-- <td> {{$item->cif}} </td> --}}
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="row pull-right mr-3 mt-3" >
                            {{ $mispo->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
