@extends('Administrator.Data-master.layouts.master')
@section('content')
<div class="card mb-4">
  <div class="card-header border-left-success h-100 py-2">
    <div class="card-body">
      <div class="row no-gutters align-items-center">
        <div class="col mr-2">
          <div class="row">
            <a class="nav-link" href="{{URL::asset('/Administrator/Data-master/manajemenresiko')}}">
              <img src="{{url('img/icon/back.png')}}" width="30px" height="30px">
            </a>
            <div class="h5 mb-0 font-weight-bold text-gray-800" style="margin-top: 12px;">Detail Data Sertifikasi Manajemen Resiko Pejabat</div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                    <input type="hidden" name="nip" value="{{$smrisk->nip}}" readonly="" class="form-control" id="nip">
                <div class="form-group">
                  <label><b>Nomor Induk Pegawai</b></label>
                  <p>{{$smrisk->nip}}</p>
                </div>
                <div class="form-group">
                  <label><b>Nama</b></label>
                  <p>{{$smrisk->namapejabat}}</p>
                </div>
                <div class="form-group">
                  <label><b>Status Sertifikasi</b></label>
                  <p>{{$smrisk->statussertifikasi}}</p>
                </div>
              </div>
              </div>
            </div>
          </div>



@endsection