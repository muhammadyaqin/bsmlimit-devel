@extends('Administrator.Data-master.layouts.master')

@section('content')
    <div class="container-fluid mt-5">
      <div class="card border-left-success">
            <div class="card-header">
                <h1 class="h3 mb-2 text-gray-800" style="margin-top: 10px;"><b>Kalkulasi Portofolio</b></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-2">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-9 pull-left">
                                <!-- <a href="{{URL::to('calc_misportfolio.php')}}" class="btn btn-primary ml-3">Hitung Portofolio</a> -->
                                <a href="{{URL('/portofolio/hitung')}}" class="btn btn-primary ml-3">Hitung Portofolio</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
