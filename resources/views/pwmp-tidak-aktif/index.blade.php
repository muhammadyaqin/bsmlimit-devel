@extends('layouts.backend')
@section('content')
    <div class="container-fluid">

      <!-- Page Heading -->
      <h1 class="h3 mb-2 text-gray-800">PWMP Tidak Aktif</h1>
      <p class="mb-4">Daftar Pejabat Pemegang Kewenangan Tidak Aktif</p>

      <!-- DataTales Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Filter</h6>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th style="font-size: 12px;">NIP</th>
                  <th style="font-size: 12px;">Nama</th>
                  <th style="font-size: 12px;">Fungsi</th>
                  <th style="font-size: 12px;">Jabatan</th>
                  <th style="font-size: 12px;">Unit Kerja</th>
                  <th style="font-size: 12px;">Limit Non Gadai</th>
                  <th style="font-size: 12px;">Limit Gadai</th>
                  <th style="font-size: 12px;">Limit Otomatis</th>
                  <th style="font-size: 12px;">Kategori Limit</th>
                  <th style="font-size: 12px;">No SK</th>
                  <th style="font-size: 12px;">Riwayat Limit</th>
                  <th style="font-size: 12px;">Region</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td style="font-size: 12px;">Tiger Nixon</td>
                  <td style="font-size: 12px;">System Architect</td>
                  <td style="font-size: 12px;">Edinburgh</td>
                  <td style="font-size: 12px;">61</td>
                  <td style="font-size: 12px;">2011/04/25</td>
                  <td style="font-size: 12px;">$320,800</td>
                  <td style="font-size: 12px;">Tiger Nixon</td>
                  <td style="font-size: 12px;">System Architect</td>
                  <td style="font-size: 12px;">Edinburgh</td>
                  <td style="font-size: 12px;">61</td>
                  <td style="font-size: 12px;">2011/04/25</td>
                  <td style="font-size: 12px;">$320,800</td>
                </tr>
                <tr>
                  <td style="font-size: 12px;">Tiger Nixon</td>
                  <td style="font-size: 12px;">System Architect</td>
                  <td style="font-size: 12px;">Edinburgh</td>
                  <td style="font-size: 12px;">61</td>
                  <td style="font-size: 12px;">2011/04/25</td>
                  <td style="font-size: 12px;">$320,800</td>
                  <td style="font-size: 12px;">Tiger Nixon</td>
                  <td style="font-size: 12px;">System Architect</td>
                  <td style="font-size: 12px;">Edinburgh</td>
                  <td style="font-size: 12px;">61</td>
                  <td style="font-size: 12px;">2011/04/25</td>
                  <td style="font-size: 12px;">$320,800</td>
                </tr>
                <tr>
                  <td style="font-size: 12px;">Tiger Nixon</td>
                  <td style="font-size: 12px;">System Architect</td>
                  <td style="font-size: 12px;">Edinburgh</td>
                  <td style="font-size: 12px;">61</td>
                  <td style="font-size: 12px;">2011/04/25</td>
                  <td style="font-size: 12px;">$320,800</td>
                  <td style="font-size: 12px;">Tiger Nixon</td>
                  <td style="font-size: 12px;">System Architect</td>
                  <td style="font-size: 12px;">Edinburgh</td>
                  <td style="font-size: 12px;">61</td>
                  <td style="font-size: 12px;">2011/04/25</td>
                  <td style="font-size: 12px;">$320,800</td>
                </tr>
                <tr>
                  <td style="font-size: 12px;">Tiger Nixon</td>
                  <td style="font-size: 12px;">System Architect</td>
                  <td style="font-size: 12px;">Edinburgh</td>
                  <td style="font-size: 12px;">61</td>
                  <td style="font-size: 12px;">2011/04/25</td>
                  <td style="font-size: 12px;">$320,800</td>
                  <td style="font-size: 12px;">Tiger Nixon</td>
                  <td style="font-size: 12px;">System Architect</td>
                  <td style="font-size: 12px;">Edinburgh</td>
                  <td style="font-size: 12px;">61</td>
                  <td style="font-size: 12px;">2011/04/25</td>
                  <td style="font-size: 12px;">$320,800</td>
                </tr>
                <tr>
                  <td style="font-size: 12px;">Tiger Nixon</td>
                  <td style="font-size: 12px;">System Architect</td>
                  <td style="font-size: 12px;">Edinburgh</td>
                  <td style="font-size: 12px;">61</td>
                  <td style="font-size: 12px;">2011/04/25</td>
                  <td style="font-size: 12px;">$320,800</td>
                  <td style="font-size: 12px;">Tiger Nixon</td>
                  <td style="font-size: 12px;">System Architect</td>
                  <td style="font-size: 12px;">Edinburgh</td>
                  <td style="font-size: 12px;">61</td>
                  <td style="font-size: 12px;">2011/04/25</td>
                  <td style="font-size: 12px;">$320,800</td>
                </tr>
                <tr>
                  <td style="font-size: 12px;">Tiger Nixon</td>
                  <td style="font-size: 12px;">System Architect</td>
                  <td style="font-size: 12px;">Edinburgh</td>
                  <td style="font-size: 12px;">61</td>
                  <td style="font-size: 12px;">2011/04/25</td>
                  <td style="font-size: 12px;">$320,800</td>
                  <td style="font-size: 12px;">Tiger Nixon</td>
                  <td style="font-size: 12px;">System Architect</td>
                  <td style="font-size: 12px;">Edinburgh</td>
                  <td style="font-size: 12px;">61</td>
                  <td style="font-size: 12px;">2011/04/25</td>
                  <td style="font-size: 12px;">$320,800</td>
                </tr>
                <tr>
                  <td style="font-size: 12px;">Tiger Nixon</td>
                  <td style="font-size: 12px;">System Architect</td>
                  <td style="font-size: 12px;">Edinburgh</td>
                  <td style="font-size: 12px;">61</td>
                  <td style="font-size: 12px;">2011/04/25</td>
                  <td style="font-size: 12px;">$320,800</td>
                  <td style="font-size: 12px;">Tiger Nixon</td>
                  <td style="font-size: 12px;">System Architect</td>
                  <td style="font-size: 12px;">Edinburgh</td>
                  <td style="font-size: 12px;">61</td>
                  <td style="font-size: 12px;">2011/04/25</td>
                  <td style="font-size: 12px;">$320,800</td>
                </tr>
                <tr>
                  <td style="font-size: 12px;">Tiger Nixon</td>
                  <td style="font-size: 12px;">System Architect</td>
                  <td style="font-size: 12px;">Edinburgh</td>
                  <td style="font-size: 12px;">61</td>
                  <td style="font-size: 12px;">2011/04/25</td>
                  <td style="font-size: 12px;">$320,800</td>
                  <td style="font-size: 12px;">Tiger Nixon</td>
                  <td style="font-size: 12px;">System Architect</td>
                  <td style="font-size: 12px;">Edinburgh</td>
                  <td style="font-size: 12px;">61</td>
                  <td style="font-size: 12px;">2011/04/25</td>
                  <td style="font-size: 12px;">$320,800</td>
                </tr>
                <tr>
                  <td style="font-size: 12px;">Tiger Nixon</td>
                  <td style="font-size: 12px;">System Architect</td>
                  <td style="font-size: 12px;">Edinburgh</td>
                  <td style="font-size: 12px;">61</td>
                  <td style="font-size: 12px;">2011/04/25</td>
                  <td style="font-size: 12px;">$320,800</td>
                  <td style="font-size: 12px;">Tiger Nixon</td>
                  <td style="font-size: 12px;">System Architect</td>
                  <td style="font-size: 12px;">Edinburgh</td>
                  <td style="font-size: 12px;">61</td>
                  <td style="font-size: 12px;">2011/04/25</td>
                  <td style="font-size: 12px;">$320,800</td>
                </tr>
                <tr>
                  <td style="font-size: 12px;">Tiger Nixon</td>
                  <td style="font-size: 12px;">System Architect</td>
                  <td style="font-size: 12px;">Edinburgh</td>
                  <td style="font-size: 12px;">61</td>
                  <td style="font-size: 12px;">2011/04/25</td>
                  <td style="font-size: 12px;">$320,800</td>
                  <td style="font-size: 12px;">Tiger Nixon</td>
                  <td style="font-size: 12px;">System Architect</td>
                  <td style="font-size: 12px;">Edinburgh</td>
                  <td style="font-size: 12px;">61</td>
                  <td style="font-size: 12px;">2011/04/25</td>
                  <td style="font-size: 12px;">$320,800</td>
                </tr>
                <tr>
                  <td style="font-size: 12px;">Tiger Nixon</td>
                  <td style="font-size: 12px;">System Architect</td>
                  <td style="font-size: 12px;">Edinburgh</td>
                  <td style="font-size: 12px;">61</td>
                  <td style="font-size: 12px;">2011/04/25</td>
                  <td style="font-size: 12px;">$320,800</td>
                  <td style="font-size: 12px;">Tiger Nixon</td>
                  <td style="font-size: 12px;">System Architect</td>
                  <td style="font-size: 12px;">Edinburgh</td>
                  <td style="font-size: 12px;">61</td>
                  <td style="font-size: 12px;">2011/04/25</td>
                  <td style="font-size: 12px;">$320,800</td>
                </tr>
                <tr>
                  <td style="font-size: 12px;">Tiger Nixon</td>
                  <td style="font-size: 12px;">System Architect</td>
                  <td style="font-size: 12px;">Edinburgh</td>
                  <td style="font-size: 12px;">61</td>
                  <td style="font-size: 12px;">2011/04/25</td>
                  <td style="font-size: 12px;">$320,800</td>
                  <td style="font-size: 12px;">Tiger Nixon</td>
                  <td style="font-size: 12px;">System Architect</td>
                  <td style="font-size: 12px;">Edinburgh</td>
                  <td style="font-size: 12px;">61</td>
                  <td style="font-size: 12px;">2011/04/25</td>
                  <td style="font-size: 12px;">$320,800</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>

    </div>
@endsection