@extends('login.app')

@section('content')

<div class="logo"><img src="public/img/Login/Logo.png" width="103.63px" height="54px"></div>
  	<div class="text"><p>Sistem Limit Wewenang <br> 
  	Memutus Pembiayaan</p></div>
  	<div class="box">
  		<div class="login-title">Selamat Datang</div>
  		<div class="subtitle">Silahkan Login Terlebih Dahulu</div>
  		<form method="POST" action="{{ route('login') }}">
        @csrf
  			<div class="form-group NIP">
  				<span><img src="public/img/Login/user.png" width="15px" height="15px"></span><label>Email</label>
    			<input type="text" class="form-control @error('email') is-invalid @enderror" placeholder="Masukkan Email Anda" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
    			@error('email')
                   <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
  			</div>
  			<div class="form-group password">
    			<span><img src="public/img/Login/password.png" width="15px" height="15px"></span><label for="exampleInputPassword1"> Kata Sandi</label>
    			<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Masukkan Password Anda" name="password" required autocomplete="current-password">
          @if ($errors->has('password'))
              <span class="invalid-feedback">
                  <strong>{{ $errors->first('password') }}</strong>
              </span>
          @endif

  			</div>
  				<div class="tombol"><button type="submit" class="btn btn-success btn-lg btn-block">Login</button></div>
		</form>
  	</div>
  	<div class="copyright">Copyright © 2020 <br>
  	PT Bank Syariah Mandiri</div>

@endsection

