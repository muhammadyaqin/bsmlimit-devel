@extends('layouts.backend')
@section('content')


    <div class="container-fluid">

      <!-- Page Heading -->
      <div class="card border-left-success">
        <div class="card-body">
          <h1 class="h3 mb-2 text-gray-800" style="margin-top: 10px;"><b>PWMP</b></h1>
        </div>
      </div>

      <style type="text/css">
        .select2-container--default .select2-selection--single{
          border: 0
        }
        .buttonimport{
          margin: 12px 0px 0px 0px;
          width: 100%;
          padding: 0px 18px;
        }
        .buttonexcel{
          margin: 0px 0px 0px 10px;
          width: 100%;
        }
        .buttonimportexcel{
          margin: 0px 0px 0px 0px;
          width: 100%;
        }
      </style>

      <div class="card shadow mb-4">
        <div class="card-header">
          <div class="col-md-12">
          <form action="{{URL('/pwmp')}}" method="get">
          @if(Auth::user()->cekRole()->role == "PUSAT") 
          <div class="row" style="margin-top: 10px;">
              <div class="col-md-6">
                <div class="form-group">
                  <label><b>Region</b></label>
                    <select id="region" name="region" class="form-control">
                        <option value="">ALL</option>
                      @foreach($getregion as $region)
                        <option @if($statusReg == $region->kode) selected @endif value="{{$region->kode}}">{{$region->kode}}</option>
                      @endforeach
                    </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label><b>Area</b></label>
                    <select id="area" name="area" class="form-control">
                        <option value="">ALL</option>
                      @foreach($getarea as $area)
                        <option @if($statusAct == $area->kode) selected @endif value="{{$area->kode}}">{{$area->nama}}</option>
                      @endforeach
                    </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label><b>Cabang</b></label>
                    <select id="cabang" name="cabang" class="form-control">
                        <option value="">ALL</option>
                      @foreach($getcabang as $cabang)
                        <option @if($statusCab == $cabang->kode) selected @endif value="{{$cabang->kode}}">{{$cabang->nama}}</option>
                      @endforeach
                    </select>
                </div>
              </div>
               <div class="col-md-6">
                <div class="form-group">
                  <label><b>Keywords</b></label>
                    <input type="text" name="name_nip" value="{{$name_nip}}" placeholder="Nama dan NIP" class="form-control" id="name_nip"  onkeyup="this.value = this.value.toUpperCase();">
                </div>
              </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                  <input type="submit" value="Search" class="btn btn-success" style="margin-top: 8px;">
                      <a href="{{url('/pwmp')}}" class="btn btn-info" style="text-align:left; margin-top: 8px;">Reset</a>
                </div>
              </div>
            @elseif(Auth::user()->cekRole()->role == "ERM")
            <div class="row" style="margin-top: 10px;">
                <div class="col-md-6">
                  <div class="form-group">
                    <label><b>Region</b></label>
                      <select id="region" name="region" class="form-control">
                          <option value="">ALL</option>
                        @foreach($getregion as $region)
                          <option @if($statusReg == $region->kode) selected @endif value="{{$region->kode}}">{{$region->kode}}</option>
                        @endforeach
                      </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label><b>Area</b></label>
                      <select id="area" name="area" class="form-control">
                          <option value="">ALL</option>
                        @foreach($getarea as $area)
                          <option @if($statusAct == $area->kode) selected @endif value="{{$area->kode}}">{{$area->nama}}</option>
                        @endforeach
                      </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label><b>Cabang</b></label>
                      <select id="cabang" name="cabang" class="form-control">
                          <option value="">ALL</option>
                        @foreach($getcabang as $cabang)
                          <option @if($statusCab == $cabang->kode) selected @endif value="{{$cabang->kode}}">{{$cabang->nama}}</option>
                        @endforeach
                      </select>
                  </div>
                </div>
                 <div class="col-md-6">
                  <div class="form-group">
                    <label><b>Keywords</b></label>
                      <input type="text" name="name_nip" value="{{$name_nip}}" placeholder="Nama dan NIP" class="form-control" id="name_nip"  onkeyup="this.value = this.value.toUpperCase();">
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <input type="submit" value="Search" class="btn btn-success" style="margin-top: 8px;">
                      <a href="{{url('/pwmp')}}" class="btn btn-info" style="text-align:left; margin-top: 8px;">Reset</a>
                </div>
              </div>
              @elseif(Auth::user()->cekRole()->role == "IOG")
              <div class="row" style="margin-top: 10px;">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label><b>Region</b></label>
                        <select id="region" name="region" class="form-control">
                            <option value="">ALL</option>
                          @foreach($getregion as $region)
                            <option @if($statusReg == $region->kode) selected @endif value="{{$region->kode}}">{{$region->kode}}</option>
                          @endforeach
                        </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label><b>Area</b></label>
                        <select id="area" name="area" class="form-control">
                            <option value="">ALL</option>
                          @foreach($getarea as $area)
                            <option @if($statusAct == $area->kode) selected @endif value="{{$area->kode}}">{{$area->nama}}</option>
                          @endforeach
                        </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label><b>Cabang</b></label>
                        <select id="cabang" name="cabang" class="form-control">
                            <option value="">ALL</option>
                          @foreach($getcabang as $cabang)
                            <option @if($statusAct == $cabang->kode) selected @endif value="{{$cabang->kode}}">{{$cabang->nama}}</option>
                          @endforeach
                        </select>
                    </div>
                  </div>
                   <div class="col-md-6">
                    <div class="form-group">
                      <label><b>Keywords</b></label>
                        <input type="text" name="name_nip" value="{{$name_nip}}" placeholder="Nama dan NIP" class="form-control" id="name_nip"  onkeyup="this.value = this.value.toUpperCase();">
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <input type="submit" value="Search" class="btn btn-success" style="margin-top: 8px;">
                        <a href="{{url('/pwmp')}}" class="btn btn-info" style="text-align:left; margin-top: 8px;">Reset</a>
                  </div>
                </div>
            @else
            <div class="row" style="margin-top: 10px;">
              <div class="col-md-6">
                <div class="form-group">
                  <label><b>Area</b></label>
                    <select id="area" name="area" class="form-control">
                        <option value="">ALL</option>
                      @foreach($getarea as $area)
                        <option @if($statusAct == $area->kode) selected @endif value="{{$area->kode}}">{{$area->nama}}</option>
                      @endforeach
                    </select>
                </div>
              </div>
               <div class="col-md-6">
                <div class="form-group">
                  <label><b>Keywords</b></label>
                    <input type="text" name="name_nip" value="{{$name_nip}}" placeholder="Nama dan NIP" class="form-control" id="name_nip"  onkeyup="this.value = this.value.toUpperCase();">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <input type="submit" value="Search" class="btn btn-success" style="margin-top: 8px;">
                      <a href="{{url('/pwmp')}}" class="btn btn-info" style="text-align:left; margin-top: 8px;">Reset</a>
                </div>
              </div>
            </div>
            @endif
          </form>
          </div>
        </div>
        <div class="card-header">
          <div class="row">
                <div class="col-md-2">
                  <div class="form-group">
                      <a href="{{URL::asset('/pwmp/export_excel/')}}" class="btn btn-success btn-icon-split buttonexcel">
                        <span class="text">Excel.Xlsx</span>
                      </a>
                  </div>
                </div>

                    {{-- notifikasi form validasi --}}
                    @if ($errors->has('file'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('file') }}</strong>
                    </span>
                    @endif
                 
                    {{-- notifikasi sukses --}}
                    @if ($sukses = Session::get('sukses'))
                    <div class="alert alert-success alert-block">
                      <button type="button" class="close" data-dismiss="alert">×</button> 
                      <strong>{{ $sukses }}</strong>
                    </div>
                    @endif
                
                <!-- <div class="col-md-2">    
                  <button type="button"  class="btn btn-primary mr-5 buttonimportexcel" data-toggle="modal" data-target="#importExcel">
                    Import Excel
                  </button>
                </div> -->

                  <!-- Import Excel -->
                  <div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <form method="post" action="/pwmp/import_excel" enctype="multipart/form-data">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Import Excel</h5>
                          </div>
                          <div class="modal-body">
               
                            {{ csrf_field() }}
               
                            <label>Pilih File Excel</label>
                            <div class="form-group">
                              <input type="file" name="file" required="required">
                            </div>
               
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Import</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
          </div>
        </div>
        <div class="card-body">
            <style type="text/css">
              table#grid_riwayatpwmp {
                width: 100%;
              }
              table#grid_riwayatpwmp th {
                font-size: 12px;
                text-align: center;
              }
              table#grid_riwayatpwmp th.tanggal {
                width: 90px;
              }
              table#grid_riwayatpwmp th.manage {
                width: 80px;
              }
              table#grid_riwayatpwmp td {
                font-size: 12px;
                vertical-align: top;
              }
              table#grid_riwayatpwmp td.nominal {
                text-align: right
              }
              table#grid_riwayatpwmp td.statuspengajuan {
                text-align: center
              }
              table#grid_riwayatpwmp td.statuspengajuan div.statusdefault {
                padding: 0px 3px;
                color: white;
              }
              table#grid_riwayatpwmp td.statuspengajuan div.statuswarning {
                color: black;
              }
              table#grid_riwayatpwmp td.manage {
                text-align: center
              }
              table#grid_riwayatpwmp td.manage a {
                margin-right: 1px;
              }
              table#grid_riwayatpwmp td.no_index {
                text-align: center;
              }

            </style>
            <table id="grid_riwayatpwmp">
              <tr>
                <th>No</th>
                <th>NIP</th>
                <th>Nama</th>
                <th>Limit Non Gadai</th>
                <th>Limit Gadai<br/>Non Logam Mulia</th>
                <th>Limit Gadai<br/>Logam Mulia</th>
                <th>Limit Otomatis</th>
                <th>No. SK</th>
                <th class="tanggal">Tgl. SK</th>
                <th class="manage">Manage</th>
              </tr>
              @foreach($pwmp as $index => $riwayat_pwmp)
                <tr>
                  <td class="no_index">{{ $numbering+$index+1 }}</td>
                  <td>{{ $riwayat_pwmp->nip }}</td>
                  <td>{{ $riwayat_pwmp->namapejabat }}</td>
                  <td class="nominal">{{ number_format($riwayat_pwmp->limitnongadai, 0, ',', '.') }}</td>
                  <td class="nominal">{{ number_format($riwayat_pwmp->limitgadainlm, 0, ',', '.') }}</td>
                  <td class="nominal">{{ number_format($riwayat_pwmp->limitgadailm, 0, ',', '.') }}</td>
                  <td class="nominal">{{ number_format($riwayat_pwmp->limitotomatis, 0, ',', '.') }}</td>
                  <td>{{ $riwayat_pwmp->nosk }}</td>
                  <td class="nominal">{{ \Carbon\Carbon::parse($riwayat_pwmp->tglsk)->format('d-m-Y') }}</td>
                  <td class="manage">
                    <a href="{{URL::to('/pwmp/'.$riwayat_pwmp->nip.'/detail')}}"><img src="{{url::asset('public/img/icon/cta_opendetail.png')}}" width="26px"></a>
                  </td>
                </tr>
              @endforeach
            </table>
           <div class="row pull-right mr-3 mt-3 justify-content-end" >
                    {{ $pwmp->appends(request()->except('page'))->links() }}
            </div>
        </div>
      </div>

    </div>

@endsection

@section('jscustom')

  <script type="text/javascript">
    $(document).ready(function() {
      $('#daterange_pengajuan').val($('#startDate').val() + ' - ' + $('#endDate').val())
      $('#daterange_pengajuan').daterangepicker({
        locale:{
          format:'DD-MM-YYYY'
        }
      })
      $("#search_nip").keyup(function() {
        $('#dataTable_ass').DataTable().search($(this).val()).draw() ;
      });

        $('#dataTable_ass').DataTable({
            "dom": 'lrtip',
            responsive: true,
            deferRender: true,
            language: {
              emptyTable: "Nip has No History",
              search: "_INPUT_",
          },
        });
    });

    $('#daterange_pengajuan').on('apply.daterangepicker', function(ev, picker) {
      $('#startDate').val(picker.startDate.format('DD-MM-YYYY'))
      $('#endDate').val(picker.endDate.format('DD-MM-YYYY'))
    });

    $('#area').select2();
    $('body').find('span.select2-selection__rendered').addClass('form-control')
    $('body').find('span.select2-selection--single').css({
      border:0
    })

    $('#status').select2();
    $('body').find('span.select2-selection__rendered').addClass('form-control')
    $('body').find('span.select2-selection--single').css({
      border:0
    })
  </script>

  <style>
    table {
      border-collapse: collapse;
      width: 30px;
    }

    td, th {
      border: 1px solid #dddddd;
      text-align: left;
      padding: 8px;
    }

    tr:nth-child(even) {

    }
  </style>

@stop