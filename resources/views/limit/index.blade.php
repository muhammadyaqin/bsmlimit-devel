@extends('limit.app')
@section('content')
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Buat Pengajuan Permohonan Limit</h1>
  </div>

          <!-- Content Row -->
          <div class="row">

            <!-- Permohonan Baru Card -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-danger shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-lg font-weight-bold text-danger text-uppercase mb-1 text-center">Permohonan Limit Baru</div>
                      <br>
                      <center><img src="{{url('img/Permohonan Limit/permohonan 2.png')}}"></center>
                      <br>
                      <a href="/limit/baru" type="button" class="btn btn-danger btn-lg btn-block">Buat Pengajuan</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Peningkatan Limit Card -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-lg font-weight-bold text-primary text-uppercase mb-1 text-center">Permohonan Peningkatan Limit</div>
                      <br>
                      <center><img src="{{url('img/Permohonan Limit/permohonan 1.png')}}"></center>
                      <br>
                      <a href="#" type="button" class="btn btn-primary btn-lg btn-block">Buat Pengajuan</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>


          </div>
@endsection