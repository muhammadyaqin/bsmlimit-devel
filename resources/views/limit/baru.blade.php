@extends('layouts.backend')
@section('content')
<div class="card mb-4">
  <div class="card-header border-left-success shadow h-100 py-2">
    <div class="card-body">
      <div class="row no-gutters align-items-center">
        <div class="col mr-2">
          <div class="row">
            @if(Auth::user()->cekRole()->role == "REGION")
              <a class="nav-link" href="{{URL::asset('/home-region')}}">
                <img src="{{url('public/img/icon/back.png')}}" width="30px" height="30px">
              </a>
            @elseif(Auth::user()->cekRole()->role == "CABANG")
              <a class="nav-link" href="{{URL::asset('/home-cabang')}}">
                <img src="{{url('public/img/icon/back.png')}}" width="30px" height="30px">
              </a>
            @elseif(Auth::user()->cekRole()->role == "ERM")
              <a class="nav-link" href="{{URL::asset('/home-erm')}}">
                <img src="{{url('public/img/icon/back.png')}}" width="30px" height="30px">
              </a>
            @endif
            <div class="h5 mb-0 font-weight-bold text-gray-800" style="margin-top: 12px;">Form Pengajuan Permohonan Limit Baru</div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="card-body"><!-- 
    <form action="{{ url('/ajukan/limit/baru') }}" method="POST">
      <input type="hidden" name="_token" value="{{csrf_token()}}"> -->
      <form id="form_add_nip">
            <div class="row">
<!--               <input type="hidden" name="_token" id="edit_pejabat_id"> -->
                <div class="col-md-6">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                          <label><b>Nomor Induk Pegawai</b></label>
                          <input type="text" name="nip" class="form-control" id="nip" placeholder="Masukkan Nama / NIP" onkeyup="keyupnip();">
                          <input type="hidden" name="recomnip" id="recomnip" />
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                          <label><b>Status Karyawan</b></label>
                          <input type="text" name="statuskaryawan" class="form-control" id="statuskaryawan" readonly="">
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label><b>Nama</b></label>
                      <input type="text" name="name" readonly="" class="form-control" id="name">
                  </div>
                  <div class="form-group">
                    <label><b>Jabatan</b></label>
                      <input type="text" name="jabatan" readonly="" class="form-control" id="jabatan">
                      <input type="hidden" id="jabatan1" name="jabatan1">
                  </div>
                  <div class="form-group">
                    <label><b>Cabang</b></label>
                      <input type="text" name="cabang" readonly="" class="form-control" id="cabang">
                      <input type="hidden" id="cabang1" name="cabang1">
                  </div>
                  <div class="form-group">
                    <label><b>Area</b></label>
                      <input type="text" name="area" readonly="" class="form-control" id="area">
                      <input type="hidden" id="area1" name="area1">
                  </div>
                  <div class="form-group">
                    <label><b>Fungsi</b></label>
                      <input type="text" name="fungsi" readonly="" class="form-control" id="fungsi">
                      <input type="hidden" id="fungsi1" name="fungsi1">
                  </div>
                  <div class="form-group">
                    <label><b>Kategori Limit</b></label>
                      <input type="text" name="kategori_limit" readonly="" class="form-control" id="kategori_limit">
                      <input type="hidden" id="kategori_limit1" name="kategori_limit1">
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                          <label><b>Sertifikasi Manajemen Resiko</b></label>
                          <div class="input-group">
                            <input type="text" name="smrisk" readonly="" class="form-control" id="smrisk">
                          </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label><b>Performa Appraisal</b></label>
                        <div class="input-group">
                          <input type="text" name="appraisal" readonly="" class="form-control" id="appraisal">
                          <input type="hidden" id="appraisal1" name="appraisal1">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                          <label><b>Sanksi</b></label>
                          <div class="input-group">
                            <input type="text" name="sanksi" readonly="" class="form-control" id="sanksi">
                            <input type="hidden" name="kodesanksi" readonly="" class="form-control" id="kodesanksi">
                          </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label><b>Fraud</b></label>
                        <div class="input-group">
                          <input type="text" name="fraud" readonly="" class="form-control" id="fraud">
                          <input type="hidden" id="fraud1" name="fraud1">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                          <label><b>Refreshment</b></label>
                          <div class="input-group">
                            <input type="text" name="refreshment" readonly="" class="form-control" id="refreshment">
                          </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <label id="label_pengalaman_pby"><b>Pengalaman Pembiayaan</b></label>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                              <div class="input-group">
                                <input type="text" name="calc_year" readonly="" class="form-control text-right" id="calc_year">
                                <div class="input-group-text">Thn</div>
                              </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                              <div class="input-group">
                                <input type="text" name="calc_month" readonly="" class="form-control text-right" id="calc_month">
                                <div class="input-group-text">Bln</div>
                              </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row" style="margin-top: 10px;">
                    <div class="col-md-12">
                        <label><b>SK Terakhir</b></label>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                  <div class="input-group">
                                    <div class="input-group-text">Nomor</div>
                                    <input type="text" name="no_sk" readonly="" class="form-control" id="no_sk">
                                  </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <div class="input-group">
                                  <div class="input-group-text">Tanggal</div>
                                  <input type="text" name="tanggal_sk1" readonly="" class="form-control" id="tanggal_sk1">
                                  <input type="hidden" id="tanggal_sk" name="tanggal_sk">
                                </div>
                              </div>
                            </div>
                          </div>
                      <!-- </div> -->
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="row" style="margin-top: 10px;">
                    <div class="col-md-12">
                      <label><b>KUALITAS PORTOFOLIO PEMBIAYAAN</b></label>
                        <div class="form-group">
                          <div class="row">
                            <div class="col-md-6">
                              <label><b>Lancar</b></label>
                              <div class="input-group">
                                <div class="input-group-text" style="margin-bottom: 10px;">Rp.</div>
                                <input type="text" name="lancar" readonly="" class="form-control text-right" style="margin-bottom: 10px;" id="lancar">
                                <input type="hidden" id="lancar1" name="lancar1">
                              </div>
                            </div>
                            <div class="col-md-3">
                              <div class="input-group">
                                <div class="input-group-text" style="margin-top: 32px;">%</div>
                                <input type="text" name="persen_lancar1" readonly="" class="form-control text-right" id="persen_lancar" style="margin-top: 32px;">
                                <input type="hidden" id="persen_lancar1" name="persen_lancar1">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <label><b>DPK</b></label>
                              <div class="input-group">
                                <div class="input-group-text" style="margin-bottom: 10px;">Rp.</div>
                                <input type="text" name="dpk" readonly="" style="margin-bottom: 10px;" class="form-control text-right" id="dpk">
                                <input type="hidden" id="dpk1" name="dpk1">
                              </div>
                            </div>
                            <div class="col-md-3">
                              <div class="input-group">
                                <div class="input-group-text" style="margin-top: 32px; marg">%</div>
                                <input type="text" name="persen_dpk" readonly="" class="form-control text-right" id="persen_dpk" style="margin-top: 32px;">
                                <input type="hidden" id="persen_dpk1" name="persen_dpk1">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <label><b>NPF</b></label>
                              <div class="input-group">
                                <div class="input-group-text" style="margin-bottom: 10px;">Rp.</div>
                                <input type="text" name="npf" readonly="" style="margin-bottom: 10px;" class="form-control text-right" id="npf">
                                <input type="hidden" id="npf1" name="npf1">
                              </div>
                            </div>
                            <div class="col-md-3">
                              <div class="input-group">
                                <div class="input-group-text" style="margin-top: 32px;">%</div>
                                <input type="text" name="persen_npf" readonly="" class="form-control text-right" id="persen_npf" style="margin-top: 32px;">
                                <input type="hidden" id="persen_npf1" name="persen_npf1">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <label><b>Total</b></label>
                              <div class="input-group">
                                <div class="input-group-text">Rp.</div>
                                <input type="text" name="total" readonly="" class="form-control text-right" id="total">
                                <input type="hidden" id="total1" name="total1">
                              </div>
                            </div>
                          </div>
                        </div>
                        <label><b>PELATIHAN PEMBIAYAAN</b></label>
                          <div class="row">
                            <div class="col-md-12" id="content_msutrainingpby">
                              <table style="width:100%">
                                <tr><th style="text-align: center;">No</th><th style="text-align: center;">Nama Pelatihan</th></tr>
                              </table>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6" style="margin-top: 20px;">
                              <label><b>DATA LIMIT TERAKHIR</b></label>
                              <div class="form-group">
                                  <label><b>Non Gadai</b></label>
                                  <div class="input-group">
                                    <input type="text" name="limit_non_gadai" readonly="" class="form-control text-right" id="limit_non_gadai">
                                  </div>
                              </div>
                            </div>
                            <div class="col-md-6" style="margin-top: 20px;">
                              <label><b>DATA LIMIT BARU</b></label>
                              <div class="form-group">
                                  <label><b>Non Gadai</b></label>
                                  <div class="input-group">
                                    <input type="text" name="pengajuan_limit_non_gadai" class="form-control text-right ribuan" id="pengajuan_limit_non_gadai" onblur="fungsinongadai()">
                                  </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                  <label><b>Gadai Logam Mulia</b></label>
                                  <div class="input-group">
                                    <input type="text" name="limit_gadai" readonly="" class="form-control text-right" id="limit_gadai">
                                  </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                  <label><b>Gadai Logam Mulia</b></label>
                                  <div class="input-group">
                                    <input type="text" name="pengajuan_limit_gadai" class="form-control text-right ribuan" id="pengajuan_limit_gadai" onblur="fungsigadailm()">
                                  </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label><b>Gadai Non Logam Mulia</b></label>
                                <div class="input-group">
                                  <input type="text" name="limit_non_logam" readonly="" class="form-control text-right" id="limit_non_logam">
                                </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label><b>Gadai Non Logam Mulia</b></label>
                                <div class="input-group">
                                  <input type="text" name="pengajuan_limit_non_logam" class="form-control text-right ribuan" id="pengajuan_limit_non_logam" onblur="fungsigadainlm()">
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                           <div class="col-md-6">
                             <div class="form-group">
                               <label><b>Limit Otomatis</b></label>
                               <div class="input-group">
                                 <input type="text" readonly name="limit_otomatis" readonly="" class="form-control text-right" id="limit_otomatis">
                               </div>
                             </div>
                           </div>
                           <div class="col-md-6">
                             <div class="form-group">
                               <label><b>Limit Otomatis</b></label>
                               <div class="input-group">
                                 <input type="text" name="pengajuan_limit_otomatis" class="form-control text-right ribuan" id="pengajuan_limit_otomatis" value="0" onblur="fungsiotomatis()">
                               </div>
                             </div>
                           </div>
                          </div>
                          <div class="form-group">
                            <label><b>FIle SKPP</b></label>
                              <div class="row">
                                <input type="file" name="unggah_file" readonly="" class="form-input" id="unggah_file" style="margin-left: 10px;">
                              </div>
                          </div>
                          <div class="row" style="margin-top: 10px;">
                            <div class="col-md-12">
                              
                            </div>
                          </div>
                          <div class="card-body">
                            @if(Auth::user()->cekRole()->role == "CABANG")
                            <button type="button" class="btn btn-success btn-block" onclick="add()" name="send" id="send">Ajukan Permohonan</button>
                            @elseif(Auth::user()->cekRole()->role == "REGION")
                            <button type="button" class="btn btn-success btn-block" onclick="addregion()" name="send" id="send">Ajukan Permohonan</button>
                            @elseif(Auth::user()->cekRole()->role == "ERM")
                             <button type="button" class="btn btn-success btn-block" onclick="addERM()" name="send" id="send">Ajukan Permohonan</button>
                            @endif
                            <input type="hidden" name="jenispengajuan" id="jenispengajuan" />
                          </div>
                      </div>
                    </div>
                  </div>
                  </div>
                  </div>
                  </div>
                </div>
      </form>
  </div>
</div>
    
</div>
@endsection

@section('jscustom')
<script src="{{asset('public/js/jquery-ui.js')}}" type="text/javascript"></script>

<script type="text/javascript">
    var keyupnip = function(){
      $('#nip').val($('#nip').val().toUpperCase());
    }

    $( "#nip" ).autocomplete({
      autoFocus: true,
      // appendTo: $( "#form_input_nip" ),
      change: function () {
        $('#recomnip').val($('#nip').val());
      },
      source: function( request, response ) {
      // console.log('masuk');

        var recomnip = $('#recomnip').val()
        // console.log(request.term + ' = ' + recomnip)
        if (request.term == "" || request.term == recomnip) {
          return;
        }
        else {
          $.ajax({
            type: "POST",
                url: "{{url('/limit/baru/autocomplete')}}",
              dataType: "json",
                data: {
                  searchNIP : request.term,
                  _token : '{{ csrf_token() }}',
                  // jenis : "pph21"
                },
                beforeSend: function(){
                  // $('#loadernpwp21').show();
                  // $('#modal').show();
                }, 
                success: function( data ) {
                  if (data.pejabatt.length == 0) {
                    //set focus sebelum alert
                    Swal.fire({
                       icon: 'error',
                       title: 'Gagal',
                       text: "Data Tidak Ditemukan",
                    })   
                  }
                  // $('#loadernpwp21').hide();
                  // var tanggal;
                  response($.map(data.pejabatt, function (item) {
                  console.log(data.pejabatt);

                  if (item.tglsk != null) {
                    tglsk = item.tglsk.split(' ')[0];
                  } 
                  else {
                    var tglsk = "";
                  }
                  if (item.kategorilimit != null || item.keterangankategorilimit != null ) {
                    kode_limit = item.kategorilimit;
                    keterangan_limit = item.keterangankategorilimit;
                  } 
                  else {
                    var kode_limit = "";
                    var keterangan_limit = "";
                  }
                  if (item.fungsi != null || item.keteranganfungsi != null ) {
                    kode_fungsi = item.fungsi;
                    keterangan_fungsi = item.keteranganfungsi;
                  } 
                  else {
                    var kode_fungsi = "";
                    var keterangan_fungsi = "";
                  }

                    return {
                        label: item.nip + ' - ' + item.namapejabat,
                        value: item.nip,
                        id: item.id,
                        name: item.namapejabat,
                        nip: item.nip,
                        jabatan: item.kodejabatan + ' - ' + item.namajabatan,
                        jabatan1: item.kodejabatan,
                        cabang: item.kodecabang + ' - ' + item.namacabang,
                        cabang1: item.kodecabang,
                        calc_month : item.jenispengajuan == 'BARU'? item.age_month: item.age_month_pwmp,
                        calc_year : item.jenispengajuan == 'BARU'? item.age_year: item.age_year_pwmp,
                        limit_non_gadai: numeral(item.limitnongadai).format('0,0'),
                        limit_non_logam: numeral(item.limitgadainlm).format('0,0'),
                        limit_logam: numeral(item.limitgadailm).format('0,0'),
                        limit_otomatis: numeral(item.limitotomatis).format('0,0'),
                        lancar: numeral(item.nomlancar).format('0,0'),
                        npf: numeral(item.nomnpf).format('0,0'),
                        dpk: numeral(item.nomdpk).format('0,0'),
                        total: numeral(item.nomospokok).format('0,0'),
                        lancar1: item.nomlancar,
                        npf1: item.nomnpf,
                        dpk1: item.nomdpk,
                        total1: item.nomospokok,
                        persen_lancar: (Math.floor(item.pctlancar*100) / 100).toFixed(2),
                        persen_dpk: (Math.floor(item.pctdpk*100) / 100).toFixed(2),
                        persen_npf: (Math.floor(item.pctnpf*100) / 100).toFixed(2),
                        smrisk: item.statussertifikasi,
                        smrisk1: item.smrisk,
                        refreshment: item.statusrefreshment,
                        appraisal: item.keteranganappraisal,
                        appraisal1: item.appraisal,
                        sanksi: item.keterangansanksi,
                        kodesanksi: item.sanksi,
                        fungsi: kode_fungsi + ' - ' + keterangan_fungsi,
                        fungsi1: kode_fungsi,
                        fraud: item.keteranganfraud,
                        fraud1: item.fraud,
                        kategori_limit: kode_limit + ' - ' + keterangan_limit,
                        kategori_limit1: kode_limit,
                        tanggal_sk: tglsk,
                        no_sk: item.nosk,
                        statuskaryawan: item.statuskaryawan.toUpperCase(),
                        area: item.kodearea + ' - ' + item.namaarea,
                        jenispengajuan: item.jenispengajuan
                    }

                  }));
                },
                error: function(err) {
                  // $('#loadernpwp21').hide();
                  console.log(err);
                }
            });
          }
        },
        minLength: 1,
        select: function( event, ui ) {
          var tglsk = "";
          if (ui.item.tanggal_sk != "") tglsk = moment(ui.item.tanggal_sk).format('DD-MM-YYYY');
          
          $(this).val(ui.item.label);
          $('#hidden_nip').val(ui.item.id);
          $('#name').val(ui.item.name);
          $('#jabatan').val(ui.item.jabatan);
          $('#jabatan1').val(ui.item.jabatan1);
          $('#cabang').val(ui.item.cabang);
          $('#cabang1').val(ui.item.cabang1);
          $('#limit_non_gadai').val(ui.item.limit_non_gadai);
          $('#limit_non_logam').val(ui.item.limit_non_logam);
          $('#limit_gadai').val(ui.item.limit_logam);
          $('#limit_otomatis').val(ui.item.limit_otomatis);
          $('#lancar').val(ui.item.lancar);
          $('#lancar1').val(ui.item.lancar1);
          $('#dpk').val(ui.item.dpk);
          $('#dpk1').val(ui.item.dpk1);
          $('#npf').val(ui.item.npf);
          $('#npf1').val(ui.item.npf1);
          $('#total').val(ui.item.total);
          $('#total1').val(ui.item.total1);
          $('#persen_lancar').val(ui.item.persen_lancar);
          $('#persen_lancar1').val(ui.item.persen_lancar);
          $('#persen_npf').val(ui.item.persen_npf);
          $('#persen_npf1').val(ui.item.persen_npf);
          $('#persen_dpk').val(ui.item.persen_dpk);
          $('#persen_dpk1').val(ui.item.persen_dpk);
          $('#fraud').val(ui.item.fraud);
          $('#fraud1').val(ui.item.fraud1);
          $('#calc_year').val(ui.item.calc_year);
          $('#calc_month').val(ui.item.calc_month);
          // $('#tanggal_sk').val(ui.item.tanggal_sk);
          $("#tanggal_sk1").val(tglsk);
          $("#tanggal_sk").val(ui.item.tanggal_sk);
          $('#no_sk').val(ui.item.no_sk);
          $('#area').val(ui.item.area);
          $('#smrisk').val(ui.item.smrisk);
          $('#refreshment').val(ui.item.refreshment);
          $('#appraisal').val(ui.item.appraisal);
          $('#sanksi').val(ui.item.sanksi);
          $('#kodesanksi').val(ui.item.kodesanksi);

          $('#address').val(ui.item.address);
          $('#statuskaryawan').val(ui.item.statuskaryawan);
          $('#nip').val(ui.item.nip);
          $('#fungsi').val(ui.item.fungsi);
          $('#fungsi1').val(ui.item.fungsi1);
          $('#kategori_limit').val(ui.item.kategori_limit);
          $('#kategori_limit1').val(ui.item.kategori_limit1);
          $('#appraisal1').val(ui.item.appraisal1);
          $('#jenispengajuan').val(ui.item.jenispengajuan);
          
          if (ui.item.jenispengajuan == 'BARU') {
            $('input#nip').css({"background-color": "#ffe6e6"});
            document.getElementById('label_pengalaman_pby').innerHTML = '<b>Pengalaman Pembiayaan<b>';

          }
          else {
            $('input#nip').css({"background-color": "#ccffcc"});
            document.getElementById('label_pengalaman_pby').innerHTML = '<b>Pengalaman PWMP<b>';
          }

          var pelatihanpembiayaan = $( '#nip' ).val();
          // console.log(pelatihanpembiayaan)
          // console.log('sini');
          $.ajax({
              type:"POST",
              url :"{{url('/limit/baru/pelatihan')}}",
              data:{
                  '_token' : '{{ csrf_token() }}',
                  'nipPelatihan' : pelatihanpembiayaan,
              },
              success: function( data ) {
                // $('#loadernpwp21').hide();
                // var tanggal;
                console.log(data)

                var sContent = ''
                sContent += '<table style="width:100%">'
                sContent += '<tr><th style="text-align: center;">No</th><th style="text-align: center;">Nama Pelatihan</th></tr>'
                var iNo = 0
                $.map(data, function (itrain) {
                  iNo += 1
                  sContent += '<tr><td style="text-align: center;">'+iNo+'</td><td style="text-align: left;" id="namatraining1">'+itrain.namatraining+'</td></tr>'
                  
                })
                sContent += '</table>'
                $('div#content_msutrainingpby').html(sContent);
              }
          });

    

          var kodejabatan = document.getElementById("jabatan1");
              if (kodejabatan.value == "BM003" || kodejabatan.value == "ACFM002") {
                $('#pengajuan_limit_otomatis').prop('readonly', true);
                $("#pengajuan_limit_otomatis").val( '0' );
              }
              else {
                $('#pengajuan_limit_otomatis').prop('readonly', true);
              }
          
          // console.log(ui.item.msutraining)
          // $.map(ui.item.msutraining, function (itrain) {
          //   $('td#tampilnamatraining').val(itrain.namatraining);
          // })
        },
      });
      // 
      // $('#nip').trigger('change');
      
      // $('#pelatihanpembiayaan').append("<tr><td>1</td><td>Thomas</td></tr>");
      // var reloadTable = function(employees) {
      //     var table = $('#pelatihanpembiayaan');
      //     table.find("tbody tr").remove();
      //     employees.forEach(function (employee) {
      //         table.append("<tr><td>" + employee.name + "</tr>");
      //     });
      // };
      // var x = document.getElementById("namatraining");
      // console.log(x)
      // $('#nip').change(function(e){

      //   console.log(e.target) 

      //     var valNip = $(this).val();
      //     console.log(valNip)
      //     setTimeout(function(){
      //       $('td#pelatihanpembiayaan').html(valNip)
      //       $('td#tampilnamatraining').html(namatraining)
      //     });
      // });

     



    function add(){
      var nip1 = document.getElementById("nip");
      var pby = document.getElementById("calc_year");
      var smrisk = document.getElementById("smrisk");
      var stskaryawan = document.getElementById("statuskaryawan");
      var sanksi = document.getElementById("sanksi");
      var kodesanksi = document.getElementById("kodesanksi");
      var cekjabatan = document.getElementById("jabatan1");
      var a = document.getElementById("pengajuan_limit_non_gadai");
      var b = document.getElementById("pengajuan_limit_gadai");
      var c = document.getElementById("pengajuan_limit_non_logam");
      var d = document.getElementById("pengajuan_limit_otomatis");
      var jenispengajuan = document.getElementById("jenispengajuan");
      var minimalpengalaman = jenispengajuan.value != 'BARU' ? 1: 2;
      var jenispengalaman = jenispengajuan.value != 'BARU' ? 'PWMP': 'Pembiayaan';


        if (nip1.value != "" ) {
          // if (pby.value < minimalpengalaman || stskaryawan.value != "OFFICER" || kodesanksi.value != "F"){
          if (stskaryawan.value != "OFFICER" || kodesanksi.value != "F"){
            var compMsg = ''
            if (stskaryawan.value != "OFFICER") compMsg += '- Status Karyawan : ' + stskaryawan.value + '<br/>'
            if (kodesanksi.value != 'F') compMsg += '- Sanksi : ' + sanksi.value
            Swal.fire({
                icon: 'error',
                title: 'PERMOHONAN TIDAK MEMENUHI KRITERIA',
                html: " <div style='text-align: left;'>"+compMsg+"</div>",
            })
          }
          else if (cekjabatan.value =="BM003" || cekjabatan.value =="ACFM002") {
            if (a.value == "" || b.value == "" || c.value=="" || d.value ==""){
              a.focus();
              Swal.fire({
                 icon: 'error',
                 title: 'Gagal',
                 text: "Mohon Lengkapi Nominal Pengajuan Limit",
              })   
            }
            else {
              var nip = $('#nip').val();
              var name = $('#name').val();
              var kodejabatan = $('#jabatan1').val();
              var kodecabang = $('#cabang1').val();
              var area = $('#area').val();
              var limitnongadai = $('#pengajuan_limit_non_gadai').val().replace(/,/g,'');
              var limitgadainlm= $('#pengajuan_limit_non_logam').val().replace(/,/g,'');
              var limitgadailm = $('#pengajuan_limit_gadai').val().replace(/,/g,'');
              var limit_otomatis = $('#pengajuan_limit_otomatis').val().replace(/,/g,'');
              var nilailimitnongadai = $('#limit_non_gadai').val().replace(/,/g,'');
              var nilailimitgadainlm= $('#limit_non_logam').val().replace(/,/g,'');
              var nilailimitgadailm = $('#limit_gadai').val().replace(/,/g,'');
              var nilailimit_otomatis = $('#limit_otomatis').val().replace(/,/g,'');
              var unggah_file = $('#unggah_file').val();
              var limit_diajukan = $('#limit_diajukan').val();
              var fungsi = $('#fungsi1').val();
              var kategorilimit = $('#kategori_limit1').val();
              var statuskaryawan = $('#statuskaryawan').val();
              var kategorilimit = $('#kategori_limit1').val();
              var smrisk = $('#smrisk').val();
              var appraisal = $('#appraisal1').val();
              var sanksi = $('#kodesanksi').val();
              var fraud = $('#fraud1').val();
              var refreshment = $('#refreshment').val();
              var calc_year = $('#calc_year').val();
              var calc_month = $('#calc_month').val();
              var lancar = $('#lancar1').val();
              var persen_lancar = $('#persen_lancar1').val();
              var dpk = $('#dpk1').val();
              var persen_dpk = $('#persen_dpk1').val();
              var npf = $('#npf1').val();
              var persen_npf = $('#persen_npf1').val();
              var total = $('#total1').val();

              var nosk = $('#no_sk').val();
              var tglsk = $('#tanggal_sk').val();
              var jenispengajuan = $('#jenispengajuan').val();
              var approve_by = "{{ Auth::user()->email }}";
                  console.log(unggah_file);
                  var formData = new FormData();
                    formData.append('file', $('#unggah_file')[0].files[0]);
                    formData.append('_token', '{{ csrf_token() }}');
                    formData.append('nip', nip);
                    formData.append('name', name);
                    
                    formData.append('lancar', lancar);
                    formData.append('persen_lancar', persen_lancar);
                    formData.append('dpk', dpk);
                    formData.append('persen_dpk', persen_dpk);
                    formData.append('npf', npf);
                    formData.append('persen_npf', persen_npf);
                    formData.append('total', total);
                    formData.append('statuskaryawan', statuskaryawan);
                    formData.append('kategorilimit', kategorilimit);
                    formData.append('smrisk', smrisk);
                    formData.append('appraisal', appraisal);
                    formData.append('sanksi', sanksi);
                    formData.append('fraud', fraud);
                    formData.append('refreshment', refreshment);
                    formData.append('calc_year', calc_year);
                    formData.append('calc_month', calc_month);
                    formData.append('kodejabatan', kodejabatan);
                    formData.append('kodecabang', kodecabang);
                    formData.append('area', area);
                    formData.append('limitnongadai', limitnongadai);
                    formData.append('limitgadainlm', limitgadainlm);
                    formData.append('limitgadailm', limitgadailm);
                    formData.append('limit_otomatis', limit_otomatis);
                    formData.append('nilailimitnongadai', nilailimitnongadai);
                    formData.append('nilailimitgadainlm', nilailimitgadainlm);
                    formData.append('nilailimitgadailm', nilailimitgadailm);
                    formData.append('nilailimit_otomatis', nilailimit_otomatis);
                    formData.append('fungsi', fungsi);
                    formData.append('nosk', nosk);
                    formData.append('tglsk', tglsk);
                    formData.append('jenispengajuan', jenispengajuan);
                    formData.append('approve_by', approve_by);
                      $.ajax({
                          type:"POST",
                        url :"{{url('/ajukan/limit/baru')}}",
                        data : formData,
                               processData: false,  // tell jQuery not to process the data
                               contentType: false, 

                        success:function(data){
                          window.location.href='{{url("/assessment")}}';
                          $('#pengalaman_pembiayaan').val('');
                          $('#nip').val('');
                          $('#name').val('');
                          Swal.fire({
                            icon: 'success',
                            title: 'Berhasil',
                            text: "Data barhasil diajukan",
                          })

                        },
                        error:function(response){
                          alert('Data gagal diajukan');
                          console.log(response);
                        }
                    });
            }
          }
          else if (a.value == "" || b.value == "" || c.value=="") {
            a.focus();
            Swal.fire({
               icon: 'error',
               title: 'Gagal',
               text: "Mohon Lengkapi Nominal Pengajuan Limit",
            })   
          }
          else {
              var nip = $('#nip').val();
              var name = $('#name').val();
              var kodejabatan = $('#jabatan1').val();
              var kodecabang = $('#cabang1').val();
              var area = $('#area').val();
              var limitnongadai = $('#pengajuan_limit_non_gadai').val().replace(/,/g,'');
              var limitgadainlm= $('#pengajuan_limit_non_logam').val().replace(/,/g,'');
              var limitgadailm = $('#pengajuan_limit_gadai').val().replace(/,/g,'');
              var limit_otomatis = $('#pengajuan_limit_otomatis').val().replace(/,/g,'');
              var nilailimitnongadai = $('#limit_non_gadai').val().replace(/,/g,'');
              var nilailimitgadainlm= $('#limit_non_logam').val().replace(/,/g,'');
              var nilailimitgadailm = $('#limit_gadai').val().replace(/,/g,'');
              var nilailimit_otomatis = $('#limit_otomatis').val().replace(/,/g,'');
              var unggah_file = $('#unggah_file').val();
              var limit_diajukan = $('#limit_diajukan').val();
              var fungsi = $('#fungsi1').val();
              var kategorilimit = $('#kategori_limit1').val();
              var smrisk = $('#smrisk').val();
              var appraisal = $('#appraisal1').val();
              var sanksi = $('#kodesanksi').val();
              var fraud = $('#fraud1').val();
              var smrisk = $('#smrisk').val();
              var lancar = $('#lancar1').val();
              var persen_lancar = $('#persen_lancar1').val();
              var dpk = $('#dpk1').val();
              var persen_dpk = $('#persen_dpk1').val();
              var npf = $('#npf1').val();
              var persen_npf = $('#persen_npf1').val();
              var total = $('#total1').val();

              var appraisal = $('#appraisal1').val();
              var fraud = $('#fraud1').val();
              var refreshment = $('#refreshment').val();
              var statuskaryawan = $('#statuskaryawan').val();
              var calc_year = $('#calc_year').val();
              var calc_month = $('#calc_month').val();
              var nosk = $('#no_sk').val();
              var tglsk = $('#tanggal_sk').val();
              var jenispengajuan = $('#jenispengajuan').val();
              var approve_by = "{{ Auth::user()->email }}";
                  console.log(unggah_file);
                  var formData = new FormData();
                    formData.append('file', $('#unggah_file')[0].files[0]);
                    formData.append('_token', '{{ csrf_token() }}');
                    formData.append('nip', nip);
                    formData.append('name', name);
                    formData.append('kodejabatan', kodejabatan);
                    formData.append('kodecabang', kodecabang);
                    formData.append('area', area);
                    formData.append('limitnongadai', limitnongadai);
                    formData.append('limitgadainlm', limitgadainlm);
                    formData.append('limitgadailm', limitgadailm);
                    formData.append('limit_otomatis', limit_otomatis);
                    formData.append('nilailimitnongadai', nilailimitnongadai);
                    formData.append('nilailimitgadainlm', nilailimitgadainlm);
                    formData.append('nilailimitgadailm', nilailimitgadailm);
                    formData.append('nilailimit_otomatis', nilailimit_otomatis);
                    formData.append('appraisal', appraisal);
                    formData.append('fraud', fraud);
                    formData.append('lancar', lancar);
                    formData.append('persen_lancar', persen_lancar);
                    formData.append('dpk', dpk);
                    formData.append('persen_dpk', persen_dpk);
                    formData.append('npf', npf);
                    formData.append('persen_npf', persen_npf);
                    formData.append('total', total);

                    formData.append('fungsi', fungsi);
                    formData.append('kategorilimit', kategorilimit);
                    formData.append('smrisk', smrisk);
                    formData.append('sanksi', sanksi);
                    formData.append('refreshment', refreshment);
                    formData.append('statuskaryawan', statuskaryawan);
                    formData.append('calc_year', calc_year);
                    formData.append('calc_month', calc_month);
                    formData.append('nosk', nosk);
                    formData.append('tglsk', tglsk);
                    formData.append('jenispengajuan', jenispengajuan);
                    formData.append('approve_by', approve_by);
                      $.ajax({
                          type:"POST",
                        url :"{{url('/ajukan/limit/baru')}}",
                        data : formData,
                               processData: false,  // tell jQuery not to process the data
                               contentType: false, 

                        success:function(data){
                          window.location.href='{{url("/assessment")}}';
                          $('#pengalaman_pembiayaan').val('');
                          $('#nip').val('');
                          $('#name').val('');
                          Swal.fire({
                            icon: 'success',
                            title: 'Berhasil',
                            text: "Data barhasil diajukan",
                          })

                        },
                        error:function(response){
                          alert('Data gagal diajukan');
                          console.log(response);
                        }
                    });
          }   
        
        }
        else {

         Swal.fire({
             icon: 'error',
             title: 'GAGAL',
             text: "KOLOM NIP KOSONG",
         })   
        }        

    }

    function addregion(){
      var nip1 = document.getElementById("nip");
      var pby = document.getElementById("calc_year");
      var smrisk = document.getElementById("smrisk");
      var stskaryawan = document.getElementById("statuskaryawan");
      var sanksi = document.getElementById("sanksi");
      var kodesanksi = document.getElementById("kodesanksi");
      var cekjabatan = document.getElementById("jabatan1");
      var a = document.getElementById("pengajuan_limit_non_gadai");
      var b = document.getElementById("pengajuan_limit_gadai");
      var c = document.getElementById("pengajuan_limit_non_logam");
      var d = document.getElementById("pengajuan_limit_otomatis");
      var jenispengajuan = document.getElementById("jenispengajuan");
      var minimalpengalaman = jenispengajuan.value != 'BARU' ? 1: 2;
      var jenispengalaman = jenispengajuan.value != 'BARU' ? 'PWMP': 'Pembiayaan';

        if (nip1.value != "" ) {
          // if (pby.value < minimalpengalaman || stskaryawan.value != "OFFICER" || kodesanksi.value != "F"){
          if (stskaryawan.value != "OFFICER" || kodesanksi.value != "F"){
            var compMsg = ''
            // if (pby.value < minimalpengalaman) compMsg += '- Pengalaman '+jenispengalaman+' < '+minimalpengalaman+' Tahun<br/>'
            if (stskaryawan.value != "OFFICER") compMsg += '- Status Karyawan : ' + stskaryawan.value + '<br/>'
            if (kodesanksi.value != 'F') compMsg += '- Sanksi : ' + sanksi.value
            Swal.fire({
                icon: 'error',
                title: 'PERMOHONAN TIDAK MEMENUHI KRITERIA',
                html: " <div style='text-align: left;'>"+compMsg+"</div>",
            })
          }
          else if (cekjabatan.value =="BM003" || cekjabatan.value =="ACFM002") {
            if (a.value == "" || b.value == "" || c.value=="" || d.value ==""){
              a.focus();
              Swal.fire({
                 icon: 'error',
                 title: 'Gagal',
                 text: "Mohon Lengkapi Nominal Pengajuan Limit",
              })   
            }
            else {
              var nip = $('#nip').val();
              var name = $('#name').val();
              var kodejabatan = $('#jabatan1').val();
              var kodecabang = $('#cabang1').val();
              var area = $('#area').val();
              var limitnongadai = $('#pengajuan_limit_non_gadai').val().replace(/,/g,'');
              var limitgadainlm= $('#pengajuan_limit_non_logam').val().replace(/,/g,'');
              var limitgadailm = $('#pengajuan_limit_gadai').val().replace(/,/g,'');
              var limit_otomatis = $('#pengajuan_limit_otomatis').val().replace(/,/g,'');
              var nilailimitnongadai = $('#limit_non_gadai').val().replace(/,/g,'');
              var nilailimitgadainlm= $('#limit_non_logam').val().replace(/,/g,'');
              var nilailimitgadailm = $('#limit_gadai').val().replace(/,/g,'');
              var nilailimit_otomatis = $('#limit_otomatis').val().replace(/,/g,'');
              var unggah_file = $('#unggah_file').val();
              var limit_diajukan = $('#limit_diajukan').val();
              var fungsi = $('#fungsi1').val();
              var kategorilimit = $('#kategori_limit1').val();

              var statuskaryawan = $('#statuskaryawan').val();
              var kategorilimit = $('#kategori_limit1').val();
              var smrisk = $('#smrisk').val();
              var appraisal = $('#appraisal1').val();
              var sanksi = $('#kodesanksi').val();
              var fraud = $('#fraud1').val();
              var refreshment = $('#refreshment').val();
              var calc_year = $('#calc_year').val();
              var calc_month = $('#calc_month').val();
              var lancar = $('#lancar1').val();
              var persen_lancar = $('#persen_lancar1').val();
              var dpk = $('#dpk1').val();
              var persen_dpk = $('#persen_dpk1').val();
              var npf = $('#npf1').val();
              var persen_npf = $('#persen_npf1').val();
              var total = $('#total1').val();

              var nosk = $('#no_sk').val();
              var tglsk = $('#tanggal_sk').val();
              var jenispengajuan = $('#jenispengajuan').val();
              var approve_by = "{{ Auth::user()->email }}";
                  console.log(unggah_file);
                  var formData = new FormData();
                    formData.append('file', $('#unggah_file')[0].files[0]);
                    formData.append('_token', '{{ csrf_token() }}');
                    formData.append('nip', nip);
                    formData.append('name', name);
                    formData.append('lancar', lancar);
                    formData.append('persen_lancar', persen_lancar);
                    formData.append('dpk', dpk);
                    formData.append('persen_dpk', persen_dpk);
                    formData.append('npf', npf);
                    formData.append('persen_npf', persen_npf);
                    formData.append('total', total);
                    formData.append('statuskaryawan', statuskaryawan);
                    formData.append('kategorilimit', kategorilimit);
                    formData.append('smrisk', smrisk);
                    formData.append('appraisal', appraisal);
                    formData.append('sanksi', sanksi);
                    formData.append('fraud', fraud);
                    formData.append('refreshment', refreshment);
                    formData.append('calc_year', calc_year);
                    formData.append('calc_month', calc_month);
                    formData.append('kodejabatan', kodejabatan);
                    formData.append('kodecabang', kodecabang);
                    formData.append('area', area);
                    formData.append('limitnongadai', limitnongadai);
                    formData.append('limitgadainlm', limitgadainlm);
                    formData.append('limitgadailm', limitgadailm);
                    formData.append('limit_otomatis', limit_otomatis);
                    formData.append('nilailimitnongadai', nilailimitnongadai);
                    formData.append('nilailimitgadainlm', nilailimitgadainlm);
                    formData.append('nilailimitgadailm', nilailimitgadailm);
                    formData.append('nilailimit_otomatis', nilailimit_otomatis);
                    formData.append('fungsi', fungsi);
                    formData.append('kategorilimit', kategorilimit);
                    formData.append('nosk', nosk);
                    formData.append('tglsk', tglsk);
                    formData.append('jenispengajuan', jenispengajuan);
                    formData.append('approve_by', approve_by);
                      $.ajax({
                          type:"POST",
                        url :"{{url('/ajukan/limit/baru/region')}}",
                        data : formData,
                               processData: false,  // tell jQuery not to process the data
                               contentType: false, 

                        success:function(data){
                          window.location.href='{{url("/assessment")}}';
                          $('#pengalaman_pembiayaan').val('');
                          $('#nip').val('');
                          $('#name').val('');
                          Swal.fire({
                            icon: 'success',
                            title: 'Berhasil',
                            text: "Data barhasil diajukan",
                          })

                        },
                        error:function(response){
                          alert('Data gagal diajukan');
                          console.log(response);
                        }
                    });
            }
          }
          else if (a.value == "" || b.value == "" || c.value=="") {
            a.focus();
            Swal.fire({
               icon: 'error',
               title: 'Gagal',
               text: "Mohon Lengkapi Nominal Pengajuan Limit",
            })   
          }
          else {
              var nip = $('#nip').val();
              var name = $('#name').val();
              var kodejabatan = $('#jabatan1').val();
              var kodecabang = $('#cabang1').val();
              var area = $('#area').val();
              var limitnongadai = $('#pengajuan_limit_non_gadai').val().replace(/,/g,'');
              var limitgadainlm= $('#pengajuan_limit_non_logam').val().replace(/,/g,'');
              var limitgadailm = $('#pengajuan_limit_gadai').val().replace(/,/g,'');
              var limit_otomatis = $('#pengajuan_limit_otomatis').val().replace(/,/g,'');
              var nilailimitnongadai = $('#limit_non_gadai').val().replace(/,/g,'');
              var nilailimitgadainlm= $('#limit_non_logam').val().replace(/,/g,'');
              var nilailimitgadailm = $('#limit_gadai').val().replace(/,/g,'');
              var nilailimit_otomatis = $('#limit_otomatis').val().replace(/,/g,'');
              var unggah_file = $('#unggah_file').val();
              var limit_diajukan = $('#limit_diajukan').val();
              var fungsi = $('#fungsi1').val();
              var kategorilimit = $('#kategori_limit1').val();
              var smrisk = $('#smrisk').val();
              var appraisal = $('#appraisal1').val();
              var sanksi = $('#kodesanksi').val();
              var fraud = $('#fraud1').val();
              var smrisk = $('#smrisk').val();

              var lancar = $('#lancar1').val();
              var persen_lancar = $('#persen_lancar1').val();
              var dpk = $('#dpk1').val();
              var persen_dpk = $('#persen_dpk1').val();
              var npf = $('#npf1').val();
              var persen_npf = $('#persen_npf1').val();
              var total = $('#total1').val();

              var appraisal = $('#appraisal1').val();
              var fraud = $('#fraud1').val();
              var refreshment = $('#refreshment').val();
              var statuskaryawan = $('#statuskaryawan').val();
              var calc_year = $('#calc_year').val();
              var calc_month = $('#calc_month').val();
              var nosk = $('#no_sk').val();
              var tglsk = $('#tanggal_sk').val();
              var jenispengajuan = $('#jenispengajuan').val();
              var approve_by = "{{ Auth::user()->email }}";
                  console.log(unggah_file);
                  var formData = new FormData();
                    formData.append('file', $('#unggah_file')[0].files[0]);
                    formData.append('_token', '{{ csrf_token() }}');
                    formData.append('nip', nip);
                    formData.append('name', name);
                    formData.append('kodejabatan', kodejabatan);
                    formData.append('kodecabang', kodecabang);
                    formData.append('area', area);
                    formData.append('limitnongadai', limitnongadai);
                    formData.append('limitgadainlm', limitgadainlm);
                    formData.append('limitgadailm', limitgadailm);
                    formData.append('limit_otomatis', limit_otomatis);
                    formData.append('nilailimitnongadai', nilailimitnongadai);
                    formData.append('nilailimitgadainlm', nilailimitgadainlm);
                    formData.append('nilailimitgadailm', nilailimitgadailm);
                    formData.append('nilailimit_otomatis', nilailimit_otomatis);
                    formData.append('fungsi', fungsi);
                    formData.append('lancar', lancar);
                    formData.append('persen_lancar', persen_lancar);
                    formData.append('dpk', dpk);
                    formData.append('persen_dpk', persen_dpk);
                    formData.append('npf', npf);
                    formData.append('persen_npf', persen_npf);
                    formData.append('total', total);
                    formData.append('statuskaryawan', statuskaryawan);
                    formData.append('kategorilimit', kategorilimit);
                    formData.append('smrisk', smrisk);
                    formData.append('appraisal', appraisal);
                    formData.append('sanksi', sanksi);
                    formData.append('fraud', fraud);
                    formData.append('refreshment', refreshment);
                    formData.append('calc_year', calc_year);
                    formData.append('calc_month', calc_month);
                    formData.append('kategorilimit', kategorilimit);
                    formData.append('nosk', nosk);
                    formData.append('tglsk', tglsk);
                    formData.append('jenispengajuan', jenispengajuan);
                    formData.append('approve_by', approve_by);
                      $.ajax({
                          type:"POST",
                        url :"{{url('/ajukan/limit/baru/region')}}",
                        data : formData,
                               processData: false,  // tell jQuery not to process the data
                               contentType: false, 

                        success:function(data){
                          window.location.href='{{url("/assessment")}}';
                          $('#pengalaman_pembiayaan').val('');
                          $('#nip').val('');
                          $('#name').val('');
                          Swal.fire({
                            icon: 'success',
                            title: 'Berhasil',
                            text: "Data barhasil diajukan",
                          })

                        },
                        error:function(response){
                          alert('Data gagal diajukan');
                          console.log(response);
                        }
                    });
          }   
        
        }
        else {

         Swal.fire({
             icon: 'error',
             title: 'GAGAL',
             text: "KOLOM NIP KOSONG",
         })   
        }        

    }

    function addERM(){
      var nip1 = document.getElementById("nip");
      var pby = document.getElementById("calc_year");
      var smrisk = document.getElementById("smrisk");
      var stskaryawan = document.getElementById("statuskaryawan");
      var sanksi = document.getElementById("sanksi");
      var kodesanksi = document.getElementById("kodesanksi");
      var cekjabatan = document.getElementById("jabatan1");
      var a = document.getElementById("pengajuan_limit_non_gadai");
      var b = document.getElementById("pengajuan_limit_gadai");
      var c = document.getElementById("pengajuan_limit_non_logam");
      var d = document.getElementById("pengajuan_limit_otomatis");
      var jenispengajuan = document.getElementById("jenispengajuan");
      var minimalpengalaman = jenispengajuan.value != 'BARU' ? 1: 2;
      var jenispengalaman = jenispengajuan.value != 'BARU' ? 'PWMP': 'Pembiayaan';

        if (nip1.value != "" ) {
          // if (pby.value < minimalpengalaman || stskaryawan.value != "OFFICER" || kodesanksi.value != "F"){
          if (stskaryawan.value != "OFFICER" || kodesanksi.value != "F"){
            var compMsg = ''
            // if (pby.value < minimalpengalaman) compMsg += '- Pengalaman '+jenispengalaman+' < '+minimalpengalaman+' Tahun<br/>'
            if (stskaryawan.value != "OFFICER") compMsg += '- Status Karyawan : ' + stskaryawan.value + '<br/>'
            if (kodesanksi.value != 'F') compMsg += '- Sanksi : ' + sanksi.value
            Swal.fire({
                icon: 'error',
                title: 'PERMOHONAN TIDAK MEMENUHI KRITERIA',
                html: " <div style='text-align: left;'>"+compMsg+"</div>",
            })
          }
          else if (cekjabatan.value =="BM003" || cekjabatan.value =="ACFM002") {
            if (a.value == "" || b.value == "" || c.value=="" || d.value ==""){
              a.focus();
              Swal.fire({
                 icon: 'error',
                 title: 'Gagal',
                 text: "Mohon Lengkapi Nominal Pengajuan Limit",
              })   
            }
            else {
              var nip = $('#nip').val();
              var name = $('#name').val();
              var kodejabatan = $('#jabatan1').val();
              var kodecabang = $('#cabang1').val();
              var area = $('#area').val();
              var limitnongadai = $('#pengajuan_limit_non_gadai').val().replace(/,/g,'');
              var limitgadainlm= $('#pengajuan_limit_non_logam').val().replace(/,/g,'');
              var limitgadailm = $('#pengajuan_limit_gadai').val().replace(/,/g,'');
              var limit_otomatis = $('#pengajuan_limit_otomatis').val().replace(/,/g,'');
              var nilailimitnongadai = $('#limit_non_gadai').val().replace(/,/g,'');
              var nilailimitgadainlm= $('#limit_non_logam').val().replace(/,/g,'');
              var nilailimitgadailm = $('#limit_gadai').val().replace(/,/g,'');
              var nilailimit_otomatis = $('#limit_otomatis').val().replace(/,/g,'');
              var unggah_file = $('#unggah_file').val();
              var limit_diajukan = $('#limit_diajukan').val();
              var fungsi = $('#fungsi1').val();
              var kategorilimit = $('#kategori_limit1').val();

              var statuskaryawan = $('#statuskaryawan').val();
              var kategorilimit = $('#kategori_limit1').val();
              var smrisk = $('#smrisk').val();
              var appraisal = $('#appraisal1').val();
              var sanksi = $('#kodesanksi').val();
              var fraud = $('#fraud1').val();
              var refreshment = $('#refreshment').val();
              var calc_year = $('#calc_year').val();
              var calc_month = $('#calc_month').val();
              var lancar = $('#lancar1').val();
              var persen_lancar = $('#persen_lancar1').val();
              var dpk = $('#dpk1').val();
              var persen_dpk = $('#persen_dpk1').val();
              var npf = $('#npf1').val();
              var persen_npf = $('#persen_npf1').val();
              var total = $('#total1').val();

              var nosk = $('#no_sk').val();
              var tglsk = $('#tanggal_sk').val();
              var jenispengajuan = $('#jenispengajuan').val();
              var approve_by = "{{ Auth::user()->email }}";
                  console.log(unggah_file);
                  var formData = new FormData();
                    formData.append('file', $('#unggah_file')[0].files[0]);
                    formData.append('_token', '{{ csrf_token() }}');
                    formData.append('nip', nip);
                    formData.append('name', name);
                    formData.append('lancar', lancar);
                    formData.append('persen_lancar', persen_lancar);
                    formData.append('dpk', dpk);
                    formData.append('persen_dpk', persen_dpk);
                    formData.append('npf', npf);
                    formData.append('persen_npf', persen_npf);
                    formData.append('total', total);
                    formData.append('statuskaryawan', statuskaryawan);
                    formData.append('kategorilimit', kategorilimit);
                    formData.append('smrisk', smrisk);
                    formData.append('appraisal', appraisal);
                    formData.append('sanksi', sanksi);
                    formData.append('fraud', fraud);
                    formData.append('refreshment', refreshment);
                    formData.append('calc_year', calc_year);
                    formData.append('calc_month', calc_month);
                    formData.append('kodejabatan', kodejabatan);
                    formData.append('kodecabang', kodecabang);
                    formData.append('area', area);
                    formData.append('limitnongadai', limitnongadai);
                    formData.append('limitgadainlm', limitgadainlm);
                    formData.append('limitgadailm', limitgadailm);
                    formData.append('limit_otomatis', limit_otomatis);
                    formData.append('nilailimitnongadai', nilailimitnongadai);
                    formData.append('nilailimitgadainlm', nilailimitgadainlm);
                    formData.append('nilailimitgadailm', nilailimitgadailm);
                    formData.append('nilailimit_otomatis', nilailimit_otomatis);
                    formData.append('fungsi', fungsi);
                    formData.append('kategorilimit', kategorilimit);
                    formData.append('nosk', nosk);
                    formData.append('tglsk', tglsk);
                    formData.append('jenispengajuan', jenispengajuan);
                    formData.append('approve_by', approve_by);
                      $.ajax({
                          type:"POST",
                        url :"{{url('/ajukan/limit/baru/erm')}}",
                        data : formData,
                               processData: false,  // tell jQuery not to process the data
                               contentType: false, 

                        success:function(data){
                          window.location.href='{{url("/erm/assessment")}}';
                          $('#pengalaman_pembiayaan').val('');
                          $('#nip').val('');
                          $('#name').val('');
                          Swal.fire({
                            icon: 'success',
                            title: 'Berhasil',
                            text: "Data barhasil diajukan",
                          })

                        },
                        error:function(response){
                          alert('Data gagal diajukan');
                          console.log(response);
                        }
                    });
            }
          }
          else if (a.value == "" || b.value == "" || c.value=="") {
            a.focus();
            Swal.fire({
               icon: 'error',
               title: 'Gagal',
               text: "Mohon Lengkapi Nominal Pengajuan Limit",
            })   
          }
          else {
              var nip = $('#nip').val();
              var name = $('#name').val();
              var kodejabatan = $('#jabatan1').val();
              var kodecabang = $('#cabang1').val();
              var area = $('#area').val();
              var limitnongadai = $('#pengajuan_limit_non_gadai').val().replace(/,/g,'');
              var limitgadainlm= $('#pengajuan_limit_non_logam').val().replace(/,/g,'');
              var limitgadailm = $('#pengajuan_limit_gadai').val().replace(/,/g,'');
              var limit_otomatis = $('#pengajuan_limit_otomatis').val().replace(/,/g,'');
              var nilailimitnongadai = $('#limit_non_gadai').val().replace(/,/g,'');
              var nilailimitgadainlm= $('#limit_non_logam').val().replace(/,/g,'');
              var nilailimitgadailm = $('#limit_gadai').val().replace(/,/g,'');
              var nilailimit_otomatis = $('#limit_otomatis').val().replace(/,/g,'');
              var unggah_file = $('#unggah_file').val();
              var limit_diajukan = $('#limit_diajukan').val();
              var fungsi = $('#fungsi1').val();
              var kategorilimit = $('#kategori_limit1').val();
              var smrisk = $('#smrisk').val();
              var appraisal = $('#appraisal1').val();
              var sanksi = $('#kodesanksi').val();
              var fraud = $('#fraud1').val();
              var smrisk = $('#smrisk').val();

              var lancar = $('#lancar1').val();
              var persen_lancar = $('#persen_lancar1').val();
              var dpk = $('#dpk1').val();
              var persen_dpk = $('#persen_dpk1').val();
              var npf = $('#npf1').val();
              var persen_npf = $('#persen_npf1').val();
              var total = $('#total1').val();

              var appraisal = $('#appraisal1').val();
              var fraud = $('#fraud1').val();
              var refreshment = $('#refreshment').val();
              var statuskaryawan = $('#statuskaryawan').val();
              var calc_year = $('#calc_year').val();
              var calc_month = $('#calc_month').val();
              var nosk = $('#no_sk').val();
              var tglsk = $('#tanggal_sk').val();
              var jenispengajuan = $('#jenispengajuan').val();
              var approve_by = "{{ Auth::user()->email }}";
                  console.log(unggah_file);
                  var formData = new FormData();
                    formData.append('file', $('#unggah_file')[0].files[0]);
                    formData.append('_token', '{{ csrf_token() }}');
                    formData.append('nip', nip);
                    formData.append('name', name);
                    formData.append('kodejabatan', kodejabatan);
                    formData.append('kodecabang', kodecabang);
                    formData.append('area', area);
                    formData.append('limitnongadai', limitnongadai);
                    formData.append('limitgadainlm', limitgadainlm);
                    formData.append('limitgadailm', limitgadailm);
                    formData.append('limit_otomatis', limit_otomatis);
                    formData.append('limit_diajukan', limit_diajukan);
                    formData.append('nilailimitnongadai', nilailimitnongadai);
                    formData.append('nilailimitgadainlm', nilailimitgadainlm);
                    formData.append('nilailimitgadailm', nilailimitgadailm);
                    formData.append('nilailimit_otomatis', nilailimit_otomatis);
                    formData.append('fungsi', fungsi);
                    formData.append('lancar', lancar);
                    formData.append('persen_lancar', persen_lancar);
                    formData.append('dpk', dpk);
                    formData.append('persen_dpk', persen_dpk);
                    formData.append('npf', npf);
                    formData.append('persen_npf', persen_npf);
                    formData.append('total', total);
                    formData.append('statuskaryawan', statuskaryawan);
                    formData.append('kategorilimit', kategorilimit);
                    formData.append('smrisk', smrisk);
                    formData.append('appraisal', appraisal);
                    formData.append('sanksi', sanksi);
                    formData.append('fraud', fraud);
                    formData.append('refreshment', refreshment);
                    formData.append('calc_year', calc_year);
                    formData.append('calc_month', calc_month);
                    formData.append('kategorilimit', kategorilimit);
                    formData.append('nosk', nosk);
                    formData.append('tglsk', tglsk);
                    formData.append('jenispengajuan', jenispengajuan);
                    formData.append('approve_by', approve_by);
                      $.ajax({
                          type:"POST",
                        url :"{{url('/ajukan/limit/baru/erm')}}",
                        data : formData,
                               processData: false,  // tell jQuery not to process the data
                               contentType: false, 

                        success:function(data){
                          window.location.href='{{url("/erm/assessment")}}';
                          $('#pengalaman_pembiayaan').val('');
                          $('#nip').val('');
                          $('#name').val('');
                          Swal.fire({
                            icon: 'success',
                            title: 'Berhasil',
                            text: "Data barhasil diajukan",
                          })

                        },
                        error:function(response){
                          alert('Data gagal diajukan');
                          console.log(response);
                        }
                    });
          }   
        
        }
        else {

         Swal.fire({
             icon: 'error',
             title: 'GAGAL',
             text: "KOLOM NIP KOSONG",
         })   
        }        

    }
</script>
<script type="text/javascript">
   function fungsigadailm() {
      var logammulia = document.getElementById("pengajuan_limit_gadai");
      var angkalogammulia = parseInt(logammulia.value.replace(/,/g, ""));
        if (angkalogammulia > 250000000) {
          logammulia.focus(); 
          logammulia.value=""; 
          Swal.fire({
               icon: 'error',
               title: 'Gagal',
               html: "Pengajuan Limit Logam Mulia <br> Tidak Boleh Lebih Dari 250 Juta",
           })
          //setTimeout(function(){x.focus();}, 1);
        }
    }
    function fungsigadainlm() {
      var nonlogam = document.getElementById("pengajuan_limit_non_logam");
      var angkanonlogam = parseInt(nonlogam.value.replace(/,/g, ""));
        if (angkanonlogam > 250000000) {
          nonlogam.focus(); 
          nonlogam.value="";
          Swal.fire({
               icon: 'error',
               title: 'Gagal',
               html: "Pengajuan Limit Non Logam Mulia <br> Tidak Boleh Lebih Dari 250 Juta",
           })    
        }
    }
    function fungsiotomatis() {
      var flimitotomatis = document.getElementById("pengajuan_limit_otomatis");
      var angkalimitotomatis = parseInt(flimitotomatis.value.replace(/,/g, ""));
      var cekjabatan1 = document.getElementById("jabatan1");
        if (cekjabatan1.value == "BM003" || cekjabatan1.value == "ACFM002" ) {
          if (angkalimitotomatis > 200000000) {
          flimitotomatis.focus();
          flimitotomatis.value=""; 
          Swal.fire({
               icon: 'error',
               title: 'Gagal',
               html: "Pengajuan Limit Otomatis <br> Tidak Boleh Lebih Dari 200 Juta",
           })   
          }
        }
    }
    function fungsinongadai() {
      var flimitotomatis = document.getElementById("pengajuan_limit_non_gadai");
      var angkalimitotomatis = parseInt(flimitotomatis.value.replace(/,/g, ""));
      var cekjabatan1 = document.getElementById("jabatan1");
        if (cekjabatan1.value == "MBM003") {
          if (angkalimitotomatis > 200000000) {
          flimitotomatis.focus();
          flimitotomatis.value=""; 
          Swal.fire({
               icon: 'error',
               title: 'Gagal',
               html: "Pengajuan Limit Non Gadai <br> Tidak Boleh Lebih Dari 200 Juta",
           })   
          }
        }
    }
</script>
<script type="text/javascript">
  $(function() {
    $('#pengajuan_limit_otomatis').prop('readonly', true);
  })
</script>
<script type="text/javascript">
</script>

<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  /*background-color: #dddddd;*/
}
</style>

@stop