@extends('layouts.backend')
@section('content')
  <div class="card">
  	<form method="POST" action="/limit/tambah">
  		<ul class="list-group list-group-flush">
  			<div class="row">
  				<li class="list-group-item header col-lg-12">Form Pengajuan Permohonan Peningkatan Limit</li>
  				<li class="list-group-item col-lg-6">
  					Nomor Induk Pegawai <br>
  					<input type="text" name="NIP" class="form-control">
  					Nama <br>
  					<input type="text" name="name" class="form-control" readonly>
  					Email <br>
  					<input type="Email" name="email" class="form-control" readonly>
  					Atasan 1 <br>
  					<input type="text" name="atasan-1" class="form-control" readonly>
  					Atasan 2 <br>
  					<input type="text" name="atasan-2" class="form-control" readonly>
  					Tanggal Bergabung
  					<div class="row">
  						<div class="col-lg-2">
  							<input type="text" class="form-control" placeholder="Tgl" readonly>
  						</div>
  						<div class="col-lg-2">
  							<input type="text" class="form-control" placeholder="Bln" readonly>
  						</div>
  						<div class="col-lg-2">
  							<input type="text" class="form-control" placeholder="Thn" readonly>
  						</div>    						
  					</div>
  					Status Karyawan <br>
  					<input type="text" name="status-karyawan" class="form-control" readonly>
  					Jabatan <br>
  					<input type="text" name="jabatan" class="form-control" readonly>
  					Unit Kerja <br>
  					<input type="text" name="unit-kerja" class="form-control" readonly>
  					Unggal File SKPP
  					<div class="custom-file mb-3">
      					<input type="file" class="custom-file-input" id="customFile" name="filename">
      					<label class="custom-file-label" for="customFile">Choose file</label>
    				</div>
  				</li>
  				<li class="list-group-item col-lg-6">
          Pengalaman Pembiayaan
          <div class="row">
              <div class="col-lg-2">
                <input type="text" class="form-control">
              </div>
              <div class="col-lg-2">
                Tahun
              </div>
              <div class="col-lg-2">
                <input type="text" class="form-control">
              </div>
              <div class="col-lg-2">
                Bulan
              </div>                 
            </div>
            Limit Sebelumnya 
              <div class="input-group">
                <div class="input-group-prepend">
                  <div class="input-group-text">Rp.</div>
                </div>
                  <input type="text" class="form-control" id="inlineFormInputGroup">
              </div>
            Limit Yang Diajukan 
              <div class="input-group">
                <div class="input-group-prepend">
                  <div class="input-group-text">Rp.</div>
                </div>
                  <input type="text" class="form-control" id="inlineFormInputGroup">
              </div>
            Sertifikasi Manajemen Resiko
            <input type="text" name="sertifikasi" class="form-control" readonly>
            Refreshment Ketentuan Pembiayaan
            <input type="text" name="refreshment" class="form-control" readonly>
            Sanksi
            <input type="text" name="sanksi" class="form-control" readonly>
            Portofolio Kualitas Pembiayaan
            <div class="row">
              <div class="col-lg-2">Lancar</div>
              <div class="col-lg-2">
              <input type="text" name="lancar" readonly="" class="form-control">
              </div>
              <div class="col-lg-2">%</div>
            </div>
            <div class="row">
              <div class="col-lg-2">DPK</div>
              <div class="col-lg-2">
              <input type="text" name="dpk" readonly="" class="form-control">
              </div>
              <div class="col-lg-2">%</div>
            </div>
            <div class="row">
              <div class="col-lg-2">NPF</div>
              <div class="col-lg-2">
              <input type="text" name="npf" readonly="" class="form-control">
              </div>
              <div class="col-lg-2">%</div>
            </div>
            <div class="row">
              <div class="col-lg-2"><b>Total</b></div>
              <div class="col-lg-8">
              <div class="input-group">
                <div class="input-group-prepend">
                  <div class="input-group-text">Rp.</div>
                </div>
                  <input type="text" class="form-control" id="inlineFormInputGroup" readonly>
              </div>
              </div>
            </div>
             Pengalaman Sebagai Pemegang Kewenangan
            <div class="row">
              <div class="col-lg-2">
                <input type="text" class="form-control">
              </div>
              <div class="col-lg-2">
                Tahun
              </div>
              <div class="col-lg-2">
                <input type="text" class="form-control">
              </div>
              <div class="col-lg-2">
                Bulan
              </div>                 
            </div>
            <br>
            <button type="submit" class="btn btn-success btn-lg btn-block">Ajukan Permohonan</button>
          </li>
  			</div>
  		</div>
  	</ul>
  </form>
</div>
@endsection