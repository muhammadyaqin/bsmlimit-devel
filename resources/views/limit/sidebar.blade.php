<ul class="navbar-nav fixed-left sidebar sidebar-dark accordion" id="accordionSidebar" style="background: #00583D; ">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <img src="{{url('img/Login/Logo.png')}}" width="76.76px" height="40px" style="position: fixed;">
      </a>
      <div class="konten-sidebar" style="position: fixed; top: 20%;">

      <!-- Nav Item - Permohonan Limit -->
      <li class="nav-item active" style="background: rgba(255, 255, 255, 0.1);">
        <a class="nav-link" href="{{('/limit/baru')}}">
          <img src="{{url('img/Sidebar/limit icon.png')}}" width="20px" height="20px">
          <span>Permohonan Limit</span></a>
      </li>


      <!-- Nav Item - Permohonan Limit -->
      <li class="nav-item">
        <a class="nav-link" href="/home">
          <img src="{{url('img/Sidebar/assessment icon.png')}}" width="20px" height="20px">
          <span>Assessment</span></a>
      </li>

      <!-- Nav Item - Permohonan Limit -->
      <li class="nav-item">
        <a class="nav-link" href="/home">
          <img src="{{url('img/Sidebar/user icon.png')}}" width="20px" height="20px">
          <span>User Maintenance</span></a>
      </li>

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <center><button class="rounded-circle border-0" id="sidebarToggle"></button></center>
      </div>
      <!-- Sidebar Toggler (Sidebar) -->
      
      <div class="footer-sidebar" style="position: fixed; top: 90%;">
        <p style="position: relative; left: 10%; font-style: normal; font-weight: normal; font-size: 14px; color: #FFFFFF;">Copyright © 2020 <br>
            PT Bank Syariah Mandiri</p>
      </div>
      </div>
</ul>