@extends('layouts.backend')
@section('content')

	<div class="container">
	    <div class="card" style="margin-bottom: 10px;">
	        <div class="card-content mt-5 ml-5 mb-5">
	            <h2> Selamat Datang,</h2>
	            <h2> Anda Masuk Sebagai <strong style="color:darkgreen">User</strong></h2>
	        </div>
	    </div>
	    @foreach (DB::table('master.userrole')->where('email', auth()->user()->email)->get() as $item)
	    	<div class="row">
	    		<div class="col-md-3">
	    			<a href={{URL('/select-role/' .$item->role)}} class="btn btn-success btn-block" style="margin-bottom: 10px;">
	    			  {{$item->role}}
	    			  <input type="hidden" name="selected_role" value="{{$item->role}}">
	    			</a>
	    		</div>
	    	</div>
	    	@if($item->role == 'ADMIN')
	    		<div class="row">
	    			<div class="col-md-3">
	    				<a href={{URL('/Administrator/index')}} class="btn btn-success btn-block" style="margin-bottom: 10px;">
	    				  {{$item->role}}
	    				</a>
	    			</div>
	    		</div>
	    	@elseif($item->role == 'CABANG')
	    		<div class="row">
		    		<div class="col-md-3">
		    			<a href={{URL('/home-cabang')}} class="btn btn-success btn-block" style="margin-bottom: 10px;">
		    			  {{$item->role}}
		    			</a>
		    		</div>
		    	</div>
	    	@elseif($item->role == 'REGION')
		    	<div class="row">
		    		<div class="col-md-3">
		    			<a href={{URL('/home-region')}} class="btn btn-success btn-block" style="margin-bottom: 10px;">
		    			  {{$item->role}}
		    			</a>
		    		</div>
		    	</div>
	    	@elseif($item->role == 'ERM')
	    		<div class="row">
		    		<div class="col-md-3">
		    			<a href={{URL('/home-erm')}} class="btn btn-success btn-block" style="margin-bottom: 10px;">
		    			  {{$item->role}}
		    			</a>
		    		</div>
		    	</div>
	    	@elseif($item->role == 'IOG')
	    		<div class="row">
	    			<div class="col-md-3">
	    				<a href={{URL('/home-iog')}} class="btn btn-success btn-block" style="margin-bottom: 10px;">
	    				  {{$item->role}}
	    				</a>
	    			</div>
	    		</div>
	    	@elseif($item->role == 'PUSAT')
	    		<div class="row">
		    		<div class="col-md-3">
		    			<a href={{URL('/home-pusat')}} class="btn btn-success btn-block" style="margin-bottom: 10px;">
		    			  {{$item->role}}
		    			</a>
		    		</div>
		    	</div>
	    	@endif
	    @endforeach
	</div>

@endsection