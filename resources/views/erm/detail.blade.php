@extends('layouts.backend')
@section('content')
<div class="card mb-4">
  <div class="card-header border-left-success shadow h-100 py-2">
    <div class="card-body">
      <div class="row no-gutters align-items-center">
        <div class="col mr-2">
          <div class="row">
            <a class="nav-link" href="{{URL::asset('/erm/assessment')}}">
              <img src="{{url('public/img/icon/back.png')}}" width="30px" height="30px">
            </a>
            <div class="h5 mb-0 font-weight-bold text-gray-800" style="margin-top: 12px;">Rincian Pengajuan Permohonan Limit</div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="card-body">
    <div class="card-body row">
        <div class="col-md-6">
          <div class="row">
            <style type="text/css">
                div#detailpwmphead{

                }
                div#detailpwmphead div.sub1default{
                  padding: 6px 12px;
                }
                div#detailpwmphead div.sub2default{
                  font-size: 16px;
                  text-align: center;
                }
                div#detailpwmphead div.sub2warning{
                  color: black;
                }

                .rincianpwmp_head {
                  margin: 0px 0px 0px 0px;
                }
                .rincianpwmp_head .headtitle {
                  width: 160px;
                }
                .rincianpwmp_head .headvalue {
                  width: 300px;
                }
                .rincianpwmp_button {
                  margin: 12px 0px 0px 0px;
                  width: 100%;
                  padding: 0px 18px;
                }
                .rincianpwmp_button_sub1 {
                  width: 100%;
                  padding: 12px 0px;
                  border-top: 1px solid #d1d3e2;
                }
                .rincianpwmp_button_sub1 form.rbutton {
                  display: inline;
                }

            </style>
              <div class="row rincianpwmp_head">
                <div class="col-md-6 headtitle">
                  <div class="form-group">
                    <label><b>Tanggal Pengajuan</b></label><br/>
                    <label><b>Approve RO</b></label><br/>
                    <label><b>Approve ERM</b></label><br/>
                    <label><b>Status</b></label>
                  </div>
                </div>
                <div id="detailpwmphead" class="col-md-6 headvalue">
                  <div class="form-group">
                    <div class="text-xxxl font-weight-bold text-success text-uppercase mb-1">{{ \Carbon\Carbon::parse($pwmp[0]->created_at)->format('d-m-Y H:i:s') }}</div>
                    @if ($pwmp[0]->status == 2 || $pwmp[0]->status == 3 || $pwmp[0]->status == 4)
                    <div class="text-xxxl font-weight-bold text-success text-uppercase mb-1">{{ date('d-m-Y H:i:s', strtotime($pwmp[0]->approve_ro_date)) }}</div>
                    @endif
                    @if ($pwmp[0]->status == 4)
                    <div class="text-xxxl font-weight-bold text-success text-uppercase mb-1">{{ date('d-m-Y H:i:s', strtotime($pwmp[0]->approve_erm_date)) }}</div>
                    @elseif ($pwmp[0]->status != 4)
                    <div class="text-xxxl font-weight-bold text-success text-uppercase mb-1"> - </div>
                    @endif
                    @if ($pwmp[0]->status == 1)
                      <div class="card bg-info text-white shadow">
                        <div class="card-body sub1default">
                          <div class="text-white-10 xs sub2default">{{$pwmp[0]->statuspengajuan}}</div>
                        </div>
                      </div>
                    @elseif ($pwmp[0]->status == 2)
                      <div class="card bg-warning text-white shadow">
                        <div class="card-body sub1default">
                          <div class="text-white-10 xs sub2default sub2warning">{{$pwmp[0]->statuspengajuan}}</div>
                        </div>
                      </div>
                    @elseif ($pwmp[0]->status == 3)
                      <div class="card bg-primary text-white text-white shadow">
                        <div class="card-body sub1default">
                          <div class="text-white-10 xs sub2default">{{$pwmp[0]->statuspengajuan}}</div>
                        </div>
                      </div>
                    @elseif ($pwmp[0]->status == 4)
                      <div class="card bg-success text-white shadow">
                        <div class="card-body sub1default">
                          <div class="text-white-10 xs sub2default">{{$pwmp[0]->statuspengajuan}}</div>
                        </div>
                      </div>
                    @elseif ($pwmp[0]->status == 5)
                      <div class="card bg-danger text-white shadow">
                        <div class="card-body sub1default">
                          <div class="text-white-10 xs sub2default">{{$pwmp[0]->statuspengajuan}}</div>
                        </div>
                      </div>
                    @elseif ($pwmp[0]->status == 6)
                      <div class="card bg-danger text-white shadow">
                        <div class="card-body sub1default">
                          <div class="text-white-10 xs sub2default">{{$pwmp[0]->statuspengajuan}}</div>
                        </div>
                      </div>
                    @endif
                  </div>
                </div>
              </div>
          </div>
        </div>
        @if ($pwmp[0]->status == 4)
        <div class="col-md-6">
          <div class="row">
            <div class="row rincianpwmp_head">
              <div class="col-md-6 headtitle">
                <div class="form-group">
                  <label><b>No SK Baru</b></label><br/>
                  <label><b>Tanggal SK Baru</b></label><br/>
                  <label><b>File SK Baru</b></label>
                </div>
              </div>
              <div id="detailpwmphead" class="col-md-6 headvalue">
                <div class="form-group">
                  <div class="text-xxxl font-weight-bold text-success text-uppercase mb-1">{{ ($pwmp[0]->nosknew) }}</div>
                  <div class="text-xxxl font-weight-bold text-success text-uppercase mb-1">{{ \Carbon\Carbon::parse($pwmp[0]->tglsknew)->format('d-m-Y') }}</div>
                   
                      <div>
                        <img src="{{url::asset('public/img/icon/cta_downloadnota.png')}}" width="34px" height="30px" style="margin-right: 10px;"><a target="_blank" href="{{ url('/public/file_skp/sk/'.$pwmp[0]->file_sk) }}">Download</a>
                      </div>
                   
                        <!-- <div class="row">
                          <img src="{{url::asset('img/icon/cta_downloadnota.png')}}" style="margin-left: 10px; margin-right: 10px; margin-bottom: 20px;" width="34px" height="30px" style="margin-left: 12px; margin-right: 10px;">File Not Found
                        </div> -->
                   
                  
                </div>
              </div>
            </div>
          </div>
        </div>
        @else

        @endif
    </div>

    <form action="{{ url('/update/'.$pwmp[0]->id.'/assessment/erm') }}" method="POST" enctype="multipart/form-data">{{ csrf_field() }}
      <input type="hidden" name="_token" value="{{csrf_token()}}">
          <div class=" card-body row" >
            <div class="col-md-6">
                  <input type="hidden" name="id" value="{{$pwmp[0]->id}}" readonly="" class="form-control" id="id">
              <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                          <label><b>Nomor Induk Pegawai</b></label>
                          <input type="text" name="nip" readonly="" value="{{$pwmp[0]->nip}}" class="form-control" id="nip">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                          <label><b>Status Karyawan</b></label>
                          <input type="text" name="nip" readonly="" value="{{$pwmp[0]->statuskaryawan}}" class="form-control" id="nip">
                      </div>
                    </div>
                  </div>
              <div class="form-group">
                <label><b>Nama</b></label>
                  <input type="text" name="name" readonly="" value="{{$pwmp[0]->namapejabat}}" class="form-control" id="name">
              </div>
              <div class="form-group">
                <label><b>Jabatan</b></label>
                  <input type="text" name="Cabang" readonly="" value="{{$pwmp[0]->kodejabatan}} - {{$pwmp[0]->namajabatan}}" class="form-control" id="cabang">
                  <input type="hidden" name="jabatan1" value="{{$pwmp[0]->kodejabatan}}" id="jabatan1">
              </div>
              <div class="form-group">
                <label><b>Cabang</b></label>
                  <input type="text" name="Cabang" readonly="" value="{{$pwmp[0]->kodecabang}} - {{$pwmp[0]->namacabang}}" class="form-control" id="cabang">
              </div>
              <div class="form-group">
                <label><b>Area</b></label>
                  <input type="text" name="area" readonly="" value="{{$pwmp[0]->kodearea}} - {{$pwmp[0]->namaarea}}" class="form-control" id="area">
                  <input type="hidden" id="area1" name="area1">
              </div>
              <div class="form-group">
                <label><b>Fungsi</b></label>
                   @if ($pwmp[0]->jenispengajuan != "A")
                    <input type="text" name="fungsi" readonly="" value=" {{$pwmp[0]->fungsi}} - {{$pwmp[0]->keteranganfungsi}}" class="form-control" id="fungsi">
                    @elseif ($pwmp[0]->jenispengajuan == "A")
                    <input type="text" name="fungsi" readonly="" value="" class="form-control" id="fungsi">
                    @endif
              </div>
              <div class="form-group">
                <label><b>Kategori Limit</b></label>
                  @if ($pwmp[0]->jenispengajuan != "A")
                    <input type="text" name="kategori_limit" value="{{$pwmp[0]->kategorilimit}} - {{$pwmp[0]->keterangankategorilimit}}" readonly="" class="form-control" id="kategori_limit">
                    @elseif ($pwmp[0]->jenispengajuan == "A")
                    <input type="text" name="kategori_limit" value="" readonly="" class="form-control" id="kategori_limit">
                    @endif
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                      <label><b>Sertifikasi Manajemen Resiko</b></label>
                      <div class="input-group">
                        <input type="text" name="smrisk" value="{{$pwmp[0]->statussertifikasi}}" readonly="" class="form-control" id="smrisk">
                      </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label><b>Performa Appraisal</b></label>
                    <div class="input-group">
                      <input type="text" name="appraisal" value="{{$pwmp[0]->keteranganappraisal}}" readonly="" class="form-control" id="appraisal">
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                      <label><b>Sanksi</b></label>
                      <div class="input-group">
                        <input type="text" name="sanksi" value="{{$pwmp[0]->keterangansanksi}}" readonly="" class="form-control" id="sanksi">
                      </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label><b>Fraud</b></label>
                    <div class="input-group">
                      <input type="text" name="fraud" value="{{$pwmp[0]->keteranganfraud}}" readonly="" class="form-control" id="fraud">
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                      <label><b>Refreshment</b></label>
                      <div class="input-group">
                        <input type="text" name="refreshment" value="{{$pwmp[0]->statusrefreshment}}" readonly="" class="form-control" id="refreshment">
                      </div>
                  </div>
                </div>
                  @if ($pwmp[0]->jenispengajuan == "A")
                  <div class="col-md-6">
                    <label id="label_pengalaman_pby"><b>Pengalaman Pembiayaan</b></label>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group">
                              <input type="text" name="calc_year" value="{{$pwmp[0]->age_year}}" readonly="" class="form-control text-right" id="calc_year">
                              <div class="input-group-text">Thn</div>
                            </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group">
                              <input type="text" name="calc_month" value="{{$pwmp[0]->age_month}}" readonly="" class="form-control text-right" id="calc_month">
                              <div class="input-group-text">Bln</div>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
                @else
                <div class="col-md-6">
                  <label id="label_pengalaman_pby"><b>Pengalaman PWMP</b></label>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                          <div class="input-group">
                            <input type="text" name="calc_year" value="{{$pwmp[0]->age_year_pwmp}}" readonly="" class="form-control text-right" id="calc_year">
                            <div class="input-group-text">Thn</div>
                          </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                          <div class="input-group">
                            <input type="text" name="calc_month" value="{{$pwmp[0]->age_month_pwmp}}" readonly="" class="form-control text-right" id="calc_month">
                            <div class="input-group-text">Bln</div>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
                @endif 
              </div>
              <div class="row" style="margin-top: 10px;">
                <div class="col-md-12">
                    <label><b>SK Terakhir</b></label>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                              <div class="input-group">
                                <div class="input-group-text">Nomor</div>
                                @if($pwmp[0]->jenispengajuan == "A")
                                <input type="text" name="no_sk" value=" " readonly="" class="form-control" id="no_sk">
                                @else
                                <input type="text" name="no_sk" value="{{$pwmp[0]->nosk}}" readonly="" class="form-control" id="no_sk">
                                @endif
                              </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <div class="input-group">
                              <div class="input-group-text">Tanggal</div>
                                @if ($pwmp[0]->jenispengajuan != "A")
                                <input type="text" name="tanggal_sk1" value="{{ \Carbon\Carbon::parse($pwmp[0]->tglsk)->format('d-m-Y') }}" readonly="" class="form-control" id="tanggal_sk1">
                                @elseif($pwmp[0]->jenispengajuan == "A")
                                <input type="text" name="tanggal_sk1" value="" readonly="" class="form-control" id="tanggal_sk1">
                                @endif
                            </div>
                          </div>
                        </div>
                      </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <label><b>KUALITAS PORTOFOLIO PEMBIAYAAN</b></label>
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6">
                      <label><b>Lancar</b></label>
                      <div class="input-group">
                        <div class="input-group-text" style="margin-bottom: 10px;">Rp.</div>
                          @if ($pwmp[0]->jenispengajuan != "A")
                          <input type="text" name="lancar" value="{{ number_format($pwmp[0]->nomlancar, 0, ',', '.') }}" readonly="" class="form-control text-right" style="margin-bottom: 10px;" id="lancar">
                          @elseif ($pwmp[0]->jenispengajuan == "A")
                          <input type="text" name="lancar" value="0" readonly="" class="form-control text-right" style="margin-bottom: 10px;" id="lancar">
                          @endif
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="input-group">
                        <div class="input-group-text" style="margin-top: 32px;">%</div>
                          @if ($pwmp[0]->jenispengajuan != "A")
                          <input type="text" name="persen_lancar" value="{{ number_format((float)$pwmp[0]->pctlancar, 2, '.', '')}}" readonly="" class="form-control text-right" id="persen_lancar" style="margin-top: 32px;">
                           @elseif ($pwmp[0]->jenispengajuan == "A")
                           <input type="text" name="persen_lancar" value="0" readonly="" class="form-control text-right" id="persen_lancar" style="margin-top: 32px;">
                           @endif
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <label><b>DPK</b></label>
                      <div class="input-group">
                        <div class="input-group-text" style="margin-bottom: 10px;">Rp.</div>
                          @if ($pwmp[0]->jenispengajuan != "A")
                          <input type="text" name="dpk" value="{{ number_format($pwmp[0]->nomdpk, 0, ',', '.') }}" readonly="" style="margin-bottom: 10px;" class="form-control text-right" id="dpk">
                          @elseif ($pwmp[0]->jenispengajuan == "A")
                          <input type="text" name="dpk" value="0" readonly="" style="margin-bottom: 10px;" class="form-control text-right" id="dpk">
                          @endif
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="input-group">
                        <div class="input-group-text" style="margin-top: 32px; marg">%</div>
                          @if ($pwmp[0]->jenispengajuan != "A")
                          <input type="text" name="persen_dpk" value="{{ number_format((float)$pwmp[0]->pctdpk, 2, '.', '')}}" readonly="" class="form-control text-right" id="persen_dpk" style="margin-top: 32px;">
                          @elseif ($pwmp[0]->jenispengajuan == "A")
                          <input type="text" name="persen_dpk" value="0" readonly="" class="form-control text-right" id="persen_dpk" style="margin-top: 32px;">
                          @endif
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <label><b>NPF</b></label>
                      <div class="input-group">
                        <div class="input-group-text" style="margin-bottom: 10px;">Rp.</div>
                          @if ($pwmp[0]->jenispengajuan != "A")
                          <input type="text" name="npf" value="{{ number_format($pwmp[0]->nomnpf, 0, ',', '.') }}" readonly="" style="margin-bottom: 10px;" class="form-control text-right" id="npf">
                          @elseif ($pwmp[0]->jenispengajuan == "A")
                          <input type="text" name="npf" value="0" readonly="" style="margin-bottom: 10px;" class="form-control text-right" id="npf">
                          @endif
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="input-group">
                        <div class="input-group-text" style="margin-top: 32px;">%</div>
                          @if ($pwmp[0]->jenispengajuan != "A")
                          <input type="text" name="persen_npf"  value="{{ number_format((float)$pwmp[0]->pctnpf, 2, '.', '')}}" readonly="" class="form-control text-right" id="persen_npf" style="margin-top: 32px;">
                          @elseif ($pwmp[0]->jenispengajuan == "A")
                          <input type="text" name="persen_npf"  value="0" readonly="" class="form-control text-right" id="persen_npf" style="margin-top: 32px;">
                          @endif
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <label><b>Total</b></label>
                      <div class="input-group">
                        <div class="input-group-text">Rp.</div>
                          @if ($pwmp[0]->jenispengajuan != "A")
                          <input type="text"  value="{{ number_format($pwmp[0]->nomospokok, 0, ',', '.') }}" name="total" readonly="" class="form-control text-right" id="total">
                          @elseif ($pwmp[0]->jenispengajuan == "A")
                          <input type="text"  value="0" name="total" readonly="" class="form-control text-right" id="total">
                          @endif
                      </div>
                    </div>
                  </div>
                </div>
              <label><b>PELATIHAN PEMBIAYAAN</b></label>
                <div class="row">
                  <div class="col-md-12">
                    <table style="width:100%">
                      <thead>
                        <tr>
                          <th style="text-align: center;">No</th>
                          <th style="text-align: center;">Nama Pelatihan</th>
                        </tr>
                        @foreach ($msu as $index => $msu)
                        <tr>
                          <td style="text-align: center">{{$index + 1}}</td>
                          <td>{{$msu->namapelatihan}}</td>
                        </tr>
                        @endforeach
                      </thead>
                      <tbody>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              <div class="row">
                <div class="col-md-6" style="margin-top: 20px;">
                  <label><b>DATA LIMIT TERAKHIR</b></label>
                  <div class="form-group">
                      <label><b>Non Gadai</b></label>
                      <div class="input-group">
                          @if ($pwmp[0]->jenispengajuan != "A")
                          <input type="text" name="limit_non_gadai" readonly="" value="{{ number_format($pwmp[0]->nilailimitnongadai, 0, ',', '.') }}" class="form-control text-right" id="limit_non_gadai">
                          @elseif ($pwmp[0]->jenispengajuan == "A")
                          <input type="text" name="limit_non_gadai" readonly="" value="0" class="form-control text-right" id="limit_non_gadai">
                          @endif
                      </div>
                  </div>
                </div>
                <div class="col-md-6" style="margin-top: 20px;">
                  @if ($pwmp[0]->status == 2)
                    @if(Auth::user()->cekRole()->role == "ERM")
                      <label><b>DATA LIMIT BARU</b></label>
                      <div class="form-group">
                          <label><b>Non Gadai</b></label>
                          <div class="input-group">
                            <input type="text" name="limitnongadai" value="{{$pwmp[0]->limitnongadai}}" class="form-control text-right ribuan" id="limitnongadai">
                          </div>
                      </div>
                    @elseif(Auth::user()->cekRole()->role != "ERM")
                      <label><b>DATA LIMIT BARU</b></label>
                      <div class="form-group">
                          <label><b>Non Gadai</b></label>
                          <div class="input-group">
                            <input type="text" name="limitnongadai" readonly="" value="{{$pwmp[0]->limitnongadai}}" class="form-control text-right ribuan" id="limitnongadai">
                          </div>
                      </div>
                    @endif
                    @elseif ($pwmp[0]->status == 3)
                      @if(Auth::user()->cekRole()->role == "ERM")
                        <label><b>DATA LIMIT BARU</b></label>
                        <div class="form-group">
                            <label><b>Non Gadai</b></label>
                            <div class="input-group">
                              <input type="text" name="limitnongadai" value="{{$pwmp[0]->limitnongadai}}" class="form-control text-right ribuan" id="limitnongadai">
                            </div>
                        </div>
                      @elseif(Auth::user()->cekRole()->role != "ERM")
                        <label><b>DATA LIMIT BARU</b></label>
                        <div class="form-group">
                            <label><b>Non Gadai</b></label>
                            <div class="input-group">
                              <input type="text" name="limitnongadai" readonly="" value="{{$pwmp[0]->limitnongadai}}" class="form-control text-right ribuan" id="limitnongadai">
                            </div>
                        </div>
                      @endif
                  @elseif ($pwmp[0]->status == 1)
                    @if(Auth::user()->cekRole()->role == "ERM")
                      <label><b>DATA LIMIT BARU</b></label>
                      <div class="form-group">
                          <label><b>Non Gadai</b></label>
                          <div class="input-group">
                            <input type="text" name="limitnongadai" readonly="" value="{{$pwmp[0]->limitnongadai}}" class="form-control text-right ribuan" id="limitnongadai">
                          </div>
                      </div>
                    @elseif(Auth::user()->cekRole()->role != "ERM")
                      <label><b>DATA LIMIT BARU</b></label>
                      <div class="form-group">
                          <label><b>Non Gadai</b></label>
                          <div class="input-group">
                            <input type="text" name="limitnongadai" readonly="" value="{{$pwmp[0]->limitnongadai}}" class="form-control text-right ribuan" id="limitnongadai">
                          </div>
                      </div>
                    @endif
                  @elseif ($pwmp[0]->status == 4)
                      <label><b>DATA LIMIT BARU</b></label>
                      <div class="form-group">
                          <label><b>Non Gadai</b></label>
                          <div class="input-group">
                            <input type="text" name="limitnongadai" readonly="" value="{{$pwmp[0]->limitnongadai}}" class="form-control text-right ribuan" id="limitnongadai">
                          </div>
                      </div>  
                  @elseif ($pwmp[0]->status == 5 || $pwmp[0]->status == 6)
                      <label><b>DATA LIMIT BARU</b></label>
                      <div class="form-group">
                          <label><b>Non Gadai</b></label>
                          <div class="input-group">
                            <input type="text" name="limitnongadai" readonly="" value="{{$pwmp[0]->limitnongadai}}" class="form-control text-right ribuan" id="limitnongadai">
                          </div>
                      </div>
                  @endif
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                      <label><b>Gadai Logam Mulia</b></label>
                      <div class="input-group">
                          @if ($pwmp[0]->jenispengajuan != "A")
                          <input type="text" name="limit_gadai" readonly="" value="{{ number_format($pwmp[0]->nilailimitgadailm, 0, ',', '.') }}"  class="form-control text-right" id="limit_gadai">
                          @elseif ($pwmp[0]->jenispengajuan == "A")
                          <input type="text" name="limit_gadai" readonly="" value="0"  class="form-control text-right" id="limit_gadai">
                          @endif
                      </div>
                  </div>
                </div>
                @if ($pwmp[0]->status == 2)
                  @if(Auth::user()->cekRole()->role == "ERM")
                    <div class="col-md-6">
                      <div class="form-group">
                          <label><b>Gadai Logam Mulia</b></label>
                          <div class="input-group">
                            <input type="text" name="limitgadailm" value="{{$pwmp[0]->limitgadailm}}" class="form-control text-right ribuan" id="limitgadailm" onblur="fungsigadailm()">
                          </div>
                      </div>
                    </div>
                  @elseif(Auth::user()->cekRole()->role != "ERM")
                    <div class="col-md-6">
                      <div class="form-group">
                          <label><b>Gadai Logam Mulia</b></label>
                          <div class="input-group">
                            <input type="text" name="limitgadailm" readonly="" value="{{$pwmp[0]->limitgadailm}}" class="form-control text-right ribuan" id="limitgadailm" onblur="fungsigadailm()">
                          </div>
                      </div>
                    </div>
                  @endif
                  @elseif ($pwmp[0]->status == 3)
                  @if(Auth::user()->cekRole()->role == "ERM")
                    <div class="col-md-6">
                      <div class="form-group">
                          <label><b>Gadai Logam Mulia</b></label>
                          <div class="input-group">
                            <input type="text" name="limitgadailm" value="{{$pwmp[0]->limitgadailm}}" class="form-control text-right ribuan" id="limitgadailm" onblur="fungsigadailm()">
                          </div>
                      </div>
                    </div>
                  @elseif(Auth::user()->cekRole()->role != "ERM")
                    <div class="col-md-6">
                      <div class="form-group">
                          <label><b>Gadai Logam Mulia</b></label>
                          <div class="input-group">
                            <input type="text" name="limitgadailm" readonly="" value="{{$pwmp[0]->limitgadailm}}" class="form-control text-right ribuan" id="limitgadailm" onblur="fungsigadailm()">
                          </div>
                      </div>
                    </div>
                  @endif
                  @elseif ($pwmp[0]->status == 4)
                    <div class="col-md-6">
                      <div class="form-group">
                          <label><b>Gadai Logam Mulia</b></label>
                          <div class="input-group">
                            <input type="text" name="limitgadailm" readonly="" value="{{$pwmp[0]->limitgadailm}}" class="form-control text-right ribuan" id="limitgadailm" onblur="fungsigadailm()">
                          </div>
                      </div>
                    </div>
                @elseif ($pwmp[0]->status == 1)
                  @if(Auth::user()->cekRole()->role == "ERM")
                    <div class="col-md-6">
                      <div class="form-group">
                          <label><b>Gadai Logam Mulia</b></label>
                          <div class="input-group">
                            <input type="text" name="limitgadailm" readonly="" value="{{$pwmp[0]->limitgadailm}}" class="form-control text-right ribuan" id="limitgadailm" onblur="fungsigadailm()">
                          </div>
                      </div>
                    </div>
                  @elseif(Auth::user()->cekRole()->role != "ERM")
                    <div class="col-md-6">
                      <div class="form-group">
                          <label><b>Gadai Logam Mulia</b></label>
                          <div class="input-group">
                            <input type="text" name="limitgadailm" readonly="" value="{{$pwmp[0]->limitgadailm}}" class="form-control text-right ribuan" id="limitgadailm" onblur="fungsigadailm()">
                          </div>
                      </div>
                    </div>
                  @endif
                @elseif ($pwmp[0]->status == 5 || $pwmp[0]->status == 6)
                    <div class="col-md-6">
                      <div class="form-group">
                          <label><b>Gadai Logam Mulia</b></label>
                          <div class="input-group">
                            <input type="text" name="limitgadailm" readonly="" value="{{$pwmp[0]->limitgadailm}}" class="form-control text-right ribuan" id="limitgadailm" onblur="fungsigadailm()">
                          </div>
                      </div>
                    </div>
                @endif
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label><b>Gadai Non Logam Mulia</b></label>
                    <div class="input-group">
                        @if ($pwmp[0]->jenispengajuan != "A")
                        <input type="text" name="limit_non_logam" readonly="" value="{{ number_format($pwmp[0]->nilailimitgadainlm, 0, ',', '.') }}" class="form-control text-right" id="limit_non_logam">
                        @elseif ($pwmp[0]->jenispengajuan == "A")
                        <input type="text" name="limit_non_logam" readonly="" value="0" class="form-control text-right" id="limit_non_logam">
                        @endif
                    </div>
                  </div>
                </div>
                  @if ($pwmp[0]->status == 2)
                    @if(Auth::user()->cekRole()->role == "ERM")
                      <div class="col-md-6">
                        <div class="form-group">
                          <label><b>Gadai Non Logam Mulia</b></label>
                          <div class="input-group">
                            <input type="text" name="limitgadainlm" class="form-control text-right ribuan" value="{{$pwmp[0]->limitgadainlm}}" id="limitgadainlm" onblur="fungsigadainlm()">
                          </div>
                        </div>
                      </div>
                    @elseif(Auth::user()->cekRole()->role != "ERM")
                      <div class="col-md-6">
                        <div class="form-group">
                          <label><b>Gadai Non Logam Mulia</b></label>
                          <div class="input-group">
                            <input type="text" name="limitgadainlm" readonly="" class="form-control text-right ribuan" value="{{$pwmp[0]->limitgadainlm}}" id="limitgadainlm" onblur="fungsigadainlm()">
                          </div>
                        </div>
                      </div>
                    @endif
                    @elseif ($pwmp[0]->status == 3)
                    @if(Auth::user()->cekRole()->role == "ERM")
                      <div class="col-md-6">
                        <div class="form-group">
                          <label><b>Gadai Non Logam Mulia</b></label>
                          <div class="input-group">
                            <input type="text" name="limitgadainlm" class="form-control text-right ribuan" value="{{$pwmp[0]->limitgadainlm}}" id="limitgadainlm" onblur="fungsigadainlm()">
                          </div>
                        </div>
                      </div>
                    @elseif(Auth::user()->cekRole()->role != "ERM")
                      <div class="col-md-6">
                        <div class="form-group">
                          <label><b>Gadai Non Logam Mulia</b></label>
                          <div class="input-group">
                            <input type="text" name="limitgadainlm" readonly="" class="form-control text-right ribuan" value="{{$pwmp[0]->limitgadainlm}}" id="limitgadainlm" onblur="fungsigadainlm()">
                          </div>
                        </div>
                      </div>
                    @endif
                    @elseif ($pwmp[0]->status == 4)
                      <div class="col-md-6">
                        <div class="form-group">
                          <label><b>Gadai Non Logam Mulia</b></label>
                          <div class="input-group">
                            <input type="text" name="limitgadainlm" readonly="" class="form-control text-right ribuan" value="{{$pwmp[0]->limitgadainlm}}" id="limitgadainlm" onblur="fungsigadainlm()">
                          </div>
                        </div>
                      </div>
                  @elseif ($pwmp[0]->status == 1)
                    @if(Auth::user()->cekRole()->role == "ERM")
                      <div class="col-md-6">
                        <div class="form-group">
                          <label><b>Gadai Non Logam Mulia</b></label>
                          <div class="input-group">
                            <input type="text" name="limitgadainlm" readonly="" class="form-control text-right ribuan" value="{{$pwmp[0]->limitgadainlm}}" id="limitgadainlm" onblur="fungsigadainlm()">
                          </div>
                        </div>
                      </div>
                    @elseif(Auth::user()->cekRole()->role != "ERM")
                      <div class="col-md-6">
                        <div class="form-group">
                          <label><b>Gadai Non Logam Mulia</b></label>
                          <div class="input-group">
                            <input type="text" name="limitgadainlm" readonly="" class="form-control text-right ribuan" value="{{$pwmp[0]->limitgadainlm}}" id="limitgadainlm" onblur="fungsigadainlm()">
                          </div>
                        </div>
                      </div>
                    @endif
                  @elseif ($pwmp[0]->status == 5 || $pwmp[0]->status == 6)
                      <div class="col-md-6">
                        <div class="form-group">
                          <label><b>Gadai Non Logam Mulia</b></label>
                          <div class="input-group">
                            <input type="text" name="limitgadainlm" readonly="" class="form-control text-right ribuan" value="{{$pwmp[0]->limitgadainlm}}" id="limitgadainlm" onblur="fungsigadainlm()">
                          </div>
                        </div>
                      </div>
                  @endif
              </div>
              <div class="row">
               <div class="col-md-6">
                 <div class="form-group">
                   <label><b>Limit Otomatis</b></label>
                   <div class="input-group">
                      @if ($pwmp[0]->jenispengajuan != "A")
                       <input type="text" name="limit_otomatis" readonly="" value="{{ number_format($pwmp[0]->nilailimitotomatis, 0, ',', '.') }}" class="form-control text-right" id="limit_otomatis">
                      @elseif ($pwmp[0]->jenispengajuan == "A")
                      <input type="text" name="limit_otomatis" readonly="" value="0" class="form-control text-right" id="limit_otomatis">
                      @endif
                   </div>
                 </div>
               </div>
                  @if ($pwmp[0]->status == 2)
                    @if(Auth::user()->cekRole()->role == "ERM")
                      <div class="col-md-6">
                        <div class="form-group">
                          <label><b>Limit Otomatis</b></label>
                            <div class="input-group">
                                @if($pwmp[0]->kodejabatan != "BM003" && $pwmp[0]->kodejabatan != "ACFM002")
                                <input type="text" name="limitotomatis" class="form-control text-right ribuan" value="0" id="limitotomatis" readonly="" onblur="fungsiotomatis()">
                                @else
                                <input type="text" name="limitotomatis" class="form-control text-right ribuan" value="{{$pwmp[0]->limitotomatis}}" id="limitotomatis" onblur="fungsiotomatis()">
                                @endif
                            </div>
                        </div>
                      </div>
                    @elseif(Auth::user()->cekRole()->role != "ERM")
                      <div class="col-md-6">
                        <div class="form-group">
                          <label><b>Limit Otomatis</b></label>
                            <div class="input-group">
                              <input type="text" name="limitotomatis" readonly="" class="form-control text-right ribuan" value="{{$pwmp[0]->limitotomatis}}" id="limitotomatis" onblur="fungsiotomatis()">
                            </div>
                        </div>
                      </div>
                    @endif
                    @elseif ($pwmp[0]->status == 3)
                    @if(Auth::user()->cekRole()->role == "ERM")
                      <div class="col-md-6">
                        <div class="form-group">
                          <label><b>Limit Otomatis</b></label>
                            <div class="input-group">
                                @if($pwmp[0]->kodejabatan != "BM003" && $pwmp[0]->kodejabatan != "ACFM002")
                                <input type="text" name="limitotomatis" class="form-control text-right ribuan" value="0" id="limitotomatis" readonly="" onblur="fungsiotomatis()">
                                @else
                                <input type="text" name="limitotomatis" class="form-control text-right ribuan" value="{{$pwmp[0]->limitotomatis}}" id="limitotomatis" onblur="fungsiotomatis()">
                                @endif
                            </div>
                        </div>
                      </div>
                    @elseif(Auth::user()->cekRole()->role != "ERM")
                      <div class="col-md-6">
                        <div class="form-group">
                          <label><b>Limit Otomatis</b></label>
                            <div class="input-group">
                              <input type="text" name="limitotomatis" readonly="" class="form-control text-right ribuan" value="{{$pwmp[0]->limitotomatis}}" id="limitotomatis" onblur="fungsiotomatis()">
                            </div>
                        </div>
                      </div>
                    @endif
                    @elseif ($pwmp[0]->status == 4)
                      <div class="col-md-6">
                        <div class="form-group">
                          <label><b>Limit Otomatis</b></label>
                            <div class="input-group">
                              <input type="text" name="limitotomatis" readonly="" class="form-control text-right ribuan" value="{{$pwmp[0]->limitotomatis}}" id="limitotomatis" onblur="fungsiotomatis()">
                            </div>
                        </div>
                      </div>
                  @elseif ($pwmp[0]->status == 1)
                    @if(Auth::user()->cekRole()->role == "ERM")
                      <div class="col-md-6">
                        <div class="form-group">
                          <label><b>Limit Otomatis</b></label>
                            <div class="input-group">
                              <input type="text" name="limitotomatis" readonly="" class="form-control text-right ribuan" value="{{$pwmp[0]->limitotomatis}}" id="limitotomatis" onblur="fungsiotomatis()">
                            </div>
                        </div>
                      </div>
                    @elseif(Auth::user()->cekRole()->role != "ERM")
                      <div class="col-md-6">
                        <div class="form-group">
                          <label><b>Limit Otomatis</b></label>
                            <div class="input-group">
                              <input type="text" name="limitotomatis" readonly="" class="form-control text-right ribuan" value="{{$pwmp[0]->limitotomatis}}" id="limitotomatis" onblur="fungsiotomatis()">
                            </div>
                        </div>
                      </div>
                    @endif
                  @elseif ($pwmp[0]->status == 5 || $pwmp[0]->status == 6)
                      <div class="col-md-6">
                        <div class="form-group">
                          <label><b>Limit Otomatis</b></label>
                            <div class="input-group">
                              <input type="text" name="limitotomatis" class="form-control text-right ribuan" value="{{$pwmp[0]->limitotomatis}}" readonly="" id="limitotomatis" onblur="fungsiotomatis()">
                            </div>
                        </div>
                      </div>
                  @endif
              </div>
              <div class="row">
                <div class="col-md-6">
                  <label><b>File SKPP</b></label>
                      <div class="row">
                          <img src="{{url::asset('public/img/icon/cta_downloadnota.png')}}" style="margin-left: 12px;margin-right: 10px; margin-bottom: 15px;" width="34px" height="30px" style="margin-left: 12px; margin-right: 10px;"><a target="_blank" href="{{ url('/public/file_skp/skp/'.$pwmp[0]->file_skpp) }}">Download</a>
                      </div>
                </div>
                <!-- @if ($pwmp[0]->status == 4)
                <div class="col-md-6">
                  <label><b>SK Permohonan Limit</b></label>
                    <div class="row">
                      <img src="{{url::asset('img/icon/cta_downloadnota.png')}}" width="34px" height="30px" style="margin-left: 12px; margin-right: 10px;"><a target="_blank" href="{{ url('/file_skp/sk/'.$pwmp[0]->file_sk) }}">Download</a>
                    </div>
                </div>
                @endif -->
              </div>
            </div>
          </div>
          <div class="row rincianpwmp_button">
            <div class="rincianpwmp_button_sub1">
              @if ($pwmp[0]->status == 2)
                <input type="submit" value="Update Limit Baru" class="btn btn-success" style="padding-left: 20px; padding-right: 20px;">
              @elseif ($pwmp[0]->status == 3)
                <input type="submit" value="Update Limit Baru" class="btn btn-success" style="padding-left: 20px; padding-right: 20px;">
              @elseif ($pwmp[0]->status == 1 || $pwmp[0]->status == 6)
              
              @endif
    </form>

    <form action="{{ url('/reject/assessment/erm') }}" method="POST" class="rbutton">
      <input type="hidden" name="_token" value="{{csrf_token()}}">
            @if ($pwmp[0]->status == 2)
              <input type="hidden" name="reject" value="{{ $pwmp[0]->id }}">
              <input type="submit" value="Reject" class="btn btn-danger" style="padding-left: 20px; padding-right: 20px;">
            @elseif ($pwmp[0]->status == 3)
              <input type="hidden" name="reject" value="{{ $pwmp[0]->id }}">
              <input type="submit" value="Reject" class="btn btn-danger" style="padding-left: 20px; padding-right: 20px;">
            @elseif ( $pwmp[0]->status == 1 || $pwmp[0]->status == 6)
            
            @endif
    </form> 

    <form action="{{ url('/generate/'.$pwmp[0]->id.'/assessment/erm') }}" method="POST" class="rbutton">
      <input type="hidden" name="_token" value="{{csrf_token()}}">
            @if ($pwmp[0]->status == 2)
              <input type="submit" value="Generate Draft SK" class="btn btn-info" style="padding-left: 20px; padding-right: 20px;">
            @elseif ($pwmp[0]->status == 3)
              <input type="submit" value="Generate Draft SK" class="btn btn-info" style="padding-left: 20px; padding-right: 20px;">
            @elseif ( $pwmp[0]->status == 1 ||$pwmp[0]->status == 6)
            
            @endif
    </form>

    <form action="{{ url('/generatenota/'.$pwmp[0]->id.'/assessment/erm') }}" method="POST" class="rbutton">
      <input type="hidden" name="_token" value="{{csrf_token()}}">
            @if ($pwmp[0]->status == 2)
              <input type="submit" value="Generate Draft Nota" class="btn btn-info" style="padding-left: 20px; padding-right: 20px;">
            @elseif ($pwmp[0]->status == 3)
              <input type="submit" value="Generate Draft Nota" class="btn btn-info" style="padding-left: 20px; padding-right: 20px;">
            @elseif ( $pwmp[0]->status == 1 ||$pwmp[0]->status == 6)
            
            @endif
    </form>
     
  </div>
</div>


<style>
  table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
  }

  td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
  }

  tr:nth-child(even) {
    /*background-color: #dddddd;*/
  }
</style>

@endsection

@section('jscustom')
<script type="text/javascript">
   function fungsigadailm() {
      var logammulia = document.getElementById("limitgadailm");
      var angkalogammulia = parseInt(logammulia.value.replace(/,/g, ""));
        if (angkalogammulia > 250000000) {
          logammulia.focus(); 
          logammulia.value=""; 
          Swal.fire({
               icon: 'error',
               title: 'Gagal',
               html: "Pengajuan Limit Logam Mulia <br> Tidak Boleh Lebih Dari 250 Juta",
           })
          //setTimeout(function(){x.focus();}, 1);
        }
    }
    function fungsigadainlm() {
      var nonlogam = document.getElementById("limitgadainlm");
      var angkanonlogam = parseInt(nonlogam.value.replace(/,/g, ""));
        if (angkanonlogam > 250000000) {
          nonlogam.focus(); 
          nonlogam.value="";
          Swal.fire({
               icon: 'error',
               title: 'Gagal',
               html: "Pengajuan Limit Non Logam Mulia <br> Tidak Boleh Lebih Dari 250 Juta",
           })    
        }
    }
    function fungsiotomatis() {
      var flimitotomatis = document.getElementById("limitotomatis");
      var angkalimitotomatis = parseInt(flimitotomatis.value.replace(/,/g, ""));
      var cekjabatan1 = document.getElementById("jabatan1");
        if (cekjabatan1.value == "BM003" || cekjabatan1.value == "ACFM002" ) {
          if (angkalimitotomatis > 200000000) {
          flimitotomatis.focus();
          flimitotomatis.value=""; 
          Swal.fire({
               icon: 'error',
               title: 'Gagal',
               html: "Pengajuan Limit Otomatis <br> Tidak Boleh Lebih Dari 200 Juta",
           })   
          }
        }
    }
</script>
@stop

