@extends('layouts.backend')
@section('content')

<style type="text/css">
  .select2-container--default .select2-selection--single{
    border: 0
  }
</style>
    <div class="container-fluid">

      <!-- Page Heading -->
      <div class="card border-left-success">
        <div class="card-body">
          <h1 class="h3 mb-2 text-gray-800" style="margin-top: 10px;"><b>Riwayat PWMP</b></h1>
        </div>
      </div>
      <div class="card shadow mb-4">
        <div class="card-header">
          <div class="col-md-12">
            <form action="{{URL('/erm/assessment')}}" method="get">
              <div class="row" style="margin-top: 10px;">
                <div class="col-md-6">
                  <div class="form-group">
                    <label><b>Tgl. Pengajuan</b></label>
                      <form>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <div class="input-group-text"><img src="{{URL::asset('public/img/Assessment/kalender icon.png')}}" width="25px" height="20px"></div>
                          </div>
                          <input type="hidden" value="{{$startDate}}" name="startDate" id="startDate">
                          <input type="hidden" value="{{$endDate}}" name="endDate" id="endDate">
                        <input type="text" class="form-control pull-right" id="daterange_pengajuan" name="test" autocomplete="off" />
                        </div>
                      </form>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label><b>Status Pengajuan</b></label>
                      <select id="status" name="status" class="form-control">
                          <option value="">ALL</option>
                        @foreach($enumerasi as $enum)
                          <option @if($keteranganStatus == $enum->kode) selected @endif value="{{$enum->kode}}">{{$enum->keterangan}}</option>
                        @endforeach
                      </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label><b>Area</b></label>
                      <select id="area" name="area" class="form-control">
                          <option value="">ALL</option>
                        @foreach($getarea as $area)
                          <option @if($kodearea == $area->kode) selected @endif value="{{$area->kode}}">{{$area->nama}}</option>
                        @endforeach
                      </select>
                  </div>
                </div>
                 <div class="col-md-6">
                  <div class="form-group">
                    <label><b>Keywords</b></label>
                      <input type="text" name="name_nip" value="{{$name_nip}}" placeholder="Nama dan NIP" class="form-control" id="name_nip"  onkeyup="this.value = this.value.toUpperCase();">
                  </div>
                </div>
                  <div class="form-group" style="margin-left: 10px;">
                    <input type="submit" value="Search" class="btn btn-success">
                    <a href="{{url('/erm/assessment')}}" class="btn btn-info">Reset</a>
                    <input type="hidden" name="excel" value="1">
                    <input type="submit" name="filterform" value="Export Excel" class="btn btn-primary">
                  </div>
              </div>
            </form>
          </div>
        </div>
<!--         <div class="card-header">
          <div class="col-md-3">
            <div class="form-group">
                <a href="{{URL::asset('/erm/assessment/export_excel')}}" class="btn btn-success btn-icon-split" style="margin-top: 10px; margin-top: 10px;border-top-width: 0px;border-left-width: 20px; border-right-width: 20px; margin-right: 20px;">
                  <span class="text">Excel.Xlsx</span>
                </a>
            </div>
          </div>
        </div> -->
        <div class="card-body">
            <style type="text/css">
              table#grid_riwayatpwmp {
                width: 100%;
              }
              table#grid_riwayatpwmp th {
                font-size: 12px;
                text-align: center;
              }
              table#grid_riwayatpwmp th.tanggal {
                width: 90px;
              }
              table#grid_riwayatpwmp th.manage {
                width: 80px;
              }
              table#grid_riwayatpwmp td {
                font-size: 12px;
                vertical-align: top;
              }
              table#grid_riwayatpwmp td.nominal {
                text-align: right
              }
              table#grid_riwayatpwmp td.statuspengajuan {
                text-align: center
              }
              table#grid_riwayatpwmp td.statuspengajuan div.statusdefault {
                padding: 0px 3px;
                color: white;
              }
              table#grid_riwayatpwmp td.statuspengajuan div.statuswarning {
                color: black;
              }
              table#grid_riwayatpwmp td.manage {
                text-align: center
              }
              table#grid_riwayatpwmp td.manage a {
                margin-right: 1px;
              }
              table#grid_riwayatpwmp td.no_index {
                text-align: center;
              }
            </style>
            <table id="grid_riwayatpwmp">
              <tr>
                <th>No</th>
                <th>NIP</th>
                <th>Nama</th>
                <th>Limit Non Gadai</th>
                <th>Limit Gadai<br/>Non Logam Mulia</th>
                <th>Limit Gadai<br/>Logam Mulia</th>
                <th>Limit Otomatis</th>
                <th class="tanggal">Tgl. SK</th>
                <th>Status Pengajuan</th>
                <th class="tanggal">Tgl. Pengajuan</th>
                <th class="manage">Manage</th>
              </tr>
              @foreach($pwmp as $index => $riwayat_pwmp)
                <tr>
                  <td class="no_index">{{ $index+1 }}</td>
                  <td>{{ $riwayat_pwmp->nip }}</td>
                  <td>{{ $riwayat_pwmp->namapejabat }}</td>
                  @if ($riwayat_pwmp->status != 4)
                  <td class="nominal">{{ number_format($riwayat_pwmp->limitnongadaifix, 0, ',', '.') }}</td>
                  <td class="nominal">{{ number_format($riwayat_pwmp->limitgadainlmfix, 0, ',', '.') }}</td>
                  <td class="nominal">{{ number_format($riwayat_pwmp->limitgadailmfix, 0, ',', '.') }}</td>
                  <td class="nominal">{{ number_format($riwayat_pwmp->limitotomatisfix, 0, ',', '.') }}</td>
                  <td class="nominal"> </td>
                  @else
                  <td class="nominal">{{ number_format($riwayat_pwmp->limitnongadaifix, 0, ',', '.') }}</td>
                  <td class="nominal">{{ number_format($riwayat_pwmp->limitgadainlmfix, 0, ',', '.') }}</td>
                  <td class="nominal">{{ number_format($riwayat_pwmp->limitgadailmfix, 0, ',', '.') }}</td>
                  <td class="nominal">{{ number_format($riwayat_pwmp->limitotomatisfix, 0, ',', '.') }}</td>
                  <td class="nominal">{{ \Carbon\Carbon::parse($riwayat_pwmp->tglskfix)->format('d-m-Y') }}</td>
                  @endif 
                  <td class="statuspengajuan">
                    @if ($riwayat_pwmp->status == 1)
                      <div class="card bg-info text-white shadow">
                        <div class="card-body statusdefault">
                          <div class="text-white-10 xs">{{$riwayat_pwmp->statuspengajuan}}</div>
                        </div>
                      </div>
                    @elseif ($riwayat_pwmp->status == 2)
                      <div class="card bg-warning text-white shadow">
                        <div class="card-body statusdefault statuswarning">
                          <div class="text-white-10 xs">{{$riwayat_pwmp->statuspengajuan}}</div>
                        </div>
                      </div>
                    @elseif ($riwayat_pwmp->status == 3)
                      <div class="card bg-primary text-white text-white shadow">
                        <div class="card-body statusdefault">
                          <div class="text-white-10 xs">{{$riwayat_pwmp->statuspengajuan}}</div>
                        </div>
                      </div>
                    @elseif ($riwayat_pwmp->status == 4)
                      <div class="card bg-success text-white shadow">
                        <div class="card-body statusdefault">
                          <div class="text-white-10 xs">{{$riwayat_pwmp->statuspengajuan}}</div>
                        </div>
                      </div>
                    @elseif ($riwayat_pwmp->status == 5)
                      <div class="card bg-danger text-white shadow">
                        <div class="card-body statusdefault">
                          <div class="text-white-10 xs">{{$riwayat_pwmp->statuspengajuan}}</div>
                        </div>
                      </div>
                    @elseif ($riwayat_pwmp->status == 6)
                      <div class="card bg-danger text-white shadow">
                        <div class="card-body statusdefault">
                          <div class="text-white-10 xs">{{$riwayat_pwmp->statuspengajuan}}</div>
                        </div>
                      </div>
                    @endif
                  </td>
                  <td>{{ \Carbon\Carbon::parse($riwayat_pwmp->created_at)->format('d-m-Y H:m:s') }}</td>
                  <td class="manage">
                    <a href="{{URL::to('/erm/assessment/'.$riwayat_pwmp->id.'/detail')}}"><img src="{{url::asset('public/img/icon/cta_opendetail.png')}}" width="26px"></a>
                    @if ($riwayat_pwmp->status == 1 )

                    @elseif ($riwayat_pwmp->status == 3)
                      <a href="#exampleModal" class="approvepermohonan" data-id="{{$riwayat_pwmp->id}}" data-nip="{{$riwayat_pwmp->nip}}" data-nongadai="{{ number_format($riwayat_pwmp->limitnongadaifix, 0, ',', ',') }}" data-gadainlm="{{ number_format($riwayat_pwmp->limitgadainlmfix, 0, ',', ',') }}" data-gadailm="{{ number_format($riwayat_pwmp->limitgadailmfix, 0, ',', ',') }}" data-otomatis="{{ number_format($riwayat_pwmp->limitotomatisfix, 0, ',', ',') }}" data-kodejabatan="{{$riwayat_pwmp->kodejabatan}}" data-kodecabang="{{$riwayat_pwmp->kodecabang}}" data-namapejabat="{{$riwayat_pwmp->namapejabat}}" data-fungsi="{{$riwayat_pwmp->fungsi}}" data-kategorilimit="{{$riwayat_pwmp->kategorilimit}}" data-jenispengajuan="{{$riwayat_pwmp->jenispengajuan}}" data-toggle="modal" data-target="#exampleModal"><img src="{{url::asset('public/img/status/confirm.png')}}" width="26px"></a>

                    @elseif ($riwayat_pwmp->status == 4 )
                      
                    @endif
                  </td>
                </tr>
              @endforeach
            </table>
            <div class="row pull-right mr-3 mt-3 justify-content-end" >
                    {{  $pwmp->appends(request()->except('page'))->links() }}
            </div>
        </div>
      </div>

    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <form method="POST" id="approveform" action="{{ url('/approve/erm') }}" enctype="multipart/form-data">{{ csrf_field() }}
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Approve Permohonan Limit</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <input type="hidden" name="idpermohonan" id="idpermohonan" value="" />
                    <input type="hidden" name="nippermohonan" id="nippermohonan" value="" />
                    <input type="hidden" name="kodejabatan1" id="kodejabatan1" value="" />
                    <input type="hidden" name="namapejabat" id="namapejabat" value="" />
                    <input type="hidden" name="kodecabang" id="kodecabang" value="" />
                    <input type="hidden" name="fungsi" id="fungsi" value="" />
                    <input type="hidden" name="kategorilimit" id="kategorilimit" value="" />
                    <input type="hidden" name="jenispengajuan" id="jenispengajuan" value="" />
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <label><b>No. SK Baru</b></label>
                    <input type="text" name="nosk" placeholder="Input No SK Baru" class="form-control @error('nosk') is-invalid @enderror" required id="nosk">
                          @error('nosk')
                            <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label><b>Tgl SK Baru</b></label>
                    <input type="date" name="tglsk" class="form-control @error('nosk') is-invalid @enderror" required id="tglsk">
                      @error('tglsk')
                        <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                        </span>
                      @enderror
                  </div>
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-md-6">
                 <label><b>DATA LIMIT BARU</b></label> 
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label><b>Limit Non Gadai</b></label>
                    <input type="text" name="limitnongadai" id="limitnongadai" class="form-control text-right ribuan" value="" /> 
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label><b>Limit Gadai Non Logam</b></label>
                    <input type="text" name="limitgadainlm" id="limitgadainlm" class="form-control text-right ribuan" value="" /> 
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label><b>Limit Gadai Logam Mulia</b></label>
                    <input type="text" name="limitgadailm" id="limitgadailm" class="form-control text-right ribuan" value="" /> 
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label><b>Limit Otomatis</b></label>
                    <input type="text" name="limitotomatis" id="limitotomatis" class="form-control text-right ribuan" value="" /> 
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label><b>Salinan SK</b></label>
                    <input type="file" name="salinan_sk" class="@error('nosk') is-invalid @enderror" required id="salinan_sk">
                      @error('salinan_sk')
                        <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                        </span>
                      @enderror
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-success">Approve</button>
            </div>
          </div>
        </form>
      </div>
    </div>
@endsection

@section('jscustom')

  <script type="text/javascript">
    $(document).ready(function() {
      $('#daterange_pengajuan').val($('#startDate').val() + ' - ' + $('#endDate').val())
      $('#daterange_pengajuan').daterangepicker({
        locale:{
          format:'DD-MM-YYYY'
        }
      })
      $("#search_nip").keyup(function() {
        $('#dataTable_ass').DataTable().search($(this).val()).draw() ;
      });

        $('#dataTable_ass').DataTable({
            "dom": 'lrtip',
            responsive: true,
            deferRender: true,
            language: {
              emptyTable: "Nip has No History",
              search: "_INPUT_",
          },
        });
    });

    $('#daterange_pengajuan').on('apply.daterangepicker', function(ev, picker) {
      $('#startDate').val(picker.startDate.format('DD-MM-YYYY'))
      $('#endDate').val(picker.endDate.format('DD-MM-YYYY'))
    });

    $('#area').select2();
    $('body').find('span.select2-selection__rendered').addClass('form-control')
    $('body').find('span.select2-selection--single').css({
      border:0
    })

    $('#status').select2();
    $('body').find('span.select2-selection__rendered').addClass('form-control')
    $('body').find('span.select2-selection--single').css({
      border:0
    })
  </script>

  <script type="text/javascript">
    $(document).on("click", ".approvepermohonan", function () {
     var idpermohonan = $(this).data('id');
     var nippermohonan = $(this).data('nip');
     var nongadai = $(this).data('nongadai');
     var gadainlm = $(this).data('gadainlm');
     var gadailm = $(this).data('gadailm');
     var otomatis = $(this).data('otomatis');
     var kodejabatan = $(this).data('kodejabatan');
     var namapejabat = $(this).data('namapejabat');
     var kodecabang = $(this).data('kodecabang');
     var fungsi = $(this).data('fungsi');
     var kategorilimit = $(this).data('kategorilimit');
     var jenispengajuan = $(this).data('jenispengajuan');
     if (kodejabatan == "BM003" || kodejabatan == "ACFM002") {
      $(".modal-body #limitotomatis").val( otomatis );
     }
     else {
       $('#limitotomatis').prop('readonly', true);
       $(".modal-body #limitotomatis").val( '0' );
     }
     $(".modal-body #idpermohonan").val( idpermohonan );
     $(".modal-body #nippermohonan").val( nippermohonan );
     $(".modal-body #limitnongadai").val( nongadai );
     $(".modal-body #limitgadainlm").val( gadainlm );
     $(".modal-body #limitgadailm").val( gadailm );
     $(".modal-body #kodejabatan1").val( kodejabatan );
     $(".modal-body #namapejabat").val( namapejabat );
     $(".modal-body #kodecabang").val( kodecabang );
     $(".modal-body #fungsi").val( fungsi );
     $(".modal-body #kategorilimit").val( kategorilimit );
     $(".modal-body #jenispengajuan").val( jenispengajuan );
});
  </script>

  <script type="text/javascript">
    $('[data-dismiss=modal]').on('click', function (e) {
        var $t = $(this),
            target = $t[0].href || $t.data("target") || $t.parents('.modal') || [];
        
      $(target)
        .find("input,textarea,select")
           .val('')
           .end()
        .find("input[type=checkbox], input[type=radio]")
           .prop("checked", "")
           .end();
    })
  </script>



  <style>
    table {
      border-collapse: collapse;
      width: 30px;
    }

    td, th {
      border: 1px solid #dddddd;
      text-align: left;
      padding: 8px;
    }

    tr:nth-child(even) {
      
    }
  </style>

@stop