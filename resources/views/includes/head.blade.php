<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">

<title>BSM - Limit Wewenang</title>

<!-- Custom fonts for this template -->
<link href="{{ URL::asset('/public/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{ URL::asset('/public/css/font-awesome.css')}}" rel="stylesheet">

<!-- Custom styles for this template -->

<link href="{{ URL::asset('/public/css/sb-admin-2.min.css')}}" rel="stylesheet">
<link href="{{url('/public/css/styles.css')}}" rel="stylesheet">
<link href="{{url('/public/autocomplete/jquery-ui.css')}}" rel="stylesheet">
<link href="{{url('/public/autocomplete/jquery-ui.structure.css')}}" rel="stylesheet">
<link href="{{url('/public/autocomplete/jquery-ui.theme.css')}}" rel="stylesheet">


<!-- Custom styles for this page -->

<link href="{{ URL::asset('/public/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
<link href="{{ URL::asset('/public/css/select_2.css')}}" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="{{ URL::asset('/public/css/daterangepicker_2.css')}}" />