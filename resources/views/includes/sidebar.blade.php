<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar" style="background: #00583D;">

      <!-- Sidebar - Brand -->
      @if(Auth::user()->cekRole()->role == "REGION")
        <div class="sidebar-brand-icon" style="margin-bottom: 35%;">
          <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{URL::asset('/home-region')}}">
            <img src="{{url('public/img/Login/Logo.png')}}" width="76.76px" height="40px">
          </a>
        </div>
      @elseif(Auth::user()->cekRole()->role == "PUSAT")
        <div class="sidebar-brand-icon" style="margin-bottom: 35%;">
          <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{URL::asset('/home-pusat')}}">
            <img src="{{url('public/img/Login/Logo.png')}}" width="76.76px" height="40px">
          </a>
        </div>
      @elseif(Auth::user()->cekRole()->role == "CABANG")
        <div class="sidebar-brand-icon" style="margin-bottom: 35%;">
          <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{URL::asset('/home-cabang')}}">
            <img src="{{url('public/img/Login/Logo.png')}}" width="76.76px" height="40px">
          </a>
        </div>
      @elseif(Auth::user()->cekRole()->role == "ERM")
        <div class="sidebar-brand-icon" style="margin-bottom: 35%;">
          <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{URL::asset('/home-erm')}}">
            <img src="{{url('public/img/Login/Logo.png')}}" width="76.76px" height="40px">
          </a>
        </div>
      @elseif(Auth::user()->cekRole()->role == "HCS")
        <div class="sidebar-brand-icon" style="margin-bottom: 35%;">
          <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{URL::asset('/home-hcs')}}">
            <img src="{{url('public/img/Login/Logo.png')}}" width="76.76px" height="40px">
          </a>
        </div>
      @elseif(Auth::user()->cekRole()->role == "IOG")
        <div class="sidebar-brand-icon" style="margin-bottom: 35%;">
          <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{URL::asset('/home-iog')}}">
            <img src="{{url('public/img/Login/Logo.png')}}" width="76.76px" height="40px">
          </a>
        </div>
      @endif
      <!-- Divider -->
      <hr class="sidebar-divider my-0">
    @if(Auth::user()->cekRole()->role == "REGION")
      <!-- Nav Item - Dashboard -->
      <li class="nav-item">
        <a class="nav-link" href="{{URL::asset('/home-region')}}">
          <img src="{{url('public/img/Sidebar/beranda.png')}}" width="20px" height="20px">
          <span>Home</span></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="{{URL::asset('/limit/baru')}}">
          <img src="{{url('public/img/Sidebar/limit icon.png')}}" width="20px" height="20px">
          <span>Permohonan Limit</span></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="{{URL::asset('/assessment')}}">
          <img src="{{url('public/img/Sidebar/assessment icon.png')}}" width="20px" height="20px">
          <span>Riwayat PWMP</span></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="{{url::asset('/pwmp')}}">
          <img src="{{url('public/img/Sidebar/menu_pwmp.png')}}" width="20px" height="20px">
          <span>PWMP</span></a>
      </li>

    @elseif(Auth::user()->cekRole()->role == "PUSAT")
      <!-- Nav Item - Dashboard -->
      <li class="nav-item">
        <a class="nav-link" href="{{URL::asset('/home-pusat')}}">
          <img src="{{url('public/img/Sidebar/beranda.png')}}" width="20px" height="20px">
          <span>Home</span></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="{{URL::asset('/limit/baru')}}">
          <img src="{{url('public/img/Sidebar/limit icon.png')}}" width="20px" height="20px">
          <span>Permohonan Limit</span></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="{{URL::asset('/assessment')}}">
          <img src="{{url('public/img/Sidebar/assessment icon.png')}}" width="20px" height="20px">
          <span>Riwayat PWMP</span></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="{{url::asset('/pwmp')}}">
          <img src="{{url('public/img/Sidebar/menu_pwmp.png')}}" width="20px" height="20px">
          <span>PWMP</span></a>
      </li>

    @elseif(Auth::user()->cekRole()->role == "CABANG")
      <!-- Nav Item - Permohonan Limit -->
      <li class="nav-item">
        <a class="nav-link" href="{{URL::asset('/home-cabang')}}">
          <img src="{{url('public/img/Sidebar/beranda.png')}}" width="20px" height="20px">
          <span>Home</span></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="{{URL::asset('/limit/baru')}}">
          <img src="{{url('public/img/Sidebar/limit icon.png')}}" width="20px" height="20px">
          <span>Permohonan Limit</span></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="{{URL::asset('/assessment')}}">
          <img src="{{url('public/img/Sidebar/assessment icon.png')}}" width="20px" height="20px">
          <span>Riwayat PWMP</span></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="{{URL::asset('/pwmp')}}">
          <img src="{{url('public/img/Sidebar/menu_pwmp.png')}}" width="20px" height="20px">
          <span>PWMP</span></a>
      </li>

      <!-- <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
          <img src="{{url('img/Sidebar/menu_pwmp.png')}}" width="20px" height="20px">
          <span>PWMP</span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="{{url('/pwmp')}}">PWMP</a>
            <a class="collapse-item" href="{{url('/pwmp-masa-percobaan')}}">Masa Percobaan</a>
            <a class="collapse-item" href="{{url('/pwmp-belum-memenuhi-syarat')}}">Belum Memenuhi Syarat</a>
            <a class="collapse-item" href="{{url('/pwmp-pindah-rumpun-jabatan')}}">Pindah Rumpun Jabatan</a>
            <a class="collapse-item" href="{{url('/pwmp-tidak-aktif')}}">Tidak Aktif</a>
          </div>
        </div>
      </li> -->
    @elseif(Auth::user()->cekRole()->role == "IOG")
      <li class="nav-item">
        <a class="nav-link" href="{{URL::asset('/home-iog')}}">
          <img src="{{url('public/img/Sidebar/beranda.png')}}" width="20px" height="20px">
          <span>Home</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{URL::asset('/pwmp')}}">
          <img src="{{url('public/img/Sidebar/menu_pwmp.png')}}" width="20px" height="20px">
          <span>PWMP</span></a>
      </li>

    @elseif(Auth::user()->cekRole()->role == "HCS")
      <li class="nav-item">
        <a class="nav-link" href="{{URL::asset('/home-hcs')}}">
          <img src="{{url('public/img/Sidebar/beranda.png')}}" width="20px" height="20px">
          <span>Home</span></a>
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages1" aria-expanded="true" aria-controls="collapsePages1">
          <img src="{{url('public/img/Sidebar/stagging.png')}}" width="20px" height="20px">
          <span>Staging</span>
        </a>
        <div id="collapsePages1" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="{{url('/Administrator/Data-master/pejabat')}}">HCS Pejabat</a>
            <a class="collapse-item" href="{{url('/Administrator/Data-master/riwayatjabatan')}}">HCS Riwayat Jabatan</a>
            <a class="collapse-item" href="{{url('/Administrator/Data-master/pengalamanpembiayaan')}}">Pengalaman Pembiayaan</a>
          </div>
        </div>
      </li>

    @elseif(Auth::user()->cekRole()->role == "ERM")
      <!-- Nav Item - Permohonan Limit -->
      <li class="nav-item">
        <a class="nav-link" href="{{URL::asset('/home-erm')}}">
          <img src="{{url('public/img/Sidebar/beranda.png')}}" width="20px" height="20px">
          <span>Home</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{URL::asset('/limit/baru')}}">
          <img src="{{url('public/img/Sidebar/limit icon.png')}}" width="20px" height="20px">
          <span>Permohonan Limit</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{url::asset('/erm/assessment')}}">
          <img src="{{url('public/img/kualitas_pembiayaan/menu_kualitas.png')}}" width="20px" height="20px">
          <span>Riwayat PWMP</span></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="{{url::asset('/pwmp')}}">
          <img src="{{url('public/img/Sidebar/menu_pwmp.png')}}" width="20px" height="20px">
          <span>PWMP</span></a>
      </li>

    @elseif(Auth::user()->cekRole()->role == "ADMIN")
      <!-- Nav Item - Permohonan Limit -->
      <li class="nav-item">
        <a class="nav-link" href="{{URL::asset('/home-admin')}}">
          <img src="{{url('public/img/Sidebar/limit icon.png')}}" width="20px" height="20px">
          <span>Home</span></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="{{url::asset('/kualitas-pembiayaan')}}">
          <img src="{{url('public/img/kualitas_pembiayaan/menu_kualitas.png')}}" width="20px" height="20px">
          <span>Kualitas Pembiayaan</span></a>
      </li>

      <!-- Nav Item - Permohonan Limit -->
      <li class="nav-item">
        <a class="nav-link" href="/user_manajemen/user/index">
          <img src="{{url('public/img/Sidebar/user icon.png')}}" width="20px" height="20px">
          <span>User Maintenance</span></a>
      </li>
     @endif

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>
      <div class="footer-sidebar" style="position: fixed; top: 90%;">
        <p style="position: relative; left: 10%; font-style: normal; font-weight: normal; font-size: 14px; color: #FFFFFF;">Copyright © 2020 <br>
            PT Bank Syariah Mandiri</p>
      </div>

    </ul>